package com.java110.cust.bmo.govHealthAnswer;

import com.java110.po.govHealthAnswer.GovHealthAnswerPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveGovHealthAnswerBMO {


    /**
     * 添加体检单提交者
     * add by wuxw
     * @param govHealthAnswerPo
     * @return
     */
    ResponseEntity<String> save(GovHealthAnswerPo govHealthAnswerPo);


}
