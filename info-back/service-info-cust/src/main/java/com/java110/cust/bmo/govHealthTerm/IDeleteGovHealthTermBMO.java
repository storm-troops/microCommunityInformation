package com.java110.cust.bmo.govHealthTerm;
import org.springframework.http.ResponseEntity;
import com.java110.po.govHealthTerm.GovHealthTermPo;

public interface IDeleteGovHealthTermBMO {


    /**
     * 修改体检项
     * add by wuxw
     * @param govHealthTermPo
     * @return
     */
    ResponseEntity<String> delete(GovHealthTermPo govHealthTermPo);


}
