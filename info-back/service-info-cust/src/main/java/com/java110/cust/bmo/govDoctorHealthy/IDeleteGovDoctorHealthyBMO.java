package com.java110.cust.bmo.govDoctorHealthy;
import org.springframework.http.ResponseEntity;
import com.java110.po.govDoctorHealthy.GovDoctorHealthyPo;

public interface IDeleteGovDoctorHealthyBMO {


    /**
     * 修改档案医生
     * add by wuxw
     * @param govDoctorHealthyPo
     * @return
     */
    ResponseEntity<String> delete(GovDoctorHealthyPo govDoctorHealthyPo);


}
