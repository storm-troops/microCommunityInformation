package com.java110.cust.bmo.govHealthTerm.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthTerm.IGetGovHealthTermBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthTermInnerServiceSMO;
import com.java110.dto.govHealthTerm.GovHealthTermDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHealthTermBMOImpl")
public class GetGovHealthTermBMOImpl implements IGetGovHealthTermBMO {

    @Autowired
    private IGovHealthTermInnerServiceSMO govHealthTermInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHealthTermDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHealthTermDto govHealthTermDto) {


        int count = govHealthTermInnerServiceSMOImpl.queryGovHealthTermsCount(govHealthTermDto);

        List<GovHealthTermDto> govHealthTermDtos = null;
        if (count > 0) {
            govHealthTermDtos = govHealthTermInnerServiceSMOImpl.queryGovHealthTerms(govHealthTermDto);
        } else {
            govHealthTermDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHealthTermDto.getRow()), count, govHealthTermDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
