package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovOldPersonServiceDao;
import com.java110.intf.cust.IGovOldPersonInnerServiceSMO;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 老人管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovOldPersonInnerServiceSMOImpl extends BaseServiceSMO implements IGovOldPersonInnerServiceSMO {

    @Autowired
    private IGovOldPersonServiceDao govOldPersonServiceDaoImpl;


    @Override
    public int saveGovOldPerson(@RequestBody  GovOldPersonPo govOldPersonPo) {
        int saveFlag = 1;
        govOldPersonServiceDaoImpl.saveGovOldPersonInfo(BeanConvertUtil.beanCovertMap(govOldPersonPo));
        return saveFlag;
    }

     @Override
    public int updateGovOldPerson(@RequestBody  GovOldPersonPo govOldPersonPo) {
        int saveFlag = 1;
         govOldPersonServiceDaoImpl.updateGovOldPersonInfo(BeanConvertUtil.beanCovertMap(govOldPersonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovOldPerson(@RequestBody  GovOldPersonPo govOldPersonPo) {
        int saveFlag = 1;
        govOldPersonPo.setStatusCd("1");
        govOldPersonServiceDaoImpl.updateGovOldPersonInfo(BeanConvertUtil.beanCovertMap(govOldPersonPo));
        return saveFlag;
    }

    @Override
    public List<GovOldPersonDto> queryGovOldPersons(@RequestBody  GovOldPersonDto govOldPersonDto) {

        //校验是否传了 分页信息

        int page = govOldPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOldPersonDto.setPage((page - 1) * govOldPersonDto.getRow());
        }

        List<GovOldPersonDto> govOldPersons = BeanConvertUtil.covertBeanList(govOldPersonServiceDaoImpl.getGovOldPersonInfo(BeanConvertUtil.beanCovertMap(govOldPersonDto)), GovOldPersonDto.class);

        return govOldPersons;
    }
    @Override
    public List<GovOldPersonDto> getGovOldTypePersonCount(@RequestBody  GovOldPersonDto govOldPersonDto) {

        //校验是否传了 分页信息

        int page = govOldPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOldPersonDto.setPage((page - 1) * govOldPersonDto.getRow());
        }

        List<GovOldPersonDto> govOldPersons = BeanConvertUtil.covertBeanList(govOldPersonServiceDaoImpl.getGovOldTypePersonCount(BeanConvertUtil.beanCovertMap(govOldPersonDto)), GovOldPersonDto.class);

        return govOldPersons;
    }


    @Override
    public int queryGovOldPersonsCount(@RequestBody GovOldPersonDto govOldPersonDto) {
        return govOldPersonServiceDaoImpl.queryGovOldPersonsCount(BeanConvertUtil.beanCovertMap(govOldPersonDto));    }

    public IGovOldPersonServiceDao getGovOldPersonServiceDaoImpl() {
        return govOldPersonServiceDaoImpl;
    }

    public void setGovOldPersonServiceDaoImpl(IGovOldPersonServiceDao govOldPersonServiceDaoImpl) {
        this.govOldPersonServiceDaoImpl = govOldPersonServiceDaoImpl;
    }
}
