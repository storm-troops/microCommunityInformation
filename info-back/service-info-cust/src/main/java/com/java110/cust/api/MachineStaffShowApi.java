package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.machineStaffShow.MachineStaffShowDto;
import com.java110.po.machineStaffShow.MachineStaffShowPo;
import com.java110.cust.bmo.machineStaffShow.IDeleteMachineStaffShowBMO;
import com.java110.cust.bmo.machineStaffShow.IGetMachineStaffShowBMO;
import com.java110.cust.bmo.machineStaffShow.ISaveMachineStaffShowBMO;
import com.java110.cust.bmo.machineStaffShow.IUpdateMachineStaffShowBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/machineStaffShow")
public class MachineStaffShowApi {

    @Autowired
    private ISaveMachineStaffShowBMO saveMachineStaffShowBMOImpl;
    @Autowired
    private IUpdateMachineStaffShowBMO updateMachineStaffShowBMOImpl;
    @Autowired
    private IDeleteMachineStaffShowBMO deleteMachineStaffShowBMOImpl;

    @Autowired
    private IGetMachineStaffShowBMO getMachineStaffShowBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machineStaffShow/saveMachineStaffShow
     * @path /app/machineStaffShow/saveMachineStaffShow
     */
    @RequestMapping(value = "/saveMachineStaffShow", method = RequestMethod.POST)
    public ResponseEntity<String> saveMachineStaffShow(@RequestHeader(value = "user-id") String userId,
                                                       @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "machineId", "请求报文中未包含machineId" );
        Assert.hasKeyAndValue( reqJson, "isShow", "请求报文中未包含isShow" );


        MachineStaffShowPo machineStaffShowPo = BeanConvertUtil.covertBean( reqJson, MachineStaffShowPo.class );
        machineStaffShowPo.setStaffId( userId );
        return saveMachineStaffShowBMOImpl.save( machineStaffShowPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machineStaffShow/updateMachineStaffShow
     * @path /app/machineStaffShow/updateMachineStaffShow
     */
    @RequestMapping(value = "/updateMachineStaffShow", method = RequestMethod.POST)
    public ResponseEntity<String> updateMachineStaffShow(@RequestHeader(value = "user-id") String userId,
                                                         @RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "machineId", "请求报文中未包含machineId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "isShow", "请求报文中未包含isShow" );

        MachineStaffShowPo machineStaffShowPo = BeanConvertUtil.covertBean( reqJson, MachineStaffShowPo.class );
        machineStaffShowPo.setStaffId( userId );
        deleteMachineStaffShowBMOImpl.delete( machineStaffShowPo );
        return  saveMachineStaffShowBMOImpl.save( machineStaffShowPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machineStaffShow/deleteMachineStaffShow
     * @path /app/machineStaffShow/deleteMachineStaffShow
     */
    @RequestMapping(value = "/deleteMachineStaffShow", method = RequestMethod.POST)
    public ResponseEntity<String> deleteMachineStaffShow(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "machineStaffId", "machineStaffId不能为空" );


        MachineStaffShowPo machineStaffShowPo = BeanConvertUtil.covertBean( reqJson, MachineStaffShowPo.class );
        return deleteMachineStaffShowBMOImpl.delete( machineStaffShowPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /machineStaffShow/queryMachineStaffShow
     * @path /app/machineStaffShow/queryMachineStaffShow
     */
    @RequestMapping(value = "/queryMachineStaffShow", method = RequestMethod.GET)
    public ResponseEntity<String> queryMachineStaffShow(
                                                        @RequestHeader(value = "user-id") String userId,
                                                        @RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "isShow") String isShow,
                                                        @RequestParam(value = "machineId",required = false) String machineId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {

        MachineStaffShowDto machineStaffShowDto = new MachineStaffShowDto();
        machineStaffShowDto.setPage( page );
        machineStaffShowDto.setRow( row );
        machineStaffShowDto.setCaId( caId );
        machineStaffShowDto.setStaffId( userId );
        machineStaffShowDto.setMachineId( machineId );
        machineStaffShowDto.setIsShow( isShow );
        return getMachineStaffShowBMOImpl.get( machineStaffShowDto );
    }
}
