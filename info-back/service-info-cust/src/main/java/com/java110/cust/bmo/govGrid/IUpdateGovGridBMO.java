package com.java110.cust.bmo.govGrid;
import com.java110.po.govGrid.GovGridPo;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IUpdateGovGridBMO {


    /**
     * 修改网格人员
     * add by wuxw
     * @param govGridPo
     * @return
     */
    ResponseEntity<String> update(GovGridPo govGridPo, GovPersonPo govPersonPo);


}
