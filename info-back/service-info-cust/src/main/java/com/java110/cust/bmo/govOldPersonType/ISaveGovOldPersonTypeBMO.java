package com.java110.cust.bmo.govOldPersonType;

import org.springframework.http.ResponseEntity;
import com.java110.po.govOldPersonType.GovOldPersonTypePo;
public interface ISaveGovOldPersonTypeBMO {


    /**
     * 添加老人类型
     * add by wuxw
     * @param govOldPersonTypePo
     * @return
     */
    ResponseEntity<String> save(GovOldPersonTypePo govOldPersonTypePo);


}
