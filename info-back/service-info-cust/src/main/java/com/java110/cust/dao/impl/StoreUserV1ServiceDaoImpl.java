package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IStoreUserV1ServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用戶管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("storeUserV1ServiceDaoImpl")
//@Transactional
public class StoreUserV1ServiceDaoImpl extends BaseServiceDao implements IStoreUserV1ServiceDao {

    private static Logger logger = LoggerFactory.getLogger(StoreUserV1ServiceDaoImpl.class);





    /**
     * 保存用戶管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveStoreUserV1Info(Map info) throws DAOException {
        logger.debug("保存用戶管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("storeUserV1ServiceDaoImpl.saveStoreUserV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存用戶管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询用戶管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getStoreUserV1Info(Map info) throws DAOException {
        logger.debug("查询用戶管理信息 入参 info : {}",info);

        List<Map> businessStoreUserV1Infos = sqlSessionTemplate.selectList("storeUserV1ServiceDaoImpl.getStoreUserV1Info",info);

        return businessStoreUserV1Infos;
    }


    /**
     * 修改用戶管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateStoreUserV1Info(Map info) throws DAOException {
        logger.debug("修改用戶管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("storeUserV1ServiceDaoImpl.updateStoreUserV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改用戶管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询用戶管理数量
     * @param info 用戶管理信息
     * @return 用戶管理数量
     */
    @Override
    public int queryStoreUserV1sCount(Map info) {
        logger.debug("查询用戶管理数据 入参 info : {}",info);

        List<Map> businessStoreUserV1Infos = sqlSessionTemplate.selectList("storeUserV1ServiceDaoImpl.queryStoreUserV1sCount", info);
        if (businessStoreUserV1Infos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessStoreUserV1Infos.get(0).get("count").toString());
    }


}
