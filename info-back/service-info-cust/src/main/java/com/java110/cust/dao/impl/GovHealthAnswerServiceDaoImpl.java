package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovHealthAnswerServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 体检单提交者服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govHealthAnswerServiceDaoImpl")
//@Transactional
public class GovHealthAnswerServiceDaoImpl extends BaseServiceDao implements IGovHealthAnswerServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovHealthAnswerServiceDaoImpl.class);





    /**
     * 保存体检单提交者信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovHealthAnswerInfo(Map info) throws DAOException {
        logger.debug("保存体检单提交者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govHealthAnswerServiceDaoImpl.saveGovHealthAnswerInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存体检单提交者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询体检单提交者信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovHealthAnswerInfo(Map info) throws DAOException {
        logger.debug("查询体检单提交者信息 入参 info : {}",info);

        List<Map> businessGovHealthAnswerInfos = sqlSessionTemplate.selectList("govHealthAnswerServiceDaoImpl.getGovHealthAnswerInfo",info);

        return businessGovHealthAnswerInfos;
    }


    /**
     * 修改体检单提交者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovHealthAnswerInfo(Map info) throws DAOException {
        logger.debug("修改体检单提交者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govHealthAnswerServiceDaoImpl.updateGovHealthAnswerInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改体检单提交者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询体检单提交者数量
     * @param info 体检单提交者信息
     * @return 体检单提交者数量
     */
    @Override
    public int queryGovHealthAnswersCount(Map info) {
        logger.debug("查询体检单提交者数据 入参 info : {}",info);

        List<Map> businessGovHealthAnswerInfos = sqlSessionTemplate.selectList("govHealthAnswerServiceDaoImpl.queryGovHealthAnswersCount", info);
        if (businessGovHealthAnswerInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovHealthAnswerInfos.get(0).get("count").toString());
    }


}
