package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHealthTitleServiceDao;
import com.java110.intf.cust.IGovHealthTitleInnerServiceSMO;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 体检项内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHealthTitleInnerServiceSMOImpl extends BaseServiceSMO implements IGovHealthTitleInnerServiceSMO {

    @Autowired
    private IGovHealthTitleServiceDao govHealthTitleServiceDaoImpl;


    @Override
    public int saveGovHealthTitle(@RequestBody  GovHealthTitlePo govHealthTitlePo) {
        int saveFlag = 1;
        govHealthTitleServiceDaoImpl.saveGovHealthTitleInfo(BeanConvertUtil.beanCovertMap(govHealthTitlePo));
        return saveFlag;
    }

     @Override
    public int updateGovHealthTitle(@RequestBody  GovHealthTitlePo govHealthTitlePo) {
        int saveFlag = 1;
         govHealthTitleServiceDaoImpl.updateGovHealthTitleInfo(BeanConvertUtil.beanCovertMap(govHealthTitlePo));
        return saveFlag;
    }

     @Override
    public int deleteGovHealthTitle(@RequestBody  GovHealthTitlePo govHealthTitlePo) {
        int saveFlag = 1;
        govHealthTitlePo.setStatusCd("1");
        govHealthTitleServiceDaoImpl.updateGovHealthTitleInfo(BeanConvertUtil.beanCovertMap(govHealthTitlePo));
        return saveFlag;
    }

    @Override
    public List<GovHealthTitleDto> queryGovHealthTitles(@RequestBody  GovHealthTitleDto govHealthTitleDto) {

        //校验是否传了 分页信息

        int page = govHealthTitleDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHealthTitleDto.setPage((page - 1) * govHealthTitleDto.getRow());
        }

        List<GovHealthTitleDto> govHealthTitles = BeanConvertUtil.covertBeanList(govHealthTitleServiceDaoImpl.getGovHealthTitleInfo(BeanConvertUtil.beanCovertMap(govHealthTitleDto)), GovHealthTitleDto.class);

        return govHealthTitles;
    }


    @Override
    public int queryGovHealthTitlesCount(@RequestBody GovHealthTitleDto govHealthTitleDto) {
        return govHealthTitleServiceDaoImpl.queryGovHealthTitlesCount(BeanConvertUtil.beanCovertMap(govHealthTitleDto));    }

    public IGovHealthTitleServiceDao getGovHealthTitleServiceDaoImpl() {
        return govHealthTitleServiceDaoImpl;
    }

    public void setGovHealthTitleServiceDaoImpl(IGovHealthTitleServiceDao govHealthTitleServiceDaoImpl) {
        this.govHealthTitleServiceDaoImpl = govHealthTitleServiceDaoImpl;
    }
}
