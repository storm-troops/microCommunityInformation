package com.java110.cust.bmo.govHealthAnswerValue;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;
public interface ISaveGovHealthAnswerValueBMO {


    /**
     * 添加体检项目答案
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> save(JSONObject reqJson);


}
