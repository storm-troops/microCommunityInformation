package com.java110.cust.smo.impl;


import com.java110.cust.dao.IUserV1ServiceDao;
import com.java110.dto.UserV1.UserV1Dto;
import com.java110.intf.cust.IUserV1InnerServiceSMO;
import com.java110.po.UserV1.UserV1Po;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 用戶管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class UserV1InnerServiceSMOImpl extends BaseServiceSMO implements IUserV1InnerServiceSMO {

    @Autowired
    private IUserV1ServiceDao userV1ServiceDaoImpl;


    @Override
    public int saveUserV1(@RequestBody UserV1Po userV1Po) {
        int saveFlag = 1;
        userV1ServiceDaoImpl.saveUserV1Info(BeanConvertUtil.beanCovertMap(userV1Po));
        return saveFlag;
    }

     @Override
    public int updateUserV1(@RequestBody  UserV1Po userV1Po) {
        int saveFlag = 1;
         userV1ServiceDaoImpl.updateUserV1Info(BeanConvertUtil.beanCovertMap(userV1Po));
        return saveFlag;
    }

     @Override
    public int deleteUserV1(@RequestBody  UserV1Po userV1Po) {
        int saveFlag = 1;
        userV1Po.setStatusCd("1");
        userV1ServiceDaoImpl.updateUserV1Info(BeanConvertUtil.beanCovertMap(userV1Po));
        return saveFlag;
    }

    @Override
    public List<UserV1Dto> queryUserV1s(@RequestBody UserV1Dto userV1Dto) {

        //校验是否传了 分页信息

        int page = userV1Dto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            userV1Dto.setPage((page - 1) * userV1Dto.getRow());
        }

        List<UserV1Dto> userV1s = BeanConvertUtil.covertBeanList(userV1ServiceDaoImpl.getUserV1Info(BeanConvertUtil.beanCovertMap(userV1Dto)), UserV1Dto.class);

        return userV1s;
    }


    @Override
    public int queryUserV1sCount(@RequestBody UserV1Dto userV1Dto) {
        return userV1ServiceDaoImpl.queryUserV1sCount(BeanConvertUtil.beanCovertMap(userV1Dto));    }

    public IUserV1ServiceDao getUserV1ServiceDaoImpl() {
        return userV1ServiceDaoImpl;
    }

    public void setUserV1ServiceDaoImpl(IUserV1ServiceDao userV1ServiceDaoImpl) {
        this.userV1ServiceDaoImpl = userV1ServiceDaoImpl;
    }
}
