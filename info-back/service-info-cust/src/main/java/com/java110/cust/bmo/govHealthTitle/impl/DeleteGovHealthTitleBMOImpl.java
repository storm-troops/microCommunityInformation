package com.java110.cust.bmo.govHealthTitle.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govHealthTitle.IDeleteGovHealthTitleBMO;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import com.java110.vo.ResultVo;
import com.java110.po.store.StorePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthTitleInnerServiceSMO;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovHealthTitleBMOImpl")
public class DeleteGovHealthTitleBMOImpl implements IDeleteGovHealthTitleBMO {

    @Autowired
    private IGovHealthTitleInnerServiceSMO govHealthTitleInnerServiceSMOImpl;

    /**
     * @param govHealthTitlePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovHealthTitlePo govHealthTitlePo) {

        int flag = govHealthTitleInnerServiceSMOImpl.deleteGovHealthTitle(govHealthTitlePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
