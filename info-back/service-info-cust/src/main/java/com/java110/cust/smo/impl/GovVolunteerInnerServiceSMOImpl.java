package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovVolunteerServiceDao;
import com.java110.intf.cust.IGovVolunteerInnerServiceSMO;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.po.govVolunteer.GovVolunteerPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 志愿者管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovVolunteerInnerServiceSMOImpl extends BaseServiceSMO implements IGovVolunteerInnerServiceSMO {

    @Autowired
    private IGovVolunteerServiceDao govVolunteerServiceDaoImpl;


    @Override
    public int saveGovVolunteer(@RequestBody  GovVolunteerPo govVolunteerPo) {
        int saveFlag = 1;
        govVolunteerServiceDaoImpl.saveGovVolunteerInfo(BeanConvertUtil.beanCovertMap(govVolunteerPo));
        return saveFlag;
    }

     @Override
    public int updateGovVolunteer(@RequestBody  GovVolunteerPo govVolunteerPo) {
        int saveFlag = 1;
         govVolunteerServiceDaoImpl.updateGovVolunteerInfo(BeanConvertUtil.beanCovertMap(govVolunteerPo));
        return saveFlag;
    }

     @Override
    public int deleteGovVolunteer(@RequestBody  GovVolunteerPo govVolunteerPo) {
        int saveFlag = 1;
        govVolunteerPo.setStatusCd("1");
        govVolunteerServiceDaoImpl.updateGovVolunteerInfo(BeanConvertUtil.beanCovertMap(govVolunteerPo));
        return saveFlag;
    }

    @Override
    public List<GovVolunteerDto> queryGovVolunteers(@RequestBody  GovVolunteerDto govVolunteerDto) {

        //校验是否传了 分页信息

        int page = govVolunteerDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govVolunteerDto.setPage((page - 1) * govVolunteerDto.getRow());
        }

        List<GovVolunteerDto> govVolunteers = BeanConvertUtil.covertBeanList(govVolunteerServiceDaoImpl.getGovVolunteerInfo(BeanConvertUtil.beanCovertMap(govVolunteerDto)), GovVolunteerDto.class);

        return govVolunteers;
    }

    @Override
    public List<GovVolunteerDto> getGovStateVolunteerCount(@RequestBody  GovVolunteerDto govVolunteerDto) {

        //校验是否传了 分页信息

        int page = govVolunteerDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govVolunteerDto.setPage((page - 1) * govVolunteerDto.getRow());
        }

        List<GovVolunteerDto> govVolunteers = BeanConvertUtil.covertBeanList(govVolunteerServiceDaoImpl.getGovStateVolunteerCount(BeanConvertUtil.beanCovertMap(govVolunteerDto)), GovVolunteerDto.class);

        return govVolunteers;
    }


    @Override
    public int queryGovVolunteersCount(@RequestBody GovVolunteerDto govVolunteerDto) {
        return govVolunteerServiceDaoImpl.queryGovVolunteersCount(BeanConvertUtil.beanCovertMap(govVolunteerDto));    }

    public IGovVolunteerServiceDao getGovVolunteerServiceDaoImpl() {
        return govVolunteerServiceDaoImpl;
    }

    public void setGovVolunteerServiceDaoImpl(IGovVolunteerServiceDao govVolunteerServiceDaoImpl) {
        this.govVolunteerServiceDaoImpl = govVolunteerServiceDaoImpl;
    }
}
