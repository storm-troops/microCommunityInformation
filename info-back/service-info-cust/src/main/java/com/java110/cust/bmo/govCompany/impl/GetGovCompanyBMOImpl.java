package com.java110.cust.bmo.govCompany.impl;


import com.java110.cust.bmo.govCompany.IGetGovCompanyBMO;
import com.java110.intf.cust.IGovCompanyInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govCompany.GovCompanyDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovCompanyBMOImpl")
public class GetGovCompanyBMOImpl implements IGetGovCompanyBMO {

    @Autowired
    private IGovCompanyInnerServiceSMO govCompanyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCompanyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCompanyDto govCompanyDto) {


        int count = govCompanyInnerServiceSMOImpl.queryGovCompanysCount(govCompanyDto);

        List<GovCompanyDto> govCompanyDtos = null;
        if (count > 0) {
            govCompanyDtos = govCompanyInnerServiceSMOImpl.queryGovCompanys(govCompanyDto);
        } else {
            govCompanyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCompanyDto.getRow()), count, govCompanyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
