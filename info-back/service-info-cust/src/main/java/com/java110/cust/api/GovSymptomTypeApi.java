package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govSymptomType.GovSymptomTypeDto;
import com.java110.po.govSymptomType.GovSymptomTypePo;
import com.java110.cust.bmo.govSymptomType.IDeleteGovSymptomTypeBMO;
import com.java110.cust.bmo.govSymptomType.IGetGovSymptomTypeBMO;
import com.java110.cust.bmo.govSymptomType.ISaveGovSymptomTypeBMO;
import com.java110.cust.bmo.govSymptomType.IUpdateGovSymptomTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govSymptomType")
public class GovSymptomTypeApi {

    @Autowired
    private ISaveGovSymptomTypeBMO saveGovSymptomTypeBMOImpl;
    @Autowired
    private IUpdateGovSymptomTypeBMO updateGovSymptomTypeBMOImpl;
    @Autowired
    private IDeleteGovSymptomTypeBMO deleteGovSymptomTypeBMOImpl;

    @Autowired
    private IGetGovSymptomTypeBMO getGovSymptomTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSymptomType/saveGovSymptomType
     * @path /app/govSymptomType/saveGovSymptomType
     */
    @RequestMapping(value = "/saveGovSymptomType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovSymptomType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");

        GovSymptomTypePo govSymptomTypePo = BeanConvertUtil.covertBean(reqJson, GovSymptomTypePo.class);
        return saveGovSymptomTypeBMOImpl.save(govSymptomTypePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSymptomType/updateGovSymptomType
     * @path /app/govSymptomType/updateGovSymptomType
     */
    @RequestMapping(value = "/updateGovSymptomType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovSymptomType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "symptomId", "请求报文中未包含symptomId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");

        GovSymptomTypePo govSymptomTypePo = BeanConvertUtil.covertBean(reqJson, GovSymptomTypePo.class);
        return updateGovSymptomTypeBMOImpl.update(govSymptomTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSymptomType/deleteGovSymptomType
     * @path /app/govSymptomType/deleteGovSymptomType
     */
    @RequestMapping(value = "/deleteGovSymptomType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovSymptomType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "symptomId", "symptomId不能为空");

        GovSymptomTypePo govSymptomTypePo = BeanConvertUtil.covertBean(reqJson, GovSymptomTypePo.class);
        return deleteGovSymptomTypeBMOImpl.delete(govSymptomTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govSymptomType/queryGovSymptomType
     * @path /app/govSymptomType/queryGovSymptomType
     */
    @RequestMapping(value = "/queryGovSymptomType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovSymptomType(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "name", required = false) String name,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovSymptomTypeDto govSymptomTypeDto = new GovSymptomTypeDto();
        govSymptomTypeDto.setPage(page);
        govSymptomTypeDto.setRow(row);
        govSymptomTypeDto.setCaId(caId);
        govSymptomTypeDto.setName(name);
        return getGovSymptomTypeBMOImpl.get(govSymptomTypeDto);
    }
}
