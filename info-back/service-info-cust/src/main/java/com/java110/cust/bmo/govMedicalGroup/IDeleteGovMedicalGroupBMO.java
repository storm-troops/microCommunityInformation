package com.java110.cust.bmo.govMedicalGroup;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalGroup.GovMedicalGroupPo;

public interface IDeleteGovMedicalGroupBMO {


    /**
     * 修改医疗团队
     * add by wuxw
     * @param govMedicalGroupPo
     * @return
     */
    ResponseEntity<String> delete(GovMedicalGroupPo govMedicalGroupPo);


}
