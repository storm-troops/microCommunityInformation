package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMedicalClassify.GovMedicalClassifyDto;
import com.java110.po.govMedicalClassify.GovMedicalClassifyPo;
import com.java110.cust.bmo.govMedicalClassify.IDeleteGovMedicalClassifyBMO;
import com.java110.cust.bmo.govMedicalClassify.IGetGovMedicalClassifyBMO;
import com.java110.cust.bmo.govMedicalClassify.ISaveGovMedicalClassifyBMO;
import com.java110.cust.bmo.govMedicalClassify.IUpdateGovMedicalClassifyBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMedicalClassify")
public class GovMedicalClassifyApi {

    @Autowired
    private ISaveGovMedicalClassifyBMO saveGovMedicalClassifyBMOImpl;
    @Autowired
    private IUpdateGovMedicalClassifyBMO updateGovMedicalClassifyBMOImpl;
    @Autowired
    private IDeleteGovMedicalClassifyBMO deleteGovMedicalClassifyBMOImpl;

    @Autowired
    private IGetGovMedicalClassifyBMO getGovMedicalClassifyBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalClassify/saveGovMedicalClassify
     * @path /app/govMedicalClassify/saveGovMedicalClassify
     */
    @RequestMapping(value = "/saveGovMedicalClassify", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMedicalClassify(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "classifyName", "请求报文中未包含classifyName");
        Assert.hasKeyAndValue(reqJson, "classifyType", "请求报文中未包含classifyType");


        GovMedicalClassifyPo govMedicalClassifyPo = BeanConvertUtil.covertBean(reqJson, GovMedicalClassifyPo.class);
        return saveGovMedicalClassifyBMOImpl.save(govMedicalClassifyPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalClassify/updateGovMedicalClassify
     * @path /app/govMedicalClassify/updateGovMedicalClassify
     */
    @RequestMapping(value = "/updateGovMedicalClassify", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMedicalClassify(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "medicalClassifyId", "请求报文中未包含medicalClassifyId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "classifyName", "请求报文中未包含classifyName");
        Assert.hasKeyAndValue(reqJson, "classifyType", "请求报文中未包含classifyType");


        GovMedicalClassifyPo govMedicalClassifyPo = BeanConvertUtil.covertBean(reqJson, GovMedicalClassifyPo.class);
        return updateGovMedicalClassifyBMOImpl.update(govMedicalClassifyPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalClassify/deleteGovMedicalClassify
     * @path /app/govMedicalClassify/deleteGovMedicalClassify
     */
    @RequestMapping(value = "/deleteGovMedicalClassify", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMedicalClassify(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "medicalClassifyId", "medicalClassifyId不能为空");


        GovMedicalClassifyPo govMedicalClassifyPo = BeanConvertUtil.covertBean(reqJson, GovMedicalClassifyPo.class);
        return deleteGovMedicalClassifyBMOImpl.delete(govMedicalClassifyPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMedicalClassify/queryGovMedicalClassify
     * @path /app/govMedicalClassify/queryGovMedicalClassify
     */
    @RequestMapping(value = "/queryGovMedicalClassify", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMedicalClassify(@RequestParam(value = "caId") String caId,
                                                          @RequestParam(value = "classifyName",required = false) String classifyName,
                                                          @RequestParam(value = "page") int page,
                                                          @RequestParam(value = "row") int row) {
        GovMedicalClassifyDto govMedicalClassifyDto = new GovMedicalClassifyDto();
        govMedicalClassifyDto.setPage(page);
        govMedicalClassifyDto.setRow(row);
        govMedicalClassifyDto.setCaId(caId);
        govMedicalClassifyDto.setClassifyName(classifyName);
        return getGovMedicalClassifyBMOImpl.get(govMedicalClassifyDto);
    }
}
