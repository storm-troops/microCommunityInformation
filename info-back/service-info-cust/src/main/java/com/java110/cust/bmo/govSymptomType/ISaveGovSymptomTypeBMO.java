package com.java110.cust.bmo.govSymptomType;

import org.springframework.http.ResponseEntity;
import com.java110.po.govSymptomType.GovSymptomTypePo;
public interface ISaveGovSymptomTypeBMO {


    /**
     * 添加症状类型
     * add by wuxw
     * @param govSymptomTypePo
     * @return
     */
    ResponseEntity<String> save(GovSymptomTypePo govSymptomTypePo);


}
