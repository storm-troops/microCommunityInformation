package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovOwnerPersonServiceDao;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 人口户籍关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovOwnerPersonInnerServiceSMOImpl extends BaseServiceSMO implements IGovOwnerPersonInnerServiceSMO {

    @Autowired
    private IGovOwnerPersonServiceDao govOwnerPersonServiceDaoImpl;


    @Override
    public int saveGovOwnerPerson(@RequestBody GovOwnerPersonPo govOwnerPersonPo) {
        int saveFlag = 1;
        govOwnerPersonServiceDaoImpl.saveGovOwnerPersonInfo(BeanConvertUtil.beanCovertMap(govOwnerPersonPo));
        return saveFlag;
    }

     @Override
    public int updateGovOwnerPerson(@RequestBody  GovOwnerPersonPo govOwnerPersonPo) {
        int saveFlag = 1;
         govOwnerPersonServiceDaoImpl.updateGovOwnerPersonInfo(BeanConvertUtil.beanCovertMap(govOwnerPersonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovOwnerPerson(@RequestBody  GovOwnerPersonPo govOwnerPersonPo) {
        int saveFlag = 1;
        govOwnerPersonPo.setStatusCd("1");
        govOwnerPersonServiceDaoImpl.updateGovOwnerPersonInfo(BeanConvertUtil.beanCovertMap(govOwnerPersonPo));
        return saveFlag;
    }

    @Override
    public List<GovOwnerPersonDto> queryGovOwnerPersons(@RequestBody  GovOwnerPersonDto govOwnerPersonDto) {

        //校验是否传了 分页信息

        int page = govOwnerPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOwnerPersonDto.setPage((page - 1) * govOwnerPersonDto.getRow());
        }

        List<GovOwnerPersonDto> govOwnerPersons = BeanConvertUtil.covertBeanList(govOwnerPersonServiceDaoImpl.getGovOwnerPersonInfo(BeanConvertUtil.beanCovertMap(govOwnerPersonDto)), GovOwnerPersonDto.class);

        return govOwnerPersons;
    }


    @Override
    public int queryGovOwnerPersonsCount(@RequestBody GovOwnerPersonDto govOwnerPersonDto) {
        return govOwnerPersonServiceDaoImpl.queryGovOwnerPersonsCount(BeanConvertUtil.beanCovertMap(govOwnerPersonDto));    }

    public IGovOwnerPersonServiceDao getGovOwnerPersonServiceDaoImpl() {
        return govOwnerPersonServiceDaoImpl;
    }

    public void setGovOwnerPersonServiceDaoImpl(IGovOwnerPersonServiceDao govOwnerPersonServiceDaoImpl) {
        this.govOwnerPersonServiceDaoImpl = govOwnerPersonServiceDaoImpl;
    }
}
