package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govOwner.IDeleteGovOwnerBMO;
import com.java110.cust.bmo.govOwner.IGetGovOwnerBMO;
import com.java110.cust.bmo.govOwner.ISaveGovOwnerBMO;
import com.java110.cust.bmo.govOwner.IUpdateGovOwnerBMO;
import com.java110.dto.govOwner.GovOwnerDto;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govOwner")
public class GovOwnerApi {

    @Autowired
    private ISaveGovOwnerBMO saveGovOwnerBMOImpl;
    @Autowired
    private IUpdateGovOwnerBMO updateGovOwnerBMOImpl;
    @Autowired
    private IDeleteGovOwnerBMO deleteGovOwnerBMOImpl;

    @Autowired
    private IGetGovOwnerBMO getGovOwnerBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govOwner/saveGovOwner
     * @path /app/govOwner/saveGovOwner
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovOwner", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovOwner(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "ownerNum", "请求报文中未包含ownerNum");
        Assert.hasKeyAndValue(reqJson, "ownerName", "请求报文中未包含ownerName");
        Assert.hasKeyAndValue(reqJson, "ownerType", "请求报文中未包含ownerType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "ownerTel", "请求报文中未包含ownerTel");
        Assert.hasKeyAndValue(reqJson, "ownerAddress", "请求报文中未包含ownerAddress");


        GovOwnerPo govOwnerPo = BeanConvertUtil.covertBean(reqJson, GovOwnerPo.class);
        return saveGovOwnerBMOImpl.save(govOwnerPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govOwner/updateGovOwner
     * @path /app/govOwner/updateGovOwner
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovOwner", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovOwner(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "ownerNum", "请求报文中未包含ownerNum");
        Assert.hasKeyAndValue(reqJson, "ownerName", "请求报文中未包含ownerName");
        Assert.hasKeyAndValue(reqJson, "ownerType", "请求报文中未包含ownerType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "ownerTel", "请求报文中未包含ownerTel");
        Assert.hasKeyAndValue(reqJson, "ownerAddress", "请求报文中未包含ownerAddress");
        Assert.hasKeyAndValue(reqJson, "govOwnerId", "govOwnerId不能为空");


        GovOwnerPo govOwnerPo = BeanConvertUtil.covertBean(reqJson, GovOwnerPo.class);
        return updateGovOwnerBMOImpl.update(govOwnerPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govOwner/deleteGovOwner
     * @path /app/govOwner/deleteGovOwner
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovOwner", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovOwner(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govOwnerId", "govOwnerId不能为空");


        GovOwnerPo govOwnerPo = BeanConvertUtil.covertBean(reqJson, GovOwnerPo.class);
        return deleteGovOwnerBMOImpl.delete(govOwnerPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govOwner/queryGovOwner
     * @path /app/govOwner/queryGovOwner
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovOwner", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovOwner(@RequestParam(value = "idCard" , required = false) String idCard,
                                                      @RequestParam(value = "ownerNum" , required = false) String ownerNum,
                                                      @RequestParam(value = "govOwnerId" , required = false) String govOwnerId,
                                                      @RequestParam(value = "owneName" , required = false) String owneName,
                                                      @RequestParam(value = "ownerType" , required = false) String ownerType,
                                                      @RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "ownerTel" , required = false) String ownerTel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovOwnerDto govOwnerDto = new GovOwnerDto();
        govOwnerDto.setPage(page);
        govOwnerDto.setRow(row);
        govOwnerDto.setIdCard(idCard);
        govOwnerDto.setOwnerNum(ownerNum);
        govOwnerDto.setOwnerName(owneName);
        govOwnerDto.setOwnerType(ownerType);
        govOwnerDto.setCaId(caId);
        govOwnerDto.setOwnerTel(ownerTel);
        govOwnerDto.setGovOwnerId( govOwnerId );
        return getGovOwnerBMOImpl.get(govOwnerDto);
    }
    /**
     * 微信删除消息模板
     * @serviceCode /govOwner/getGovOwnerPersonRel
     * @path /app/govOwner/queryGovOwner
     * @param
     * @return
     */
    @RequestMapping(value = "/getGovOwnerPersonRel", method = RequestMethod.GET)
    public ResponseEntity<String> getGovOwnerPersonRel(@RequestParam(value = "govPersonId") String govPersonId,
                                                      @RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovOwnerDto govOwnerDto = new GovOwnerDto();
        govOwnerDto.setPage(page);
        govOwnerDto.setRow(row);
        govOwnerDto.setCaId(caId);
        return getGovOwnerBMOImpl.getGovOwnerPersonRel(govOwnerDto,govPersonId);
    }
}
