package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovMajorEventsServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 重特大事件服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMajorEventsServiceDaoImpl")
//@Transactional
public class GovMajorEventsServiceDaoImpl extends BaseServiceDao implements IGovMajorEventsServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMajorEventsServiceDaoImpl.class);





    /**
     * 保存重特大事件信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMajorEventsInfo(Map info) throws DAOException {
        logger.debug("保存重特大事件信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMajorEventsServiceDaoImpl.saveGovMajorEventsInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存重特大事件信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询重特大事件信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMajorEventsInfo(Map info) throws DAOException {
        logger.debug("查询重特大事件信息 入参 info : {}",info);

        List<Map> businessGovMajorEventsInfos = sqlSessionTemplate.selectList("govMajorEventsServiceDaoImpl.getGovMajorEventsInfo",info);

        return businessGovMajorEventsInfos;
    }


    /**
     * 修改重特大事件信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMajorEventsInfo(Map info) throws DAOException {
        logger.debug("修改重特大事件信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMajorEventsServiceDaoImpl.updateGovMajorEventsInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改重特大事件信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询重特大事件数量
     * @param info 重特大事件信息
     * @return 重特大事件数量
     */
    @Override
    public int queryGovMajorEventssCount(Map info) {
        logger.debug("查询重特大事件数据 入参 info : {}",info);

        List<Map> businessGovMajorEventsInfos = sqlSessionTemplate.selectList("govMajorEventsServiceDaoImpl.queryGovMajorEventssCount", info);
        if (businessGovMajorEventsInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMajorEventsInfos.get(0).get("count").toString());
    }


}
