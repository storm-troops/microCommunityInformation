package com.java110.cust.bmo.govVolunteer;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteer.GovVolunteerPo;
public interface ISaveGovVolunteerBMO {


    /**
     * 添加志愿者管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> save(JSONObject reqJson);


}
