package com.java110.cust.bmo.govOldPerson.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govOldPerson.IUpdateGovOldPersonBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govOldPersonAttr.GovOldPersonAttrDto;
import com.java110.intf.cust.IGovOldPersonAttrInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovOldPersonInnerServiceSMO;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.store.StorePo;

import java.util.List;

@Service("updateGovOldPersonBMOImpl")
public class UpdateGovOldPersonBMOImpl implements IUpdateGovOldPersonBMO {

    @Autowired
    private IGovOldPersonInnerServiceSMO govOldPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovOldPersonAttrInnerServiceSMO govOldPersonAttrInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * @param govOldPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovOldPersonPo govOldPersonPo, GovPersonPo govPersonPo,JSONObject reqJson) {

        int flag = govOldPersonInnerServiceSMOImpl.updateGovOldPerson(govOldPersonPo);

        if (flag < 1) {
            throw new IllegalArgumentException("修改老年人员失败");
        }
        flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("修改固定人口失败");
        }

        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.EDIT_OLD_PERSON_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException( responseEntity.getBody());
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private void getGovPersonPo(GovOldPersonPo govOldPersonPo) {
        GovOldPersonAttrPo govOldPersonAttrPo = new GovOldPersonAttrPo();
        govOldPersonAttrPo.setAttrId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_attrId));
        govOldPersonAttrPo.setOldId(govOldPersonPo.getOldId());
        govOldPersonAttrPo.setSpecCd(GovOldPersonAttrPo.OLD_TIME_AMOUNT);
        govOldPersonAttrPo.setValue(govOldPersonPo.getTimeAmount());

        GovOldPersonAttrDto govOldPersonAttrDto = new GovOldPersonAttrDto();
        govOldPersonAttrDto.setOldId(govOldPersonPo.getOldId());
        govOldPersonAttrDto.setSpecCd(GovOldPersonAttrPo.OLD_TIME_AMOUNT);
        List<GovOldPersonAttrDto> govOldPersonAttrDtos = govOldPersonAttrInnerServiceSMOImpl.queryGovOldPersonAttrs(govOldPersonAttrDto);
        if (govOldPersonAttrDtos == null && govOldPersonAttrDtos.size() < 1) {
            int flag = govOldPersonAttrInnerServiceSMOImpl.saveGovOldPersonAttr(govOldPersonAttrPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存老人初始化时间金额失败");
            }
        } else {
            govOldPersonAttrPo.setAttrId(govOldPersonAttrDtos.get(0).getAttrId());
            int flag = govOldPersonAttrInnerServiceSMOImpl.updateGovOldPersonAttr(govOldPersonAttrPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改老人初始化时间金额失败");
            }
        }

    }
}
