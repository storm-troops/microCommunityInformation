package com.java110.cust.bmo.govVolunteer;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteer.GovVolunteerPo;

public interface IDeleteGovVolunteerBMO {


    /**
     * 修改志愿者管理
     * add by wuxw
     * @param govVolunteerPo
     * @return
     */
    ResponseEntity<String> delete(GovVolunteerPo govVolunteerPo, GovPersonPo govPersonPo);


}
