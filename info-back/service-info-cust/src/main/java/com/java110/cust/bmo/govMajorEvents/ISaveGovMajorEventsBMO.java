package com.java110.cust.bmo.govMajorEvents;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMajorEvents.GovMajorEventsPo;
public interface ISaveGovMajorEventsBMO {


    /**
     * 添加重特大事件
     * add by wuxw
     * @param govMajorEventsPo
     * @return
     */
    ResponseEntity<String> save(GovMajorEventsPo govMajorEventsPo);

    ResponseEntity<String> savePhone(GovMajorEventsPo govMajorEventsPo,JSONArray photos);

}
