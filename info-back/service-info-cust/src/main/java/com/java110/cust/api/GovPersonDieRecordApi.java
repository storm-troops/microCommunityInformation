package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPersonDieRecord.GovPersonDieRecordDto;
import com.java110.po.govPersonDieRecord.GovPersonDieRecordPo;
import com.java110.cust.bmo.govPersonDieRecord.IDeleteGovPersonDieRecordBMO;
import com.java110.cust.bmo.govPersonDieRecord.IGetGovPersonDieRecordBMO;
import com.java110.cust.bmo.govPersonDieRecord.ISaveGovPersonDieRecordBMO;
import com.java110.cust.bmo.govPersonDieRecord.IUpdateGovPersonDieRecordBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPersonDieRecord")
public class GovPersonDieRecordApi {

    @Autowired
    private ISaveGovPersonDieRecordBMO saveGovPersonDieRecordBMOImpl;
    @Autowired
    private IUpdateGovPersonDieRecordBMO updateGovPersonDieRecordBMOImpl;
    @Autowired
    private IDeleteGovPersonDieRecordBMO deleteGovPersonDieRecordBMOImpl;

    @Autowired
    private IGetGovPersonDieRecordBMO getGovPersonDieRecordBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDieRecord/saveGovPersonDieRecord
     * @path /app/govPersonDieRecord/saveGovPersonDieRecord
     */
    @RequestMapping(value = "/saveGovPersonDieRecord", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPersonDieRecord(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "dieId", "请求报文中未包含dieId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "activityTime", "请求报文中未包含activityTime");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        GovPersonDieRecordPo govPersonDieRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonDieRecordPo.class);
        return saveGovPersonDieRecordBMOImpl.save(govPersonDieRecordPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDieRecord/updateGovPersonDieRecord
     * @path /app/govPersonDieRecord/updateGovPersonDieRecord
     */
    @RequestMapping(value = "/updateGovPersonDieRecord", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPersonDieRecord(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "dieId", "请求报文中未包含dieId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "activityTime", "请求报文中未包含activityTime");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "dieRecordId", "dieRecordId不能为空");


        GovPersonDieRecordPo govPersonDieRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonDieRecordPo.class);
        return updateGovPersonDieRecordBMOImpl.update(govPersonDieRecordPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDieRecord/deleteGovPersonDieRecord
     * @path /app/govPersonDieRecord/deleteGovPersonDieRecord
     */
    @RequestMapping(value = "/deleteGovPersonDieRecord", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPersonDieRecord(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "dieRecordId", "dieRecordId不能为空");


        GovPersonDieRecordPo govPersonDieRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonDieRecordPo.class);
        return deleteGovPersonDieRecordBMOImpl.delete(govPersonDieRecordPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govPersonDieRecord/queryGovPersonDieRecord
     * @path /app/govPersonDieRecord/queryGovPersonDieRecord
     */
    @RequestMapping(value = "/queryGovPersonDieRecord", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPersonDieRecord(@RequestParam(value = "caId") String caId,
                                                          @RequestParam(value = "title" , required = false) String title,
                                                          @RequestParam(value = "activityTime" , required = false) String activityTime,
                                                          @RequestParam(value = "page") int page,
                                                          @RequestParam(value = "row") int row) {
        GovPersonDieRecordDto govPersonDieRecordDto = new GovPersonDieRecordDto();
        govPersonDieRecordDto.setPage(page);
        govPersonDieRecordDto.setRow(row);
        govPersonDieRecordDto.setCaId(caId);
        govPersonDieRecordDto.setActivityTime(activityTime);
        govPersonDieRecordDto.setTitle(title);
        return getGovPersonDieRecordBMOImpl.get(govPersonDieRecordDto);
    }
}
