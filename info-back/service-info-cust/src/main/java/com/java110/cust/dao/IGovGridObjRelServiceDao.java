package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 网格对象关系组件内部之间使用，没有给外围系统提供服务能力
 * 网格对象关系服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovGridObjRelServiceDao {


    /**
     * 保存 网格对象关系信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovGridObjRelInfo(Map info) throws DAOException;




    /**
     * 查询网格对象关系信息（instance过程）
     * 根据bId 查询网格对象关系信息
     * @param info bId 信息
     * @return 网格对象关系信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovGridObjRelInfo(Map info) throws DAOException;



    /**
     * 修改网格对象关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovGridObjRelInfo(Map info) throws DAOException;


    /**
     * 查询网格对象关系总数
     *
     * @param info 网格对象关系信息
     * @return 网格对象关系数量
     */
    int queryGovGridObjRelsCount(Map info);

}
