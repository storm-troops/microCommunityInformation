package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHealthServiceDao;
import com.java110.intf.cust.IGovHealthInnerServiceSMO;
import com.java110.dto.govHealth.GovHealthDto;
import com.java110.po.govHealth.GovHealthPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 体检项目内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHealthInnerServiceSMOImpl extends BaseServiceSMO implements IGovHealthInnerServiceSMO {

    @Autowired
    private IGovHealthServiceDao govHealthServiceDaoImpl;


    @Override
    public int saveGovHealth(@RequestBody  GovHealthPo govHealthPo) {
        int saveFlag = 1;
        govHealthServiceDaoImpl.saveGovHealthInfo(BeanConvertUtil.beanCovertMap(govHealthPo));
        return saveFlag;
    }

     @Override
    public int updateGovHealth(@RequestBody  GovHealthPo govHealthPo) {
        int saveFlag = 1;
         govHealthServiceDaoImpl.updateGovHealthInfo(BeanConvertUtil.beanCovertMap(govHealthPo));
        return saveFlag;
    }

     @Override
    public int deleteGovHealth(@RequestBody  GovHealthPo govHealthPo) {
        int saveFlag = 1;
        govHealthPo.setStatusCd("1");
        govHealthServiceDaoImpl.updateGovHealthInfo(BeanConvertUtil.beanCovertMap(govHealthPo));
        return saveFlag;
    }

    @Override
    public List<GovHealthDto> queryGovHealths(@RequestBody  GovHealthDto govHealthDto) {

        //校验是否传了 分页信息

        int page = govHealthDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHealthDto.setPage((page - 1) * govHealthDto.getRow());
        }

        List<GovHealthDto> govHealths = BeanConvertUtil.covertBeanList(govHealthServiceDaoImpl.getGovHealthInfo(BeanConvertUtil.beanCovertMap(govHealthDto)), GovHealthDto.class);

        return govHealths;
    }


    @Override
    public int queryGovHealthsCount(@RequestBody GovHealthDto govHealthDto) {
        return govHealthServiceDaoImpl.queryGovHealthsCount(BeanConvertUtil.beanCovertMap(govHealthDto));    }

    public IGovHealthServiceDao getGovHealthServiceDaoImpl() {
        return govHealthServiceDaoImpl;
    }

    public void setGovHealthServiceDaoImpl(IGovHealthServiceDao govHealthServiceDaoImpl) {
        this.govHealthServiceDaoImpl = govHealthServiceDaoImpl;
    }
}
