package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovEventsTypeServiceDao;
import com.java110.intf.cust.IGovEventsTypeInnerServiceSMO;
import com.java110.dto.govEventsType.GovEventsTypeDto;
import com.java110.po.govEventsType.GovEventsTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 事件类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovEventsTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovEventsTypeInnerServiceSMO {

    @Autowired
    private IGovEventsTypeServiceDao govEventsTypeServiceDaoImpl;


    @Override
    public int saveGovEventsType(@RequestBody  GovEventsTypePo govEventsTypePo) {
        int saveFlag = 1;
        govEventsTypeServiceDaoImpl.saveGovEventsTypeInfo(BeanConvertUtil.beanCovertMap(govEventsTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovEventsType(@RequestBody  GovEventsTypePo govEventsTypePo) {
        int saveFlag = 1;
         govEventsTypeServiceDaoImpl.updateGovEventsTypeInfo(BeanConvertUtil.beanCovertMap(govEventsTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovEventsType(@RequestBody  GovEventsTypePo govEventsTypePo) {
        int saveFlag = 1;
        govEventsTypePo.setStatusCd("1");
        govEventsTypeServiceDaoImpl.updateGovEventsTypeInfo(BeanConvertUtil.beanCovertMap(govEventsTypePo));
        return saveFlag;
    }

    @Override
    public List<GovEventsTypeDto> queryGovEventsTypes(@RequestBody  GovEventsTypeDto govEventsTypeDto) {

        //校验是否传了 分页信息

        int page = govEventsTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govEventsTypeDto.setPage((page - 1) * govEventsTypeDto.getRow());
        }

        List<GovEventsTypeDto> govEventsTypes = BeanConvertUtil.covertBeanList(govEventsTypeServiceDaoImpl.getGovEventsTypeInfo(BeanConvertUtil.beanCovertMap(govEventsTypeDto)), GovEventsTypeDto.class);

        return govEventsTypes;
    }


    @Override
    public int queryGovEventsTypesCount(@RequestBody GovEventsTypeDto govEventsTypeDto) {
        return govEventsTypeServiceDaoImpl.queryGovEventsTypesCount(BeanConvertUtil.beanCovertMap(govEventsTypeDto));    }

    public IGovEventsTypeServiceDao getGovEventsTypeServiceDaoImpl() {
        return govEventsTypeServiceDaoImpl;
    }

    public void setGovEventsTypeServiceDaoImpl(IGovEventsTypeServiceDao govEventsTypeServiceDaoImpl) {
        this.govEventsTypeServiceDaoImpl = govEventsTypeServiceDaoImpl;
    }
}
