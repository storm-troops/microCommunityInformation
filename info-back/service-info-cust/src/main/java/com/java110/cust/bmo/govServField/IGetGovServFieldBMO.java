package com.java110.cust.bmo.govServField;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govServField.GovServFieldDto;
public interface IGetGovServFieldBMO {


    /**
     * 查询服务领域
     * add by wuxw
     * @param  govServFieldDto
     * @return
     */
    ResponseEntity<String> get(GovServFieldDto govServFieldDto);


}
