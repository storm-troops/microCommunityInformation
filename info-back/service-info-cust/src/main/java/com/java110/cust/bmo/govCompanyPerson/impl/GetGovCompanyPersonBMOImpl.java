package com.java110.cust.bmo.govCompanyPerson.impl;

import com.java110.cust.bmo.govCompanyPerson.IGetGovCompanyPersonBMO;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovCompanyPersonBMOImpl")
public class GetGovCompanyPersonBMOImpl implements IGetGovCompanyPersonBMO {

    @Autowired
    private IGovCompanyPersonInnerServiceSMO govCompanyPersonInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCompanyPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCompanyPersonDto govCompanyPersonDto) {


        int count = govCompanyPersonInnerServiceSMOImpl.queryGovCompanyPersonsCount(govCompanyPersonDto);

        List<GovCompanyPersonDto> govCompanyPersonDtos = null;
        if (count > 0) {
            govCompanyPersonDtos = govCompanyPersonInnerServiceSMOImpl.queryGovCompanyPersons(govCompanyPersonDto);
        } else {
            govCompanyPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCompanyPersonDto.getRow()), count, govCompanyPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
