package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IUOrgCommunityV1ServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 组织区域关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("uOrgCommunityV1ServiceDaoImpl")
//@Transactional
public class UOrgCommunityV1ServiceDaoImpl extends BaseServiceDao implements IUOrgCommunityV1ServiceDao {

    private static Logger logger = LoggerFactory.getLogger(UOrgCommunityV1ServiceDaoImpl.class);





    /**
     * 保存组织区域关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveUOrgCommunityV1Info(Map info) throws DAOException {
        logger.debug("保存组织区域关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("uOrgCommunityV1ServiceDaoImpl.saveUOrgCommunityV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存组织区域关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询组织区域关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getUOrgCommunityV1Info(Map info) throws DAOException {
        logger.debug("查询组织区域关系信息 入参 info : {}",info);

        List<Map> businessUOrgCommunityV1Infos = sqlSessionTemplate.selectList("uOrgCommunityV1ServiceDaoImpl.getUOrgCommunityV1Info",info);

        return businessUOrgCommunityV1Infos;
    }


    /**
     * 修改组织区域关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateUOrgCommunityV1Info(Map info) throws DAOException {
        logger.debug("修改组织区域关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("uOrgCommunityV1ServiceDaoImpl.updateUOrgCommunityV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改组织区域关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询组织区域关系数量
     * @param info 组织区域关系信息
     * @return 组织区域关系数量
     */
    @Override
    public int queryUOrgCommunityV1sCount(Map info) {
        logger.debug("查询组织区域关系数据 入参 info : {}",info);

        List<Map> businessUOrgCommunityV1Infos = sqlSessionTemplate.selectList("uOrgCommunityV1ServiceDaoImpl.queryUOrgCommunityV1sCount", info);
        if (businessUOrgCommunityV1Infos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessUOrgCommunityV1Infos.get(0).get("count").toString());
    }


}
