package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovMedicalDoctorRelServiceDao;
import com.java110.intf.cust.IGovMedicalDoctorRelInnerServiceSMO;
import com.java110.dto.govMedicalDoctorRel.GovMedicalDoctorRelDto;
import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 档案医生内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMedicalDoctorRelInnerServiceSMOImpl extends BaseServiceSMO implements IGovMedicalDoctorRelInnerServiceSMO {

    @Autowired
    private IGovMedicalDoctorRelServiceDao govMedicalDoctorRelServiceDaoImpl;


    @Override
    public int saveGovMedicalDoctorRel(@RequestBody  GovMedicalDoctorRelPo govMedicalDoctorRelPo) {
        int saveFlag = 1;
        govMedicalDoctorRelServiceDaoImpl.saveGovMedicalDoctorRelInfo(BeanConvertUtil.beanCovertMap(govMedicalDoctorRelPo));
        return saveFlag;
    }

     @Override
    public int updateGovMedicalDoctorRel(@RequestBody  GovMedicalDoctorRelPo govMedicalDoctorRelPo) {
        int saveFlag = 1;
         govMedicalDoctorRelServiceDaoImpl.updateGovMedicalDoctorRelInfo(BeanConvertUtil.beanCovertMap(govMedicalDoctorRelPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMedicalDoctorRel(@RequestBody  GovMedicalDoctorRelPo govMedicalDoctorRelPo) {
        int saveFlag = 1;
        govMedicalDoctorRelPo.setStatusCd("1");
        govMedicalDoctorRelServiceDaoImpl.updateGovMedicalDoctorRelInfo(BeanConvertUtil.beanCovertMap(govMedicalDoctorRelPo));
        return saveFlag;
    }

    @Override
    public List<GovMedicalDoctorRelDto> queryGovMedicalDoctorRels(@RequestBody  GovMedicalDoctorRelDto govMedicalDoctorRelDto) {

        //校验是否传了 分页信息

        int page = govMedicalDoctorRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMedicalDoctorRelDto.setPage((page - 1) * govMedicalDoctorRelDto.getRow());
        }

        List<GovMedicalDoctorRelDto> govMedicalDoctorRels = BeanConvertUtil.covertBeanList(govMedicalDoctorRelServiceDaoImpl.getGovMedicalDoctorRelInfo(BeanConvertUtil.beanCovertMap(govMedicalDoctorRelDto)), GovMedicalDoctorRelDto.class);

        return govMedicalDoctorRels;
    }


    @Override
    public int queryGovMedicalDoctorRelsCount(@RequestBody GovMedicalDoctorRelDto govMedicalDoctorRelDto) {
        return govMedicalDoctorRelServiceDaoImpl.queryGovMedicalDoctorRelsCount(BeanConvertUtil.beanCovertMap(govMedicalDoctorRelDto));    }

    public IGovMedicalDoctorRelServiceDao getGovMedicalDoctorRelServiceDaoImpl() {
        return govMedicalDoctorRelServiceDaoImpl;
    }

    public void setGovMedicalDoctorRelServiceDaoImpl(IGovMedicalDoctorRelServiceDao govMedicalDoctorRelServiceDaoImpl) {
        this.govMedicalDoctorRelServiceDaoImpl = govMedicalDoctorRelServiceDaoImpl;
    }
}
