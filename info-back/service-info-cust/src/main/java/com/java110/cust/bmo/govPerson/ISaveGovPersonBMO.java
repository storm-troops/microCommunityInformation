package com.java110.cust.bmo.govPerson;

import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovPersonBMO {


    /**
     * 添加人口管理
     * add by wuxw
     * @param govPersonPo
     * @return
     */
    ResponseEntity<String> save(GovPersonPo govPersonPo);
    /**
     * 添加人口管理
     * add by wuxw
     * @param govPersonPo
     * @return
     */
    ResponseEntity<String> saveGovPetition(GovPersonPo govPersonPo);
    /**
     * 添加人口管理
     * add by wuxw
     * @param govPersonPo
     * @return
     */
    ResponseEntity<String> saveGovPersonCompany(GovPersonPo govPersonPo);


}
