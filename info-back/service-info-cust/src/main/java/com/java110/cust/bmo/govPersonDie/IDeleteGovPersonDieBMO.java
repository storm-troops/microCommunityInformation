package com.java110.cust.bmo.govPersonDie;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPersonDie.GovPersonDiePo;

public interface IDeleteGovPersonDieBMO {


    /**
     * 修改死亡登记
     * add by wuxw
     * @param govPersonDiePo
     * @return
     */
    ResponseEntity<String> delete(GovPersonDiePo govPersonDiePo);


}
