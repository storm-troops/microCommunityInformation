package com.java110.cust.bmo.UserV1.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.UserV1.IDeleteUserV1BMO;
import com.java110.po.UserV1.UserV1Po;
import com.java110.vo.ResultVo;
import com.java110.po.store.StorePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IUserV1InnerServiceSMO;
import com.java110.dto.UserV1.UserV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteUserV1BMOImpl")
public class DeleteUserV1BMOImpl implements IDeleteUserV1BMO {

    @Autowired
    private IUserV1InnerServiceSMO UserV1InnerServiceSMOImpl;

    /**
     * @param UserV1Po 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(UserV1Po UserV1Po) {

        int flag = UserV1InnerServiceSMOImpl.deleteUserV1(UserV1Po);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
