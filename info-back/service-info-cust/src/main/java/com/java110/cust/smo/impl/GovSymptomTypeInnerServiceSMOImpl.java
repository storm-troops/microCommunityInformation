package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovSymptomTypeServiceDao;
import com.java110.intf.cust.IGovSymptomTypeInnerServiceSMO;
import com.java110.dto.govSymptomType.GovSymptomTypeDto;
import com.java110.po.govSymptomType.GovSymptomTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 症状类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovSymptomTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovSymptomTypeInnerServiceSMO {

    @Autowired
    private IGovSymptomTypeServiceDao govSymptomTypeServiceDaoImpl;


    @Override
    public int saveGovSymptomType(@RequestBody  GovSymptomTypePo govSymptomTypePo) {
        int saveFlag = 1;
        govSymptomTypeServiceDaoImpl.saveGovSymptomTypeInfo(BeanConvertUtil.beanCovertMap(govSymptomTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovSymptomType(@RequestBody  GovSymptomTypePo govSymptomTypePo) {
        int saveFlag = 1;
         govSymptomTypeServiceDaoImpl.updateGovSymptomTypeInfo(BeanConvertUtil.beanCovertMap(govSymptomTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovSymptomType(@RequestBody  GovSymptomTypePo govSymptomTypePo) {
        int saveFlag = 1;
        govSymptomTypePo.setStatusCd("1");
        govSymptomTypeServiceDaoImpl.updateGovSymptomTypeInfo(BeanConvertUtil.beanCovertMap(govSymptomTypePo));
        return saveFlag;
    }

    @Override
    public List<GovSymptomTypeDto> queryGovSymptomTypes(@RequestBody  GovSymptomTypeDto govSymptomTypeDto) {

        //校验是否传了 分页信息

        int page = govSymptomTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govSymptomTypeDto.setPage((page - 1) * govSymptomTypeDto.getRow());
        }

        List<GovSymptomTypeDto> govSymptomTypes = BeanConvertUtil.covertBeanList(govSymptomTypeServiceDaoImpl.getGovSymptomTypeInfo(BeanConvertUtil.beanCovertMap(govSymptomTypeDto)), GovSymptomTypeDto.class);

        return govSymptomTypes;
    }


    @Override
    public int queryGovSymptomTypesCount(@RequestBody GovSymptomTypeDto govSymptomTypeDto) {
        return govSymptomTypeServiceDaoImpl.queryGovSymptomTypesCount(BeanConvertUtil.beanCovertMap(govSymptomTypeDto));    }

    public IGovSymptomTypeServiceDao getGovSymptomTypeServiceDaoImpl() {
        return govSymptomTypeServiceDaoImpl;
    }

    public void setGovSymptomTypeServiceDaoImpl(IGovSymptomTypeServiceDao govSymptomTypeServiceDaoImpl) {
        this.govSymptomTypeServiceDaoImpl = govSymptomTypeServiceDaoImpl;
    }
}
