package com.java110.cust.bmo.govOldPersonAttr.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govOldPersonAttr.IGetGovOldPersonAttrBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovOldPersonAttrInnerServiceSMO;
import com.java110.dto.govOldPersonAttr.GovOldPersonAttrDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovOldPersonAttrBMOImpl")
public class GetGovOldPersonAttrBMOImpl implements IGetGovOldPersonAttrBMO {

    @Autowired
    private IGovOldPersonAttrInnerServiceSMO govOldPersonAttrInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govOldPersonAttrDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovOldPersonAttrDto govOldPersonAttrDto) {


        int count = govOldPersonAttrInnerServiceSMOImpl.queryGovOldPersonAttrsCount(govOldPersonAttrDto);

        List<GovOldPersonAttrDto> govOldPersonAttrDtos = null;
        if (count > 0) {
            govOldPersonAttrDtos = govOldPersonAttrInnerServiceSMOImpl.queryGovOldPersonAttrs(govOldPersonAttrDto);
        } else {
            govOldPersonAttrDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govOldPersonAttrDto.getRow()), count, govOldPersonAttrDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
