package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovMedicalGroupServiceDao;
import com.java110.intf.cust.IGovMedicalGroupInnerServiceSMO;
import com.java110.dto.govMedicalGroup.GovMedicalGroupDto;
import com.java110.po.govMedicalGroup.GovMedicalGroupPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 医疗团队内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMedicalGroupInnerServiceSMOImpl extends BaseServiceSMO implements IGovMedicalGroupInnerServiceSMO {

    @Autowired
    private IGovMedicalGroupServiceDao govMedicalGroupServiceDaoImpl;


    @Override
    public int saveGovMedicalGroup(@RequestBody  GovMedicalGroupPo govMedicalGroupPo) {
        int saveFlag = 1;
        govMedicalGroupServiceDaoImpl.saveGovMedicalGroupInfo(BeanConvertUtil.beanCovertMap(govMedicalGroupPo));
        return saveFlag;
    }

     @Override
    public int updateGovMedicalGroup(@RequestBody  GovMedicalGroupPo govMedicalGroupPo) {
        int saveFlag = 1;
         govMedicalGroupServiceDaoImpl.updateGovMedicalGroupInfo(BeanConvertUtil.beanCovertMap(govMedicalGroupPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMedicalGroup(@RequestBody  GovMedicalGroupPo govMedicalGroupPo) {
        int saveFlag = 1;
        govMedicalGroupPo.setStatusCd("1");
        govMedicalGroupServiceDaoImpl.updateGovMedicalGroupInfo(BeanConvertUtil.beanCovertMap(govMedicalGroupPo));
        return saveFlag;
    }

    @Override
    public List<GovMedicalGroupDto> queryGovMedicalGroups(@RequestBody  GovMedicalGroupDto govMedicalGroupDto) {

        //校验是否传了 分页信息

        int page = govMedicalGroupDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMedicalGroupDto.setPage((page - 1) * govMedicalGroupDto.getRow());
        }

        List<GovMedicalGroupDto> govMedicalGroups = BeanConvertUtil.covertBeanList(govMedicalGroupServiceDaoImpl.getGovMedicalGroupInfo(BeanConvertUtil.beanCovertMap(govMedicalGroupDto)), GovMedicalGroupDto.class);

        return govMedicalGroups;
    }


    @Override
    public int queryGovMedicalGroupsCount(@RequestBody GovMedicalGroupDto govMedicalGroupDto) {
        return govMedicalGroupServiceDaoImpl.queryGovMedicalGroupsCount(BeanConvertUtil.beanCovertMap(govMedicalGroupDto));    }

    public IGovMedicalGroupServiceDao getGovMedicalGroupServiceDaoImpl() {
        return govMedicalGroupServiceDaoImpl;
    }

    public void setGovMedicalGroupServiceDaoImpl(IGovMedicalGroupServiceDao govMedicalGroupServiceDaoImpl) {
        this.govMedicalGroupServiceDaoImpl = govMedicalGroupServiceDaoImpl;
    }
}
