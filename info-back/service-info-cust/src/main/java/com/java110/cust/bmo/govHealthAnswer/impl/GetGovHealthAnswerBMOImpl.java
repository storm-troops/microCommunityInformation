package com.java110.cust.bmo.govHealthAnswer.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthAnswer.IGetGovHealthAnswerBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthAnswerInnerServiceSMO;
import com.java110.dto.govHealthAnswer.GovHealthAnswerDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHealthAnswerBMOImpl")
public class GetGovHealthAnswerBMOImpl implements IGetGovHealthAnswerBMO {

    @Autowired
    private IGovHealthAnswerInnerServiceSMO govHealthAnswerInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHealthAnswerDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHealthAnswerDto govHealthAnswerDto) {


        int count = govHealthAnswerInnerServiceSMOImpl.queryGovHealthAnswersCount(govHealthAnswerDto);

        List<GovHealthAnswerDto> govHealthAnswerDtos = null;
        if (count > 0) {
            govHealthAnswerDtos = govHealthAnswerInnerServiceSMOImpl.queryGovHealthAnswers(govHealthAnswerDto);
        } else {
            govHealthAnswerDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHealthAnswerDto.getRow()), count, govHealthAnswerDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
