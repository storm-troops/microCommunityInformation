package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHealthTitleValueServiceDao;
import com.java110.intf.cust.IGovHealthTitleValueInnerServiceSMO;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import com.java110.po.govHealthTitleValue.GovHealthTitleValuePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 体检项值内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHealthTitleValueInnerServiceSMOImpl extends BaseServiceSMO implements IGovHealthTitleValueInnerServiceSMO {

    @Autowired
    private IGovHealthTitleValueServiceDao govHealthTitleValueServiceDaoImpl;


    @Override
    public int saveGovHealthTitleValue(@RequestBody  GovHealthTitleValuePo govHealthTitleValuePo) {
        int saveFlag = 1;
        govHealthTitleValueServiceDaoImpl.saveGovHealthTitleValueInfo(BeanConvertUtil.beanCovertMap(govHealthTitleValuePo));
        return saveFlag;
    }

     @Override
    public int updateGovHealthTitleValue(@RequestBody  GovHealthTitleValuePo govHealthTitleValuePo) {
        int saveFlag = 1;
         govHealthTitleValueServiceDaoImpl.updateGovHealthTitleValueInfo(BeanConvertUtil.beanCovertMap(govHealthTitleValuePo));
        return saveFlag;
    }

     @Override
    public int deleteGovHealthTitleValue(@RequestBody  GovHealthTitleValuePo govHealthTitleValuePo) {
        int saveFlag = 1;
        govHealthTitleValuePo.setStatusCd("1");
        govHealthTitleValueServiceDaoImpl.updateGovHealthTitleValueInfo(BeanConvertUtil.beanCovertMap(govHealthTitleValuePo));
        return saveFlag;
    }

    @Override
    public List<GovHealthTitleValueDto> queryGovHealthTitleValues(@RequestBody  GovHealthTitleValueDto govHealthTitleValueDto) {

        //校验是否传了 分页信息

        int page = govHealthTitleValueDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHealthTitleValueDto.setPage((page - 1) * govHealthTitleValueDto.getRow());
        }

        List<GovHealthTitleValueDto> govHealthTitleValues = BeanConvertUtil.covertBeanList(govHealthTitleValueServiceDaoImpl.getGovHealthTitleValueInfo(BeanConvertUtil.beanCovertMap(govHealthTitleValueDto)), GovHealthTitleValueDto.class);

        return govHealthTitleValues;
    }


    @Override
    public int queryGovHealthTitleValuesCount(@RequestBody GovHealthTitleValueDto govHealthTitleValueDto) {
        return govHealthTitleValueServiceDaoImpl.queryGovHealthTitleValuesCount(BeanConvertUtil.beanCovertMap(govHealthTitleValueDto));    }

    public IGovHealthTitleValueServiceDao getGovHealthTitleValueServiceDaoImpl() {
        return govHealthTitleValueServiceDaoImpl;
    }

    public void setGovHealthTitleValueServiceDaoImpl(IGovHealthTitleValueServiceDao govHealthTitleValueServiceDaoImpl) {
        this.govHealthTitleValueServiceDaoImpl = govHealthTitleValueServiceDaoImpl;
    }
}
