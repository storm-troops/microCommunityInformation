package com.java110.cust.bmo.govHealthTitleValue;
import com.java110.po.govHealthTitleValue.GovHealthTitleValuePo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IUpdateGovHealthTitleValueBMO {


    /**
     * 修改体检项值
     * add by wuxw
     * @param govHealthTitleValuePo
     * @return
     */
    ResponseEntity<String> update(GovHealthTitleValuePo govHealthTitleValuePo);


}
