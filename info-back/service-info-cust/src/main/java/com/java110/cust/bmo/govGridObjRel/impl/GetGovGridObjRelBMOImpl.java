package com.java110.cust.bmo.govGridObjRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govGridObjRel.IGetGovGridObjRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovGridObjRelInnerServiceSMO;
import com.java110.dto.govGridObjRel.GovGridObjRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovGridObjRelBMOImpl")
public class GetGovGridObjRelBMOImpl implements IGetGovGridObjRelBMO {

    @Autowired
    private IGovGridObjRelInnerServiceSMO govGridObjRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govGridObjRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovGridObjRelDto govGridObjRelDto) {


        int count = govGridObjRelInnerServiceSMOImpl.queryGovGridObjRelsCount(govGridObjRelDto);

        List<GovGridObjRelDto> govGridObjRelDtos = null;
        if (count > 0) {
            govGridObjRelDtos = govGridObjRelInnerServiceSMOImpl.queryGovGridObjRels(govGridObjRelDto);
        } else {
            govGridObjRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govGridObjRelDto.getRow()), count, govGridObjRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
