package com.java110.cust.bmo.govOldPersonType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govOldPersonType.IGetGovOldPersonTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovOldPersonTypeInnerServiceSMO;
import com.java110.dto.govOldPersonType.GovOldPersonTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovOldPersonTypeBMOImpl")
public class GetGovOldPersonTypeBMOImpl implements IGetGovOldPersonTypeBMO {

    @Autowired
    private IGovOldPersonTypeInnerServiceSMO govOldPersonTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govOldPersonTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovOldPersonTypeDto govOldPersonTypeDto) {


        int count = govOldPersonTypeInnerServiceSMOImpl.queryGovOldPersonTypesCount(govOldPersonTypeDto);

        List<GovOldPersonTypeDto> govOldPersonTypeDtos = null;
        if (count > 0) {
            govOldPersonTypeDtos = govOldPersonTypeInnerServiceSMOImpl.queryGovOldPersonTypes(govOldPersonTypeDto);
        } else {
            govOldPersonTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govOldPersonTypeDto.getRow()), count, govOldPersonTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
