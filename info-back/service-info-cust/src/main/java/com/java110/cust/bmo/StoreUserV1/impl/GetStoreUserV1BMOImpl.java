package com.java110.cust.bmo.StoreUserV1.impl;

import com.java110.cust.bmo.StoreUserV1.IGetStoreUserV1BMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IStoreUserV1InnerServiceSMO;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getStoreUserV1BMOImpl")
public class GetStoreUserV1BMOImpl implements IGetStoreUserV1BMO {

    @Autowired
    private IStoreUserV1InnerServiceSMO StoreUserV1InnerServiceSMOImpl;

    /**
     *
     *
     * @param  StoreUserV1Dto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(StoreUserV1Dto StoreUserV1Dto) {


        int count = StoreUserV1InnerServiceSMOImpl.queryStoreUserV1sCount(StoreUserV1Dto);

        List<StoreUserV1Dto> StoreUserV1Dtos = null;
        if (count > 0) {
            StoreUserV1Dtos = StoreUserV1InnerServiceSMOImpl.queryStoreUserV1s(StoreUserV1Dto);
        } else {
            StoreUserV1Dtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) StoreUserV1Dto.getRow()), count, StoreUserV1Dtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
