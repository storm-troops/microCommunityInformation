package com.java110.cust.bmo.govMajorEvents;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMajorEvents.GovMajorEventsPo;

public interface IUpdateGovMajorEventsBMO {


    /**
     * 修改重特大事件
     * add by wuxw
     * @param govMajorEventsPo
     * @return
     */
    ResponseEntity<String> update(GovMajorEventsPo govMajorEventsPo);


}
