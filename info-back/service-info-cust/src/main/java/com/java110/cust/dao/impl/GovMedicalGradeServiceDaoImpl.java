package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovMedicalGradeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 医疗分级服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMedicalGradeServiceDaoImpl")
//@Transactional
public class GovMedicalGradeServiceDaoImpl extends BaseServiceDao implements IGovMedicalGradeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMedicalGradeServiceDaoImpl.class);





    /**
     * 保存医疗分级信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMedicalGradeInfo(Map info) throws DAOException {
        logger.debug("保存医疗分级信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMedicalGradeServiceDaoImpl.saveGovMedicalGradeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存医疗分级信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询医疗分级信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMedicalGradeInfo(Map info) throws DAOException {
        logger.debug("查询医疗分级信息 入参 info : {}",info);

        List<Map> businessGovMedicalGradeInfos = sqlSessionTemplate.selectList("govMedicalGradeServiceDaoImpl.getGovMedicalGradeInfo",info);

        return businessGovMedicalGradeInfos;
    }


    /**
     * 修改医疗分级信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMedicalGradeInfo(Map info) throws DAOException {
        logger.debug("修改医疗分级信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMedicalGradeServiceDaoImpl.updateGovMedicalGradeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改医疗分级信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询医疗分级数量
     * @param info 医疗分级信息
     * @return 医疗分级数量
     */
    @Override
    public int queryGovMedicalGradesCount(Map info) {
        logger.debug("查询医疗分级数据 入参 info : {}",info);

        List<Map> businessGovMedicalGradeInfos = sqlSessionTemplate.selectList("govMedicalGradeServiceDaoImpl.queryGovMedicalGradesCount", info);
        if (businessGovMedicalGradeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMedicalGradeInfos.get(0).get("count").toString());
    }


}
