package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovOwnerServiceDao;
import com.java110.dto.govOwner.GovOwnerDto;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 户籍管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovOwnerInnerServiceSMOImpl extends BaseServiceSMO implements IGovOwnerInnerServiceSMO {

    @Autowired
    private IGovOwnerServiceDao govOwnerServiceDaoImpl;


    @Override
    public int saveGovOwner(@RequestBody GovOwnerPo govOwnerPo) {
        int saveFlag = 1;
        govOwnerServiceDaoImpl.saveGovOwnerInfo(BeanConvertUtil.beanCovertMap(govOwnerPo));
        return saveFlag;
    }

     @Override
    public int updateGovOwner(@RequestBody  GovOwnerPo govOwnerPo) {
        int saveFlag = 1;
         govOwnerServiceDaoImpl.updateGovOwnerInfo(BeanConvertUtil.beanCovertMap(govOwnerPo));
        return saveFlag;
    }

     @Override
    public int deleteGovOwner(@RequestBody  GovOwnerPo govOwnerPo) {
        int saveFlag = 1;
        govOwnerPo.setStatusCd("1");
        govOwnerServiceDaoImpl.updateGovOwnerInfo(BeanConvertUtil.beanCovertMap(govOwnerPo));
        return saveFlag;
    }

    @Override
    public List<GovOwnerDto> queryGovOwners(@RequestBody  GovOwnerDto govOwnerDto) {

        //校验是否传了 分页信息

        int page = govOwnerDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOwnerDto.setPage((page - 1) * govOwnerDto.getRow());
        }

        List<GovOwnerDto> govOwners = BeanConvertUtil.covertBeanList(govOwnerServiceDaoImpl.getGovOwnerInfo(BeanConvertUtil.beanCovertMap(govOwnerDto)), GovOwnerDto.class);

        return govOwners;
    }
    @Override
    public List<GovOwnerDto> getGovOwnerPersonRel(@RequestBody  GovOwnerDto govOwnerDto) {

        //校验是否传了 分页信息

        int page = govOwnerDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOwnerDto.setPage((page - 1) * govOwnerDto.getRow());
        }

        List<GovOwnerDto> govOwners = BeanConvertUtil.covertBeanList(govOwnerServiceDaoImpl.getGovOwnerPersonRel(BeanConvertUtil.beanCovertMap(govOwnerDto)), GovOwnerDto.class);

        return govOwners;
    }


    @Override
    public int queryGovOwnersCount(@RequestBody GovOwnerDto govOwnerDto) {
        return govOwnerServiceDaoImpl.queryGovOwnersCount(BeanConvertUtil.beanCovertMap(govOwnerDto));    }
    @Override
    public int getGovOwnerPersonRelCount(@RequestBody GovOwnerDto govOwnerDto) {
        return govOwnerServiceDaoImpl.getGovOwnerPersonRelCount(BeanConvertUtil.beanCovertMap(govOwnerDto));    }

    public IGovOwnerServiceDao getGovOwnerServiceDaoImpl() {
        return govOwnerServiceDaoImpl;
    }

    public void setGovOwnerServiceDaoImpl(IGovOwnerServiceDao govOwnerServiceDaoImpl) {
        this.govOwnerServiceDaoImpl = govOwnerServiceDaoImpl;
    }
}
