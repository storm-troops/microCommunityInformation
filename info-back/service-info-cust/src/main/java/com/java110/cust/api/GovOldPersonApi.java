package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govOldPerson.IDeleteGovOldPersonBMO;
import com.java110.cust.bmo.govOldPerson.IGetGovOldPersonBMO;
import com.java110.cust.bmo.govOldPerson.ISaveGovOldPersonBMO;
import com.java110.cust.bmo.govOldPerson.IUpdateGovOldPersonBMO;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govOldPerson")
public class GovOldPersonApi {

    @Autowired
    private ISaveGovOldPersonBMO saveGovOldPersonBMOImpl;
    @Autowired
    private IUpdateGovOldPersonBMO updateGovOldPersonBMOImpl;
    @Autowired
    private IDeleteGovOldPersonBMO deleteGovOldPersonBMOImpl;

    @Autowired
    private IGetGovOldPersonBMO getGovOldPersonBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPerson/saveGovOldPerson
     * @path /app/govOldPerson/saveGovOldPerson
     */
    @RequestMapping(value = "/saveGovOldPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovOldPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personAge", "请求报文中未包含personAge");
        Assert.hasKeyAndValue(reqJson, "contactPerson", "请求报文中未包含contactPerson");
        Assert.hasKeyAndValue(reqJson, "contactTel", "请求报文中未包含contactTel");
        Assert.hasKeyAndValue(reqJson, "servPerson", "请求报文中未包含servPerson");
        Assert.hasKeyAndValue(reqJson, "servTel", "请求报文中未包含servTel");
        Assert.hasKeyAndValue(reqJson, "timeAmount", "请求报文中未包含timeAmount");
        Assert.hasKeyAndValue(reqJson, "typeId", "请求报文中未包含typeId");

        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");

        GovOldPersonPo govOldPersonPo = BeanConvertUtil.covertBean(reqJson, GovOldPersonPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return saveGovOldPersonBMOImpl.save(govOldPersonPo,govPersonPo,reqJson);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPerson/updateGovOldPerson
     * @path /app/govOldPerson/updateGovOldPerson
     */
    @RequestMapping(value = "/updateGovOldPerson", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovOldPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "oldId", "请求报文中未包含oldId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personAge", "请求报文中未包含personAge");
        Assert.hasKeyAndValue(reqJson, "contactPerson", "请求报文中未包含contactPerson");
        Assert.hasKeyAndValue(reqJson, "contactTel", "请求报文中未包含contactTel");
        Assert.hasKeyAndValue(reqJson, "servPerson", "请求报文中未包含servPerson");
        Assert.hasKeyAndValue(reqJson, "servTel", "请求报文中未包含servTel");
        Assert.hasKeyAndValue(reqJson, "timeAmount", "请求报文中未包含timeAmount");

        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");

        GovOldPersonPo govOldPersonPo = BeanConvertUtil.covertBean(reqJson, GovOldPersonPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return updateGovOldPersonBMOImpl.update(govOldPersonPo,govPersonPo,reqJson);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPerson/deleteGovOldPerson
     * @path /app/govOldPerson/deleteGovOldPerson
     */
    @RequestMapping(value = "/deleteGovOldPerson", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovOldPerson(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "oldId", "oldId不能为空");


        GovOldPersonPo govOldPersonPo = BeanConvertUtil.covertBean(reqJson, GovOldPersonPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return deleteGovOldPersonBMOImpl.delete(govOldPersonPo,govPersonPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govOldPerson/queryGovOldPerson
     * @path /app/govOldPerson/queryGovOldPerson
     */
    @RequestMapping(value = "/queryGovOldPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovOldPerson(@RequestParam(value = "caId",required = false) String caId,
                                                    @RequestParam(value = "govCommunityId",required = false) String govCommunityId,
                                                    @RequestParam(value = "govPersonId",required = false) String govPersonId,
                                                    @RequestParam(value = "personName",required = false) String personName,
                                                    @RequestParam(value = "birthday",required = false) String birthday,
                                                    @RequestParam(value = "oldId",required = false) String oldId,
                                                    @RequestParam(value = "personTel",required = false) String personTel,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "row") int row) {

        GovOldPersonDto govOldPersonDto = new GovOldPersonDto();
        govOldPersonDto.setPage(page);
        govOldPersonDto.setRow(row);
        govOldPersonDto.setCaId(caId);
        govOldPersonDto.setGovCommunityId(govCommunityId);
        govOldPersonDto.setPersonName(personName);
        govOldPersonDto.setPersonTel(personTel);
        govOldPersonDto.setOldId(oldId);
        govOldPersonDto.setGovPersonId(govPersonId);
        govOldPersonDto.setBirthday(birthday);
        return getGovOldPersonBMOImpl.get(govOldPersonDto);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govOldPerson/getGovOldTypePersonCount
     * @path /app/govOldPerson/queryGovOldPerson
     */
    @RequestMapping(value = "/getGovOldTypePersonCount", method = RequestMethod.GET)
    public ResponseEntity<String> getGovOldTypePersonCount(@RequestParam(value = "caId",required = false) String caId,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "row") int row) {

        GovOldPersonDto govOldPersonDto = new GovOldPersonDto();
        govOldPersonDto.setPage(page);
        govOldPersonDto.setRow(row);
        govOldPersonDto.setCaId(caId);
        return getGovOldPersonBMOImpl.getGovOldTypePersonCount(govOldPersonDto);
    }
    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govOldPerson/queryGovOldAccountDetail
     * @path /app/govOldPerson/queryGovOldPerson
     */
    @RequestMapping(value = "/queryGovOldAccountDetail", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovOldAccountDetail(@RequestParam(value = "caId",required = false) String caId,
                                                    @RequestParam(value = "govCommunityId",required = false) String govCommunityId,
                                                    @RequestParam(value = "oldId",required = false) String oldId,
                                                    @RequestParam(value = "detailType",required = false) String detailType,
                                                    @RequestParam(value = "orderId",required = false) String orderId,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "row") int row) {

        GovOldPersonDto govOldPersonDto = new GovOldPersonDto();
        govOldPersonDto.setPage(page);
        govOldPersonDto.setRow(row);
        govOldPersonDto.setCaId(caId);
        govOldPersonDto.setGovCommunityId(govCommunityId);
        govOldPersonDto.setOldId(oldId);
        govOldPersonDto.setDetailType(detailType);
        govOldPersonDto.setOrderId(orderId);
        return getGovOldPersonBMOImpl.queryGovOldAccountDetail(govOldPersonDto);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govOldPerson/queryAccountAmount
     * @path /app/govOldPerson/queryGovOldPerson
     */
    @RequestMapping(value = "/queryAccountAmount", method = RequestMethod.GET)
    public ResponseEntity<String> queryAccountAmount() {


        return getGovOldPersonBMOImpl.queryAccountAmount();
    }
    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govOldPerson/queryStoreOrderCart
     * @path
     */
    @RequestMapping(value = "/queryStoreOrderCart", method = RequestMethod.GET)
    public ResponseEntity<String> queryStoreOrderCart( @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "row") int row) {


        return getGovOldPersonBMOImpl.queryStoreOrderCart(page,row);
    }
}
