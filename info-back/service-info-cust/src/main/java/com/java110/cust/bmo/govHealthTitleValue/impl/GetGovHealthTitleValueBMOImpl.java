package com.java110.cust.bmo.govHealthTitleValue.impl;

import com.java110.cust.bmo.govHealthTitleValue.IGetGovHealthTitleValueBMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthTitleValueInnerServiceSMO;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHealthTitleValueBMOImpl")
public class GetGovHealthTitleValueBMOImpl implements IGetGovHealthTitleValueBMO {

    @Autowired
    private IGovHealthTitleValueInnerServiceSMO govHealthTitleValueInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHealthTitleValueDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHealthTitleValueDto govHealthTitleValueDto) {


        int count = govHealthTitleValueInnerServiceSMOImpl.queryGovHealthTitleValuesCount(govHealthTitleValueDto);

        List<GovHealthTitleValueDto> govHealthTitleValueDtos = null;
        if (count > 0) {
            govHealthTitleValueDtos = govHealthTitleValueInnerServiceSMOImpl.queryGovHealthTitleValues(govHealthTitleValueDto);
        } else {
            govHealthTitleValueDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHealthTitleValueDto.getRow()), count, govHealthTitleValueDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
