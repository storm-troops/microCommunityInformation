package com.java110.cust.bmo.govOwner;
import com.java110.dto.govOwner.GovOwnerDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovOwnerBMO {


    /**
     * 查询户籍管理
     * add by wuxw
     * @param  govOwnerDto
     * @return
     */
    ResponseEntity<String> get(GovOwnerDto govOwnerDto);

    /**
     * 查询户籍管理
     * add by wuxw
     * @param  govOwnerDto
     * @return
     */
    ResponseEntity<String> getGovOwnerPersonRel(GovOwnerDto govOwnerDto,String govPersonId);


}
