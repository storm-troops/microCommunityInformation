package com.java110.cust.bmo.govVolunteer.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govVolunteer.IGetGovVolunteerBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.dto.govVolunteerServ.GovVolunteerServDto;
import com.java110.intf.cust.IGovVolunteerServInnerServiceSMO;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovVolunteerInnerServiceSMO;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovVolunteerBMOImpl")
public class GetGovVolunteerBMOImpl implements IGetGovVolunteerBMO {

    @Autowired
    private IGovVolunteerInnerServiceSMO govVolunteerInnerServiceSMOImpl;
    @Autowired
    private IGovVolunteerServInnerServiceSMO govVolunteerServInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     *
     *
     * @param  govVolunteerDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovVolunteerDto govVolunteerDto) {


        int count = govVolunteerInnerServiceSMOImpl.queryGovVolunteersCount(govVolunteerDto);

        List<GovVolunteerDto> govVolunteerDtos = null;
        if (count > 0) {
            govVolunteerDtos = govVolunteerInnerServiceSMOImpl.queryGovVolunteers(govVolunteerDto);
            resfGovVolunteerServ(govVolunteerDtos);
        } else {
            govVolunteerDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govVolunteerDto.getRow()), count, govVolunteerDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  govVolunteerDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovStateVolunteerCount(GovVolunteerDto govVolunteerDto) {



        List<GovVolunteerDto> govVolunteerDtos = govVolunteerInnerServiceSMOImpl.getGovStateVolunteerCount(govVolunteerDto);

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) 10 / (double) govVolunteerDto.getRow()), 10, govVolunteerDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void resfGovVolunteerServ(List<GovVolunteerDto> govVolunteerDtos) {

        if (govVolunteerDtos == null || govVolunteerDtos.size() < 1) {
            return;
        }
        List<String> govVolunteerIds = new ArrayList<>();
        for (GovVolunteerDto govVolunteerDto : govVolunteerDtos) {
            govVolunteerIds.add(govVolunteerDto.getVolunteerId());
        }

        GovVolunteerServDto govVolunteerServDto = new GovVolunteerServDto();
        govVolunteerServDto.setVolunteerIds(govVolunteerIds.toArray(new String[govVolunteerIds.size()]));
        govVolunteerServDto.setCaId(govVolunteerDtos.get(0).getCaId());
        List<GovVolunteerServDto> govVolunteerServDtos = govVolunteerServInnerServiceSMOImpl.queryGovVolunteerServs(govVolunteerServDto);

        JSONArray tmpGovVolunteerDtos = null;
        for (GovVolunteerDto govVolunteerDto : govVolunteerDtos) {
            tmpGovVolunteerDtos = new JSONArray();
            for (GovVolunteerServDto govVolunteerServ : govVolunteerServDtos) {
                if (govVolunteerDto.getVolunteerId().equals(govVolunteerServ.getVolunteerId())) {
                    tmpGovVolunteerDtos.add(govVolunteerServ.getServFieldId());
                }
            }
            govVolunteerDto.setVolunteerServ(tmpGovVolunteerDtos.toJSONString());
        }

    }
    /**
     *
     *
     * @param  govVolunteerDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getVolunteerAccountDetail(GovVolunteerDto govVolunteerDto) {


        JSONObject reqJson = new JSONObject();
        reqJson.put("objId",govVolunteerDto.getVolunteerId());
        reqJson.put("acctType",GovVolunteerDto.ACCT_TYPE);
        reqJson.put("extUserSpecCd",GovVolunteerDto.EXT_USER_SPEC_CD);
        reqJson.put("objType",GovVolunteerDto.OBJ_TYPE);
        reqJson.put("detailType",govVolunteerDto.getDetailType());
        reqJson.put("orderId",govVolunteerDto.getOrderId());
        reqJson.put("page",govVolunteerDto.getPage());
        reqJson.put("row",govVolunteerDto.getRow());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.GET_ACCOUNT_DETAIL_MALL, HttpMethod.GET);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(responseEntity.getBody());
            }
        }
        return responseEntity;
    }
}
