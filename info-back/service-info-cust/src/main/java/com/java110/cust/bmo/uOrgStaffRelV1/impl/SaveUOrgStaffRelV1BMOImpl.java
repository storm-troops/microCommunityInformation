package com.java110.cust.bmo.uOrgStaffRelV1.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.uOrgStaffRelV1.ISaveUOrgStaffRelV1BMO;
import com.java110.po.uOrgStaffRelV1.UOrgStaffRelV1Po;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.intf.cust.IUOrgStaffRelV1InnerServiceSMO;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveUOrgStaffRelV1BMOImpl")
public class SaveUOrgStaffRelV1BMOImpl implements ISaveUOrgStaffRelV1BMO {

    @Autowired
    private IUOrgStaffRelV1InnerServiceSMO uOrgStaffRelV1InnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param uOrgStaffRelV1Po
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(UOrgStaffRelV1Po uOrgStaffRelV1Po) {

        uOrgStaffRelV1Po.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_relId));
        int flag = uOrgStaffRelV1InnerServiceSMOImpl.saveUOrgStaffRelV1(uOrgStaffRelV1Po);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
