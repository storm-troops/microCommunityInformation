package com.java110.cust.bmo.govServField;

import org.springframework.http.ResponseEntity;
import com.java110.po.govServField.GovServFieldPo;
public interface ISaveGovServFieldBMO {


    /**
     * 添加服务领域
     * add by wuxw
     * @param govServFieldPo
     * @return
     */
    ResponseEntity<String> save(GovServFieldPo govServFieldPo);


}
