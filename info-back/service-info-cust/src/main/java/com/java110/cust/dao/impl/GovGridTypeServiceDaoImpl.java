package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovGridTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 网格类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govGridTypeServiceDaoImpl")
//@Transactional
public class GovGridTypeServiceDaoImpl extends BaseServiceDao implements IGovGridTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovGridTypeServiceDaoImpl.class);





    /**
     * 保存网格类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovGridTypeInfo(Map info) throws DAOException {
        logger.debug("保存网格类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govGridTypeServiceDaoImpl.saveGovGridTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存网格类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询网格类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovGridTypeInfo(Map info) throws DAOException {
        logger.debug("查询网格类型信息 入参 info : {}",info);

        List<Map> businessGovGridTypeInfos = sqlSessionTemplate.selectList("govGridTypeServiceDaoImpl.getGovGridTypeInfo",info);

        return businessGovGridTypeInfos;
    }


    /**
     * 修改网格类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovGridTypeInfo(Map info) throws DAOException {
        logger.debug("修改网格类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govGridTypeServiceDaoImpl.updateGovGridTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改网格类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询网格类型数量
     * @param info 网格类型信息
     * @return 网格类型数量
     */
    @Override
    public int queryGovGridTypesCount(Map info) {
        logger.debug("查询网格类型数据 入参 info : {}",info);

        List<Map> businessGovGridTypeInfos = sqlSessionTemplate.selectList("govGridTypeServiceDaoImpl.queryGovGridTypesCount", info);
        if (businessGovGridTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovGridTypeInfos.get(0).get("count").toString());
    }


}
