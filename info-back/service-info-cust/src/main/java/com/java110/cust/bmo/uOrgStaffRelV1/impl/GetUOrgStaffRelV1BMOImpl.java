package com.java110.cust.bmo.uOrgStaffRelV1.impl;

import com.java110.cust.bmo.uOrgStaffRelV1.IGetUOrgStaffRelV1BMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IUOrgStaffRelV1InnerServiceSMO;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getUOrgStaffRelV1BMOImpl")
public class GetUOrgStaffRelV1BMOImpl implements IGetUOrgStaffRelV1BMO {

    @Autowired
    private IUOrgStaffRelV1InnerServiceSMO uOrgStaffRelV1InnerServiceSMOImpl;

    /**
     *
     *
     * @param  uOrgStaffRelV1Dto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(UOrgStaffRelV1Dto uOrgStaffRelV1Dto) {


        int count = uOrgStaffRelV1InnerServiceSMOImpl.queryUOrgStaffRelV1sCount(uOrgStaffRelV1Dto);

        List<UOrgStaffRelV1Dto> uOrgStaffRelV1Dtos = null;
        if (count > 0) {
            uOrgStaffRelV1Dtos = uOrgStaffRelV1InnerServiceSMOImpl.queryUOrgStaffRelV1s(uOrgStaffRelV1Dto);
        } else {
            uOrgStaffRelV1Dtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) uOrgStaffRelV1Dto.getRow()), count, uOrgStaffRelV1Dtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
