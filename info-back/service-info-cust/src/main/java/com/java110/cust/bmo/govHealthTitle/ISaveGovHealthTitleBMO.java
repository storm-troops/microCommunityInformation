package com.java110.cust.bmo.govHealthTitle;

import com.alibaba.fastjson.JSONArray;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveGovHealthTitleBMO {


    /**
     * 添加体检项
     * add by wuxw
     * @param govHealthTitlePo
     * @return
     */
    ResponseEntity<String> save(GovHealthTitlePo govHealthTitlePo, JSONArray titleValues);


}
