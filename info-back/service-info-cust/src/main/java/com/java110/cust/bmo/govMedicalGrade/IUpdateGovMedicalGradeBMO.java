package com.java110.cust.bmo.govMedicalGrade;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalGrade.GovMedicalGradePo;

public interface IUpdateGovMedicalGradeBMO {


    /**
     * 修改医疗分级
     * add by wuxw
     * @param govMedicalGradePo
     * @return
     */
    ResponseEntity<String> update(GovMedicalGradePo govMedicalGradePo);


}
