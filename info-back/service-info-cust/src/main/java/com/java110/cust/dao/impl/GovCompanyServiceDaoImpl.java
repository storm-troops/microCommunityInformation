package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovCompanyServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 公司组织服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCompanyServiceDaoImpl")
//@Transactional
public class GovCompanyServiceDaoImpl extends BaseServiceDao implements IGovCompanyServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCompanyServiceDaoImpl.class);





    /**
     * 保存公司组织信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCompanyInfo(Map info) throws DAOException {
        logger.debug("保存公司组织信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCompanyServiceDaoImpl.saveGovCompanyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存公司组织信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询公司组织信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCompanyInfo(Map info) throws DAOException {
        logger.debug("查询公司组织信息 入参 info : {}",info);

        List<Map> businessGovCompanyInfos = sqlSessionTemplate.selectList("govCompanyServiceDaoImpl.getGovCompanyInfo",info);

        return businessGovCompanyInfos;
    }


    /**
     * 修改公司组织信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCompanyInfo(Map info) throws DAOException {
        logger.debug("修改公司组织信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCompanyServiceDaoImpl.updateGovCompanyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改公司组织信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询公司组织数量
     * @param info 公司组织信息
     * @return 公司组织数量
     */
    @Override
    public int queryGovCompanysCount(Map info) {
        logger.debug("查询公司组织数据 入参 info : {}",info);

        List<Map> businessGovCompanyInfos = sqlSessionTemplate.selectList("govCompanyServiceDaoImpl.queryGovCompanysCount", info);
        if (businessGovCompanyInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCompanyInfos.get(0).get("count").toString());
    }


}
