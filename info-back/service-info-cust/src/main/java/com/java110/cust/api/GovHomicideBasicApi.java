package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govHomicideBasic.GovHomicideBasicDto;
import com.java110.po.govHomicideBasic.GovHomicideBasicPo;
import com.java110.cust.bmo.govHomicideBasic.IDeleteGovHomicideBasicBMO;
import com.java110.cust.bmo.govHomicideBasic.IGetGovHomicideBasicBMO;
import com.java110.cust.bmo.govHomicideBasic.ISaveGovHomicideBasicBMO;
import com.java110.cust.bmo.govHomicideBasic.IUpdateGovHomicideBasicBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHomicideBasic")
public class GovHomicideBasicApi {

    @Autowired
    private ISaveGovHomicideBasicBMO saveGovHomicideBasicBMOImpl;
    @Autowired
    private IUpdateGovHomicideBasicBMO updateGovHomicideBasicBMOImpl;
    @Autowired
    private IDeleteGovHomicideBasicBMO deleteGovHomicideBasicBMOImpl;

    @Autowired
    private IGetGovHomicideBasicBMO getGovHomicideBasicBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHomicideBasic/saveGovHomicideBasic
     * @path /app/govHomicideBasic/saveGovHomicideBasic
     */
    @RequestMapping(value = "/saveGovHomicideBasic", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHomicideBasic(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "homicideNum", "请求报文中未包含homicideNum" );
        Assert.hasKeyAndValue( reqJson, "homicideName", "请求报文中未包含homicideName" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "victimId", "请求报文中未包含victimId" );
        Assert.hasKeyAndValue( reqJson, "victimName", "请求报文中未包含victimName" );
        Assert.hasKeyAndValue( reqJson, "suspicionId", "请求报文中未包含suspicionId" );
        Assert.hasKeyAndValue( reqJson, "suspicionName", "请求报文中未包含suspicionName" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );


        GovHomicideBasicPo govHomicideBasicPo = BeanConvertUtil.covertBean( reqJson, GovHomicideBasicPo.class );
        return saveGovHomicideBasicBMOImpl.save( govHomicideBasicPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHomicideBasic/updateGovHomicideBasic
     * @path /app/govHomicideBasic/updateGovHomicideBasic
     */
    @RequestMapping(value = "/updateGovHomicideBasic", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHomicideBasic(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govHomicideId", "请求报文中未包含govHomicideId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "homicideNum", "请求报文中未包含homicideNum" );
        Assert.hasKeyAndValue( reqJson, "homicideName", "请求报文中未包含homicideName" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "victimId", "请求报文中未包含victimId" );
        Assert.hasKeyAndValue( reqJson, "victimName", "请求报文中未包含victimName" );
        Assert.hasKeyAndValue( reqJson, "suspicionId", "请求报文中未包含suspicionId" );
        Assert.hasKeyAndValue( reqJson, "suspicionName", "请求报文中未包含suspicionName" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );


        GovHomicideBasicPo govHomicideBasicPo = BeanConvertUtil.covertBean( reqJson, GovHomicideBasicPo.class );
        return updateGovHomicideBasicBMOImpl.update( govHomicideBasicPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHomicideBasic/deleteGovHomicideBasic
     * @path /app/govHomicideBasic/deleteGovHomicideBasic
     */
    @RequestMapping(value = "/deleteGovHomicideBasic", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHomicideBasic(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govHomicideId", "govHomicideId不能为空" );


        GovHomicideBasicPo govHomicideBasicPo = BeanConvertUtil.covertBean( reqJson, GovHomicideBasicPo.class );
        return deleteGovHomicideBasicBMOImpl.delete( govHomicideBasicPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHomicideBasic/queryGovHomicideBasic
     * @path /app/govHomicideBasic/queryGovHomicideBasic
     */
    @RequestMapping(value = "/queryGovHomicideBasic", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHomicideBasic(@RequestParam(value = "caId",required = false) String caId,
                                                        @RequestParam(value = "homicideNum",required = false) String homicideNum,
                                                        @RequestParam(value = "govHomicideId",required = false) String govHomicideId,
                                                        @RequestParam(value = "homicideName",required = false) String homicideName,
                                                        @RequestParam(value = "suspicionId",required = false) String suspicionId,
                                                        @RequestParam(value = "victimId",required = false) String victimId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovHomicideBasicDto govHomicideBasicDto = new GovHomicideBasicDto();
        govHomicideBasicDto.setPage( page );
        govHomicideBasicDto.setRow( row );
        govHomicideBasicDto.setCaId( caId );
        govHomicideBasicDto.setHomicideNum( homicideNum );
        govHomicideBasicDto.setHomicideName( homicideName );
        govHomicideBasicDto.setGovHomicideId( govHomicideId );
        govHomicideBasicDto.setSuspicionId( suspicionId );
        govHomicideBasicDto.setVictimId( victimId );
        return getGovHomicideBasicBMOImpl.get( govHomicideBasicDto );
    }
}
