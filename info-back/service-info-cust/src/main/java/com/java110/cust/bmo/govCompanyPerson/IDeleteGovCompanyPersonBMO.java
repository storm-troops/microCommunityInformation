package com.java110.cust.bmo.govCompanyPerson;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovCompanyPersonBMO {


    /**
     * 修改小区位置
     * add by wuxw
     * @param govCompanyPersonPo
     * @return
     */
    ResponseEntity<String> delete(GovCompanyPersonPo govCompanyPersonPo);


}
