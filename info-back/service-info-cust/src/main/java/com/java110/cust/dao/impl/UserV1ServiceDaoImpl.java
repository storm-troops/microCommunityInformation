package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IUserV1ServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用戶管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("userV1ServiceDaoImpl")
//@Transactional
public class UserV1ServiceDaoImpl extends BaseServiceDao implements IUserV1ServiceDao {

    private static Logger logger = LoggerFactory.getLogger(UserV1ServiceDaoImpl.class);





    /**
     * 保存用戶管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveUserV1Info(Map info) throws DAOException {
        logger.debug("保存用戶管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("userV1ServiceDaoImpl.saveUserV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存用戶管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询用戶管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getUserV1Info(Map info) throws DAOException {
        logger.debug("查询用戶管理信息 入参 info : {}",info);

        List<Map> businessUserV1Infos = sqlSessionTemplate.selectList("userV1ServiceDaoImpl.getUserV1Info",info);

        return businessUserV1Infos;
    }


    /**
     * 修改用戶管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateUserV1Info(Map info) throws DAOException {
        logger.debug("修改用戶管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("userV1ServiceDaoImpl.updateUserV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改用戶管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询用戶管理数量
     * @param info 用戶管理信息
     * @return 用戶管理数量
     */
    @Override
    public int queryUserV1sCount(Map info) {
        logger.debug("查询用戶管理数据 入参 info : {}",info);

        List<Map> businessUserV1Infos = sqlSessionTemplate.selectList("userV1ServiceDaoImpl.queryUserV1sCount", info);
        if (businessUserV1Infos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessUserV1Infos.get(0).get("count").toString());
    }


}
