package com.java110.cust.bmo.govMedicalDoctorRel;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
public interface ISaveGovMedicalDoctorRelBMO {


    /**
     * 添加档案医生
     * add by wuxw
     * @param govMedicalDoctorRelPo
     * @return
     */
    ResponseEntity<String> save(GovMedicalDoctorRelPo govMedicalDoctorRelPo);


}
