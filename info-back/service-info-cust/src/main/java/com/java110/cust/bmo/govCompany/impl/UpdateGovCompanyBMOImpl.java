package com.java110.cust.bmo.govCompany.impl;


import com.java110.core.annotation.Java110Transactional;

import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govCompany.IUpdateGovCompanyBMO;
import com.java110.dto.govCommunityCompany.GovCommunityCompanyDto;
import com.java110.intf.assets.IGovCommunityCompanyInnerServiceSMO;
import com.java110.intf.cust.IGovCompanyInnerServiceSMO;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import com.java110.po.govCompany.GovCompanyPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("updateGovCompanyBMOImpl")
public class UpdateGovCompanyBMOImpl implements IUpdateGovCompanyBMO {

    @Autowired
    private IGovCompanyInnerServiceSMO govCompanyInnerServiceSMOImpl;

    @Autowired
    private IGovCommunityCompanyInnerServiceSMO govCommunityCompanyInnerServiceSMOImpl;
    /**
     *
     *
     * @param govCompanyPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCompanyPo govCompanyPo) {

        int flag = govCompanyInnerServiceSMOImpl.updateGovCompany(govCompanyPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存组织或企业失败");
        }
        if(!StringUtil.isEmpty(govCompanyPo.getGovCommunityId())){
            GovCommunityCompanyDto govCommunityCompanyDto = new GovCommunityCompanyDto();
            govCommunityCompanyDto.setGovCompanyId(govCompanyPo.getGovCompanyId());
            govCommunityCompanyDto.setGovCommunityId(govCompanyPo.getGovCommunityId());
            govCommunityCompanyDto.setCaId(govCompanyPo.getCaId());
            List<GovCommunityCompanyDto>  govCommunityCompanyDtos = govCommunityCompanyInnerServiceSMOImpl.queryGovCommunityCompanys(govCommunityCompanyDto);
            if(govCommunityCompanyDtos != null && govCommunityCompanyDtos.size() > 0){
                GovCommunityCompanyPo govCommunityCompanyPo = new GovCommunityCompanyPo();
                govCommunityCompanyPo.setGovOpId(govCommunityCompanyDtos.get(0).getGovOpId());
                govCommunityCompanyInnerServiceSMOImpl.deleteGovCommunityCompany(govCommunityCompanyPo);
            }
            GovCommunityCompanyPo govCommunityCompanyPo = new GovCommunityCompanyPo();
            govCommunityCompanyPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
            govCommunityCompanyPo.setGovCompanyId(govCompanyPo.getGovCompanyId());
            govCommunityCompanyPo.setGovCommunityId(govCompanyPo.getGovCommunityId());
            govCommunityCompanyPo.setCaId(govCompanyPo.getCaId());
            flag = govCommunityCompanyInnerServiceSMOImpl.saveGovCommunityCompany(govCommunityCompanyPo);
            if (flag < 1) {
                return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存组织或企业与小区关系失败");
            }
        }


        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
