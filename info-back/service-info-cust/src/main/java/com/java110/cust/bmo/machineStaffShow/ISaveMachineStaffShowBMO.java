package com.java110.cust.bmo.machineStaffShow;

import org.springframework.http.ResponseEntity;
import com.java110.po.machineStaffShow.MachineStaffShowPo;
public interface ISaveMachineStaffShowBMO {


    /**
     * 添加摄像头员工关系
     * add by wuxw
     * @param machineStaffShowPo
     * @return
     */
    ResponseEntity<String> save(MachineStaffShowPo machineStaffShowPo);


}
