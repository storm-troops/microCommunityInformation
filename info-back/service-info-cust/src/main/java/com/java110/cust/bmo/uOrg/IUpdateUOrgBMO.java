package com.java110.cust.bmo.uOrg;
import com.java110.po.uOrg.UOrgPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IUpdateUOrgBMO {


    /**
     * 修改组织管理
     * add by wuxw
     * @param uOrgPo
     * @return
     */
    ResponseEntity<String> update(UOrgPo uOrgPo);


}
