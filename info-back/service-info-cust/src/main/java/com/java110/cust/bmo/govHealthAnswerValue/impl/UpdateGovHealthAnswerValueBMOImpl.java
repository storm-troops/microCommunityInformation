package com.java110.cust.bmo.govHealthAnswerValue.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthAnswerValue.IUpdateGovHealthAnswerValueBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthAnswerValueInnerServiceSMO;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;
import java.util.List;

@Service("updateGovHealthAnswerValueBMOImpl")
public class UpdateGovHealthAnswerValueBMOImpl implements IUpdateGovHealthAnswerValueBMO {

    @Autowired
    private IGovHealthAnswerValueInnerServiceSMO govHealthAnswerValueInnerServiceSMOImpl;

    /**
     *
     *
     * @param govHealthAnswerValuePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovHealthAnswerValuePo govHealthAnswerValuePo) {

        int flag = govHealthAnswerValueInnerServiceSMOImpl.updateGovHealthAnswerValue(govHealthAnswerValuePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
