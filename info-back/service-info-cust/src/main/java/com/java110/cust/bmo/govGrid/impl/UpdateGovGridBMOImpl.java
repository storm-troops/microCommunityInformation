package com.java110.cust.bmo.govGrid.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govGrid.IUpdateGovGridBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govGrid.GovGridPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovGridInnerServiceSMO;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.store.StorePo;

import java.util.List;

@Service("updateGovGridBMOImpl")
public class UpdateGovGridBMOImpl implements IUpdateGovGridBMO {

    @Autowired
    private IGovGridInnerServiceSMO govGridInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     * @param govGridPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovGridPo govGridPo, GovPersonPo govPersonPo) {

        int flag = govGridInnerServiceSMOImpl.updateGovGrid(govGridPo);

        if (flag < 1) {
            throw new IllegalArgumentException("修改网格人员失败");
        }
        flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("修改固定人口失败");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
