package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 体检单提交者组件内部之间使用，没有给外围系统提供服务能力
 * 体检单提交者服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovHealthAnswerServiceDao {


    /**
     * 保存 体检单提交者信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovHealthAnswerInfo(Map info) throws DAOException;




    /**
     * 查询体检单提交者信息（instance过程）
     * 根据bId 查询体检单提交者信息
     * @param info bId 信息
     * @return 体检单提交者信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovHealthAnswerInfo(Map info) throws DAOException;



    /**
     * 修改体检单提交者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovHealthAnswerInfo(Map info) throws DAOException;


    /**
     * 查询体检单提交者总数
     *
     * @param info 体检单提交者信息
     * @return 体检单提交者数量
     */
    int queryGovHealthAnswersCount(Map info);

}
