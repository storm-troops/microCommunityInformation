package com.java110.cust.bmo.govGrid.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govGrid.ISaveGovGridBMO;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.dto.UserV1.UserV1Dto;
import com.java110.dto.govGridType.GovGridTypeDto;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import com.java110.intf.cust.*;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import com.java110.po.UserV1.UserV1Po;
import com.java110.po.govGrid.GovGridPo;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.po.uOrgStaffRelV1.UOrgStaffRelV1Po;
import com.java110.utils.constant.StoreUserRelConstant;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovGridBMOImpl")
public class SaveGovGridBMOImpl implements ISaveGovGridBMO {

    @Autowired
    private IGovGridInnerServiceSMO govGridInnerServiceSMOImpl;
    @Autowired
    private IUserV1InnerServiceSMO userV1InnerServiceSMOImpl;
    @Autowired
    private IStoreUserV1InnerServiceSMO storeUserV1InnerServiceSMOImpl;
    @Autowired
    private IUOrgStaffRelV1InnerServiceSMO orgStaffRelV1InnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovGridTypeInnerServiceSMO govGridTypeInnerServiceSMOImpl;
    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;
    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govGridPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovGridPo govGridPo, String storeId, GovPersonPo govPersonPo, JSONObject reqJson) {

        govGridPo.setGovGridId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govGridId));
        //先存入人口表 按照固定人口写入
        GovPersonPo govPerson = getGovPersonPo(govPersonPo);
        //保存网格人员
        govGridPo.setGovPersonId(govPerson.getGovPersonId());
        govGridPo.setDatasourceType("999999");
        int flag = govGridInnerServiceSMOImpl.saveGovGrid(govGridPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存网格人员失败");
        }
        //处理用户表
        UserV1Po userV1Po = getUserV1Po(govGridPo);
        // 员工岗位
        saveStoreUser(storeId, userV1Po);
        //处理员工角色关系
        saveOrgStaffRel(govGridPo, storeId, userV1Po);


        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private void saveOrgStaffRel(GovGridPo govGridPo, String storeId, UserV1Po userV1Po) {
        int flag;
        GovGridTypeDto govGridTypeDto = new GovGridTypeDto();
        govGridTypeDto.setCaId(govGridPo.getCaId());
        govGridTypeDto.setGovTypeId(govGridPo.getGovTypeId());
        List<GovGridTypeDto> govGridTypeDtos = govGridTypeInnerServiceSMOImpl.queryGovGridTypes(govGridTypeDto);
        if (govGridTypeDtos == null || govGridTypeDtos.size() < 0) {
            throw new IllegalArgumentException("查询网格类型失败");
        }
        UOrgStaffRelV1Po uOrgStaffRelV1Po = new UOrgStaffRelV1Po();
        uOrgStaffRelV1Po.setbId("-1");
        uOrgStaffRelV1Po.setRelCd(StoreUserRelConstant.REL_COMMON);
        uOrgStaffRelV1Po.setOrgId(govGridTypeDtos.get(0).getOrgId());
        uOrgStaffRelV1Po.setStaffId(userV1Po.getUserId());
        uOrgStaffRelV1Po.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_storeUserId));
        uOrgStaffRelV1Po.setStoreId(storeId);

        UOrgStaffRelV1Dto uOrgStaffRelV1Dto = new UOrgStaffRelV1Dto();
        uOrgStaffRelV1Dto.setOrgId(govGridTypeDtos.get(0).getOrgId());
        uOrgStaffRelV1Dto.setRelCd(StoreUserRelConstant.REL_COMMON);
        uOrgStaffRelV1Dto.setStaffId(userV1Po.getUserId());
        uOrgStaffRelV1Dto.setStoreId(storeId);
        List<UOrgStaffRelV1Dto> uOrgStaffRelV1Dtos = orgStaffRelV1InnerServiceSMOImpl.queryUOrgStaffRelV1s(uOrgStaffRelV1Dto);

        if (uOrgStaffRelV1Dtos == null || uOrgStaffRelV1Dtos.size() < 1) {
            flag = orgStaffRelV1InnerServiceSMOImpl.saveUOrgStaffRelV1(uOrgStaffRelV1Po);
            if (flag < 1) {
                throw new IllegalArgumentException("保存员工角色关系失败");
            }
        }
    }

    private void saveStoreUser(String storeId, UserV1Po userV1Po) {
        int flag;
        StoreUserV1Po storeUserV1Po = new StoreUserV1Po();
        storeUserV1Po.setbId("-1");
        storeUserV1Po.setUserId(userV1Po.getUserId());
        storeUserV1Po.setStoreId(storeId);
        storeUserV1Po.setRelCd(StoreUserRelConstant.REL_COMMON);
        storeUserV1Po.setStoreUserId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_storeUserId));

        StoreUserV1Dto storeUserV1Dto = new StoreUserV1Dto();
        storeUserV1Dto.setStoreId(storeId);
        storeUserV1Dto.setUserId(userV1Po.getUserId());
        storeUserV1Dto.setRelCd(StoreUserRelConstant.REL_COMMON);
        List<StoreUserV1Dto> storeUserV1Dtos = storeUserV1InnerServiceSMOImpl.queryStoreUserV1s(storeUserV1Dto);
        if (storeUserV1Dtos == null || storeUserV1Dtos.size() < 1) {
            flag = storeUserV1InnerServiceSMOImpl.saveStoreUserV1(storeUserV1Po);
            if (flag < 1) {
                throw new IllegalArgumentException("保存员工岗位关系失败");
            }
        }
    }


    private UserV1Po getUserV1Po(GovGridPo govGridPo) {
        int flag;
        String userId = GenerateCodeFactory.getUserId();
        //添加用户
        UserV1Po userV1Po = new UserV1Po();
        userV1Po.setUserId(userId);
        userV1Po.setAddress("无");
        userV1Po.setAge("30");
        userV1Po.setSex("0");
        userV1Po.setName(govGridPo.getPersonName());
        userV1Po.setTel(govGridPo.getPersonTel());
        userV1Po.setbId("-1");
        userV1Po.setLevelCd("01");
        userV1Po.setEmail("1000000@qq.com");
        userV1Po.setPassword("123456");
        UserV1Dto userV1Dto = new UserV1Dto();
        userV1Dto.setName(govGridPo.getPersonName());
        userV1Dto.setTel(govGridPo.getPersonTel());
        List<UserV1Dto> userV1Dtos = userV1InnerServiceSMOImpl.queryUserV1s(userV1Dto);
        if (userV1Dtos == null || userV1Dtos.size() < 1) {
            flag = userV1InnerServiceSMOImpl.saveUserV1(userV1Po);
            if (flag < 1) {
                throw new IllegalArgumentException("保存用户表失败");
            }
        } else {
            userV1Po.setUserId(userV1Dtos.get(0).getUserId());
        }
        return userV1Po;
    }

    private GovPersonPo getGovPersonPo(GovPersonPo govPersonPo) {
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govGridId));
        govPersonPo.setPersonType("2002");
        govPersonPo.setRamark("系統自动生成");
        govPersonPo.setReligiousBelief("无");
        govPersonPo.setNativePlace("中国");
        govPersonPo.setIdType("1");
        govPersonPo.setBirthday(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_B));
        govPersonPo.setDatasourceType("999999");
        govPersonPo.setPrePersonName(govPersonPo.getPersonName());
        govPersonPo.setPoliticalOutlook("5012");

        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPersonTel(govPersonPo.getPersonTel());
        govPersonDto.setPersonName(govPersonPo.getPersonName());
        govPersonDto.setCaId(govPersonPo.getCaId());
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存固定人口失败");
            }
        } else {
            govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
        }
        isGovLabelNull(GovLabelDto.LABLE_GRID_CD,"P",govPersonPo.getCaId(),"网格人员");
        isGovLabelRelNull(govPersonPo.getGovPersonId(),GovLabelDto.LABLE_GRID_CD,govPersonPo.getCaId());
        return govPersonPo;
    }
    protected void isGovLabelNull(String lableCd,String labelType,String caId,String lableName){
        //处理标签信息
        GovLabelDto govLabelDto = new GovLabelDto();
        govLabelDto.setLabelCd( lableCd);
        govLabelDto.setLabelType( labelType );
        govLabelDto.setCaId( caId );
        List<GovLabelDto> govLabelDtos = govLabelInnerServiceSMOImpl.queryGovLabels( govLabelDto );
        if (govLabelDtos == null || govLabelDtos.size() < 1) {
            GovLabelPo govLabelPo = new GovLabelPo();
            govLabelPo.setGovLabelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govLabelPo.setLabelCd( lableCd );
            govLabelPo.setLabelType( labelType );
            govLabelPo.setCaId( caId );
            govLabelPo.setLabelName( lableName );
            int flag = govLabelInnerServiceSMOImpl.saveGovLabel( govLabelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "保存"+lableName+"信息失败" );
            }
        }
    }

    protected void isGovLabelRelNull(String govPersonId,String lableCd,String caId){
        //处理人员与标签关系
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setGovPersonId( govPersonId );
        govPersonLabelRelDto.setLabelCd( lableCd );
        govPersonLabelRelDto.setCaId( caId );
        List<GovPersonLabelRelDto> govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels( govPersonLabelRelDto );
        if (govPersonLabelRelDtos == null || govPersonLabelRelDtos.size() < 1) {
            GovPersonLabelRelPo govPersonLabelRelPo = new GovPersonLabelRelPo();
            govPersonLabelRelPo.setLabelRelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govPersonLabelRelPo.setGovPersonId( govPersonId );
            govPersonLabelRelPo.setLabelCd( lableCd );
            govPersonLabelRelPo.setCaId( caId );
            int flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel( govPersonLabelRelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "打标签嫌疑人失败" );
            }
        }
    }
}
