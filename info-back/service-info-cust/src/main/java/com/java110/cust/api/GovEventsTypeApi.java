package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govEventsType.GovEventsTypeDto;
import com.java110.po.govEventsType.GovEventsTypePo;
import com.java110.cust.bmo.govEventsType.IDeleteGovEventsTypeBMO;
import com.java110.cust.bmo.govEventsType.IGetGovEventsTypeBMO;
import com.java110.cust.bmo.govEventsType.ISaveGovEventsTypeBMO;
import com.java110.cust.bmo.govEventsType.IUpdateGovEventsTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govEventsType")
public class GovEventsTypeApi {

    @Autowired
    private ISaveGovEventsTypeBMO saveGovEventsTypeBMOImpl;
    @Autowired
    private IUpdateGovEventsTypeBMO updateGovEventsTypeBMOImpl;
    @Autowired
    private IDeleteGovEventsTypeBMO deleteGovEventsTypeBMOImpl;

    @Autowired
    private IGetGovEventsTypeBMO getGovEventsTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govEventsType/saveGovEventsType
     * @path /app/govEventsType/saveGovEventsType
     */
    @RequestMapping(value = "/saveGovEventsType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovEventsType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "typeName", "请求报文中未包含typeName" );


        GovEventsTypePo govEventsTypePo = BeanConvertUtil.covertBean( reqJson, GovEventsTypePo.class );
        return saveGovEventsTypeBMOImpl.save( govEventsTypePo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govEventsType/updateGovEventsType
     * @path /app/govEventsType/updateGovEventsType
     */
    @RequestMapping(value = "/updateGovEventsType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovEventsType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "typeName", "请求报文中未包含typeName" );


        GovEventsTypePo govEventsTypePo = BeanConvertUtil.covertBean( reqJson, GovEventsTypePo.class );
        return updateGovEventsTypeBMOImpl.update( govEventsTypePo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govEventsType/deleteGovEventsType
     * @path /app/govEventsType/deleteGovEventsType
     */
    @RequestMapping(value = "/deleteGovEventsType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovEventsType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "typeId", "typeId不能为空" );


        GovEventsTypePo govEventsTypePo = BeanConvertUtil.covertBean( reqJson, GovEventsTypePo.class );
        return deleteGovEventsTypeBMOImpl.delete( govEventsTypePo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govEventsType/queryGovEventsType
     * @path /app/govEventsType/queryGovEventsType
     */
    @RequestMapping(value = "/queryGovEventsType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovEventsType(@RequestParam(value = "caId",required = false) String caId,
                                                     @RequestParam(value = "page") int page,
                                                     @RequestParam(value = "row") int row) {

        GovEventsTypeDto govEventsTypeDto = new GovEventsTypeDto();
        govEventsTypeDto.setCaId( caId );
        govEventsTypeDto.setPage( page );
        govEventsTypeDto.setRow( row );
        return getGovEventsTypeBMOImpl.get( govEventsTypeDto );
    }
}
