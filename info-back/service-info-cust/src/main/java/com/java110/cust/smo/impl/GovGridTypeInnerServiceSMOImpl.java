package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovGridTypeServiceDao;
import com.java110.dto.govGridType.GovGridTypeDto;
import com.java110.intf.cust.IGovGridTypeInnerServiceSMO;
import com.java110.po.govGridType.GovGridTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 网格类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovGridTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovGridTypeInnerServiceSMO {

    @Autowired
    private IGovGridTypeServiceDao govGridTypeServiceDaoImpl;


    @Override
    public int saveGovGridType(@RequestBody GovGridTypePo govGridTypePo) {
        int saveFlag = 1;
        govGridTypeServiceDaoImpl.saveGovGridTypeInfo(BeanConvertUtil.beanCovertMap(govGridTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovGridType(@RequestBody  GovGridTypePo govGridTypePo) {
        int saveFlag = 1;
         govGridTypeServiceDaoImpl.updateGovGridTypeInfo(BeanConvertUtil.beanCovertMap(govGridTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovGridType(@RequestBody  GovGridTypePo govGridTypePo) {
        int saveFlag = 1;
        govGridTypePo.setStatusCd("1");
        govGridTypeServiceDaoImpl.updateGovGridTypeInfo(BeanConvertUtil.beanCovertMap(govGridTypePo));
        return saveFlag;
    }

    @Override
    public List<GovGridTypeDto> queryGovGridTypes(@RequestBody  GovGridTypeDto govGridTypeDto) {

        //校验是否传了 分页信息

        int page = govGridTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govGridTypeDto.setPage((page - 1) * govGridTypeDto.getRow());
        }

        List<GovGridTypeDto> govGridTypes = BeanConvertUtil.covertBeanList(govGridTypeServiceDaoImpl.getGovGridTypeInfo(BeanConvertUtil.beanCovertMap(govGridTypeDto)), GovGridTypeDto.class);

        return govGridTypes;
    }


    @Override
    public int queryGovGridTypesCount(@RequestBody GovGridTypeDto govGridTypeDto) {
        return govGridTypeServiceDaoImpl.queryGovGridTypesCount(BeanConvertUtil.beanCovertMap(govGridTypeDto));    }

    public IGovGridTypeServiceDao getGovGridTypeServiceDaoImpl() {
        return govGridTypeServiceDaoImpl;
    }

    public void setGovGridTypeServiceDaoImpl(IGovGridTypeServiceDao govGridTypeServiceDaoImpl) {
        this.govGridTypeServiceDaoImpl = govGridTypeServiceDaoImpl;
    }
}
