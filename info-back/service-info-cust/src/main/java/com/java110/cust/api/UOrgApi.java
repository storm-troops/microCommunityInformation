package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.uOrg.IDeleteUOrgBMO;
import com.java110.cust.bmo.uOrg.IGetUOrgBMO;
import com.java110.cust.bmo.uOrg.ISaveUOrgBMO;
import com.java110.cust.bmo.uOrg.IUpdateUOrgBMO;
import com.java110.dto.uOrg.UOrgDto;
import com.java110.po.uOrg.UOrgPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/uOrg")
public class UOrgApi {

    @Autowired
    private ISaveUOrgBMO saveUOrgBMOImpl;
    @Autowired
    private IUpdateUOrgBMO updateUOrgBMOImpl;
    @Autowired
    private IDeleteUOrgBMO deleteUOrgBMOImpl;

    @Autowired
    private IGetUOrgBMO getUOrgBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrg/saveUOrg
     * @path /app/uOrg/saveUOrg
     */
    @RequestMapping(value = "/saveUOrg", method = RequestMethod.POST)
    public ResponseEntity<String> saveUOrg(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");
        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.hasKeyAndValue(reqJson, "orgLevel", "请求报文中未包含orgLevel");
        Assert.hasKeyAndValue(reqJson, "parentOrgId", "请求报文中未包含parentOrgId");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");
        Assert.hasKeyAndValue(reqJson, "belongCommunityId", "请求报文中未包含belongCommunityId");
        Assert.hasKeyAndValue(reqJson, "allowOperation", "请求报文中未包含allowOperation");


        UOrgPo uOrgPo = BeanConvertUtil.covertBean(reqJson, UOrgPo.class);
        return saveUOrgBMOImpl.save(uOrgPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrg/updateUOrg
     * @path /app/uOrg/updateUOrg
     */
    @RequestMapping(value = "/updateUOrg", method = RequestMethod.POST)
    public ResponseEntity<String> updateUOrg(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");
        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.hasKeyAndValue(reqJson, "orgLevel", "请求报文中未包含orgLevel");
        Assert.hasKeyAndValue(reqJson, "parentOrgId", "请求报文中未包含parentOrgId");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");
        Assert.hasKeyAndValue(reqJson, "belongCommunityId", "请求报文中未包含belongCommunityId");
        Assert.hasKeyAndValue(reqJson, "allowOperation", "请求报文中未包含allowOperation");
        Assert.hasKeyAndValue(reqJson, "orgId", "orgId不能为空");


        UOrgPo uOrgPo = BeanConvertUtil.covertBean(reqJson, UOrgPo.class);
        return updateUOrgBMOImpl.update(uOrgPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrg/deleteUOrg
     * @path /app/uOrg/deleteUOrg
     */
    @RequestMapping(value = "/deleteUOrg", method = RequestMethod.POST)
    public ResponseEntity<String> deleteUOrg(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "orgId", "orgId不能为空");


        UOrgPo uOrgPo = BeanConvertUtil.covertBean(reqJson, UOrgPo.class);
        return deleteUOrgBMOImpl.delete(uOrgPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /uOrg/queryUOrg
     * @path /app/uOrg/queryUOrg
     */
    @RequestMapping(value = "/queryUOrg", method = RequestMethod.GET)
    public ResponseEntity<String> queryUOrg(@RequestParam(value = "page") int page,
                                            @RequestParam(value = "row") int row) {
        UOrgDto uOrgDto = new UOrgDto();
        uOrgDto.setPage(page);
        uOrgDto.setRow(row);
        return getUOrgBMOImpl.get(uOrgDto);
    }
}
