package com.java110.cust.bmo.govHealthTerm;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govHealthTerm.GovHealthTermDto;
public interface IGetGovHealthTermBMO {


    /**
     * 查询体检项
     * add by wuxw
     * @param  govHealthTermDto
     * @return
     */
    ResponseEntity<String> get(GovHealthTermDto govHealthTermDto);


}
