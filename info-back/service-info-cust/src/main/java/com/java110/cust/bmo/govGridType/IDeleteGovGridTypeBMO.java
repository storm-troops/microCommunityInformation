package com.java110.cust.bmo.govGridType;
import com.java110.po.govGridType.GovGridTypePo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovGridTypeBMO {


    /**
     * 修改网格类型
     * add by wuxw
     * @param govGridTypePo
     * @return
     */
    ResponseEntity<String> delete(GovGridTypePo govGridTypePo);


}
