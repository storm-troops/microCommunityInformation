package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govSchoolPeripheralPerson.GovSchoolPeripheralPersonDto;
import com.java110.po.govSchoolPeripheralPerson.GovSchoolPeripheralPersonPo;
import com.java110.cust.bmo.govSchoolPeripheralPerson.IDeleteGovSchoolPeripheralPersonBMO;
import com.java110.cust.bmo.govSchoolPeripheralPerson.IGetGovSchoolPeripheralPersonBMO;
import com.java110.cust.bmo.govSchoolPeripheralPerson.ISaveGovSchoolPeripheralPersonBMO;
import com.java110.cust.bmo.govSchoolPeripheralPerson.IUpdateGovSchoolPeripheralPersonBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govSchoolPeripheralPerson")
public class GovSchoolPeripheralPersonApi {

    @Autowired
    private ISaveGovSchoolPeripheralPersonBMO saveGovSchoolPeripheralPersonBMOImpl;
    @Autowired
    private IUpdateGovSchoolPeripheralPersonBMO updateGovSchoolPeripheralPersonBMOImpl;
    @Autowired
    private IDeleteGovSchoolPeripheralPersonBMO deleteGovSchoolPeripheralPersonBMOImpl;

    @Autowired
    private IGetGovSchoolPeripheralPersonBMO getGovSchoolPeripheralPersonBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSchoolPeripheralPerson/saveGovSchoolPeripheralPerson
     * @path /app/govSchoolPeripheralPerson/saveGovSchoolPeripheralPerson
     */
    @RequestMapping(value = "/saveGovSchoolPeripheralPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovSchoolPeripheralPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "schoolId", "请求报文中未包含schoolId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "schoolName", "请求报文中未包含schoolName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "extentInjury", "请求报文中未包含extentInjury");
        Assert.hasKeyAndValue(reqJson, "isAttention", "请求报文中未包含isAttention");


        GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo = BeanConvertUtil.covertBean(reqJson, GovSchoolPeripheralPersonPo.class);
        return saveGovSchoolPeripheralPersonBMOImpl.save(govSchoolPeripheralPersonPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSchoolPeripheralPerson/updateGovSchoolPeripheralPerson
     * @path /app/govSchoolPeripheralPerson/updateGovSchoolPeripheralPerson
     */
    @RequestMapping(value = "/updateGovSchoolPeripheralPerson", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovSchoolPeripheralPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "schPersonId", "请求报文中未包含schPersonId");
        Assert.hasKeyAndValue(reqJson, "schoolId", "请求报文中未包含schoolId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "schoolName", "请求报文中未包含schoolName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "extentInjury", "请求报文中未包含extentInjury");
        Assert.hasKeyAndValue(reqJson, "isAttention", "请求报文中未包含isAttention");


        GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo = BeanConvertUtil.covertBean(reqJson, GovSchoolPeripheralPersonPo.class);
        return updateGovSchoolPeripheralPersonBMOImpl.update(govSchoolPeripheralPersonPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSchoolPeripheralPerson/deleteGovSchoolPeripheralPerson
     * @path /app/govSchoolPeripheralPerson/deleteGovSchoolPeripheralPerson
     */
    @RequestMapping(value = "/deleteGovSchoolPeripheralPerson", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovSchoolPeripheralPerson(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "schPersonId", "schPersonId不能为空");
        GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo = BeanConvertUtil.covertBean(reqJson, GovSchoolPeripheralPersonPo.class);
        return deleteGovSchoolPeripheralPersonBMOImpl.delete(govSchoolPeripheralPersonPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govSchoolPeripheralPerson/queryGovSchoolPeripheralPerson
     * @path /app/govSchoolPeripheralPerson/queryGovSchoolPeripheralPerson
     */
    @RequestMapping(value = "/queryGovSchoolPeripheralPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovSchoolPeripheralPerson(@RequestParam(value = "caId") String caId,
                                                                 @RequestParam(value = "schoolName", required = false) String schoolName,
                                                                 @RequestParam(value = "idCard", required = false) String idCard,
                                                                 @RequestParam(value = "personName", required = false) String personName,
                                                                 @RequestParam(value = "personTel", required = false) String personTel,
                                                                 @RequestParam(value = "page") int page,
                                                                 @RequestParam(value = "row") int row) {
        GovSchoolPeripheralPersonDto govSchoolPeripheralPersonDto = new GovSchoolPeripheralPersonDto();
        govSchoolPeripheralPersonDto.setPage(page);
        govSchoolPeripheralPersonDto.setRow(row);
        govSchoolPeripheralPersonDto.setSchoolName(schoolName);
        govSchoolPeripheralPersonDto.setIdCard(idCard);
        govSchoolPeripheralPersonDto.setPersonName(personName);
        govSchoolPeripheralPersonDto.setPersonTel(personTel);
        govSchoolPeripheralPersonDto.setCaId(caId);
        return getGovSchoolPeripheralPersonBMOImpl.get(govSchoolPeripheralPersonDto);
    }
}
