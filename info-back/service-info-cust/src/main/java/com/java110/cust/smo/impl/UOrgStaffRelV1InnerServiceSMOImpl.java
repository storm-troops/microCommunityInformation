package com.java110.cust.smo.impl;


import com.java110.cust.dao.IUOrgStaffRelV1ServiceDao;
import com.java110.intf.cust.IUOrgStaffRelV1InnerServiceSMO;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import com.java110.po.uOrgStaffRelV1.UOrgStaffRelV1Po;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 员工角色关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class UOrgStaffRelV1InnerServiceSMOImpl extends BaseServiceSMO implements IUOrgStaffRelV1InnerServiceSMO {

    @Autowired
    private IUOrgStaffRelV1ServiceDao uOrgStaffRelV1ServiceDaoImpl;


    @Override
    public int saveUOrgStaffRelV1(@RequestBody  UOrgStaffRelV1Po uOrgStaffRelV1Po) {
        int saveFlag = 1;
        uOrgStaffRelV1ServiceDaoImpl.saveUOrgStaffRelV1Info(BeanConvertUtil.beanCovertMap(uOrgStaffRelV1Po));
        return saveFlag;
    }

     @Override
    public int updateUOrgStaffRelV1(@RequestBody  UOrgStaffRelV1Po uOrgStaffRelV1Po) {
        int saveFlag = 1;
         uOrgStaffRelV1ServiceDaoImpl.updateUOrgStaffRelV1Info(BeanConvertUtil.beanCovertMap(uOrgStaffRelV1Po));
        return saveFlag;
    }

     @Override
    public int deleteUOrgStaffRelV1(@RequestBody  UOrgStaffRelV1Po uOrgStaffRelV1Po) {
        int saveFlag = 1;
        uOrgStaffRelV1Po.setStatusCd("1");
        uOrgStaffRelV1ServiceDaoImpl.updateUOrgStaffRelV1Info(BeanConvertUtil.beanCovertMap(uOrgStaffRelV1Po));
        return saveFlag;
    }

    @Override
    public List<UOrgStaffRelV1Dto> queryUOrgStaffRelV1s(@RequestBody  UOrgStaffRelV1Dto uOrgStaffRelV1Dto) {

        //校验是否传了 分页信息

        int page = uOrgStaffRelV1Dto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            uOrgStaffRelV1Dto.setPage((page - 1) * uOrgStaffRelV1Dto.getRow());
        }

        List<UOrgStaffRelV1Dto> uOrgStaffRelV1s = BeanConvertUtil.covertBeanList(uOrgStaffRelV1ServiceDaoImpl.getUOrgStaffRelV1Info(BeanConvertUtil.beanCovertMap(uOrgStaffRelV1Dto)), UOrgStaffRelV1Dto.class);

        return uOrgStaffRelV1s;
    }


    @Override
    public int queryUOrgStaffRelV1sCount(@RequestBody UOrgStaffRelV1Dto uOrgStaffRelV1Dto) {
        return uOrgStaffRelV1ServiceDaoImpl.queryUOrgStaffRelV1sCount(BeanConvertUtil.beanCovertMap(uOrgStaffRelV1Dto));    }

    public IUOrgStaffRelV1ServiceDao getUOrgStaffRelV1ServiceDaoImpl() {
        return uOrgStaffRelV1ServiceDaoImpl;
    }

    public void setUOrgStaffRelV1ServiceDaoImpl(IUOrgStaffRelV1ServiceDao uOrgStaffRelV1ServiceDaoImpl) {
        this.uOrgStaffRelV1ServiceDaoImpl = uOrgStaffRelV1ServiceDaoImpl;
    }
}
