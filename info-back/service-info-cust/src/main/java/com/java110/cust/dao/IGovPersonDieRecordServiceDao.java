package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 临终送别组件内部之间使用，没有给外围系统提供服务能力
 * 临终送别服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovPersonDieRecordServiceDao {


    /**
     * 保存 临终送别信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovPersonDieRecordInfo(Map info) throws DAOException;




    /**
     * 查询临终送别信息（instance过程）
     * 根据bId 查询临终送别信息
     * @param info bId 信息
     * @return 临终送别信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovPersonDieRecordInfo(Map info) throws DAOException;



    /**
     * 修改临终送别信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovPersonDieRecordInfo(Map info) throws DAOException;


    /**
     * 查询临终送别总数
     *
     * @param info 临终送别信息
     * @return 临终送别数量
     */
    int queryGovPersonDieRecordsCount(Map info);

}
