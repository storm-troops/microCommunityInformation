package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govOldPersonAttr.GovOldPersonAttrDto;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;
import com.java110.cust.bmo.govOldPersonAttr.IDeleteGovOldPersonAttrBMO;
import com.java110.cust.bmo.govOldPersonAttr.IGetGovOldPersonAttrBMO;
import com.java110.cust.bmo.govOldPersonAttr.ISaveGovOldPersonAttrBMO;
import com.java110.cust.bmo.govOldPersonAttr.IUpdateGovOldPersonAttrBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govOldPersonAttr")
public class GovOldPersonAttrApi {

    @Autowired
    private ISaveGovOldPersonAttrBMO saveGovOldPersonAttrBMOImpl;
    @Autowired
    private IUpdateGovOldPersonAttrBMO updateGovOldPersonAttrBMOImpl;
    @Autowired
    private IDeleteGovOldPersonAttrBMO deleteGovOldPersonAttrBMOImpl;

    @Autowired
    private IGetGovOldPersonAttrBMO getGovOldPersonAttrBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPersonAttr/saveGovOldPersonAttr
     * @path /app/govOldPersonAttr/saveGovOldPersonAttr
     */
    @RequestMapping(value = "/saveGovOldPersonAttr", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovOldPersonAttr(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "attrId", "请求报文中未包含attrId");
        Assert.hasKeyAndValue(reqJson, "oldId", "请求报文中未包含oldId");
        Assert.hasKeyAndValue(reqJson, "specCd", "请求报文中未包含specCd");
        Assert.hasKeyAndValue(reqJson, "value", "请求报文中未包含value");


        GovOldPersonAttrPo govOldPersonAttrPo = BeanConvertUtil.covertBean(reqJson, GovOldPersonAttrPo.class);
        return saveGovOldPersonAttrBMOImpl.save(govOldPersonAttrPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPersonAttr/updateGovOldPersonAttr
     * @path /app/govOldPersonAttr/updateGovOldPersonAttr
     */
    @RequestMapping(value = "/updateGovOldPersonAttr", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovOldPersonAttr(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "attrId", "请求报文中未包含attrId");
        Assert.hasKeyAndValue(reqJson, "oldId", "请求报文中未包含oldId");
        Assert.hasKeyAndValue(reqJson, "specCd", "请求报文中未包含specCd");
        Assert.hasKeyAndValue(reqJson, "value", "请求报文中未包含value");
        Assert.hasKeyAndValue(reqJson, "attrId", "attrId不能为空");


        GovOldPersonAttrPo govOldPersonAttrPo = BeanConvertUtil.covertBean(reqJson, GovOldPersonAttrPo.class);
        return updateGovOldPersonAttrBMOImpl.update(govOldPersonAttrPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPersonAttr/deleteGovOldPersonAttr
     * @path /app/govOldPersonAttr/deleteGovOldPersonAttr
     */
    @RequestMapping(value = "/deleteGovOldPersonAttr", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovOldPersonAttr(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "attrId", "attrId不能为空");


        GovOldPersonAttrPo govOldPersonAttrPo = BeanConvertUtil.covertBean(reqJson, GovOldPersonAttrPo.class);
        return deleteGovOldPersonAttrBMOImpl.delete(govOldPersonAttrPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govOldPersonAttr/queryGovOldPersonAttr
     * @path /app/govOldPersonAttr/queryGovOldPersonAttr
     */
    @RequestMapping(value = "/queryGovOldPersonAttr", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovOldPersonAttr(@RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovOldPersonAttrDto govOldPersonAttrDto = new GovOldPersonAttrDto();
        govOldPersonAttrDto.setPage(page);
        govOldPersonAttrDto.setRow(row);
        return getGovOldPersonAttrBMOImpl.get(govOldPersonAttrDto);
    }
}
