package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHealthAnswerValueServiceDao;
import com.java110.intf.cust.IGovHealthAnswerValueInnerServiceSMO;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 体检项目答案内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHealthAnswerValueInnerServiceSMOImpl extends BaseServiceSMO implements IGovHealthAnswerValueInnerServiceSMO {

    @Autowired
    private IGovHealthAnswerValueServiceDao govHealthAnswerValueServiceDaoImpl;


    @Override
    public int saveGovHealthAnswerValue(@RequestBody  GovHealthAnswerValuePo govHealthAnswerValuePo) {
        int saveFlag = 1;
        govHealthAnswerValueServiceDaoImpl.saveGovHealthAnswerValueInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerValuePo));
        return saveFlag;
    }

     @Override
    public int updateGovHealthAnswerValue(@RequestBody  GovHealthAnswerValuePo govHealthAnswerValuePo) {
        int saveFlag = 1;
         govHealthAnswerValueServiceDaoImpl.updateGovHealthAnswerValueInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerValuePo));
        return saveFlag;
    }

     @Override
    public int deleteGovHealthAnswerValue(@RequestBody  GovHealthAnswerValuePo govHealthAnswerValuePo) {
        int saveFlag = 1;
        govHealthAnswerValuePo.setStatusCd("1");
        govHealthAnswerValueServiceDaoImpl.updateGovHealthAnswerValueInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerValuePo));
        return saveFlag;
    }

    @Override
    public List<GovHealthAnswerValueDto> queryGovHealthAnswerValues(@RequestBody  GovHealthAnswerValueDto govHealthAnswerValueDto) {

        //校验是否传了 分页信息

        int page = govHealthAnswerValueDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHealthAnswerValueDto.setPage((page - 1) * govHealthAnswerValueDto.getRow());
        }

        List<GovHealthAnswerValueDto> govHealthAnswerValues = BeanConvertUtil.covertBeanList(govHealthAnswerValueServiceDaoImpl.getGovHealthAnswerValueInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerValueDto)), GovHealthAnswerValueDto.class);

        return govHealthAnswerValues;
    }


    @Override
    public int queryGovHealthAnswerValuesCount(@RequestBody GovHealthAnswerValueDto govHealthAnswerValueDto) {
        return govHealthAnswerValueServiceDaoImpl.queryGovHealthAnswerValuesCount(BeanConvertUtil.beanCovertMap(govHealthAnswerValueDto));    }

    public IGovHealthAnswerValueServiceDao getGovHealthAnswerValueServiceDaoImpl() {
        return govHealthAnswerValueServiceDaoImpl;
    }

    public void setGovHealthAnswerValueServiceDaoImpl(IGovHealthAnswerValueServiceDao govHealthAnswerValueServiceDaoImpl) {
        this.govHealthAnswerValueServiceDaoImpl = govHealthAnswerValueServiceDaoImpl;
    }
}
