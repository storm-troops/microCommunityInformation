package com.java110.cust.bmo.govOwner.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govOwner.IUpdateGovOwnerBMO;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("updateGovOwnerBMOImpl")
public class UpdateGovOwnerBMOImpl implements IUpdateGovOwnerBMO {

    @Autowired
    private IGovOwnerInnerServiceSMO govOwnerInnerServiceSMOImpl;
    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    /**
     *
     *
     * @param govOwnerPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovOwnerPo govOwnerPo) {

        int flag = govOwnerInnerServiceSMOImpl.updateGovOwner(govOwnerPo);

        if (flag > 0) {
            GovOwnerPersonDto govOwnerPersonDto = new GovOwnerPersonDto();
            govOwnerPersonDto.setGovOwnerId( govOwnerPo.getGovOwnerId() );
            List<GovOwnerPersonDto> govOwnerPersonDtos = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersons( govOwnerPersonDto );
            if(null != govOwnerPersonDtos && govOwnerPersonDtos.size() > 0){
                GovOwnerPersonDto govOwnerPerson = govOwnerPersonDtos.get( 0 );
                //修改户籍人口信息到人口表
                GovPersonPo govPersonPo = new GovPersonPo();
                govPersonPo.setGovPersonId( govOwnerPerson.getGovPersonId() );
                govPersonPo.setCaId( govOwnerPo.getCaId() );
                govPersonPo.setIdCard( govOwnerPo.getIdCard() );
                govPersonPo.setPersonName( govOwnerPo.getOwnerName() );
                govPersonPo.setPersonTel( govOwnerPo.getOwnerTel() );
                govPersonInnerServiceSMOImpl.updateGovPerson( govPersonPo );
            }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
