package com.java110.cust.bmo.govPersonDie;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPersonDie.GovPersonDiePo;
public interface ISaveGovPersonDieBMO {


    /**
     * 添加死亡登记
     * add by wuxw
     * @param govPersonDiePo
     * @return
     */
    ResponseEntity<String> save(GovPersonDiePo govPersonDiePo);

    ResponseEntity<String> savePhone(GovPersonDiePo govPersonDiePo,JSONArray photos);
}
