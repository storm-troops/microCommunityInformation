package com.java110.cust.bmo.govOldPerson.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govOldPerson.ISaveGovOldPersonBMO;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.intf.cust.IGovOldPersonAttrInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.constant.MallConstant;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.intf.cust.IGovOldPersonInnerServiceSMO;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovOldPersonBMOImpl")
public class SaveGovOldPersonBMOImpl implements ISaveGovOldPersonBMO {

    @Autowired
    private IGovOldPersonInnerServiceSMO govOldPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovOldPersonAttrInnerServiceSMO govOldPersonAttrInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * 添加小区信息
     *
     * @param govOldPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovOldPersonPo govOldPersonPo, GovPersonPo govPersonPo, JSONObject reqJson) {

        govOldPersonPo.setOldId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_oldId));
        GovPersonPo govPerson= getGovPersonPo(govPersonPo);
        govOldPersonPo.setDatasourceType("999999");
        govOldPersonPo.setGovPersonId(govPerson.getGovPersonId());
        int flag = govOldPersonInnerServiceSMOImpl.saveGovOldPerson(govOldPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存失败");
        }
        getGovPersonAttrPo(govOldPersonPo);

        reqJson.put("oldId",govOldPersonPo.getOldId());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.ADD_OLD_PERSON_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException( responseEntity.getBody());
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private GovPersonPo getGovPersonPo(GovPersonPo govPersonPo) {
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govGridId));
        govPersonPo.setPersonType("2002");
        govPersonPo.setRamark("系統自动生成");
        govPersonPo.setReligiousBelief("无");
        govPersonPo.setNativePlace("中国");
        govPersonPo.setIdType("1");
        govPersonPo.setDatasourceType("999999");
        govPersonPo.setPrePersonName(govPersonPo.getPersonName());
        govPersonPo.setPoliticalOutlook("5012");

        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPersonTel(govPersonPo.getPersonTel());
        govPersonDto.setPersonName(govPersonPo.getPersonName());
        govPersonDto.setCaId(govPersonPo.getCaId());
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存固定人口失败");
            }
        } else {
            govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
            int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改固定人口失败");
            }
        }
        return govPersonPo;
    }

    private void getGovPersonAttrPo(GovOldPersonPo govOldPersonPo) {
        GovOldPersonAttrPo govOldPersonAttrPo = new GovOldPersonAttrPo();
        govOldPersonAttrPo.setAttrId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_attrId));
        govOldPersonAttrPo.setOldId(govOldPersonPo.getOldId());
        govOldPersonAttrPo.setSpecCd(GovOldPersonAttrPo.OLD_TIME_AMOUNT);
        govOldPersonAttrPo.setValue(govOldPersonPo.getTimeAmount());
        int flag = govOldPersonAttrInnerServiceSMOImpl.saveGovOldPersonAttr(govOldPersonAttrPo);

        if (flag < 1) {
            throw new IllegalArgumentException("保存老人初始化时间金额失败");
        }
    }

}
