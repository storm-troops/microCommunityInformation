package com.java110.cust.bmo.govHealthTitle;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetGovHealthTitleBMO {


    /**
     * 查询体检项
     * add by wuxw
     * @param  govHealthTitleDto
     * @return
     */
    ResponseEntity<String> get(GovHealthTitleDto govHealthTitleDto);
    /**
     * 查询体检项
     * add by wuxw
     * @param  govHealthTitleDto
     * @return
     */
    ResponseEntity<String> getTermInfo(GovHealthTitleDto govHealthTitleDto);


}
