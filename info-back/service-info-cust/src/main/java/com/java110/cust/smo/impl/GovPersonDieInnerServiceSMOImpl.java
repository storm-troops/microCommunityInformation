package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovPersonDieServiceDao;
import com.java110.intf.cust.IGovPersonDieInnerServiceSMO;
import com.java110.dto.govPersonDie.GovPersonDieDto;
import com.java110.po.govPersonDie.GovPersonDiePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 死亡登记内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPersonDieInnerServiceSMOImpl extends BaseServiceSMO implements IGovPersonDieInnerServiceSMO {

    @Autowired
    private IGovPersonDieServiceDao govPersonDieServiceDaoImpl;


    @Override
    public int saveGovPersonDie(@RequestBody  GovPersonDiePo govPersonDiePo) {
        int saveFlag = 1;
        govPersonDieServiceDaoImpl.saveGovPersonDieInfo(BeanConvertUtil.beanCovertMap(govPersonDiePo));
        return saveFlag;
    }

     @Override
    public int updateGovPersonDie(@RequestBody  GovPersonDiePo govPersonDiePo) {
        int saveFlag = 1;
         govPersonDieServiceDaoImpl.updateGovPersonDieInfo(BeanConvertUtil.beanCovertMap(govPersonDiePo));
        return saveFlag;
    }

     @Override
    public int deleteGovPersonDie(@RequestBody  GovPersonDiePo govPersonDiePo) {
        int saveFlag = 1;
        govPersonDiePo.setStatusCd("1");
        govPersonDieServiceDaoImpl.updateGovPersonDieInfo(BeanConvertUtil.beanCovertMap(govPersonDiePo));
        return saveFlag;
    }

    @Override
    public List<GovPersonDieDto> queryGovPersonDies(@RequestBody  GovPersonDieDto govPersonDieDto) {

        //校验是否传了 分页信息

        int page = govPersonDieDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonDieDto.setPage((page - 1) * govPersonDieDto.getRow());
        }

        List<GovPersonDieDto> govPersonDies = BeanConvertUtil.covertBeanList(govPersonDieServiceDaoImpl.getGovPersonDieInfo(BeanConvertUtil.beanCovertMap(govPersonDieDto)), GovPersonDieDto.class);

        return govPersonDies;
    }


    @Override
    public int queryGovPersonDiesCount(@RequestBody GovPersonDieDto govPersonDieDto) {
        return govPersonDieServiceDaoImpl.queryGovPersonDiesCount(BeanConvertUtil.beanCovertMap(govPersonDieDto));    }

    public IGovPersonDieServiceDao getGovPersonDieServiceDaoImpl() {
        return govPersonDieServiceDaoImpl;
    }

    public void setGovPersonDieServiceDaoImpl(IGovPersonDieServiceDao govPersonDieServiceDaoImpl) {
        this.govPersonDieServiceDaoImpl = govPersonDieServiceDaoImpl;
    }
}
