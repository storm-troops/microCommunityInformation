package com.java110.cust.bmo.govGridType.impl;


import com.java110.cust.bmo.govGridType.IGetGovGridTypeBMO;
import com.java110.intf.cust.IGovGridTypeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govGridType.GovGridTypeDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovGridTypeBMOImpl")
public class GetGovGridTypeBMOImpl implements IGetGovGridTypeBMO {

    @Autowired
    private IGovGridTypeInnerServiceSMO govGridTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govGridTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovGridTypeDto govGridTypeDto) {


        int count = govGridTypeInnerServiceSMOImpl.queryGovGridTypesCount(govGridTypeDto);

        List<GovGridTypeDto> govGridTypeDtos = null;
        if (count > 0) {
            govGridTypeDtos = govGridTypeInnerServiceSMOImpl.queryGovGridTypes(govGridTypeDto);
        } else {
            govGridTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govGridTypeDto.getRow()), count, govGridTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
