package com.java110.cust.bmo.govHealthAnswerValue;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
public interface IGetGovHealthAnswerValueBMO {


    /**
     * 查询体检项目答案
     * add by wuxw
     * @param  govHealthAnswerValueDto
     * @return
     */
    ResponseEntity<String> get(GovHealthAnswerValueDto govHealthAnswerValueDto);


}
