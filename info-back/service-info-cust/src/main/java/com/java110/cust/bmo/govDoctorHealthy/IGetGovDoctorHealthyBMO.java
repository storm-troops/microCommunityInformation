package com.java110.cust.bmo.govDoctorHealthy;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govDoctorHealthy.GovDoctorHealthyDto;
public interface IGetGovDoctorHealthyBMO {


    /**
     * 查询档案医生
     * add by wuxw
     * @param  govDoctorHealthyDto
     * @return
     */
    ResponseEntity<String> get(GovDoctorHealthyDto govDoctorHealthyDto);


}
