package com.java110.cust.bmo.machineStaffShow;
import org.springframework.http.ResponseEntity;
import com.java110.dto.machineStaffShow.MachineStaffShowDto;
public interface IGetMachineStaffShowBMO {


    /**
     * 查询摄像头员工关系
     * add by wuxw
     * @param  machineStaffShowDto
     * @return
     */
    ResponseEntity<String> get(MachineStaffShowDto machineStaffShowDto);


}
