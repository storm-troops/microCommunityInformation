package com.java110.cust.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMajorEvents.GovMajorEventsDto;
import com.java110.po.govMajorEvents.GovMajorEventsPo;
import com.java110.cust.bmo.govMajorEvents.IDeleteGovMajorEventsBMO;
import com.java110.cust.bmo.govMajorEvents.IGetGovMajorEventsBMO;
import com.java110.cust.bmo.govMajorEvents.ISaveGovMajorEventsBMO;
import com.java110.cust.bmo.govMajorEvents.IUpdateGovMajorEventsBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMajorEvents")
public class GovMajorEventsApi {

    @Autowired
    private ISaveGovMajorEventsBMO saveGovMajorEventsBMOImpl;
    @Autowired
    private IUpdateGovMajorEventsBMO updateGovMajorEventsBMOImpl;
    @Autowired
    private IDeleteGovMajorEventsBMO deleteGovMajorEventsBMOImpl;

    @Autowired
    private IGetGovMajorEventsBMO getGovMajorEventsBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMajorEvents/saveGovMajorEvents
     * @path /app/govMajorEvents/saveGovMajorEvents
     */
    @RequestMapping(value = "/saveGovMajorEvents", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMajorEvents(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "eventsName", "请求报文中未包含eventsName" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "levelCd", "请求报文中未包含levelCd" );
        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "areaCode", "请求报文中未包含areaCode" );


        GovMajorEventsPo govMajorEventsPo = BeanConvertUtil.covertBean( reqJson, GovMajorEventsPo.class );
        return saveGovMajorEventsBMOImpl.save( govMajorEventsPo );
    }

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMajorEvents/savePhoneGovMajorEvents
     * @path /app/govMajorEvents/savePhoneGovMajorEvents
     */
    @RequestMapping(value = "/savePhoneGovMajorEvents", method = RequestMethod.POST)
    public ResponseEntity<String> savePhoneGovMajorEvents(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "eventsName", "请求报文中未包含eventsName" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "levelCd", "请求报文中未包含levelCd" );
        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "areaCode", "请求报文中未包含areaCode" );


        GovMajorEventsPo govMajorEventsPo = BeanConvertUtil.covertBean( reqJson, GovMajorEventsPo.class );


        JSONArray photos = reqJson.getJSONArray("photos");
        return saveGovMajorEventsBMOImpl.savePhone(govMajorEventsPo,photos );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMajorEvents/updateGovMajorEvents
     * @path /app/govMajorEvents/updateGovMajorEvents
     */
    @RequestMapping(value = "/updateGovMajorEvents", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMajorEvents(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govEventsId", "请求报文中未包含govEventsId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "eventsName", "请求报文中未包含eventsName" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "levelCd", "请求报文中未包含levelCd" );
        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "areaCode", "请求报文中未包含areaCode" );


        GovMajorEventsPo govMajorEventsPo = BeanConvertUtil.covertBean( reqJson, GovMajorEventsPo.class );
        return updateGovMajorEventsBMOImpl.update( govMajorEventsPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMajorEvents/deleteGovMajorEvents
     * @path /app/govMajorEvents/deleteGovMajorEvents
     */
    @RequestMapping(value = "/deleteGovMajorEvents", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMajorEvents(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govEventsId", "govEventsId不能为空" );


        GovMajorEventsPo govMajorEventsPo = BeanConvertUtil.covertBean( reqJson, GovMajorEventsPo.class );
        return deleteGovMajorEventsBMOImpl.delete( govMajorEventsPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govMajorEvents/queryGovMajorEvents
     * @path /app/govMajorEvents/queryGovMajorEvents
     */
    @RequestMapping(value = "/queryGovMajorEvents", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMajorEvents(@RequestParam(value = "caId",required = false) String caId,
                                                      @RequestParam(value = "eventsName",required = false) String eventsName,
                                                      @RequestParam(value = "levelCd",required = false) String levelCd,
                                                      @RequestParam(value = "typeId",required = false) String typeId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {

        GovMajorEventsDto govMajorEventsDto = new GovMajorEventsDto();
        govMajorEventsDto.setPage( page );
        govMajorEventsDto.setRow( row );
        govMajorEventsDto.setCaId( caId );
        govMajorEventsDto.setEventsName( eventsName );
        govMajorEventsDto.setLevelCd( levelCd );
        govMajorEventsDto.setTypeId( typeId );
        return getGovMajorEventsBMOImpl.get( govMajorEventsDto );
    }
}
