package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovSymptomTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 症状类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govSymptomTypeServiceDaoImpl")
//@Transactional
public class GovSymptomTypeServiceDaoImpl extends BaseServiceDao implements IGovSymptomTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovSymptomTypeServiceDaoImpl.class);





    /**
     * 保存症状类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovSymptomTypeInfo(Map info) throws DAOException {
        logger.debug("保存症状类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govSymptomTypeServiceDaoImpl.saveGovSymptomTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存症状类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询症状类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovSymptomTypeInfo(Map info) throws DAOException {
        logger.debug("查询症状类型信息 入参 info : {}",info);

        List<Map> businessGovSymptomTypeInfos = sqlSessionTemplate.selectList("govSymptomTypeServiceDaoImpl.getGovSymptomTypeInfo",info);

        return businessGovSymptomTypeInfos;
    }


    /**
     * 修改症状类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovSymptomTypeInfo(Map info) throws DAOException {
        logger.debug("修改症状类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govSymptomTypeServiceDaoImpl.updateGovSymptomTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改症状类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询症状类型数量
     * @param info 症状类型信息
     * @return 症状类型数量
     */
    @Override
    public int queryGovSymptomTypesCount(Map info) {
        logger.debug("查询症状类型数据 入参 info : {}",info);

        List<Map> businessGovSymptomTypeInfos = sqlSessionTemplate.selectList("govSymptomTypeServiceDaoImpl.queryGovSymptomTypesCount", info);
        if (businessGovSymptomTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovSymptomTypeInfos.get(0).get("count").toString());
    }


}
