package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govOwnerPerson.IDeleteGovOwnerPersonBMO;
import com.java110.cust.bmo.govOwnerPerson.IGetGovOwnerPersonBMO;
import com.java110.cust.bmo.govOwnerPerson.ISaveGovOwnerPersonBMO;
import com.java110.cust.bmo.govOwnerPerson.IUpdateGovOwnerPersonBMO;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govOwnerPerson")
public class GovOwnerPersonApi {

    @Autowired
    private ISaveGovOwnerPersonBMO saveGovOwnerPersonBMOImpl;
    @Autowired
    private IUpdateGovOwnerPersonBMO updateGovOwnerPersonBMOImpl;
    @Autowired
    private IDeleteGovOwnerPersonBMO deleteGovOwnerPersonBMOImpl;

    @Autowired
    private IGetGovOwnerPersonBMO getGovOwnerPersonBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govOwnerPerson/saveGovOwnerPerson
     * @path /app/govOwnerPerson/saveGovOwnerPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovOwnerPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovOwnerPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govOwnerId", "请求报文中未包含govOwnerId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");


        GovOwnerPersonPo govOwnerPersonPo = BeanConvertUtil.covertBean(reqJson, GovOwnerPersonPo.class);
        return saveGovOwnerPersonBMOImpl.save(govOwnerPersonPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govOwnerPerson/updateGovOwnerPerson
     * @path /app/govOwnerPerson/updateGovOwnerPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovOwnerPerson", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovOwnerPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govOwnerId", "请求报文中未包含govOwnerId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "govOpId", "govOpId不能为空");


        GovOwnerPersonPo govOwnerPersonPo = BeanConvertUtil.covertBean(reqJson, GovOwnerPersonPo.class);
        return updateGovOwnerPersonBMOImpl.update(govOwnerPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govOwnerPerson/deleteGovOwnerPerson
     * @path /app/govOwnerPerson/deleteGovOwnerPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovOwnerPerson", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovOwnerPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govOpId", "govOpId不能为空");


        GovOwnerPersonPo govOwnerPersonPo = BeanConvertUtil.covertBean(reqJson, GovOwnerPersonPo.class);
        return deleteGovOwnerPersonBMOImpl.delete(govOwnerPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govOwnerPerson/queryGovOwnerPerson
     * @path /app/govOwnerPerson/queryGovOwnerPerson
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovOwnerPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovOwnerPerson(@RequestParam(value = "govPersonId" , required = false) String govPersonId,
                                                      @RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "govOpId" , required = false) String govOpId,
                                                      @RequestParam(value = "govOwnerId" , required = false) String govOwnerId,
                                                      @RequestParam(value = "relCd" , required = false) String relCd,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovOwnerPersonDto govOwnerPersonDto = new GovOwnerPersonDto();
        govOwnerPersonDto.setPage(page);
        govOwnerPersonDto.setRow(row);
        govOwnerPersonDto.setGovPersonId(govPersonId);
        govOwnerPersonDto.setCaId(caId);
        govOwnerPersonDto.setGovOpId(govOpId);
        govOwnerPersonDto.setGovOwnerId(govOwnerId);
        govOwnerPersonDto.setRelCd(relCd);
        return getGovOwnerPersonBMOImpl.get(govOwnerPersonDto);
    }
}
