package com.java110.cust.bmo.govPersonDie;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govPersonDie.GovPersonDieDto;
public interface IGetGovPersonDieBMO {


    /**
     * 查询死亡登记
     * add by wuxw
     * @param  govPersonDieDto
     * @return
     */
    ResponseEntity<String> get(GovPersonDieDto govPersonDieDto);


}
