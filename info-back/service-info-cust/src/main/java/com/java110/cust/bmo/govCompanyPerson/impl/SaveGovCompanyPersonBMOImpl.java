package com.java110.cust.bmo.govCompanyPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govCompanyPerson.ISaveGovCompanyPersonBMO;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovCompanyPersonBMOImpl")
public class SaveGovCompanyPersonBMOImpl implements ISaveGovCompanyPersonBMO {

    @Autowired
    private IGovCompanyPersonInnerServiceSMO govCompanyPersonInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govCompanyPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCompanyPersonPo govCompanyPersonPo) {

        govCompanyPersonPo.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_relId));
        int flag = govCompanyPersonInnerServiceSMOImpl.saveGovCompanyPerson(govCompanyPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
