package com.java110.assets.bmo.cityArea;
import org.springframework.http.ResponseEntity;
import com.java110.po.cityArea.CityAreaPo;

public interface IDeleteCityAreaBMO {


    /**
     * 修改选择省市区
     * add by wuxw
     * @param cityAreaPo
     * @return
     */
    ResponseEntity<String> delete(CityAreaPo cityAreaPo);


}
