package com.java110.assets.bmo.machineType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.machineType.IDeleteMachineTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import com.java110.po.machineType.MachineTypePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.assets.IMachineTypeInnerServiceSMO;
import com.java110.dto.machineType.MachineTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteMachineTypeBMOImpl")
public class DeleteMachineTypeBMOImpl implements IDeleteMachineTypeBMO {

    @Autowired
    private IMachineTypeInnerServiceSMO machineTypeInnerServiceSMOImpl;

    /**
     * @param machineTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(MachineTypePo machineTypePo) {

        int flag = machineTypeInnerServiceSMOImpl.deleteMachineType(machineTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
