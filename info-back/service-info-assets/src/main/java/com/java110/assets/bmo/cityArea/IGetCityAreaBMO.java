package com.java110.assets.bmo.cityArea;
import org.springframework.http.ResponseEntity;
import com.java110.dto.cityArea.CityAreaDto;
public interface IGetCityAreaBMO {


    /**
     * 查询选择省市区
     * add by wuxw
     * @param  cityAreaDto
     * @return
     */
    ResponseEntity<String> get(CityAreaDto cityAreaDto);

    ResponseEntity<String> getAreas(CityAreaDto cityAreaDto);
}
