package com.java110.assets.bmo.govCommunity;

import com.alibaba.fastjson.JSONObject;
import com.java110.po.govCommunity.GovCommunityPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCommunityBMO {


    /**
     * 添加小区信息
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> save(JSONObject reqJson);


}
