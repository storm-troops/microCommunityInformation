package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovCommunityAttrServiceDao;
import com.java110.intf.assets.IGovCommunityAttrInnerServiceSMO;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.po.govCommunityAttr.GovCommunityAttrPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 小区属性内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCommunityAttrInnerServiceSMOImpl extends BaseServiceSMO implements IGovCommunityAttrInnerServiceSMO {

    @Autowired
    private IGovCommunityAttrServiceDao govCommunityAttrServiceDaoImpl;


    @Override
    public int saveGovCommunityAttr(@RequestBody  GovCommunityAttrPo govCommunityAttrPo) {
        int saveFlag = 1;
        govCommunityAttrServiceDaoImpl.saveGovCommunityAttrInfo(BeanConvertUtil.beanCovertMap(govCommunityAttrPo));
        return saveFlag;
    }

     @Override
    public int updateGovCommunityAttr(@RequestBody  GovCommunityAttrPo govCommunityAttrPo) {
        int saveFlag = 1;
         govCommunityAttrServiceDaoImpl.updateGovCommunityAttrInfo(BeanConvertUtil.beanCovertMap(govCommunityAttrPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCommunityAttr(@RequestBody  GovCommunityAttrPo govCommunityAttrPo) {
        int saveFlag = 1;
        govCommunityAttrPo.setStatusCd("1");
        govCommunityAttrServiceDaoImpl.updateGovCommunityAttrInfo(BeanConvertUtil.beanCovertMap(govCommunityAttrPo));
        return saveFlag;
    }

    @Override
    public List<GovCommunityAttrDto> queryGovCommunityAttrs(@RequestBody  GovCommunityAttrDto govCommunityAttrDto) {

        //校验是否传了 分页信息

        int page = govCommunityAttrDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityAttrDto.setPage((page - 1) * govCommunityAttrDto.getRow());
        }

        List<GovCommunityAttrDto> govCommunityAttrs = BeanConvertUtil.covertBeanList(govCommunityAttrServiceDaoImpl.getGovCommunityAttrInfo(BeanConvertUtil.beanCovertMap(govCommunityAttrDto)), GovCommunityAttrDto.class);

        return govCommunityAttrs;
    }


    @Override
    public int queryGovCommunityAttrsCount(@RequestBody GovCommunityAttrDto govCommunityAttrDto) {
        return govCommunityAttrServiceDaoImpl.queryGovCommunityAttrsCount(BeanConvertUtil.beanCovertMap(govCommunityAttrDto));    }

    public IGovCommunityAttrServiceDao getGovCommunityAttrServiceDaoImpl() {
        return govCommunityAttrServiceDaoImpl;
    }

    public void setGovCommunityAttrServiceDaoImpl(IGovCommunityAttrServiceDao govCommunityAttrServiceDaoImpl) {
        this.govCommunityAttrServiceDaoImpl = govCommunityAttrServiceDaoImpl;
    }
}
