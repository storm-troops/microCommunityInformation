package com.java110.assets.dao;


import com.java110.utils.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * 停車場组件内部之间使用，没有给外围系统提供服务能力
 * 停車場服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 * <p>
 * Created by wuxw on 2016/12/27.
 */
public interface IParkingAreaServiceDao {


    /**
     * 保存 停車場信息
     *
     * @param info
     * @throws DAOException DAO异常
     */
    void saveParkingAreaInfo(Map info) throws DAOException;


    /**
     * 查询停車場信息（instance过程）
     * 根据bId 查询停車場信息
     *
     * @param info bId 信息
     * @return 停車場信息
     * @throws DAOException DAO异常
     */
    List<Map> getParkingAreaInfo(Map info) throws DAOException;


    /**
     * 修改停車場信息
     *
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateParkingAreaInfo(Map info) throws DAOException;


    /**
     * 查询停車場总数
     *
     * @param info 停車場信息
     * @return 停車場数量
     */
    int queryParkingAreasCount(Map info);

}
