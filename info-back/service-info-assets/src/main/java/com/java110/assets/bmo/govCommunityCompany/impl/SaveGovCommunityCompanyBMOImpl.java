package com.java110.assets.bmo.govCommunityCompany.impl;

import com.java110.assets.bmo.govCommunityCompany.ISaveGovCommunityCompanyBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IGovCommunityCompanyInnerServiceSMO;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovCommunityCompanyBMOImpl")
public class SaveGovCommunityCompanyBMOImpl implements ISaveGovCommunityCompanyBMO {

    @Autowired
    private IGovCommunityCompanyInnerServiceSMO govCommunityCompanyInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govCommunityCompanyPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCommunityCompanyPo govCommunityCompanyPo) {

        govCommunityCompanyPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
        int flag = govCommunityCompanyInnerServiceSMOImpl.saveGovCommunityCompany(govCommunityCompanyPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
