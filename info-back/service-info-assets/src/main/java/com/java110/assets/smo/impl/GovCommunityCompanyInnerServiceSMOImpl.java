package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovCommunityCompanyServiceDao;
import com.java110.dto.govCommunityCompany.GovCommunityCompanyDto;
import com.java110.intf.assets.IGovCommunityCompanyInnerServiceSMO;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 小区位置内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCommunityCompanyInnerServiceSMOImpl extends BaseServiceSMO implements IGovCommunityCompanyInnerServiceSMO {

    @Autowired
    private IGovCommunityCompanyServiceDao govCommunityCompanyServiceDaoImpl;


    @Override
    public int saveGovCommunityCompany(@RequestBody GovCommunityCompanyPo govCommunityCompanyPo) {
        int saveFlag = 1;
        govCommunityCompanyServiceDaoImpl.saveGovCommunityCompanyInfo(BeanConvertUtil.beanCovertMap(govCommunityCompanyPo));
        return saveFlag;
    }

     @Override
    public int updateGovCommunityCompany(@RequestBody  GovCommunityCompanyPo govCommunityCompanyPo) {
        int saveFlag = 1;
         govCommunityCompanyServiceDaoImpl.updateGovCommunityCompanyInfo(BeanConvertUtil.beanCovertMap(govCommunityCompanyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCommunityCompany(@RequestBody  GovCommunityCompanyPo govCommunityCompanyPo) {
        int saveFlag = 1;
        govCommunityCompanyPo.setStatusCd("1");
        govCommunityCompanyServiceDaoImpl.updateGovCommunityCompanyInfo(BeanConvertUtil.beanCovertMap(govCommunityCompanyPo));
        return saveFlag;
    }

    @Override
    public List<GovCommunityCompanyDto> queryGovCommunityCompanys(@RequestBody  GovCommunityCompanyDto govCommunityCompanyDto) {

        //校验是否传了 分页信息

        int page = govCommunityCompanyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityCompanyDto.setPage((page - 1) * govCommunityCompanyDto.getRow());
        }

        List<GovCommunityCompanyDto> govCommunityCompanys = BeanConvertUtil.covertBeanList(govCommunityCompanyServiceDaoImpl.getGovCommunityCompanyInfo(BeanConvertUtil.beanCovertMap(govCommunityCompanyDto)), GovCommunityCompanyDto.class);

        return govCommunityCompanys;
    }


    @Override
    public int queryGovCommunityCompanysCount(@RequestBody GovCommunityCompanyDto govCommunityCompanyDto) {
        return govCommunityCompanyServiceDaoImpl.queryGovCommunityCompanysCount(BeanConvertUtil.beanCovertMap(govCommunityCompanyDto));    }

    public IGovCommunityCompanyServiceDao getGovCommunityCompanyServiceDaoImpl() {
        return govCommunityCompanyServiceDaoImpl;
    }

    public void setGovCommunityCompanyServiceDaoImpl(IGovCommunityCompanyServiceDao govCommunityCompanyServiceDaoImpl) {
        this.govCommunityCompanyServiceDaoImpl = govCommunityCompanyServiceDaoImpl;
    }
}
