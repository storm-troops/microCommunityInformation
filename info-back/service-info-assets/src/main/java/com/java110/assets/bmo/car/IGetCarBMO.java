package com.java110.assets.bmo.car;
import com.java110.dto.car.CarDto;
import org.springframework.http.ResponseEntity;
public interface IGetCarBMO {


    /**
     * 查询车辆
     * add by wuxw
     * @param  carDto
     * @return
     */
    ResponseEntity<String> get(CarDto carDto);


}
