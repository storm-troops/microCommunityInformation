package com.java110.assets.bmo.govCommunityCompany.impl;

import com.java110.assets.bmo.govCommunityCompany.IGetGovCommunityCompanyBMO;
import com.java110.intf.assets.IGovCommunityCompanyInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govCommunityCompany.GovCommunityCompanyDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovCommunityCompanyBMOImpl")
public class GetGovCommunityCompanyBMOImpl implements IGetGovCommunityCompanyBMO {

    @Autowired
    private IGovCommunityCompanyInnerServiceSMO govCommunityCompanyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCommunityCompanyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCommunityCompanyDto govCommunityCompanyDto) {


        int count = govCommunityCompanyInnerServiceSMOImpl.queryGovCommunityCompanysCount(govCommunityCompanyDto);

        List<GovCommunityCompanyDto> govCommunityCompanyDtos = null;
        if (count > 0) {
            govCommunityCompanyDtos = govCommunityCompanyInnerServiceSMOImpl.queryGovCommunityCompanys(govCommunityCompanyDto);
        } else {
            govCommunityCompanyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityCompanyDto.getRow()), count, govCommunityCompanyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
