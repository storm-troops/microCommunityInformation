package com.java110.assets.bmo.govCommunityLocation;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovCommunityLocationBMO {


    /**
     * 修改小区位置
     * add by wuxw
     * @param govCommunityLocationPo
     * @return
     */
    ResponseEntity<String> delete(GovCommunityLocationPo govCommunityLocationPo);


}
