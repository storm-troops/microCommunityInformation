package com.java110.assets.bmo.car;
import com.java110.po.car.CarPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteCarBMO {


    /**
     * 修改车辆
     * add by wuxw
     * @param carPo
     * @return
     */
    ResponseEntity<String> delete(CarPo carPo);


}
