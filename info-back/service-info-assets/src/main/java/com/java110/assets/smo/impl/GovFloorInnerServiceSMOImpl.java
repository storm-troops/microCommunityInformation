package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovFloorServiceDao;
import com.java110.dto.govFloor.GovFloorDto;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 建筑物管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovFloorInnerServiceSMOImpl extends BaseServiceSMO implements IGovFloorInnerServiceSMO {

    @Autowired
    private IGovFloorServiceDao govFloorServiceDaoImpl;


    @Override
    public int saveGovFloor(@RequestBody GovFloorPo govFloorPo) {
        int saveFlag = 1;
        govFloorServiceDaoImpl.saveGovFloorInfo(BeanConvertUtil.beanCovertMap(govFloorPo));
        return saveFlag;
    }

     @Override
    public int updateGovFloor(@RequestBody  GovFloorPo govFloorPo) {
        int saveFlag = 1;
         govFloorServiceDaoImpl.updateGovFloorInfo(BeanConvertUtil.beanCovertMap(govFloorPo));
        return saveFlag;
    }

     @Override
    public int deleteGovFloor(@RequestBody  GovFloorPo govFloorPo) {
        int saveFlag = 1;
        govFloorPo.setStatusCd("1");
        govFloorServiceDaoImpl.updateGovFloorInfo(BeanConvertUtil.beanCovertMap(govFloorPo));
        return saveFlag;
    }

    @Override
    public List<GovFloorDto> queryGovFloors(@RequestBody  GovFloorDto govFloorDto) {

        //校验是否传了 分页信息

        int page = govFloorDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govFloorDto.setPage((page - 1) * govFloorDto.getRow());
        }

        List<GovFloorDto> govFloors = BeanConvertUtil.covertBeanList(govFloorServiceDaoImpl.getGovFloorInfo(BeanConvertUtil.beanCovertMap(govFloorDto)), GovFloorDto.class);

        return govFloors;
    }


    @Override
    public int queryGovFloorsCount(@RequestBody GovFloorDto govFloorDto) {
        return govFloorServiceDaoImpl.queryGovFloorsCount(BeanConvertUtil.beanCovertMap(govFloorDto));    }

    public IGovFloorServiceDao getGovFloorServiceDaoImpl() {
        return govFloorServiceDaoImpl;
    }

    public void setGovFloorServiceDaoImpl(IGovFloorServiceDao govFloorServiceDaoImpl) {
        this.govFloorServiceDaoImpl = govFloorServiceDaoImpl;
    }
}
