package com.java110.assets.bmo.govCommunityCompany;

import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCommunityCompanyBMO {


    /**
     * 添加小区位置
     * add by wuxw
     * @param govCommunityCompanyPo
     * @return
     */
    ResponseEntity<String> save(GovCommunityCompanyPo govCommunityCompanyPo);


}
