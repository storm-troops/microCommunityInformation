package com.java110.assets.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 选择省市区组件内部之间使用，没有给外围系统提供服务能力
 * 选择省市区服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface ICityAreaServiceDao {


    /**
     * 保存 选择省市区信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveCityAreaInfo(Map info) throws DAOException;




    /**
     * 查询选择省市区信息（instance过程）
     * 根据bId 查询选择省市区信息
     * @param info bId 信息
     * @return 选择省市区信息
     * @throws DAOException DAO异常
     */
    List<Map> getCityAreaInfo(Map info) throws DAOException;


    /**
     * 查询选择省市区信息（instance过程）
     * 根据bId 查询选择省市区信息
     * @param info bId 信息
     * @return 选择省市区信息
     * @throws DAOException DAO异常
     */
    List<Map> getAreas(Map info) throws DAOException;


    /**
     * 修改选择省市区信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateCityAreaInfo(Map info) throws DAOException;


    /**
     * 查询选择省市区总数
     *
     * @param info 选择省市区信息
     * @return 选择省市区数量
     */
    int queryCityAreasCount(Map info);

}
