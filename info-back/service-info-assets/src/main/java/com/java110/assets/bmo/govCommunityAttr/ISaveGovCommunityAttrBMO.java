package com.java110.assets.bmo.govCommunityAttr;

import org.springframework.http.ResponseEntity;
import com.java110.po.govCommunityAttr.GovCommunityAttrPo;
public interface ISaveGovCommunityAttrBMO {


    /**
     * 添加小区属性
     * add by wuxw
     * @param govCommunityAttrPo
     * @return
     */
    ResponseEntity<String> save(GovCommunityAttrPo govCommunityAttrPo);


}
