package com.java110.assets.bmo.parkingArea;
import com.java110.dto.parkingArea.ParkingAreaDto;
import org.springframework.http.ResponseEntity;
public interface IGetParkingAreaBMO {


    /**
     * 查询停車場
     * add by wuxw
     * @param  parkingAreaDto
     * @return
     */
    ResponseEntity<String> get(ParkingAreaDto parkingAreaDto);


}
