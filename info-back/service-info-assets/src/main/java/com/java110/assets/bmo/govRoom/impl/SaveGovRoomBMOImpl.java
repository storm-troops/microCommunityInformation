package com.java110.assets.bmo.govRoom.impl;

import com.java110.assets.bmo.govRoom.ISaveGovRoomBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovRoomBMOImpl")
public class SaveGovRoomBMOImpl implements ISaveGovRoomBMO {

    @Autowired
    private IGovRoomInnerServiceSMO govRoomInnerServiceSMOImpl;

    @Autowired
    private IGovOwnerInnerServiceSMO govOwnerInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govRoomPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovRoomPo govRoomPo) {

        govRoomPo.setGovRoomId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govRoomId));
        govRoomPo.setDatasourceType( "999999" );
        int flag = govRoomInnerServiceSMOImpl.saveGovRoom(govRoomPo);

        if (flag > 0) {
            if(!StringUtil.isNullOrNone( govRoomPo.getOwnerId() ) && !GovRoomPo.OWNER_FINAL_VAL.equals( govRoomPo.getOwnerId() )){
                GovOwnerPo govOwnerPo =new GovOwnerPo();
                govOwnerPo.setGovOwnerId( govRoomPo.getOwnerId() );
                govOwnerPo.setRoomId( govRoomPo.getGovRoomId() );
                govOwnerPo.setRoomNum( govRoomPo.getRoomNum() );
                govOwnerInnerServiceSMOImpl.updateGovOwner( govOwnerPo );
            }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
