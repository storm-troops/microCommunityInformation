package com.java110.assets.bmo.govCommunityCompany;
import com.java110.dto.govCommunityCompany.GovCommunityCompanyDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCommunityCompanyBMO {


    /**
     * 查询小区位置
     * add by wuxw
     * @param  govCommunityCompanyDto
     * @return
     */
    ResponseEntity<String> get(GovCommunityCompanyDto govCommunityCompanyDto);


}
