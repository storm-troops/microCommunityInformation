package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovBuildingTypeServiceDao;
import com.java110.dto.govBuildingType.GovBuildingTypeDto;
import com.java110.intf.assets.IGovBuildingTypeInnerServiceSMO;
import com.java110.po.govBuildingType.GovBuildingTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 建筑分类内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovBuildingTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovBuildingTypeInnerServiceSMO {

    @Autowired
    private IGovBuildingTypeServiceDao govBuildingTypeServiceDaoImpl;


    @Override
    public int saveGovBuildingType(@RequestBody GovBuildingTypePo govBuildingTypePo) {
        int saveFlag = 1;
        govBuildingTypeServiceDaoImpl.saveGovBuildingTypeInfo(BeanConvertUtil.beanCovertMap(govBuildingTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovBuildingType(@RequestBody  GovBuildingTypePo govBuildingTypePo) {
        int saveFlag = 1;
         govBuildingTypeServiceDaoImpl.updateGovBuildingTypeInfo(BeanConvertUtil.beanCovertMap(govBuildingTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovBuildingType(@RequestBody  GovBuildingTypePo govBuildingTypePo) {
        int saveFlag = 1;
        govBuildingTypePo.setStatusCd("1");
        govBuildingTypeServiceDaoImpl.updateGovBuildingTypeInfo(BeanConvertUtil.beanCovertMap(govBuildingTypePo));
        return saveFlag;
    }

    @Override
    public List<GovBuildingTypeDto> queryGovBuildingTypes(@RequestBody  GovBuildingTypeDto govBuildingTypeDto) {

        //校验是否传了 分页信息

        int page = govBuildingTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govBuildingTypeDto.setPage((page - 1) * govBuildingTypeDto.getRow());
        }

        List<GovBuildingTypeDto> govBuildingTypes = BeanConvertUtil.covertBeanList(govBuildingTypeServiceDaoImpl.getGovBuildingTypeInfo(BeanConvertUtil.beanCovertMap(govBuildingTypeDto)), GovBuildingTypeDto.class);

        return govBuildingTypes;
    }


    @Override
    public int queryGovBuildingTypesCount(@RequestBody GovBuildingTypeDto govBuildingTypeDto) {
        return govBuildingTypeServiceDaoImpl.queryGovBuildingTypesCount(BeanConvertUtil.beanCovertMap(govBuildingTypeDto));    }

    public IGovBuildingTypeServiceDao getGovBuildingTypeServiceDaoImpl() {
        return govBuildingTypeServiceDaoImpl;
    }

    public void setGovBuildingTypeServiceDaoImpl(IGovBuildingTypeServiceDao govBuildingTypeServiceDaoImpl) {
        this.govBuildingTypeServiceDaoImpl = govBuildingTypeServiceDaoImpl;
    }
}
