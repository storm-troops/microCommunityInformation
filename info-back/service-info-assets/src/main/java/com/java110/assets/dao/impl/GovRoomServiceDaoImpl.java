package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovRoomServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 房屋管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govRoomServiceDaoImpl")
//@Transactional
public class GovRoomServiceDaoImpl extends BaseServiceDao implements IGovRoomServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovRoomServiceDaoImpl.class);





    /**
     * 保存房屋管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovRoomInfo(Map info) throws DAOException {
        logger.debug("保存房屋管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govRoomServiceDaoImpl.saveGovRoomInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存房屋管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询房屋管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovRoomInfo(Map info) throws DAOException {
        logger.debug("查询房屋管理信息 入参 info : {}",info);

        List<Map> businessGovRoomInfos = sqlSessionTemplate.selectList("govRoomServiceDaoImpl.getGovRoomInfo",info);

        return businessGovRoomInfos;
    }


    /**
     * 修改房屋管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovRoomInfo(Map info) throws DAOException {
        logger.debug("修改房屋管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govRoomServiceDaoImpl.updateGovRoomInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改房屋管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询房屋管理数量
     * @param info 房屋管理信息
     * @return 房屋管理数量
     */
    @Override
    public int queryGovRoomsCount(Map info) {
        logger.debug("查询房屋管理数据 入参 info : {}",info);

        List<Map> businessGovRoomInfos = sqlSessionTemplate.selectList("govRoomServiceDaoImpl.queryGovRoomsCount", info);
        if (businessGovRoomInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovRoomInfos.get(0).get("count").toString());
    }


}
