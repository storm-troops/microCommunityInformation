package com.java110.assets.bmo.machineType;
import org.springframework.http.ResponseEntity;
import com.java110.po.machineType.MachineTypePo;

public interface IUpdateMachineTypeBMO {


    /**
     * 修改设备类型
     * add by wuxw
     * @param machineTypePo
     * @return
     */
    ResponseEntity<String> update(MachineTypePo machineTypePo);


}
