package com.java110.assets.bmo.car.impl;

import com.java110.assets.bmo.car.IGetCarBMO;
import com.java110.dto.car.CarDto;
import com.java110.intf.assets.ICarInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getCarBMOImpl")
public class GetCarBMOImpl implements IGetCarBMO {

    @Autowired
    private ICarInnerServiceSMO carInnerServiceSMOImpl;

    /**
     * @param carDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(CarDto carDto) {


        int count = carInnerServiceSMOImpl.queryCarsCount(carDto);

        List<CarDto> carDtos = null;
        if (count > 0) {
            carDtos = carInnerServiceSMOImpl.queryCars(carDto);
        } else {
            carDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) carDto.getRow()), count, carDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
