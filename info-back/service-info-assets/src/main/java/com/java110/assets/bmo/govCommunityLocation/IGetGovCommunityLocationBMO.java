package com.java110.assets.bmo.govCommunityLocation;
import com.java110.dto.govCommunityLocation.GovCommunityLocationDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCommunityLocationBMO {


    /**
     * 查询小区位置
     * add by wuxw
     * @param  govCommunityLocationDto
     * @return
     */
    ResponseEntity<String> get(GovCommunityLocationDto govCommunityLocationDto);


}
