package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovRoomServiceDao;
import com.java110.dto.govRoom.GovRoomDto;
import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 房屋管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovRoomInnerServiceSMOImpl extends BaseServiceSMO implements IGovRoomInnerServiceSMO {

    @Autowired
    private IGovRoomServiceDao govRoomServiceDaoImpl;


    @Override
    public int saveGovRoom(@RequestBody GovRoomPo govRoomPo) {
        int saveFlag = 1;
        govRoomServiceDaoImpl.saveGovRoomInfo(BeanConvertUtil.beanCovertMap(govRoomPo));
        return saveFlag;
    }

     @Override
    public int updateGovRoom(@RequestBody  GovRoomPo govRoomPo) {
        int saveFlag = 1;
         govRoomServiceDaoImpl.updateGovRoomInfo(BeanConvertUtil.beanCovertMap(govRoomPo));
        return saveFlag;
    }

     @Override
    public int deleteGovRoom(@RequestBody  GovRoomPo govRoomPo) {
        int saveFlag = 1;
        govRoomPo.setStatusCd("1");
        govRoomServiceDaoImpl.updateGovRoomInfo(BeanConvertUtil.beanCovertMap(govRoomPo));
        return saveFlag;
    }

    @Override
    public List<GovRoomDto> queryGovRooms(@RequestBody  GovRoomDto govRoomDto) {

        //校验是否传了 分页信息

        int page = govRoomDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govRoomDto.setPage((page - 1) * govRoomDto.getRow());
        }

        List<GovRoomDto> govRooms = BeanConvertUtil.covertBeanList(govRoomServiceDaoImpl.getGovRoomInfo(BeanConvertUtil.beanCovertMap(govRoomDto)), GovRoomDto.class);

        return govRooms;
    }


    @Override
    public int queryGovRoomsCount(@RequestBody GovRoomDto govRoomDto) {
        return govRoomServiceDaoImpl.queryGovRoomsCount(BeanConvertUtil.beanCovertMap(govRoomDto));    }

    public IGovRoomServiceDao getGovRoomServiceDaoImpl() {
        return govRoomServiceDaoImpl;
    }

    public void setGovRoomServiceDaoImpl(IGovRoomServiceDao govRoomServiceDaoImpl) {
        this.govRoomServiceDaoImpl = govRoomServiceDaoImpl;
    }
}
