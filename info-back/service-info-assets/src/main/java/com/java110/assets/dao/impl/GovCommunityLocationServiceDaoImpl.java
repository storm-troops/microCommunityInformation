package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovCommunityLocationServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 小区位置服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCommunityLocationServiceDaoImpl")
//@Transactional
public class GovCommunityLocationServiceDaoImpl extends BaseServiceDao implements IGovCommunityLocationServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCommunityLocationServiceDaoImpl.class);





    /**
     * 保存小区位置信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCommunityLocationInfo(Map info) throws DAOException {
        logger.debug("保存小区位置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCommunityLocationServiceDaoImpl.saveGovCommunityLocationInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存小区位置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询小区位置信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityLocationInfo(Map info) throws DAOException {
        logger.debug("查询小区位置信息 入参 info : {}",info);

        List<Map> businessGovCommunityLocationInfos = sqlSessionTemplate.selectList("govCommunityLocationServiceDaoImpl.getGovCommunityLocationInfo",info);

        return businessGovCommunityLocationInfos;
    }


    /**
     * 修改小区位置信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCommunityLocationInfo(Map info) throws DAOException {
        logger.debug("修改小区位置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCommunityLocationServiceDaoImpl.updateGovCommunityLocationInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改小区位置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询小区位置数量
     * @param info 小区位置信息
     * @return 小区位置数量
     */
    @Override
    public int queryGovCommunityLocationsCount(Map info) {
        logger.debug("查询小区位置数据 入参 info : {}",info);

        List<Map> businessGovCommunityLocationInfos = sqlSessionTemplate.selectList("govCommunityLocationServiceDaoImpl.queryGovCommunityLocationsCount", info);
        if (businessGovCommunityLocationInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityLocationInfos.get(0).get("count").toString());
    }


}
