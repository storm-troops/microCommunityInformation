package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.fileRel.IDeleteFileRelBMO;
import com.java110.assets.bmo.fileRel.IGetFileRelBMO;
import com.java110.assets.bmo.fileRel.ISaveFileRelBMO;
import com.java110.assets.bmo.fileRel.IUpdateFileRelBMO;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.po.fileRel.FileRelPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/fileRel")
public class FileRelApi {

    @Autowired
    private ISaveFileRelBMO saveFileRelBMOImpl;
    @Autowired
    private IUpdateFileRelBMO updateFileRelBMOImpl;
    @Autowired
    private IDeleteFileRelBMO deleteFileRelBMOImpl;

    @Autowired
    private IGetFileRelBMO getFileRelBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /fileRel/saveFileRel
     * @path /app/fileRel/saveFileRel
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveFileRel", method = RequestMethod.POST)
    public ResponseEntity<String> saveFileRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "relType", "请求报文中未包含relType");
        Assert.hasKeyAndValue(reqJson, "objId", "请求报文中未包含objId");
        Assert.hasKeyAndValue(reqJson, "fileName", "请求报文中未包含fileName");


        FileRelPo fileRelPo = BeanConvertUtil.covertBean(reqJson, FileRelPo.class);
        return saveFileRelBMOImpl.save(fileRelPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /fileRel/updateFileRel
     * @path /app/fileRel/updateFileRel
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateFileRel", method = RequestMethod.POST)
    public ResponseEntity<String> updateFileRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "fileRelId", "请求报文中未包含fileRelId");
        Assert.hasKeyAndValue(reqJson, "relType", "请求报文中未包含relType");
        Assert.hasKeyAndValue(reqJson, "objId", "请求报文中未包含objId");
        Assert.hasKeyAndValue(reqJson, "fileName", "请求报文中未包含fileName");
        Assert.hasKeyAndValue(reqJson, "fileRelId", "fileRelId不能为空");


        FileRelPo fileRelPo = BeanConvertUtil.covertBean(reqJson, FileRelPo.class);
        return updateFileRelBMOImpl.update(fileRelPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /fileRel/deleteFileRel
     * @path /app/fileRel/deleteFileRel
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteFileRel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteFileRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "fileRelId", "fileRelId不能为空");


        FileRelPo fileRelPo = BeanConvertUtil.covertBean(reqJson, FileRelPo.class);
        return deleteFileRelBMOImpl.delete(fileRelPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /fileRel/queryFileRel
     * @path /app/fileRel/queryFileRel
     * @param
     * @return
     */
    @RequestMapping(value = "/queryFileRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryFileRel(@RequestParam(value = "fileRelId") String fileRelId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        FileRelDto fileRelDto = new FileRelDto();
        fileRelDto.setPage(page);
        fileRelDto.setRow(row);
        fileRelDto.setFileRelId(fileRelId);
        return getFileRelBMOImpl.get(fileRelDto);
    }
}
