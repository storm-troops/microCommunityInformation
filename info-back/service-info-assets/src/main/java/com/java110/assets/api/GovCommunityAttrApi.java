package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.po.govCommunityAttr.GovCommunityAttrPo;
import com.java110.assets.bmo.govCommunityAttr.IDeleteGovCommunityAttrBMO;
import com.java110.assets.bmo.govCommunityAttr.IGetGovCommunityAttrBMO;
import com.java110.assets.bmo.govCommunityAttr.ISaveGovCommunityAttrBMO;
import com.java110.assets.bmo.govCommunityAttr.IUpdateGovCommunityAttrBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCommunityAttr")
public class GovCommunityAttrApi {

    @Autowired
    private ISaveGovCommunityAttrBMO saveGovCommunityAttrBMOImpl;
    @Autowired
    private IUpdateGovCommunityAttrBMO updateGovCommunityAttrBMOImpl;
    @Autowired
    private IDeleteGovCommunityAttrBMO deleteGovCommunityAttrBMOImpl;

    @Autowired
    private IGetGovCommunityAttrBMO getGovCommunityAttrBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityAttr/saveGovCommunityAttr
     * @path /app/govCommunityAttr/saveGovCommunityAttr
     */
    @RequestMapping(value = "/saveGovCommunityAttr", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCommunityAttr(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "attrId", "请求报文中未包含attrId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "specCd", "请求报文中未包含specCd");
        Assert.hasKeyAndValue(reqJson, "value", "请求报文中未包含value");


        GovCommunityAttrPo govCommunityAttrPo = BeanConvertUtil.covertBean(reqJson, GovCommunityAttrPo.class);
        return saveGovCommunityAttrBMOImpl.save(govCommunityAttrPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityAttr/updateGovCommunityAttr
     * @path /app/govCommunityAttr/updateGovCommunityAttr
     */
    @RequestMapping(value = "/updateGovCommunityAttr", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCommunityAttr(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "attrId", "请求报文中未包含attrId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "specCd", "请求报文中未包含specCd");
        Assert.hasKeyAndValue(reqJson, "value", "请求报文中未包含value");
        Assert.hasKeyAndValue(reqJson, "attrId", "attrId不能为空");


        GovCommunityAttrPo govCommunityAttrPo = BeanConvertUtil.covertBean(reqJson, GovCommunityAttrPo.class);
        return updateGovCommunityAttrBMOImpl.update(govCommunityAttrPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityAttr/deleteGovCommunityAttr
     * @path /app/govCommunityAttr/deleteGovCommunityAttr
     */
    @RequestMapping(value = "/deleteGovCommunityAttr", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCommunityAttr(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "attrId", "attrId不能为空");


        GovCommunityAttrPo govCommunityAttrPo = BeanConvertUtil.covertBean(reqJson, GovCommunityAttrPo.class);
        return deleteGovCommunityAttrBMOImpl.delete(govCommunityAttrPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govCommunityAttr/queryGovCommunityAttr
     * @path /app/govCommunityAttr/queryGovCommunityAttr
     */
    @RequestMapping(value = "/queryGovCommunityAttr", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCommunityAttr(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovCommunityAttrDto govCommunityAttrDto = new GovCommunityAttrDto();
        govCommunityAttrDto.setPage(page);
        govCommunityAttrDto.setRow(row);
        govCommunityAttrDto.setCaId(caId);
        return getGovCommunityAttrBMOImpl.get(govCommunityAttrDto);
    }
}
