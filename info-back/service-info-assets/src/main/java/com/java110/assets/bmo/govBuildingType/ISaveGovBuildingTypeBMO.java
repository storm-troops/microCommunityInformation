package com.java110.assets.bmo.govBuildingType;

import com.java110.po.govBuildingType.GovBuildingTypePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovBuildingTypeBMO {


    /**
     * 添加建筑分类
     * add by wuxw
     * @param govBuildingTypePo
     * @return
     */
    ResponseEntity<String> save(GovBuildingTypePo govBuildingTypePo);


}
