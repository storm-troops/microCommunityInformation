package com.java110.assets.bmo.parkingArea.impl;

import com.java110.assets.bmo.parkingArea.IDeleteParkingAreaBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.intf.assets.IParkingAreaInnerServiceSMO;
import com.java110.po.parkingArea.ParkingAreaPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteParkingAreaBMOImpl")
public class DeleteParkingAreaBMOImpl implements IDeleteParkingAreaBMO {

    @Autowired
    private IParkingAreaInnerServiceSMO parkingAreaInnerServiceSMOImpl;

    /**
     * @param parkingAreaPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(ParkingAreaPo parkingAreaPo) {

        int flag = parkingAreaInnerServiceSMOImpl.deleteParkingArea(parkingAreaPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
