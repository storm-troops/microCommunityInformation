package com.java110.assets.bmo.car.impl;

import com.java110.assets.bmo.car.IUpdateCarBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.intf.assets.ICarInnerServiceSMO;
import com.java110.po.car.CarPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateCarBMOImpl")
public class UpdateCarBMOImpl implements IUpdateCarBMO {

    @Autowired
    private ICarInnerServiceSMO carInnerServiceSMOImpl;

    /**
     * @param carPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(CarPo carPo) {

        int flag = carInnerServiceSMOImpl.updateCar(carPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
