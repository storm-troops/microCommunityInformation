package com.java110.assets.bmo.fileRel;
import com.java110.dto.fileRel.FileRelDto;
import org.springframework.http.ResponseEntity;
public interface IGetFileRelBMO {


    /**
     * 查询文件关系管理
     * add by wuxw
     * @param  fileRelDto
     * @return
     */
    ResponseEntity<String> get(FileRelDto fileRelDto);


}
