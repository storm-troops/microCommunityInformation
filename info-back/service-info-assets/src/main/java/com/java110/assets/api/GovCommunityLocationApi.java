package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunityLocation.IDeleteGovCommunityLocationBMO;
import com.java110.assets.bmo.govCommunityLocation.IGetGovCommunityLocationBMO;
import com.java110.assets.bmo.govCommunityLocation.ISaveGovCommunityLocationBMO;
import com.java110.assets.bmo.govCommunityLocation.IUpdateGovCommunityLocationBMO;
import com.java110.dto.govCommunityLocation.GovCommunityLocationDto;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/govCommunityLocation")
public class GovCommunityLocationApi {

    @Autowired
    private ISaveGovCommunityLocationBMO saveGovCommunityLocationBMOImpl;
    @Autowired
    private IUpdateGovCommunityLocationBMO updateGovCommunityLocationBMOImpl;
    @Autowired
    private IDeleteGovCommunityLocationBMO deleteGovCommunityLocationBMOImpl;

    @Autowired
    private IGetGovCommunityLocationBMO getGovCommunityLocationBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityLocation/saveGovCommunityLocation
     * @path /app/govCommunityLocation/saveGovCommunityLocation
     */
    @RequestMapping(value = "/saveGovCommunityLocation", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCommunityLocation(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");


        GovCommunityLocationPo govCommunityLocationPo = BeanConvertUtil.covertBean(reqJson, GovCommunityLocationPo.class);
        return saveGovCommunityLocationBMOImpl.save(govCommunityLocationPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityLocation/updateGovCommunityLocation
     * @path /app/govCommunityLocation/updateGovCommunityLocation
     */
    @RequestMapping(value = "/updateGovCommunityLocation", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCommunityLocation(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "locationId", "locationId不能为空");


        GovCommunityLocationPo govCommunityLocationPo = BeanConvertUtil.covertBean(reqJson, GovCommunityLocationPo.class);
        return updateGovCommunityLocationBMOImpl.update(govCommunityLocationPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityLocation/deleteGovCommunityLocation
     * @path /app/govCommunityLocation/deleteGovCommunityLocation
     */
    @RequestMapping(value = "/deleteGovCommunityLocation", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCommunityLocation(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "locationId", "locationId不能为空");


        GovCommunityLocationPo govCommunityLocationPo = BeanConvertUtil.covertBean(reqJson, GovCommunityLocationPo.class);
        return deleteGovCommunityLocationBMOImpl.delete(govCommunityLocationPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govCommunityLocation/queryGovCommunityLocation
     * @path /app/govCommunityLocation/queryGovCommunityLocation
     */
    @RequestMapping(value = "/queryGovCommunityLocation", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCommunityLocation(@RequestParam(value = "caId") String caId,
                                                            @RequestParam(value = "page") int page,
                                                            @RequestParam(value = "row") int row) {
        GovCommunityLocationDto govCommunityLocationDto = new GovCommunityLocationDto();
        govCommunityLocationDto.setPage(page);
        govCommunityLocationDto.setRow(row);
        govCommunityLocationDto.setCaId(caId);
        return getGovCommunityLocationBMOImpl.get(govCommunityLocationDto);
    }
}
