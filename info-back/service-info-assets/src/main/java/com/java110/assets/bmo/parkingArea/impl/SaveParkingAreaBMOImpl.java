package com.java110.assets.bmo.parkingArea.impl;

import com.java110.assets.bmo.parkingArea.ISaveParkingAreaBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IParkingAreaInnerServiceSMO;
import com.java110.po.parkingArea.ParkingAreaPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveParkingAreaBMOImpl")
public class SaveParkingAreaBMOImpl implements ISaveParkingAreaBMO {

    @Autowired
    private IParkingAreaInnerServiceSMO parkingAreaInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param parkingAreaPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(ParkingAreaPo parkingAreaPo) {

        parkingAreaPo.setPaId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_paId));
        int flag = parkingAreaInnerServiceSMOImpl.saveParkingArea(parkingAreaPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
