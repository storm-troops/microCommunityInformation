package com.java110.assets.bmo.cityArea.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.cityArea.IGetCityAreaBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.cityArea.ApiAreaDto;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.assets.ICityAreaInnerServiceSMO;
import com.java110.dto.cityArea.CityAreaDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getCityAreaBMOImpl")
public class GetCityAreaBMOImpl implements IGetCityAreaBMO {

    @Autowired
    private ICityAreaInnerServiceSMO cityAreaInnerServiceSMOImpl;

    /**
     *
     *
     * @param  cityAreaDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(CityAreaDto cityAreaDto) {


        int count = cityAreaInnerServiceSMOImpl.queryCityAreasCount(cityAreaDto);

        List<CityAreaDto> cityAreaDtos = null;
        if (count > 0) {
            cityAreaDtos = cityAreaInnerServiceSMOImpl.queryCityAreas(cityAreaDto);
        } else {
            cityAreaDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) cityAreaDto.getRow()), count, cityAreaDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }


    /**
     *
     *
     * @param  cityAreaDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getAreas(CityAreaDto cityAreaDto) {
        List<CityAreaDto> cityAreaDtos = cityAreaInnerServiceSMOImpl.getAreas(cityAreaDto);
        ApiAreaDto apiAreaDto = new ApiAreaDto();
        apiAreaDto.setTotal(1);
        apiAreaDto.setRecords(1);
        apiAreaDto.setAreas(cityAreaDtos);
        ResponseEntity<String> responseEntity = new ResponseEntity<String>(JSONObject.toJSONString(apiAreaDto), HttpStatus.OK);
        return responseEntity;
    }
}
