package com.java110.assets.bmo.govCommunityLocation.impl;

import com.java110.assets.bmo.govCommunityLocation.IUpdateGovCommunityLocationBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.intf.assets.IGovCommunityLocationInnerServiceSMO;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovCommunityLocationBMOImpl")
public class UpdateGovCommunityLocationBMOImpl implements IUpdateGovCommunityLocationBMO {

    @Autowired
    private IGovCommunityLocationInnerServiceSMO govCommunityLocationInnerServiceSMOImpl;

    /**
     * @param govCommunityLocationPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCommunityLocationPo govCommunityLocationPo) {

        int flag = govCommunityLocationInnerServiceSMOImpl.updateGovCommunityLocation(govCommunityLocationPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
