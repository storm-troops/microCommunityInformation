package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovCommunityAreaServiceDao;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govCommunityArea.GovCommunityAreaDto;
import com.java110.intf.assets.IGovCommunityAreaInnerServiceSMO;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 区域管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCommunityAreaInnerServiceSMOImpl extends BaseServiceSMO implements IGovCommunityAreaInnerServiceSMO {

    @Autowired
    private IGovCommunityAreaServiceDao govCommunityAreaServiceDaoImpl;


    @Override
    public int saveGovCommunityArea(@RequestBody GovCommunityAreaPo govCommunityAreaPo) {
        int saveFlag = 1;
        govCommunityAreaServiceDaoImpl.saveGovCommunityAreaInfo( BeanConvertUtil.beanCovertMap(govCommunityAreaPo));
        return saveFlag;
    }

     @Override
    public int updateGovCommunityArea(@RequestBody  GovCommunityAreaPo govCommunityAreaPo) {
        int saveFlag = 1;
         govCommunityAreaServiceDaoImpl.updateGovCommunityAreaInfo(BeanConvertUtil.beanCovertMap(govCommunityAreaPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCommunityArea(@RequestBody  GovCommunityAreaPo govCommunityAreaPo) {
        int saveFlag = 1;
        govCommunityAreaPo.setStatusCd("1");
        govCommunityAreaServiceDaoImpl.updateGovCommunityAreaInfo(BeanConvertUtil.beanCovertMap(govCommunityAreaPo));
        return saveFlag;
    }

    @Override
    public List<GovCommunityAreaDto> queryGovCommunityAreas(@RequestBody  GovCommunityAreaDto govCommunityAreaDto) {

        //校验是否传了 分页信息

        int page = govCommunityAreaDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityAreaDto.setPage((page - 1) * govCommunityAreaDto.getRow());
        }

        List<GovCommunityAreaDto> govCommunityAreas = BeanConvertUtil.covertBeanList(govCommunityAreaServiceDaoImpl.getGovCommunityAreaInfo(BeanConvertUtil.beanCovertMap(govCommunityAreaDto)), GovCommunityAreaDto.class);

        return govCommunityAreas;
    }
    @Override
    public List<GovCommunityAreaDto> getGovCommunityAreaStaff(@RequestBody  GovCommunityAreaDto govCommunityAreaDto) {

        //校验是否传了 分页信息

        int page = govCommunityAreaDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityAreaDto.setPage((page - 1) * govCommunityAreaDto.getRow());
        }

        List<GovCommunityAreaDto> govCommunityAreas = BeanConvertUtil.covertBeanList(govCommunityAreaServiceDaoImpl.getGovCommunityAreaStaff(BeanConvertUtil.beanCovertMap(govCommunityAreaDto)), GovCommunityAreaDto.class);

        return govCommunityAreas;
    }


    @Override
    public int queryGovCommunityAreasCount(@RequestBody GovCommunityAreaDto govCommunityAreaDto) {
        return govCommunityAreaServiceDaoImpl.queryGovCommunityAreasCount(BeanConvertUtil.beanCovertMap(govCommunityAreaDto));    }
    @Override
    public int getGovCommunityAreaStaffCount(@RequestBody GovCommunityAreaDto govCommunityAreaDto) {
        return govCommunityAreaServiceDaoImpl.getGovCommunityAreaStaffCount(BeanConvertUtil.beanCovertMap(govCommunityAreaDto));    }

    public IGovCommunityAreaServiceDao getGovCommunityAreaServiceDaoImpl() {
        return govCommunityAreaServiceDaoImpl;
    }

    public void setGovCommunityAreaServiceDaoImpl(IGovCommunityAreaServiceDao govCommunityAreaServiceDaoImpl) {
        this.govCommunityAreaServiceDaoImpl = govCommunityAreaServiceDaoImpl;
    }
}
