package com.java110.assets.bmo.car;

import com.java110.po.car.CarPo;
import org.springframework.http.ResponseEntity;
public interface ISaveCarBMO {


    /**
     * 添加车辆
     * add by wuxw
     * @param carPo
     * @return
     */
    ResponseEntity<String> save(CarPo carPo);


}
