package com.java110.assets.smo.impl;


import com.java110.assets.dao.IMachineTypeServiceDao;
import com.java110.intf.assets.IMachineTypeInnerServiceSMO;
import com.java110.dto.machineType.MachineTypeDto;
import com.java110.po.machineType.MachineTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 设备类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class MachineTypeInnerServiceSMOImpl extends BaseServiceSMO implements IMachineTypeInnerServiceSMO {

    @Autowired
    private IMachineTypeServiceDao machineTypeServiceDaoImpl;


    @Override
    public int saveMachineType(@RequestBody  MachineTypePo machineTypePo) {
        int saveFlag = 1;
        machineTypeServiceDaoImpl.saveMachineTypeInfo(BeanConvertUtil.beanCovertMap(machineTypePo));
        return saveFlag;
    }

     @Override
    public int updateMachineType(@RequestBody  MachineTypePo machineTypePo) {
        int saveFlag = 1;
         machineTypeServiceDaoImpl.updateMachineTypeInfo(BeanConvertUtil.beanCovertMap(machineTypePo));
        return saveFlag;
    }

     @Override
    public int deleteMachineType(@RequestBody  MachineTypePo machineTypePo) {
        int saveFlag = 1;
        machineTypePo.setStatusCd("1");
        machineTypeServiceDaoImpl.updateMachineTypeInfo(BeanConvertUtil.beanCovertMap(machineTypePo));
        return saveFlag;
    }

    @Override
    public List<MachineTypeDto> queryMachineTypes(@RequestBody  MachineTypeDto machineTypeDto) {

        //校验是否传了 分页信息

        int page = machineTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            machineTypeDto.setPage((page - 1) * machineTypeDto.getRow());
        }

        List<MachineTypeDto> machineTypes = BeanConvertUtil.covertBeanList(machineTypeServiceDaoImpl.getMachineTypeInfo(BeanConvertUtil.beanCovertMap(machineTypeDto)), MachineTypeDto.class);

        return machineTypes;
    }


    @Override
    public int queryMachineTypesCount(@RequestBody MachineTypeDto machineTypeDto) {
        return machineTypeServiceDaoImpl.queryMachineTypesCount(BeanConvertUtil.beanCovertMap(machineTypeDto));    }

    public IMachineTypeServiceDao getMachineTypeServiceDaoImpl() {
        return machineTypeServiceDaoImpl;
    }

    public void setMachineTypeServiceDaoImpl(IMachineTypeServiceDao machineTypeServiceDaoImpl) {
        this.machineTypeServiceDaoImpl = machineTypeServiceDaoImpl;
    }
}
