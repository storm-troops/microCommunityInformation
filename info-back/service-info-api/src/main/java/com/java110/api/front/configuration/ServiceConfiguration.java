package com.java110.api.front.configuration;

import com.java110.api.front.filter.JwtFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wuxw on 2018/5/2.
 */
@Configuration
public class ServiceConfiguration {
    @Bean
    public FilterRegistrationBean jwtFilter() {
        StringBuffer exclusions = new StringBuffer();
        exclusions.append("/callComponent/login/*,");
        exclusions.append("/callComponent/register/*,");
        exclusions.append("/callComponent/validate-code/*,");
        exclusions.append("/callComponent/validate-tel/*,");
        exclusions.append("/app/govActivitiesType/queryGovActivitiesType,");
        exclusions.append("/app/govActivities/queryGovActivities,");
        exclusions.append("/app/govCommunityArea/queryGovCommunityArea,");
        exclusions.append("/app/govReportSetting/queryGovReportSetting,");
        exclusions.append("/app/reportPool/savePhoneReportPool,");
        exclusions.append("/app/reportPool/queryReportPool,");
        exclusions.append("/app/govAdvert/queryGovAdvert,");
        exclusions.append("/app/govReportUser/queryGovReportUser,");
        exclusions.append("/app/reportPool/deleteReportPool,");
        exclusions.append("/app/govAdvert/queryGovAdvertItems,");

        /**活动报名**/
        exclusions.append("/app/govActivityType/queryGovActivityType,");
        exclusions.append("/app/govActivity/queryGovActivity,");
        exclusions.append("/app/govActivityPerson/queryGovActivityPerson,");
        exclusions.append("/app/govActivityPerson/deleteGovActivityPerson,");
        exclusions.append("/app/govActivityPerson/saveGovActivityPerson,");

        /**办事指南**/
        exclusions.append("/app/govWorkGuide/queryGovWorkGuide,");
        exclusions.append("/app/govGuideSubscribe/queryGovGuideSubscribe,");
        exclusions.append("/app/govGuideSubscribe/saveGovGuideSubscribe,");
        exclusions.append("/app/govGuideSubscribe/deleteGovGuideSubscribe,");

        /**企业信息**/
        exclusions.append("/app/govCompany/queryGovCompany,");
        exclusions.append("/app/govCompany/saveGovCompany,");

        /**查询小区、村**/
        exclusions.append("/app/govCommunity/queryGovCommunity,");

        /**老人过世登记**/
        exclusions.append("/app/govPersonDie/savePhonePersonDie,");

        /**政务宣传**/
        exclusions.append("/app/govCiviladmin/queryGovCiviladmin,");

        /**事件上报**/
        exclusions.append("/app/govEventsType/queryGovEventsType,");
        exclusions.append("/app/govMajorEvents/savePhoneGovMajorEvents,");
        exclusions.append("/app/govProtectionCase/savePhoneGovProtectionCase,");
        exclusions.append("/app/govRoadProtection/queryGovRoadProtection,");
        exclusions.append("/app/govSchool/queryGovSchool,");
        exclusions.append("/callComponent/core/list,");

        /**特殊人员跟进**/
        exclusions.append("/app/govSpecialFollow/savePhoneGovSpecialFollow,");
        exclusions.append("/app/govPerson/queryLabelGovPerson,");

        /**评论点赞**/
        exclusions.append("/app/govActivityLikes/saveGovActivityLikes,");
        exclusions.append("/app/govActivityLikes/deleteGovActivityLikes,");
        exclusions.append("/app/govActivityLikes/queryGovActivityLikes,");
        exclusions.append("/app/govActivityReply/saveGovActivityReply,");
        exclusions.append("/app/govActivityReply/deleteGovActivityReply,");
        exclusions.append("/app/govActivityReply/queryGovActivityReply,");

        /**帮扶人员**/
        exclusions.append("/app/govPerson/queryGovPerson,");
        exclusions.append("/app/govHelpPolicyList/queryGovHelpPolicyList,");



        exclusions.append("/flow/login,");
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new JwtFilter());
        registrationBean.addUrlPatterns("/");
        registrationBean.addUrlPatterns("/callComponent/*");
        registrationBean.addUrlPatterns("/flow/*");
        registrationBean.addUrlPatterns("/app/*");
        registrationBean.addInitParameter("excludedUri",exclusions.toString());

        return registrationBean;
    }

}
