package com.java110.api.listener.store;

import com.alibaba.fastjson.JSONObject;
import com.java110.api.listener.AbstractServiceApiListener;
import com.java110.core.annotation.Java110Listener;
import com.java110.core.context.DataFlowContext;
import com.java110.core.event.service.api.ServiceDataFlowEvent;
import com.java110.dto.store.StoreAttrDto;
import com.java110.dto.store.StoreDto;
import com.java110.intf.store.IStoreInnerServiceSMO;
import com.java110.utils.constant.ServiceCodeConstant;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 保存商户信息
 * Created by Administrator on 2019/3/29.
 */
@Java110Listener("listStoresListener")
public class ListStoresListener extends AbstractServiceApiListener {

    @Autowired
    private IStoreInnerServiceSMO storeInnerServiceSMOImpl;


    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public String getServiceCode() {
        return ServiceCodeConstant.SERVICE_CODE_LIST_STORES;
    }

    @Override
    public HttpMethod getHttpMethod() {
        return HttpMethod.GET;
    }

    @Override
    protected void validate(ServiceDataFlowEvent event, JSONObject reqJson) {
        super.validatePageInfo(reqJson);
    }

    @Override
    protected void doSoService(ServiceDataFlowEvent event, DataFlowContext context, JSONObject reqJson) {
        StoreDto storeDto = BeanConvertUtil.covertBean(reqJson, StoreDto.class);
        int storeCount = storeInnerServiceSMOImpl.getStoreCount(storeDto);
        List<StoreDto> storeDtos = null;
        List<StoreDto> stores = null;
        if (storeCount > 0) {
            storeDtos = storeInnerServiceSMOImpl.getStores(storeDto);
            stores = BeanConvertUtil.covertBeanList(storeDtos, StoreDto.class);
            refreshStoreAttr(stores);
        } else {
            stores = new ArrayList<>();
        }
        ResultVo apiStoreVo = new ResultVo();
        apiStoreVo.setTotal(storeCount);
        apiStoreVo.setRecords((int) Math.ceil((double) storeCount / (double) reqJson.getInteger("row")));
        apiStoreVo.setData(stores);
        ResponseEntity<String> responseEntity = new ResponseEntity<String>(JSONObject.toJSONString(apiStoreVo), HttpStatus.OK);
        context.setResponseEntity(responseEntity);

    }

    private void refreshStoreAttr(List<StoreDto> stores) {
        StoreAttrDto storeAttrDto = new StoreAttrDto();
        storeAttrDto.setStoreIds(getStoreIds(stores));
        List<StoreAttrDto> storeAttrDtos = storeInnerServiceSMOImpl.getStoreAttrs(storeAttrDto);
        for (StoreDto storeDataVo : stores) {
            List<StoreAttrDto> storeAttrs = new ArrayList<StoreAttrDto>();
            for (StoreAttrDto tmpStoreAttrDto : storeAttrDtos) {

                if(storeDataVo.getStoreId().equals(tmpStoreAttrDto.getStoreId())){
                    storeAttrs.add(tmpStoreAttrDto);
                }

                if (!storeDataVo.getStoreId().equals(tmpStoreAttrDto.getStoreId())) {
                    continue;
                }
            }
            storeDataVo.setStoreAttrs(storeAttrs);
        }
    }

    /**
     * 查询商户ID
     *
     * @param apiStoreDataVos
     * @return
     */
    private String[] getStoreIds(List<StoreDto> apiStoreDataVos) {
        List<String> storeIds = new ArrayList<>();
        for (StoreDto storeDataVo : apiStoreDataVos) {
            storeIds.add(storeDataVo.getStoreId());
        }

        return storeIds.toArray(new String[storeIds.size()]);
    }


}
