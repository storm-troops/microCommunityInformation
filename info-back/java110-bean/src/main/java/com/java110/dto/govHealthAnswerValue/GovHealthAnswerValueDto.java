package com.java110.dto.govHealthAnswerValue;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 体检项目答案数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHealthAnswerValueDto extends PageDto implements Serializable {
    public static final String TITLETYPE_RADIO ="1001";
    public static final String TITLETYPE_CHECKBOX ="2002";
    public static final String ITLETYPE_TEXTAREA ="3003";
    private String doctor;
private String doctorRemark;
private String valueId;
private String healthPersonId;
private String personTitleId;
private String titleId;
private String caId;
private String healthId;
private String valueContent;
private String personId;
private String [] personIds;


    private Date createTime;

    private String statusCd = "0";


    public String getDoctor() {
        return doctor;
    }
public void setDoctor(String doctor) {
        this.doctor = doctor;
    }
public String getDoctorRemark() {
        return doctorRemark;
    }
public void setDoctorRemark(String doctorRemark) {
        this.doctorRemark = doctorRemark;
    }
public String getValueId() {
        return valueId;
    }
public void setValueId(String valueId) {
        this.valueId = valueId;
    }
public String getHealthPersonId() {
        return healthPersonId;
    }
public void setHealthPersonId(String healthPersonId) {
        this.healthPersonId = healthPersonId;
    }
public String getPersonTitleId() {
        return personTitleId;
    }
public void setPersonTitleId(String personTitleId) {
        this.personTitleId = personTitleId;
    }
public String getTitleId() {
        return titleId;
    }
public void setTitleId(String titleId) {
        this.titleId = titleId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getHealthId() {
        return healthId;
    }
public void setHealthId(String healthId) {
        this.healthId = healthId;
    }
public String getValueContent() {
        return valueContent;
    }
public void setValueContent(String valueContent) {
        this.valueContent = valueContent;
    }
public String getPersonId() {
        return personId;
    }
public void setPersonId(String personId) {
        this.personId = personId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String[] getPersonIds() {
        return personIds;
    }

    public void setPersonIds(String[] personIds) {
        this.personIds = personIds;
    }
}
