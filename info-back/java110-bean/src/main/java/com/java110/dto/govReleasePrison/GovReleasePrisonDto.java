package com.java110.dto.govReleasePrison;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 刑满释放人员数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovReleasePrisonDto extends PageDto implements Serializable {

    private String address;
private String releaseTime;
private String caId;
private String sentenceStartTime;
private String name;
private String sentenceReason;
private String tel;
private String sentenceEndTime;
private String ramark;
private String releasePrisonId;


    private Date createTime;

    private String statusCd = "0";


    public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getReleaseTime() {
        return releaseTime;
    }
public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSentenceStartTime() {
        return sentenceStartTime;
    }
public void setSentenceStartTime(String sentenceStartTime) {
        this.sentenceStartTime = sentenceStartTime;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getSentenceReason() {
        return sentenceReason;
    }
public void setSentenceReason(String sentenceReason) {
        this.sentenceReason = sentenceReason;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getSentenceEndTime() {
        return sentenceEndTime;
    }
public void setSentenceEndTime(String sentenceEndTime) {
        this.sentenceEndTime = sentenceEndTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getReleasePrisonId() {
        return releasePrisonId;
    }
public void setReleasePrisonId(String releasePrisonId) {
        this.releasePrisonId = releasePrisonId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
