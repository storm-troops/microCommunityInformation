package com.java110.dto.govReportTypeUser;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 报事类型人员数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovReportTypeUserDto extends PageDto implements Serializable {

    private String reportType;
private String caId;
private String typeUserId;
private String staffName;
private String remark;
private String state;
private String staffId;


    private Date createTime;

    private String statusCd = "0";


    public String getReportType() {
        return reportType;
    }
public void setReportType(String reportType) {
        this.reportType = reportType;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeUserId() {
        return typeUserId;
    }
public void setTypeUserId(String typeUserId) {
        this.typeUserId = typeUserId;
    }
public String getStaffName() {
        return staffName;
    }
public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getStaffId() {
        return staffId;
    }
public void setStaffId(String staffId) {
        this.staffId = staffId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
