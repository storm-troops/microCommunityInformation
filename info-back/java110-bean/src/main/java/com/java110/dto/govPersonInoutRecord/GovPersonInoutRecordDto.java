package com.java110.dto.govPersonInoutRecord;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 开门记录数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPersonInoutRecordDto extends PageDto implements Serializable {

    private String openTypeCd;
private String govCommunityId;
private String idCard;
private String recordId;
private String faceUrl;
private String locationId;
private String caId;
private String name;
private String tel;
private String state;
private String recordTypeCd;
    private String locationName;
    private String communityName;


    private Date createTime;

    private String statusCd = "0";


    public String getOpenTypeCd() {
        return openTypeCd;
    }
public void setOpenTypeCd(String openTypeCd) {
        this.openTypeCd = openTypeCd;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getRecordId() {
        return recordId;
    }
public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
public String getFaceUrl() {
        return faceUrl;
    }
public void setFaceUrl(String faceUrl) {
        this.faceUrl = faceUrl;
    }
public String getLocationId() {
        return locationId;
    }
public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getRecordTypeCd() {
        return recordTypeCd;
    }
public void setRecordTypeCd(String recordTypeCd) {
        this.recordTypeCd = recordTypeCd;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
}
