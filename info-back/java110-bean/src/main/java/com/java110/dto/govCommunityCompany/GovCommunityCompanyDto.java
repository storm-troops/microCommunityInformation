package com.java110.dto.govCommunityCompany;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 小区位置数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovCommunityCompanyDto extends PageDto implements Serializable {

    private String datasourceType;
private String govCompanyId;
private String govCommunityId;
private String caId;
private String govOpId;
private String relCd;


    private Date createTime;

    private String statusCd = "0";


    public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovCompanyId() {
        return govCompanyId;
    }
public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovOpId() {
        return govOpId;
    }
public void setGovOpId(String govOpId) {
        this.govOpId = govOpId;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
