package com.java110.dto.govVolunteerServ;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 服务领域数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovVolunteerServDto extends PageDto implements Serializable {

    private String servId;
private String caId;
private String name;
private String volunteerId;
private String ramark;
private String servFieldId;
    private String [] volunteerIds;
    private Date createTime;

    private String statusCd = "0";


    public String getServId() {
        return servId;
    }
public void setServId(String servId) {
        this.servId = servId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getVolunteerId() {
        return volunteerId;
    }
public void setVolunteerId(String volunteerId) {
        this.volunteerId = volunteerId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String[] getVolunteerIds() {
        return volunteerIds;
    }

    public void setVolunteerIds(String[] volunteerIds) {
        this.volunteerIds = volunteerIds;
    }

    public String getServFieldId() {
        return servFieldId;
    }

    public void setServFieldId(String servFieldId) {
        this.servFieldId = servFieldId;
    }
}
