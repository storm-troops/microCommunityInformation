package com.java110.dto.govHelpPolicy;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 帮扶政策数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHelpPolicyDto extends PageDto implements Serializable {

    private String helpBrief;
private String govHelpId;
private String helpTime;
private String helpName;
private String helpNameLike;
private String caId;


    private Date createTime;

    private String statusCd = "0";


    public String getHelpBrief() {
        return helpBrief;
    }
public void setHelpBrief(String helpBrief) {
        this.helpBrief = helpBrief;
    }
public String getGovHelpId() {
        return govHelpId;
    }
public void setGovHelpId(String govHelpId) {
        this.govHelpId = govHelpId;
    }
public String getHelpTime() {
        return helpTime;
    }
public void setHelpTime(String helpTime) {
        this.helpTime = helpTime;
    }
public String getHelpName() {
        return helpName;
    }
public void setHelpName(String helpName) {
        this.helpName = helpName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getHelpNameLike() {
        return helpNameLike;
    }

    public void setHelpNameLike(String helpNameLike) {
        this.helpNameLike = helpNameLike;
    }
}
