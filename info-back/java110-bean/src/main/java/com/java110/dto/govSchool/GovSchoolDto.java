package com.java110.dto.govSchool;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 学校数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovSchoolDto extends PageDto implements Serializable {

    private String masterTel;
private String safetyLeader;
private String schoolmaster;
private String schoolType;
private String leaderTel;
private String schoolAddress;
private String partSafetyLeader;
private String securityLeader;
private String partLeaderTel;
private String areaCode;
private String areaName;
private String securityPersonnelNum;
private String schoolId;
private String caId;
private String schoolName;
private String studentNum;
private String securityLeaderTel;


    private Date createTime;

    private String statusCd = "0";


    public String getMasterTel() {
        return masterTel;
    }
public void setMasterTel(String masterTel) {
        this.masterTel = masterTel;
    }
public String getSafetyLeader() {
        return safetyLeader;
    }
public void setSafetyLeader(String safetyLeader) {
        this.safetyLeader = safetyLeader;
    }
public String getSchoolmaster() {
        return schoolmaster;
    }
public void setSchoolmaster(String schoolmaster) {
        this.schoolmaster = schoolmaster;
    }
public String getSchoolType() {
        return schoolType;
    }
public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }
public String getLeaderTel() {
        return leaderTel;
    }
public void setLeaderTel(String leaderTel) {
        this.leaderTel = leaderTel;
    }
public String getSchoolAddress() {
        return schoolAddress;
    }
public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }
public String getPartSafetyLeader() {
        return partSafetyLeader;
    }
public void setPartSafetyLeader(String partSafetyLeader) {
        this.partSafetyLeader = partSafetyLeader;
    }
public String getSecurityLeader() {
        return securityLeader;
    }
public void setSecurityLeader(String securityLeader) {
        this.securityLeader = securityLeader;
    }
public String getPartLeaderTel() {
        return partLeaderTel;
    }
public void setPartLeaderTel(String partLeaderTel) {
        this.partLeaderTel = partLeaderTel;
    }
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getSecurityPersonnelNum() {
        return securityPersonnelNum;
    }
public void setSecurityPersonnelNum(String securityPersonnelNum) {
        this.securityPersonnelNum = securityPersonnelNum;
    }
public String getSchoolId() {
        return schoolId;
    }
public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSchoolName() {
        return schoolName;
    }
public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
public String getStudentNum() {
        return studentNum;
    }
public void setStudentNum(String studentNum) {
        this.studentNum = studentNum;
    }
public String getSecurityLeaderTel() {
        return securityLeaderTel;
    }
public void setSecurityLeaderTel(String securityLeaderTel) {
        this.securityLeaderTel = securityLeaderTel;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
