package com.java110.dto.govLabel;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 标签管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovLabelDto extends PageDto implements Serializable {

    public static final String LABLE_VICTIM_CD = "6006";//受害人标签
    public static final String LABLE_SUSPICION_CD = "6007";//嫌疑人标签
    public static final String LABLE_GRID_CD = "6008";//网格人员标签

    public static final String LABLE_DOCTOR_CD = "6011";//档案医生
    public static final String LABLE_HEALTHY_CD = "6012";//健康管理员
    public static final String LABLE_DOCTOR_OL_CD = "6013";//医生

    private String govLabelId;
private String caId;
private String labelType;
private String labelName;
private String labelCd;
private String [] labelCds;
private String ramark;


    private Date createTime;

    private String statusCd = "0";


    public String getGovLabelId() {
        return govLabelId;
    }
public void setGovLabelId(String govLabelId) {
        this.govLabelId = govLabelId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getLabelType() {
        return labelType;
    }
public void setLabelType(String labelType) {
        this.labelType = labelType;
    }
public String getLabelName() {
        return labelName;
    }
public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
public String getLabelCd() {
        return labelCd;
    }
public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String[] getLabelCds() {
        return labelCds;
    }

    public void setLabelCds(String[] labelCds) {
        this.labelCds = labelCds;
    }
}
