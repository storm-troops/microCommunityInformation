package com.java110.dto.govOwner;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 户籍管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovOwnerDto extends PageDto implements Serializable {

    private String ownerType;
private String idCard;
private String ownerTel;
private String roomId;
private String ramark;
private String ownerNum;
private String roomNum;
private String ownerName;
private String caId;
private String ownerAddress;
private String govOwnerId;
private String caName;
private String relCdName;
private String personName;
private String personTel;


    private Date createTime;

    private String statusCd = "0";


    public String getOwnerType() {
        return ownerType;
    }
public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getOwnerTel() {
        return ownerTel;
    }
public void setOwnerTel(String ownerTel) {
        this.ownerTel = ownerTel;
    }
public String getRoomId() {
        return roomId;
    }
public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getOwnerNum() {
        return ownerNum;
    }
public void setOwnerNum(String ownerNum) {
        this.ownerNum = ownerNum;
    }
public String getRoomNum() {
        return roomNum;
    }
public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }
public String getOwnerName() {
        return ownerName;
    }
public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getOwnerAddress() {
        return ownerAddress;
    }
public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }
public String getGovOwnerId() {
        return govOwnerId;
    }
public void setGovOwnerId(String govOwnerId) {
        this.govOwnerId = govOwnerId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }
    private String datasourceType;
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }

    public String getRelCdName() {
        return relCdName;
    }

    public void setRelCdName(String relCdName) {
        this.relCdName = relCdName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }
}
