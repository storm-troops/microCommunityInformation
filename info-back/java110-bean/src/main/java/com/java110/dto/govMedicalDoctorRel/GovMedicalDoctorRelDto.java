package com.java110.dto.govMedicalDoctorRel;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 档案医生数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMedicalDoctorRelDto extends PageDto implements Serializable {

    private String relId;
private String caId;
private String groupId;
private String name;
private String govDoctorId;
private String ramark;


    private Date createTime;

    private String statusCd = "0";


    public String getRelId() {
        return relId;
    }
public void setRelId(String relId) {
        this.relId = relId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGroupId() {
        return groupId;
    }
public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getGovDoctorId() {
        return govDoctorId;
    }
public void setGovDoctorId(String govDoctorId) {
        this.govDoctorId = govDoctorId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
