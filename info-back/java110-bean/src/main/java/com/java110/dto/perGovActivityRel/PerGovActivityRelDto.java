package com.java110.dto.perGovActivityRel;

import com.java110.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 生日记录关系数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class PerGovActivityRelDto extends PageDto implements Serializable {

    private String actRelId;
    private String govPersonId;
    private String caId;
    private String perActivitiesId;
    private String state;

    private Date createTime;

    private String statusCd = "0";
    private String oldId;
    private String personName;
    private String personTel;
    private String personAge;
    private String birthday;
    private String communityName;

    public String getActRelId() {
        return actRelId;
    }

    public void setActRelId(String actRelId) {
        this.actRelId = actRelId;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getPerActivitiesId() {
        return perActivitiesId;
    }

    public void setPerActivitiesId(String perActivitiesId) {
        this.perActivitiesId = perActivitiesId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getOldId() {
        return oldId;
    }

    public void setOldId(String oldId) {
        this.oldId = oldId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public String getPersonAge() {
        return personAge;
    }

    public void setPersonAge(String personAge) {
        this.personAge = personAge;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
}
