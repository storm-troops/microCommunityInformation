package com.java110.dto.govPartyMemberChange;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 党关系管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPartyMemberChangeDto extends PageDto implements Serializable {

    private String targetOrgName;
private String govMemberId;
private String chagneType;
private String ramark;
private String personName;
private String srcOrgId;
private String datasourceType;
private String targetOrgId;
private String caId;
private String govChangeId;
private String srcOrgName;
private String state;
private String caName;
private String stateName;

    private Date createTime;

    private String statusCd = "0";


    public String getTargetOrgName() {
        return targetOrgName;
    }
public void setTargetOrgName(String targetOrgName) {
        this.targetOrgName = targetOrgName;
    }
public String getGovMemberId() {
        return govMemberId;
    }
public void setGovMemberId(String govMemberId) {
        this.govMemberId = govMemberId;
    }
public String getChagneType() {
        return chagneType;
    }
public void setChagneType(String chagneType) {
        this.chagneType = chagneType;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getSrcOrgId() {
        return srcOrgId;
    }
public void setSrcOrgId(String srcOrgId) {
        this.srcOrgId = srcOrgId;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getTargetOrgId() {
        return targetOrgId;
    }
public void setTargetOrgId(String targetOrgId) {
        this.targetOrgId = targetOrgId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovChangeId() {
        return govChangeId;
    }
public void setGovChangeId(String govChangeId) {
        this.govChangeId = govChangeId;
    }
public String getSrcOrgName() {
        return srcOrgName;
    }
public void setSrcOrgName(String srcOrgName) {
        this.srcOrgName = srcOrgName;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
