package com.java110.dto.govActivityReply;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 活动评论数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovActivityReplyDto extends PageDto implements Serializable {

    private String parentReplyId;
private String replyId;
private String actId;
private String context;
private String sessionId;
private String userName;
private String userId;
private String preUserName;
private String preAuthorId;
private List<GovActivityReplyDto> childs;

    private Date createTime;

    private String statusCd = "0";


    public String getParentReplyId() {
        return parentReplyId;
    }
public void setParentReplyId(String parentReplyId) {
        this.parentReplyId = parentReplyId;
    }
public String getReplyId() {
        return replyId;
    }
public void setReplyId(String replyId) {
        this.replyId = replyId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getSessionId() {
        return sessionId;
    }
public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
public String getUserName() {
        return userName;
    }
public void setUserName(String userName) {
        this.userName = userName;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }
public String getPreUserName() {
        return preUserName;
    }
public void setPreUserName(String preUserName) {
        this.preUserName = preUserName;
    }
public String getPreAuthorId() {
        return preAuthorId;
    }
public void setPreAuthorId(String preAuthorId) {
        this.preAuthorId = preAuthorId;
    }

    public List<GovActivityReplyDto> getChilds() {
        return childs;
    }

    public void setChilds(List<GovActivityReplyDto> childs) {
        this.childs = childs;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
