package com.java110.dto.govActivity;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 活动数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovActivityDto extends PageDto implements Serializable {

    private String actAddress;
private String contactName;
private String contactLink;
private String caId;
private String typeId;
private String actId;
private String context;
private String actTime;
private String actName;
private String personCount;
    private String replyNum;
    private String likeNum;

    private Date createTime;

    private String statusCd = "0";


    public String getActAddress() {
        return actAddress;
    }
public void setActAddress(String actAddress) {
        this.actAddress = actAddress;
    }
public String getContactName() {
        return contactName;
    }
public void setContactName(String contactName) {
        this.contactName = contactName;
    }
public String getContactLink() {
        return contactLink;
    }
public void setContactLink(String contactLink) {
        this.contactLink = contactLink;
    }
public String getTypeId() {
    return typeId;
}
public void setTypeId(String typeId) {
    this.typeId = typeId;
}
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getActTime() {
        return actTime;
    }
public void setActTime(String actTime) {
        this.actTime = actTime;
    }
public String getActName() {
        return actName;
    }
public void setActName(String actName) {
        this.actName = actName;
    }
public String getPersonCount() {
        return personCount;
    }
public void setPersonCount(String personCount) {
        this.personCount = personCount;
    }

    public String getReplyNum() {
        return replyNum;
    }

    public void setReplyNum(String replyNum) {
        this.replyNum = replyNum;
    }

    public String getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(String likeNum) {
        this.likeNum = likeNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
