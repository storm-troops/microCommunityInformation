package com.java110.dto.govHealthTitleValue;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 体检项值数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHealthTitleValueDto extends PageDto implements Serializable {

    private String valueId;
private String titleId;
private String caId;
private String value;
private String seq;
    private String[] titleIds;

    private Date createTime;

    private String statusCd = "0";


    public String getValueId() {
        return valueId;
    }
public void setValueId(String valueId) {
        this.valueId = valueId;
    }
public String getTitleId() {
        return titleId;
    }
public void setTitleId(String titleId) {
        this.titleId = titleId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getValue() {
        return value;
    }
public void setValue(String value) {
        this.value = value;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String[] getTitleIds() {
        return titleIds;
    }

    public void setTitleIds(String[] titleIds) {
        this.titleIds = titleIds;
    }
}
