package com.java110.po.govMajorEvents;

import java.io.Serializable;
import java.util.Date;

public class GovMajorEventsPo implements Serializable {

    private String eventsName;
private String areaCode;
private String govEventsId;
private String caId;
private String typeId;
private String statusCd = "0";
private String levelCd;
private String eventsAddress;
private String ramark;
private String happenTime;
private String typeName;
private String levelName;
public String getEventsName() {
        return eventsName;
    }
public void setEventsName(String eventsName) {
        this.eventsName = eventsName;
    }
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getGovEventsId() {
        return govEventsId;
    }
public void setGovEventsId(String govEventsId) {
        this.govEventsId = govEventsId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getLevelCd() {
        return levelCd;
    }
public void setLevelCd(String levelCd) {
        this.levelCd = levelCd;
    }
public String getEventsAddress() {
        return eventsAddress;
    }
public void setEventsAddress(String eventsAddress) {
        this.eventsAddress = eventsAddress;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getHappenTime() {
        return happenTime;
    }
public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }
}
