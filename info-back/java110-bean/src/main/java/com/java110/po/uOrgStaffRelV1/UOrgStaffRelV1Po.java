package com.java110.po.uOrgStaffRelV1;

import java.io.Serializable;
import java.util.Date;

public class UOrgStaffRelV1Po implements Serializable {

    private String relId;
private String statusCd = "0";
private String storeId;
private String orgId;
private String staffId;
private String relCd;
private String bId;
public String getRelId() {
        return relId;
    }
public void setRelId(String relId) {
        this.relId = relId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getStoreId() {
        return storeId;
    }
public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
public String getStaffId() {
        return staffId;
    }
public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }
}
