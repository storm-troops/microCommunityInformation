package com.java110.po.govMentalDisorders;

import java.io.Serializable;
import java.util.Date;

public class GovMentalDisordersPo implements Serializable {

    private String emergencyTel;
private String address;
private String disordersReason;
private String caId;
private String emergencyPerson;
private String name;
private String tel;
private String disordersId;
private String disordersStartTime;
private String ramark;
public String getEmergencyTel() {
        return emergencyTel;
    }
public void setEmergencyTel(String emergencyTel) {
        this.emergencyTel = emergencyTel;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getDisordersReason() {
        return disordersReason;
    }
public void setDisordersReason(String disordersReason) {
        this.disordersReason = disordersReason;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEmergencyPerson() {
        return emergencyPerson;
    }
public void setEmergencyPerson(String emergencyPerson) {
        this.emergencyPerson = emergencyPerson;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getDisordersId() {
        return disordersId;
    }
public void setDisordersId(String disordersId) {
        this.disordersId = disordersId;
    }
public String getDisordersStartTime() {
        return disordersStartTime;
    }
public void setDisordersStartTime(String disordersStartTime) {
        this.disordersStartTime = disordersStartTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
