package com.java110.po.govActivityLikes;

import java.io.Serializable;
import java.util.Date;

public class GovActivityLikesPo implements Serializable {

    private String flag;
private String likeId;
private String actId;
private String statusCd = "0";
private String userName;
private String userId;
public String getFlag() {
        return flag;
    }
public void setFlag(String flag) {
        this.flag = flag;
    }
public String getLikeId() {
        return likeId;
    }
public void setLikeId(String likeId) {
        this.likeId = likeId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getUserName() {
        return userName;
    }
public void setUserName(String userName) {
        this.userName = userName;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
