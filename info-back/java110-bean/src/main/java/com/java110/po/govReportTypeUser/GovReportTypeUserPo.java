package com.java110.po.govReportTypeUser;

import java.io.Serializable;
import java.util.Date;

public class GovReportTypeUserPo implements Serializable {

    private String reportType;
private String caId;
private String typeUserId;
private String staffName;
private String remark;
private String statusCd = "0";
private String state;
private String staffId;
public String getReportType() {
        return reportType;
    }
public void setReportType(String reportType) {
        this.reportType = reportType;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeUserId() {
        return typeUserId;
    }
public void setTypeUserId(String typeUserId) {
        this.typeUserId = typeUserId;
    }
public String getStaffName() {
        return staffName;
    }
public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getStaffId() {
        return staffId;
    }
public void setStaffId(String staffId) {
        this.staffId = staffId;
    }



}
