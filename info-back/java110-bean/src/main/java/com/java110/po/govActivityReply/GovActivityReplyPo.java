package com.java110.po.govActivityReply;

import java.io.Serializable;
import java.util.Date;

public class GovActivityReplyPo implements Serializable {

    private String parentReplyId;
private String replyId;
private String actId;
private String context;
private String statusCd = "0";
private String sessionId;
private String userName;
private String userId;
private String preUserName;
private String preAuthorId;
public String getParentReplyId() {
        return parentReplyId;
    }
public void setParentReplyId(String parentReplyId) {
        this.parentReplyId = parentReplyId;
    }
public String getReplyId() {
        return replyId;
    }
public void setReplyId(String replyId) {
        this.replyId = replyId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getSessionId() {
        return sessionId;
    }
public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
public String getUserName() {
        return userName;
    }
public void setUserName(String userName) {
        this.userName = userName;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }
public String getPreUserName() {
        return preUserName;
    }
public void setPreUserName(String preUserName) {
        this.preUserName = preUserName;
    }
public String getPreAuthorId() {
        return preAuthorId;
    }
public void setPreAuthorId(String preAuthorId) {
        this.preAuthorId = preAuthorId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
