package com.java110.po.govDrug;

import java.io.Serializable;
import java.util.Date;

public class GovDrugPo implements Serializable {

    private String emergencyTel;
private String address;
private String caId;
private String emergencyPerson;
private String name;
private String drugId;
private String tel;
private String drugStartTime;
private String drugReason;
private String drugEndTime;
private String ramark;
public String getEmergencyTel() {
        return emergencyTel;
    }
public void setEmergencyTel(String emergencyTel) {
        this.emergencyTel = emergencyTel;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEmergencyPerson() {
        return emergencyPerson;
    }
public void setEmergencyPerson(String emergencyPerson) {
        this.emergencyPerson = emergencyPerson;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getDrugId() {
        return drugId;
    }
public void setDrugId(String drugId) {
        this.drugId = drugId;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getDrugStartTime() {
        return drugStartTime;
    }
public void setDrugStartTime(String drugStartTime) {
        this.drugStartTime = drugStartTime;
    }
public String getDrugReason() {
        return drugReason;
    }
public void setDrugReason(String drugReason) {
        this.drugReason = drugReason;
    }
public String getDrugEndTime() {
        return drugEndTime;
    }
public void setDrugEndTime(String drugEndTime) {
        this.drugEndTime = drugEndTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
