package com.java110.po.govCommunityLocation;

import java.io.Serializable;

public class GovCommunityLocationPo implements Serializable {

    private String datasourceType;
    private String locationId;
    private String govCommunityId;
    private String caId;
    private String name;
    private String statusCd = "0";

    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getGovCommunityId() {
        return govCommunityId;
    }

    public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }


}
