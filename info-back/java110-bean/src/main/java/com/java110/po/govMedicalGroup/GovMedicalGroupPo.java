package com.java110.po.govMedicalGroup;

import java.io.Serializable;
import java.util.Date;

public class GovMedicalGroupPo implements Serializable {

    private String hospitalId;
private String groupId;
private String caId;
private String name;
private String statusCd = "0";
private String seq;
private String ramark;
public String getHospitalId() {
        return hospitalId;
    }
public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }
public String getGroupId() {
        return groupId;
    }
public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
