package com.java110.po.govHealthTitleValue;

import java.io.Serializable;
import java.util.Date;

public class GovHealthTitleValuePo implements Serializable {

    private String valueId;
private String titleId;
private String caId;
private String statusCd = "0";
private String value;
private String seq;
public String getValueId() {
        return valueId;
    }
public void setValueId(String valueId) {
        this.valueId = valueId;
    }
public String getTitleId() {
        return titleId;
    }
public void setTitleId(String titleId) {
        this.titleId = titleId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getValue() {
        return value;
    }
public void setValue(String value) {
        this.value = value;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }



}
