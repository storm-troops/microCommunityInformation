package com.java110.po.govHomicideBasic;

import java.io.Serializable;
import java.util.Date;

public class GovHomicideBasicPo implements Serializable {

    private String suspicionName;
private String govHomicideId;
private String suspicionId;
private String statusCd = "0";
private String leadRamark;
private String ramark;
private String homicideName;
private String caId;
private String victimName;
private String homicideNum;
private String endTime;
private String happenTime;
private String victimId;
public String getSuspicionName() {
        return suspicionName;
    }
public void setSuspicionName(String suspicionName) {
        this.suspicionName = suspicionName;
    }
public String getGovHomicideId() {
        return govHomicideId;
    }
public void setGovHomicideId(String govHomicideId) {
        this.govHomicideId = govHomicideId;
    }
public String getSuspicionId() {
        return suspicionId;
    }
public void setSuspicionId(String suspicionId) {
        this.suspicionId = suspicionId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getLeadRamark() {
        return leadRamark;
    }
public void setLeadRamark(String leadRamark) {
        this.leadRamark = leadRamark;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getHomicideName() {
        return homicideName;
    }
public void setHomicideName(String homicideName) {
        this.homicideName = homicideName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getVictimName() {
        return victimName;
    }
public void setVictimName(String victimName) {
        this.victimName = victimName;
    }
public String getHomicideNum() {
        return homicideNum;
    }
public void setHomicideNum(String homicideNum) {
        this.homicideNum = homicideNum;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getHappenTime() {
        return happenTime;
    }
public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }
public String getVictimId() {
        return victimId;
    }
public void setVictimId(String victimId) {
        this.victimId = victimId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
