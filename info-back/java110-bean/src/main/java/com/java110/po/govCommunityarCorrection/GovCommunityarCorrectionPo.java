package com.java110.po.govCommunityarCorrection;

import java.io.Serializable;
import java.util.Date;

public class GovCommunityarCorrectionPo implements Serializable {

    private String correctionReason;
private String address;
private String caId;
private String name;
private String tel;
private String correctionEndTime;
private String correctionId;
private String correctionStartTime;
private String ramark;
public String getCorrectionReason() {
        return correctionReason;
    }
public void setCorrectionReason(String correctionReason) {
        this.correctionReason = correctionReason;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getCorrectionEndTime() {
        return correctionEndTime;
    }
public void setCorrectionEndTime(String correctionEndTime) {
        this.correctionEndTime = correctionEndTime;
    }
public String getCorrectionId() {
        return correctionId;
    }
public void setCorrectionId(String correctionId) {
        this.correctionId = correctionId;
    }
public String getCorrectionStartTime() {
        return correctionStartTime;
    }
public void setCorrectionStartTime(String correctionStartTime) {
        this.correctionStartTime = correctionStartTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
