package com.java110.po.govRenovationCheck;

import java.io.Serializable;
import java.util.Date;

public class GovRenovationCheckPo implements Serializable {

    private String datasourceType;
private String remediationEffect;
private String govRenovationId;
private String mainActivities;
private String remediationSummary;
private String caId;
private String effectEvaluation;
private String statusCd = "0";
private String govCheckId;
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getRemediationEffect() {
        return remediationEffect;
    }
public void setRemediationEffect(String remediationEffect) {
        this.remediationEffect = remediationEffect;
    }
public String getGovRenovationId() {
        return govRenovationId;
    }
public void setGovRenovationId(String govRenovationId) {
        this.govRenovationId = govRenovationId;
    }
public String getMainActivities() {
        return mainActivities;
    }
public void setMainActivities(String mainActivities) {
        this.mainActivities = mainActivities;
    }
public String getRemediationSummary() {
        return remediationSummary;
    }
public void setRemediationSummary(String remediationSummary) {
        this.remediationSummary = remediationSummary;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEffectEvaluation() {
        return effectEvaluation;
    }
public void setEffectEvaluation(String effectEvaluation) {
        this.effectEvaluation = effectEvaluation;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getGovCheckId() {
        return govCheckId;
    }
public void setGovCheckId(String govCheckId) {
        this.govCheckId = govCheckId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
