package com.java110.po.govHealthTerm;

import java.io.Serializable;
import java.util.Date;

public class GovHealthTermPo implements Serializable {

    private String termId;
private String caId;
private String statusCd = "0";
private String termName;
private String seq;
public String getTermId() {
        return termId;
    }
public void setTermId(String termId) {
        this.termId = termId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getTermName() {
        return termName;
    }
public void setTermName(String termName) {
        this.termName = termName;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
