package com.java110.po.govPersonDie;

import java.io.Serializable;
import java.util.Date;

public class GovPersonDiePo implements Serializable {

    private String govCommunityId;
private String dieId;
private String dieType;
private String contactPerson;
private String statusCd = "0";
private String contactTel;
private String ramark;
private String diePlace;
private String funeralPlace;
private String govPersonId;
private String caId;
private String startTime;
private String imgProve;
private String endTime;
private String personType;
private String dieTime;

private String userId;
private String personName;
private String idCard;
private String nation;

public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getDieId() {
        return dieId;
    }
public void setDieId(String dieId) {
        this.dieId = dieId;
    }
public String getDieType() {
        return dieType;
    }
public void setDieType(String dieType) {
        this.dieType = dieType;
    }
public String getContactPerson() {
        return contactPerson;
    }
public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getContactTel() {
        return contactTel;
    }
public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getDiePlace() {
        return diePlace;
    }
public void setDiePlace(String diePlace) {
        this.diePlace = diePlace;
    }
public String getFuneralPlace() {
        return funeralPlace;
    }
public void setFuneralPlace(String funeralPlace) {
        this.funeralPlace = funeralPlace;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getImgProve() {
        return imgProve;
    }
public void setImgProve(String imgProve) {
        this.imgProve = imgProve;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getPersonType() {
        return personType;
    }
public void setPersonType(String personType) {
        this.personType = personType;
    }
public String getDieTime() {
        return dieTime;
    }
public void setDieTime(String dieTime) {
        this.dieTime = dieTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
