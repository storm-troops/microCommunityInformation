package com.java110.po.govPersonInoutRecord;

import java.io.Serializable;
import java.util.Date;

public class GovPersonInoutRecordPo implements Serializable {

    private String openTypeCd;
private String govCommunityId;
private String idCard;
private String statusCd = "0";
private String recordId;
private String faceUrl;
private String locationId;
private String caId;
private String name;
private String tel;
private String state;
private String recordTypeCd;
public String getOpenTypeCd() {
        return openTypeCd;
    }
public void setOpenTypeCd(String openTypeCd) {
        this.openTypeCd = openTypeCd;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRecordId() {
        return recordId;
    }
public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
public String getFaceUrl() {
        return faceUrl;
    }
public void setFaceUrl(String faceUrl) {
        this.faceUrl = faceUrl;
    }
public String getLocationId() {
        return locationId;
    }
public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getRecordTypeCd() {
        return recordTypeCd;
    }
public void setRecordTypeCd(String recordTypeCd) {
        this.recordTypeCd = recordTypeCd;
    }



}
