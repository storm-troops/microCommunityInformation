package com.java110.po.govSchoolPeripheralPerson;

import java.io.Serializable;
import java.util.Date;

public class GovSchoolPeripheralPersonPo implements Serializable {

    private String isAttention;
private String govPersonId;
private String extentInjury;
private String schoolId;
private String caId;
private String remark;
private String statusCd = "0";
private String schPersonId;
private String schoolName;
    private String idCard;
    private String personName;
    private String personSex;
    private String personTel;
public String getIsAttention() {
        return isAttention;
    }
public void setIsAttention(String isAttention) {
        this.isAttention = isAttention;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getExtentInjury() {
        return extentInjury;
    }
public void setExtentInjury(String extentInjury) {
        this.extentInjury = extentInjury;
    }
public String getSchoolId() {
        return schoolId;
    }
public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getSchPersonId() {
        return schPersonId;
    }
public void setSchPersonId(String schPersonId) {
        this.schPersonId = schPersonId;
    }
public String getSchoolName() {
        return schoolName;
    }
public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
