package com.java110.po.govFollowupSurvey;

import java.io.Serializable;
import java.util.Date;

public class GovFollowupSurveyPo implements Serializable {

    private String surveyType;
private String surveyId;
private String referralReason;
private String surveyWay;
private String adrs;
private String medication;
private String remark;
private String statusCd = "0";
private String lifeStyleGuide;
private String surveyAdvice;
private String symptoms;
private String nextSurveyTime;
private String surveyDoctorId;
private String surveyTime;
private String drugCompliance;
private String govPersonId;
private String surveyDoctor;
private String caId;
private String referralDepartment;
private String surveyConclusion;
public String getSurveyType() {
        return surveyType;
    }
public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }
public String getSurveyId() {
        return surveyId;
    }
public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }
public String getReferralReason() {
        return referralReason;
    }
public void setReferralReason(String referralReason) {
        this.referralReason = referralReason;
    }
public String getSurveyWay() {
        return surveyWay;
    }
public void setSurveyWay(String surveyWay) {
        this.surveyWay = surveyWay;
    }
public String getAdrs() {
        return adrs;
    }
public void setAdrs(String adrs) {
        this.adrs = adrs;
    }
public String getMedication() {
        return medication;
    }
public void setMedication(String medication) {
        this.medication = medication;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getLifeStyleGuide() {
        return lifeStyleGuide;
    }
public void setLifeStyleGuide(String lifeStyleGuide) {
        this.lifeStyleGuide = lifeStyleGuide;
    }
public String getSurveyAdvice() {
        return surveyAdvice;
    }
public void setSurveyAdvice(String surveyAdvice) {
        this.surveyAdvice = surveyAdvice;
    }
public String getSymptoms() {
        return symptoms;
    }
public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }
public String getNextSurveyTime() {
        return nextSurveyTime;
    }
public void setNextSurveyTime(String nextSurveyTime) {
        this.nextSurveyTime = nextSurveyTime;
    }
public String getSurveyDoctorId() {
        return surveyDoctorId;
    }
public void setSurveyDoctorId(String surveyDoctorId) {
        this.surveyDoctorId = surveyDoctorId;
    }
public String getSurveyTime() {
        return surveyTime;
    }
public void setSurveyTime(String surveyTime) {
        this.surveyTime = surveyTime;
    }
public String getDrugCompliance() {
        return drugCompliance;
    }
public void setDrugCompliance(String drugCompliance) {
        this.drugCompliance = drugCompliance;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getSurveyDoctor() {
        return surveyDoctor;
    }
public void setSurveyDoctor(String surveyDoctor) {
        this.surveyDoctor = surveyDoctor;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getReferralDepartment() {
        return referralDepartment;
    }
public void setReferralDepartment(String referralDepartment) {
        this.referralDepartment = referralDepartment;
    }
public String getSurveyConclusion() {
        return surveyConclusion;
    }
public void setSurveyConclusion(String surveyConclusion) {
        this.surveyConclusion = surveyConclusion;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
