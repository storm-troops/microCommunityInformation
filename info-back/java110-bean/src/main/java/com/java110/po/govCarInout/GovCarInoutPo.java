package com.java110.po.govCarInout;

import java.io.Serializable;
import java.util.Date;

public class GovCarInoutPo implements Serializable {

    private String inoutTime;
private String govCommunityId;
private String idCard;
private String carNum;
private String remark;
private String statusCd = "0";
private String personName;
private String carImgUrl;
private String inoutId;
private String locationId;
private String caId;
private String state;
private String personLink;
public String getInoutTime() {
        return inoutTime;
    }
public void setInoutTime(String inoutTime) {
        this.inoutTime = inoutTime;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getCarNum() {
        return carNum;
    }
public void setCarNum(String carNum) {
        this.carNum = carNum;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getCarImgUrl() {
        return carImgUrl;
    }
public void setCarImgUrl(String carImgUrl) {
        this.carImgUrl = carImgUrl;
    }
public String getInoutId() {
        return inoutId;
    }
public void setInoutId(String inoutId) {
        this.inoutId = inoutId;
    }
public String getLocationId() {
        return locationId;
    }
public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }



}
