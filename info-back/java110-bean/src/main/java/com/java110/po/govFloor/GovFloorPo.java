package com.java110.po.govFloor;

import java.io.Serializable;
import java.util.Date;

public class GovFloorPo implements Serializable {

    private String layerCount;
private String govCommunityId;
private String unitCount;
private String floorType;
private String govFloorId;
private String statusCd = "0";
private String floorIcon;
private String oldFloorIcon;
private String floorNum;
private String ramark;
private String floorUse;
private String personName;
private String caId;
private String floorName;
private String floorArea;
private String personLink;
public String getLayerCount() {
        return layerCount;
    }
public void setLayerCount(String layerCount) {
        this.layerCount = layerCount;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getUnitCount() {
        return unitCount;
    }
public void setUnitCount(String unitCount) {
        this.unitCount = unitCount;
    }
public String getFloorType() {
        return floorType;
    }
public void setFloorType(String floorType) {
        this.floorType = floorType;
    }
public String getGovFloorId() {
        return govFloorId;
    }
public void setGovFloorId(String govFloorId) {
        this.govFloorId = govFloorId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getFloorIcon() {
        return floorIcon;
    }
public void setFloorIcon(String floorIcon) {
        this.floorIcon = floorIcon;
    }
public String getFloorNum() {
        return floorNum;
    }
public void setFloorNum(String floorNum) {
        this.floorNum = floorNum;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getFloorUse() {
        return floorUse;
    }
public void setFloorUse(String floorUse) {
        this.floorUse = floorUse;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getFloorName() {
        return floorName;
    }
public void setFloorName(String floorName) {
        this.floorName = floorName;
    }
public String getFloorArea() {
        return floorArea;
    }
public void setFloorArea(String floorArea) {
        this.floorArea = floorArea;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }

    public String getOldFloorIcon() {
        return oldFloorIcon;
    }

    public void setOldFloorIcon(String oldFloorIcon) {
        this.oldFloorIcon = oldFloorIcon;
    }
    private String datasourceType="999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
