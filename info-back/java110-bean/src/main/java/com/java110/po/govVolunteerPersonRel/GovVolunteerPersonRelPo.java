package com.java110.po.govVolunteerPersonRel;

import java.io.Serializable;
import java.util.Date;

public class GovVolunteerPersonRelPo implements Serializable {

    private String govPersonId;
private String serviceRecordId;
private String govCommunityId;
private String caId;
private String volunteerPersonRelId;
private String statusCd = "0";
private String volunteerId;
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getServiceRecordId() {
        return serviceRecordId;
    }
public void setServiceRecordId(String serviceRecordId) {
        this.serviceRecordId = serviceRecordId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getVolunteerPersonRelId() {
        return volunteerPersonRelId;
    }
public void setVolunteerPersonRelId(String volunteerPersonRelId) {
        this.volunteerPersonRelId = volunteerPersonRelId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getVolunteerId() {
        return volunteerId;
    }
public void setVolunteerId(String volunteerId) {
        this.volunteerId = volunteerId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
