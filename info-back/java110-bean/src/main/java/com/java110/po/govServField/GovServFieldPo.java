package com.java110.po.govServField;

import java.io.Serializable;
import java.util.Date;

public class GovServFieldPo implements Serializable {

    private String servId;
private String caId;
private String name;
private String statusCd = "0";
private String ramark;
private String isShow;
public String getServId() {
        return servId;
    }
public void setServId(String servId) {
        this.servId = servId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getIsShow() {
        return isShow;
    }
public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    private Date createTime;


     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
