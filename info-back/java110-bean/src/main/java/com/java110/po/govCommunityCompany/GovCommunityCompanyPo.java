package com.java110.po.govCommunityCompany;

import java.io.Serializable;
import java.util.Date;

public class GovCommunityCompanyPo implements Serializable {

    private String datasourceType="999999";
private String govCompanyId;
private String govCommunityId;
private String caId;
private String govOpId;
private String statusCd = "0";
private String relCd;
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovCompanyId() {
        return govCompanyId;
    }
public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovOpId() {
        return govOpId;
    }
public void setGovOpId(String govOpId) {
        this.govOpId = govOpId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }



}
