package com.java110.gov.bmo.govAreaRenovation;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govAreaRenovation.GovAreaRenovationDto;
public interface IGetGovAreaRenovationBMO {


    /**
     * 查询重点地区排查整治
     * add by wuxw
     * @param  govAreaRenovationDto
     * @return
     */
    ResponseEntity<String> get(GovAreaRenovationDto govAreaRenovationDto);


}
