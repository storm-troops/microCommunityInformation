package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovPetitionTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 信访类型表服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPetitionTypeServiceDaoImpl")
//@Transactional
public class GovPetitionTypeServiceDaoImpl extends BaseServiceDao implements IGovPetitionTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPetitionTypeServiceDaoImpl.class);





    /**
     * 保存信访类型表信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPetitionTypeInfo(Map info) throws DAOException {
        logger.debug("保存信访类型表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPetitionTypeServiceDaoImpl.saveGovPetitionTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存信访类型表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询信访类型表信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPetitionTypeInfo(Map info) throws DAOException {
        logger.debug("查询信访类型表信息 入参 info : {}",info);

        List<Map> businessGovPetitionTypeInfos = sqlSessionTemplate.selectList("govPetitionTypeServiceDaoImpl.getGovPetitionTypeInfo",info);

        return businessGovPetitionTypeInfos;
    }


    /**
     * 修改信访类型表信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPetitionTypeInfo(Map info) throws DAOException {
        logger.debug("修改信访类型表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPetitionTypeServiceDaoImpl.updateGovPetitionTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改信访类型表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询信访类型表数量
     * @param info 信访类型表信息
     * @return 信访类型表数量
     */
    @Override
    public int queryGovPetitionTypesCount(Map info) {
        logger.debug("查询信访类型表数据 入参 info : {}",info);

        List<Map> businessGovPetitionTypeInfos = sqlSessionTemplate.selectList("govPetitionTypeServiceDaoImpl.queryGovPetitionTypesCount", info);
        if (businessGovPetitionTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPetitionTypeInfos.get(0).get("count").toString());
    }


}
