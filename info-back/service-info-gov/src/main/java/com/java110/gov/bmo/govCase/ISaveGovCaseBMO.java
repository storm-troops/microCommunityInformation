package com.java110.gov.bmo.govCase;

import org.springframework.http.ResponseEntity;
import com.java110.po.govCase.GovCasePo;
public interface ISaveGovCaseBMO {


    /**
     * 添加命案基本信息
     * add by wuxw
     * @param govCasePo
     * @return
     */
    ResponseEntity<String> save(GovCasePo govCasePo);


}
