package com.java110.gov.bmo.govVolunteerServiceRecord;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteerServiceRecord.GovVolunteerServiceRecordPo;
public interface ISaveGovVolunteerServiceRecordBMO {


    /**
     * 添加服务记录表
     * add by wuxw
     * @param govVolunteerServiceRecordPo
     * @return
     */
    ResponseEntity<String> save(GovVolunteerServiceRecordPo govVolunteerServiceRecordPo,JSONArray oldPersons,JSONArray volunteers);


}
