package com.java110.gov.bmo.govHelpPolicy;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govHelpPolicy.GovHelpPolicyDto;
public interface IGetGovHelpPolicyBMO {


    /**
     * 查询帮扶政策
     * add by wuxw
     * @param  govHelpPolicyDto
     * @return
     */
    ResponseEntity<String> get(GovHelpPolicyDto govHelpPolicyDto);


}
