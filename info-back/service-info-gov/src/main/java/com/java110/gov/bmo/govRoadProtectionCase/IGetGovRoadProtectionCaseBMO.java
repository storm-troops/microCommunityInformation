package com.java110.gov.bmo.govRoadProtectionCase;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govRoadProtectionCase.GovRoadProtectionCaseDto;
public interface IGetGovRoadProtectionCaseBMO {


    /**
     * 查询路案事件
     * add by wuxw
     * @param  govRoadProtectionCaseDto
     * @return
     */
    ResponseEntity<String> get(GovRoadProtectionCaseDto govRoadProtectionCaseDto);


}
