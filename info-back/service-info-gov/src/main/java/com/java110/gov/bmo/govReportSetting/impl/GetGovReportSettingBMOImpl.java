package com.java110.gov.bmo.govReportSetting.impl;

import com.java110.dto.govReportSetting.GovReportSettingDto;
import com.java110.gov.bmo.govReportSetting.IGetGovReportSettingBMO;
import com.java110.intf.gov.IGovReportSettingInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovReportSettingBMOImpl")
public class GetGovReportSettingBMOImpl implements IGetGovReportSettingBMO {

    @Autowired
    private IGovReportSettingInnerServiceSMO govReportSettingInnerServiceSMOImpl;

    /**
     * @param govReportSettingDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovReportSettingDto govReportSettingDto) {


        int count = govReportSettingInnerServiceSMOImpl.queryGovReportSettingsCount(govReportSettingDto);

        List<GovReportSettingDto> govReportSettingDtos = null;
        if (count > 0) {
            govReportSettingDtos = govReportSettingInnerServiceSMOImpl.queryGovReportSettings(govReportSettingDto);
        } else {
            govReportSettingDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govReportSettingDto.getRow()), count, govReportSettingDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
