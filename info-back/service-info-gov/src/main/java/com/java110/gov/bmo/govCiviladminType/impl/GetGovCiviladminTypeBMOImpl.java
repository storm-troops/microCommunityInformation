package com.java110.gov.bmo.govCiviladminType.impl;

import com.java110.dto.govCiviladminType.GovCiviladminTypeDto;
import com.java110.gov.bmo.govCiviladminType.IGetGovCiviladminTypeBMO;
import com.java110.intf.gov.IGovCiviladminTypeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovCiviladminTypeBMOImpl")
public class GetGovCiviladminTypeBMOImpl implements IGetGovCiviladminTypeBMO {

    @Autowired
    private IGovCiviladminTypeInnerServiceSMO govCiviladminTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCiviladminTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCiviladminTypeDto govCiviladminTypeDto) {


        int count = govCiviladminTypeInnerServiceSMOImpl.queryGovCiviladminTypesCount(govCiviladminTypeDto);

        List<GovCiviladminTypeDto> govCiviladminTypeDtos = null;
        if (count > 0) {
            govCiviladminTypeDtos = govCiviladminTypeInnerServiceSMOImpl.queryGovCiviladminTypes(govCiviladminTypeDto);
        } else {
            govCiviladminTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCiviladminTypeDto.getRow()), count, govCiviladminTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
