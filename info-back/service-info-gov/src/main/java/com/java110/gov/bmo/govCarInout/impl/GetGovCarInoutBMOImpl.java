package com.java110.gov.bmo.govCarInout.impl;

import com.java110.dto.govCarInout.GovCarInoutDto;
import com.java110.gov.bmo.govCarInout.IGetGovCarInoutBMO;
import com.java110.intf.gov.IGovCarInoutInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovCarInoutBMOImpl")
public class GetGovCarInoutBMOImpl implements IGetGovCarInoutBMO {

    @Autowired
    private IGovCarInoutInnerServiceSMO govCarInoutInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCarInoutDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCarInoutDto govCarInoutDto) {


        int count = govCarInoutInnerServiceSMOImpl.queryGovCarInoutsCount(govCarInoutDto);

        List<GovCarInoutDto> govCarInoutDtos = null;
        if (count > 0) {
            govCarInoutDtos = govCarInoutInnerServiceSMOImpl.queryGovCarInouts(govCarInoutDto);
        } else {
            govCarInoutDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCarInoutDto.getRow()), count, govCarInoutDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
