package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovReportSettingServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 报事设置服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govReportSettingServiceDaoImpl")
//@Transactional
public class GovReportSettingServiceDaoImpl extends BaseServiceDao implements IGovReportSettingServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovReportSettingServiceDaoImpl.class);





    /**
     * 保存报事设置信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovReportSettingInfo(Map info) throws DAOException {
        logger.debug("保存报事设置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govReportSettingServiceDaoImpl.saveGovReportSettingInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存报事设置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询报事设置信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovReportSettingInfo(Map info) throws DAOException {
        logger.debug("查询报事设置信息 入参 info : {}",info);

        List<Map> businessGovReportSettingInfos = sqlSessionTemplate.selectList("govReportSettingServiceDaoImpl.getGovReportSettingInfo",info);

        return businessGovReportSettingInfos;
    }


    /**
     * 修改报事设置信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovReportSettingInfo(Map info) throws DAOException {
        logger.debug("修改报事设置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govReportSettingServiceDaoImpl.updateGovReportSettingInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改报事设置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询报事设置数量
     * @param info 报事设置信息
     * @return 报事设置数量
     */
    @Override
    public int queryGovReportSettingsCount(Map info) {
        logger.debug("查询报事设置数据 入参 info : {}",info);

        List<Map> businessGovReportSettingInfos = sqlSessionTemplate.selectList("govReportSettingServiceDaoImpl.queryGovReportSettingsCount", info);
        if (businessGovReportSettingInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovReportSettingInfos.get(0).get("count").toString());
    }


}
