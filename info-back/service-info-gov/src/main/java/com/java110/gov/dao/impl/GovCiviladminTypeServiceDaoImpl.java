package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovCiviladminTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 民政服务宣传类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCiviladminTypeServiceDaoImpl")
//@Transactional
public class GovCiviladminTypeServiceDaoImpl extends BaseServiceDao implements IGovCiviladminTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCiviladminTypeServiceDaoImpl.class);





    /**
     * 保存民政服务宣传类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCiviladminTypeInfo(Map info) throws DAOException {
        logger.debug("保存民政服务宣传类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCiviladminTypeServiceDaoImpl.saveGovCiviladminTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存民政服务宣传类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询民政服务宣传类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCiviladminTypeInfo(Map info) throws DAOException {
        logger.debug("查询民政服务宣传类型信息 入参 info : {}",info);

        List<Map> businessGovCiviladminTypeInfos = sqlSessionTemplate.selectList("govCiviladminTypeServiceDaoImpl.getGovCiviladminTypeInfo",info);

        return businessGovCiviladminTypeInfos;
    }


    /**
     * 修改民政服务宣传类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCiviladminTypeInfo(Map info) throws DAOException {
        logger.debug("修改民政服务宣传类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCiviladminTypeServiceDaoImpl.updateGovCiviladminTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改民政服务宣传类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询民政服务宣传类型数量
     * @param info 民政服务宣传类型信息
     * @return 民政服务宣传类型数量
     */
    @Override
    public int queryGovCiviladminTypesCount(Map info) {
        logger.debug("查询民政服务宣传类型数据 入参 info : {}",info);

        List<Map> businessGovCiviladminTypeInfos = sqlSessionTemplate.selectList("govCiviladminTypeServiceDaoImpl.queryGovCiviladminTypesCount", info);
        if (businessGovCiviladminTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCiviladminTypeInfos.get(0).get("count").toString());
    }


}
