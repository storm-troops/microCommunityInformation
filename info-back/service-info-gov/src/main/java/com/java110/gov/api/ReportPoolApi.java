package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.gov.bmo.reportPool.IDeleteReportPoolBMO;
import com.java110.gov.bmo.reportPool.IGetReportPoolBMO;
import com.java110.gov.bmo.reportPool.ISaveReportPoolBMO;
import com.java110.gov.bmo.reportPool.IUpdateReportPoolBMO;
import com.java110.po.reportPool.ReportPoolPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;


@RestController
@RequestMapping(value = "/reportPool")
public class ReportPoolApi {

    @Autowired
    private ISaveReportPoolBMO saveReportPoolBMOImpl;
    @Autowired
    private IUpdateReportPoolBMO updateReportPoolBMOImpl;
    @Autowired
    private IDeleteReportPoolBMO deleteReportPoolBMOImpl;

    @Autowired
    private IGetReportPoolBMO getReportPoolBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /reportPool/saveReportPool
     * @path /app/reportPool/saveReportPool
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveReportPool", method = RequestMethod.POST)
    public ResponseEntity<String> saveReportPool(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportType", "请求报文中未包含reportType");
        Assert.hasKeyAndValue(reqJson, "reportName", "请求报文中未包含reportName");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");


        ReportPoolPo reportPoolPo = BeanConvertUtil.covertBean(reqJson, ReportPoolPo.class);
        return saveReportPoolBMOImpl.save(reportPoolPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /reportPool/updateReportPool
     * @path /app/reportPool/updateReportPool
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateReportPool", method = RequestMethod.POST)
    public ResponseEntity<String> updateReportPool(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportType", "请求报文中未包含reportType");
        Assert.hasKeyAndValue(reqJson, "reportName", "请求报文中未包含reportName");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "reportId", "reportId不能为空");


        ReportPoolPo reportPoolPo = BeanConvertUtil.covertBean(reqJson, ReportPoolPo.class);
        return updateReportPoolBMOImpl.update(reportPoolPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /reportPool/deleteReportPool
     * @path /app/reportPool/deleteReportPool
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteReportPool", method = RequestMethod.POST)
    public ResponseEntity<String> deleteReportPool(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportId", "reportId不能为空");


        ReportPoolPo reportPoolPo = BeanConvertUtil.covertBean(reqJson, ReportPoolPo.class);
        return deleteReportPoolBMOImpl.delete(reportPoolPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /reportPool/queryReportPool
     * @path /app/reportPool/queryReportPool
     * @param
     * @return
     */
    @RequestMapping(value = "/queryReportPool", method = RequestMethod.GET)
    public ResponseEntity<String> queryReportPool(@RequestParam(value = "caId", required = false) String caId,
                                                  @RequestParam(value = "reportName" , required = false) String reportName,
                                                  @RequestParam(value = "reportType" , required = false) String reportType,
                                                  @RequestParam(value = "reportId" , required = false) String reportId,
                                                  @RequestParam(value = "tel" , required = false) String tel,
                                                  @RequestParam(value = "state" , required = false) String state,
                                                  @RequestParam(value = "states" , required = false) String states,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setPage(page);
        reportPoolDto.setRow(row);
        reportPoolDto.setCaId(caId);
        reportPoolDto.setuState(state);
        reportPoolDto.setReportName(reportName);
        reportPoolDto.setReportType(reportType);
        reportPoolDto.setReportId(reportId);
        reportPoolDto.setTel(tel);
        if (states != null && states.length() > 0) {
            String[] rstates = states.split(",");
            reportPoolDto.setStates(Arrays.asList(rstates));
        }
        return getReportPoolBMOImpl.get(reportPoolDto);
    }
    /**
     * 保存电话报事
     *
     * @param reqJson
     * @return
     * @serviceCode /reportPool/saveTelReportPool
     * @path /app/reportPool/saveTelReportPool
     */
    @RequestMapping(value = "/saveTelReportPool", method = RequestMethod.POST)
    public ResponseEntity<String> saveTelHousekeepingServ(
            @RequestHeader(value = "user-id") String userId,
            @RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportType", "请求报文中未包含reportType");
        Assert.hasKeyAndValue(reqJson, "reportName", "请求报文中未包含reportName");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "appointmentTime", "请求报文中未包含appointmentTime");

        ReportPoolPo reportPoolPo = BeanConvertUtil.covertBean(reqJson, ReportPoolPo.class);
        return saveReportPoolBMOImpl.saveTel(reportPoolPo, userId);
    }

    /**
     * 保存小程序报事
     *
     * @param reqJson
     * @return
     * @serviceCode /reportPool/saveTelReportPool
     * @path /app/reportPool/saveTelReportPool
     */
    @RequestMapping(value = "/savePhoneReportPool", method = RequestMethod.POST)
    public ResponseEntity<String> savePhoneReportPool(
            @RequestHeader(value = "user-id") String userId,
            @RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportType", "请求报文中未包含reportType");
        Assert.hasKeyAndValue(reqJson, "reportName", "请求报文中未包含reportName");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "appointmentTime", "请求报文中未包含appointmentTime");

        ReportPoolPo reportPoolPo = BeanConvertUtil.covertBean(reqJson, ReportPoolPo.class);
        JSONArray photos = reqJson.getJSONArray("photos");
        return saveReportPoolBMOImpl.savePhone(reportPoolPo, userId,photos);
    }


    /**
     * 保存电话报事
     *
     * @param
     * @return
     * @serviceCode /reportPool/quseryReportStaff
     * @path /app/reportPool/quseryReportStaff
     */
    @RequestMapping(value = "/quseryReportStaff", method = RequestMethod.GET)
    public ResponseEntity<String> quseryReportStaff(
            @RequestHeader(value = "user-id") String userId,
            @RequestParam(value = "caId") String caId,
            @RequestParam(value = "reportName" , required = false) String reportName,
            @RequestParam(value = "tel" , required = false) String tel,
            @RequestParam(value = "reportId" , required = false) String reportId,
            @RequestParam(value = "state" , required = false) String state,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "row") int row) {

        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setPage(page);
        reportPoolDto.setRow(row);
        reportPoolDto.setStaffId(userId);
        reportPoolDto.setCaId(caId);
        reportPoolDto.setReportName(reportName);
        reportPoolDto.setState(state);
        reportPoolDto.setReportId(reportId);
        reportPoolDto.setTel(tel);
        return getReportPoolBMOImpl.quseryReportStaff(reportPoolDto);
    }
    /**
     * 保存电话报事
     *
     * @param
     * @return
     * @serviceCode /reportPool/queryStaffFinishReport
     * @path /app/reportPool/queryStaffFinishReport
     */
    @RequestMapping(value = "/queryStaffFinishReport", method = RequestMethod.GET)
    public ResponseEntity<String> queryStaffFinishReport(
            @RequestHeader(value = "user-id") String userId,
            @RequestParam(value = "caId") String caId,
            @RequestParam(value = "reportName" , required = false) String reportName,
            @RequestParam(value = "tel" , required = false) String tel,
            @RequestParam(value = "reportId" , required = false) String reportId,
            @RequestParam(value = "state" , required = false) String state,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "row") int row) {
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setPage(page);
        reportPoolDto.setRow(row);
        reportPoolDto.setStaffId(userId);
        reportPoolDto.setCaId(caId);
        reportPoolDto.setState(state);
        reportPoolDto.setReportName(reportName);
        reportPoolDto.setReportId(reportId);
        reportPoolDto.setTel(tel);
        return getReportPoolBMOImpl.queryStaffFinishReport(reportPoolDto);
    }

    /**
     * 查询 家政已办
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /reportPool/quseryReportFinishStaffs
     * @path /app/reportPool/quseryReportFinishStaffs
     */
    @RequestMapping(value = "/quseryReportFinishStaffs", method = RequestMethod.GET)
    public ResponseEntity<String> quseryReportFinishStaffs(
            @RequestHeader(value = "user-id") String userId,
            @RequestParam(value = "caId") String caId,
            @RequestParam(value = "reportName" , required = false) String reportName,
            @RequestParam(value = "reportType" , required = false) String reportType,
            @RequestParam(value = "tel" , required = false) String tel,
            @RequestParam(value = "reportId" , required = false) String reportId,
            @RequestParam(value = "state" , required = false) String state,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "row") int row) {
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setPage(page);
        reportPoolDto.setRow(row);
        reportPoolDto.setStaffId(userId);
        reportPoolDto.setCaId(caId);
        reportPoolDto.setState(state);
        reportPoolDto.setReportName(reportName);
        reportPoolDto.setReportType(reportType);
        reportPoolDto.setReportId(reportId);
        reportPoolDto.setTel(tel);
        return getReportPoolBMOImpl.quseryReportFinishStaffs(reportPoolDto);
    }

    /**
     * 大屏查询报事工单
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /reportPool/quseryLargeReportCount
     * @path /app/reportPool/quseryLargeReportCount
     */
    @RequestMapping(value = "/quseryLargeReportCount", method = RequestMethod.GET)
    public ResponseEntity<String> quseryLargeReportCount(
            @RequestHeader(value = "user-id") String userId,
            @RequestParam(value = "caId",required = false) String caId,
            @RequestParam(value = "state" , required = false) String state,
            @RequestParam(value = "page",required = false) int page,
            @RequestParam(value = "row",required = false) int row) {
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setPage(page);
        reportPoolDto.setRow(row);
        reportPoolDto.setStaffId(userId);
        reportPoolDto.setCaId(caId);
        reportPoolDto.setState(state);
        return getReportPoolBMOImpl.quseryLargeReportCount(reportPoolDto);
    }
}
