package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govHelpPolicy.GovHelpPolicyDto;
import com.java110.po.govHelpPolicy.GovHelpPolicyPo;
import com.java110.gov.bmo.govHelpPolicy.IDeleteGovHelpPolicyBMO;
import com.java110.gov.bmo.govHelpPolicy.IGetGovHelpPolicyBMO;
import com.java110.gov.bmo.govHelpPolicy.ISaveGovHelpPolicyBMO;
import com.java110.gov.bmo.govHelpPolicy.IUpdateGovHelpPolicyBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHelpPolicy")
public class GovHelpPolicyApi {

    @Autowired
    private ISaveGovHelpPolicyBMO saveGovHelpPolicyBMOImpl;
    @Autowired
    private IUpdateGovHelpPolicyBMO updateGovHelpPolicyBMOImpl;
    @Autowired
    private IDeleteGovHelpPolicyBMO deleteGovHelpPolicyBMOImpl;

    @Autowired
    private IGetGovHelpPolicyBMO getGovHelpPolicyBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHelpPolicy/saveGovHelpPolicy
     * @path /app/govHelpPolicy/saveGovHelpPolicy
     */
    @RequestMapping(value = "/saveGovHelpPolicy", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHelpPolicy(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "helpName", "请求报文中未包含helpName" );
        Assert.hasKeyAndValue( reqJson, "helpTime", "请求报文中未包含helpTime" );


        GovHelpPolicyPo govHelpPolicyPo = BeanConvertUtil.covertBean( reqJson, GovHelpPolicyPo.class );
        return saveGovHelpPolicyBMOImpl.save( govHelpPolicyPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHelpPolicy/updateGovHelpPolicy
     * @path /app/govHelpPolicy/updateGovHelpPolicy
     */
    @RequestMapping(value = "/updateGovHelpPolicy", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHelpPolicy(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govHelpId", "请求报文中未包含govHelpId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "helpName", "请求报文中未包含helpName" );
        Assert.hasKeyAndValue( reqJson, "helpTime", "请求报文中未包含helpTime" );

        GovHelpPolicyPo govHelpPolicyPo = BeanConvertUtil.covertBean( reqJson, GovHelpPolicyPo.class );
        return updateGovHelpPolicyBMOImpl.update( govHelpPolicyPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHelpPolicy/deleteGovHelpPolicy
     * @path /app/govHelpPolicy/deleteGovHelpPolicy
     */
    @RequestMapping(value = "/deleteGovHelpPolicy", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHelpPolicy(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govHelpId", "govHelpId不能为空" );


        GovHelpPolicyPo govHelpPolicyPo = BeanConvertUtil.covertBean( reqJson, GovHelpPolicyPo.class );
        return deleteGovHelpPolicyBMOImpl.delete( govHelpPolicyPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govHelpPolicy/queryGovHelpPolicy
     * @path /app/govHelpPolicy/queryGovHelpPolicy
     */
    @RequestMapping(value = "/queryGovHelpPolicy", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHelpPolicy(@RequestParam(value = "caId",required = false) String caId,
                                                     @RequestParam(value = "helpNameLike",required = false) String helpNameLike,
                                                     @RequestParam(value = "page") int page,
                                                     @RequestParam(value = "row") int row) {

        GovHelpPolicyDto govHelpPolicyDto = new GovHelpPolicyDto();
        govHelpPolicyDto.setPage( page );
        govHelpPolicyDto.setRow( row );
        govHelpPolicyDto.setCaId( caId );
        govHelpPolicyDto.setHelpNameLike( helpNameLike );
        return getGovHelpPolicyBMOImpl.get( govHelpPolicyDto );
    }
}
