package com.java110.gov.bmo.govReportReturnVisit.impl;

import com.java110.gov.bmo.govReportReturnVisit.IGetGovReportReturnVisitBMO;
import com.java110.intf.gov.IGovReportReturnVisitInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govReportReturnVisit.GovReportReturnVisitDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovReportReturnVisitBMOImpl")
public class GetGovReportReturnVisitBMOImpl implements IGetGovReportReturnVisitBMO {

    @Autowired
    private IGovReportReturnVisitInnerServiceSMO govReportReturnVisitInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govReportReturnVisitDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovReportReturnVisitDto govReportReturnVisitDto) {


        int count = govReportReturnVisitInnerServiceSMOImpl.queryGovReportReturnVisitsCount(govReportReturnVisitDto);

        List<GovReportReturnVisitDto> govReportReturnVisitDtos = null;
        if (count > 0) {
            govReportReturnVisitDtos = govReportReturnVisitInnerServiceSMOImpl.queryGovReportReturnVisits(govReportReturnVisitDto);
        } else {
            govReportReturnVisitDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govReportReturnVisitDto.getRow()), count, govReportReturnVisitDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
