package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovMeetingListServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 会议列表服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMeetingListServiceDaoImpl")
//@Transactional
public class GovMeetingListServiceDaoImpl extends BaseServiceDao implements IGovMeetingListServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMeetingListServiceDaoImpl.class);





    /**
     * 保存会议列表信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMeetingListInfo(Map info) throws DAOException {
        logger.debug("保存会议列表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMeetingListServiceDaoImpl.saveGovMeetingListInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存会议列表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询会议列表信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMeetingListInfo(Map info) throws DAOException {
        logger.debug("查询会议列表信息 入参 info : {}",info);

        List<Map> businessGovMeetingListInfos = sqlSessionTemplate.selectList("govMeetingListServiceDaoImpl.getGovMeetingListInfo",info);

        return businessGovMeetingListInfos;
    }


    /**
     * 修改会议列表信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMeetingListInfo(Map info) throws DAOException {
        logger.debug("修改会议列表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMeetingListServiceDaoImpl.updateGovMeetingListInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改会议列表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询会议列表数量
     * @param info 会议列表信息
     * @return 会议列表数量
     */
    @Override
    public int queryGovMeetingListsCount(Map info) {
        logger.debug("查询会议列表数据 入参 info : {}",info);

        List<Map> businessGovMeetingListInfos = sqlSessionTemplate.selectList("govMeetingListServiceDaoImpl.queryGovMeetingListsCount", info);
        if (businessGovMeetingListInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMeetingListInfos.get(0).get("count").toString());
    }


}
