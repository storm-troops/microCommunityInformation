package com.java110.gov.bmo.govVolunteerServiceRecord;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteerServiceRecord.GovVolunteerServiceRecordPo;

public interface IUpdateGovVolunteerServiceRecordBMO {


    /**
     * 修改服务记录表
     * add by wuxw
     * @param govVolunteerServiceRecordPo
     * @return
     */
    ResponseEntity<String> update(GovVolunteerServiceRecordPo govVolunteerServiceRecordPo);


}
