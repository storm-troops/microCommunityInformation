package com.java110.gov.bmo.govRenovationCheck;

import org.springframework.http.ResponseEntity;
import com.java110.po.govRenovationCheck.GovRenovationCheckPo;
public interface ISaveGovRenovationCheckBMO {


    /**
     * 添加重点地区整治情况
     * add by wuxw
     * @param govRenovationCheckPo
     * @return
     */
    ResponseEntity<String> save(GovRenovationCheckPo govRenovationCheckPo);


}
