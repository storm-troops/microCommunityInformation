package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPartyMemberServiceDao;
import com.java110.dto.govPartyMember.GovPartyMemberDto;
import com.java110.intf.gov.IGovPartyMemberInnerServiceSMO;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 党员管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPartyMemberInnerServiceSMOImpl extends BaseServiceSMO implements IGovPartyMemberInnerServiceSMO {

    @Autowired
    private IGovPartyMemberServiceDao govPartyMemberServiceDaoImpl;


    @Override
    public int saveGovPartyMember(@RequestBody GovPartyMemberPo govPartyMemberPo) {
        int saveFlag = 1;
        govPartyMemberServiceDaoImpl.saveGovPartyMemberInfo(BeanConvertUtil.beanCovertMap(govPartyMemberPo));
        return saveFlag;
    }

     @Override
    public int updateGovPartyMember(@RequestBody  GovPartyMemberPo govPartyMemberPo) {
        int saveFlag = 1;
         govPartyMemberServiceDaoImpl.updateGovPartyMemberInfo(BeanConvertUtil.beanCovertMap(govPartyMemberPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPartyMember(@RequestBody  GovPartyMemberPo govPartyMemberPo) {
        int saveFlag = 1;
        govPartyMemberPo.setStatusCd("1");
        govPartyMemberServiceDaoImpl.updateGovPartyMemberInfo(BeanConvertUtil.beanCovertMap(govPartyMemberPo));
        return saveFlag;
    }

    @Override
    public List<GovPartyMemberDto> queryGovPartyMembers(@RequestBody  GovPartyMemberDto govPartyMemberDto) {

        //校验是否传了 分页信息

        int page = govPartyMemberDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPartyMemberDto.setPage((page - 1) * govPartyMemberDto.getRow());
        }

        List<GovPartyMemberDto> govPartyMembers = BeanConvertUtil.covertBeanList(govPartyMemberServiceDaoImpl.getGovPartyMemberInfo(BeanConvertUtil.beanCovertMap(govPartyMemberDto)), GovPartyMemberDto.class);

        return govPartyMembers;
    }


    @Override
    public int queryGovPartyMembersCount(@RequestBody GovPartyMemberDto govPartyMemberDto) {
        return govPartyMemberServiceDaoImpl.queryGovPartyMembersCount(BeanConvertUtil.beanCovertMap(govPartyMemberDto));    }

    public IGovPartyMemberServiceDao getGovPartyMemberServiceDaoImpl() {
        return govPartyMemberServiceDaoImpl;
    }

    public void setGovPartyMemberServiceDaoImpl(IGovPartyMemberServiceDao govPartyMemberServiceDaoImpl) {
        this.govPartyMemberServiceDaoImpl = govPartyMemberServiceDaoImpl;
    }
}
