package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovCaseServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 命案基本信息服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCaseServiceDaoImpl")
//@Transactional
public class GovCaseServiceDaoImpl extends BaseServiceDao implements IGovCaseServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCaseServiceDaoImpl.class);





    /**
     * 保存命案基本信息信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCaseInfo(Map info) throws DAOException {
        logger.debug("保存命案基本信息信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCaseServiceDaoImpl.saveGovCaseInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存命案基本信息信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询命案基本信息信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCaseInfo(Map info) throws DAOException {
        logger.debug("查询命案基本信息信息 入参 info : {}",info);

        List<Map> businessGovCaseInfos = sqlSessionTemplate.selectList("govCaseServiceDaoImpl.getGovCaseInfo",info);

        return businessGovCaseInfos;
    }


    /**
     * 修改命案基本信息信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCaseInfo(Map info) throws DAOException {
        logger.debug("修改命案基本信息信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCaseServiceDaoImpl.updateGovCaseInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改命案基本信息信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询命案基本信息数量
     * @param info 命案基本信息信息
     * @return 命案基本信息数量
     */
    @Override
    public int queryGovCasesCount(Map info) {
        logger.debug("查询命案基本信息数据 入参 info : {}",info);

        List<Map> businessGovCaseInfos = sqlSessionTemplate.selectList("govCaseServiceDaoImpl.queryGovCasesCount", info);
        if (businessGovCaseInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCaseInfos.get(0).get("count").toString());
    }


}
