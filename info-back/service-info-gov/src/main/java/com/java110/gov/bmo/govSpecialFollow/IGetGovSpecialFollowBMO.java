package com.java110.gov.bmo.govSpecialFollow;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govSpecialFollow.GovSpecialFollowDto;
public interface IGetGovSpecialFollowBMO {


    /**
     * 查询特殊人员跟进记录
     * add by wuxw
     * @param  govSpecialFollowDto
     * @return
     */
    ResponseEntity<String> get(GovSpecialFollowDto govSpecialFollowDto);


}
