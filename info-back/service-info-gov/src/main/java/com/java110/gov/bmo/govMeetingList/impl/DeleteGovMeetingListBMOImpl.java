package com.java110.gov.bmo.govMeetingList.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.gov.bmo.govMeetingList.IDeleteGovMeetingListBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.vo.ResultVo;
import com.java110.po.govMeetingList.GovMeetingListPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovMeetingListInnerServiceSMO;
import com.java110.dto.govMeetingList.GovMeetingListDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovMeetingListBMOImpl")
public class DeleteGovMeetingListBMOImpl implements IDeleteGovMeetingListBMO {

    @Autowired
    private IGovMeetingListInnerServiceSMO govMeetingListInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    /**
     * @param govMeetingListPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovMeetingListPo govMeetingListPo) {

        //删除原文件关系
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setObjId(govMeetingListPo.getGovMeetingId());
        fileRelPo.setRelType(FileRelDto.REL_MEETING_TYPE);
        int flag = fileRelInnerServiceSMOImpl.deleteFileRel(fileRelPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除失败");
        }
        flag = govMeetingListInnerServiceSMOImpl.deleteGovMeetingList(govMeetingListPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除失败");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "删除成功");
    }

}
