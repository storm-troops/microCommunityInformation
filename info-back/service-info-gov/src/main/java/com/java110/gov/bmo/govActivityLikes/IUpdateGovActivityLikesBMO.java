package com.java110.gov.bmo.govActivityLikes;
import org.springframework.http.ResponseEntity;
import com.java110.po.govActivityLikes.GovActivityLikesPo;

public interface IUpdateGovActivityLikesBMO {


    /**
     * 修改活动点赞
     * add by wuxw
     * @param govActivityLikesPo
     * @return
     */
    ResponseEntity<String> update(GovActivityLikesPo govActivityLikesPo);


}
