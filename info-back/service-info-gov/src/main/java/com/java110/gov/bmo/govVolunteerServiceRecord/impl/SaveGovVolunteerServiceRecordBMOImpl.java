package com.java110.gov.bmo.govVolunteerServiceRecord.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govVolunteerServiceRecord.ISaveGovVolunteerServiceRecordBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.gov.IGovVolunteerPersonRelInnerServiceSMO;
import com.java110.po.govVolunteer.GovVolunteerPo;
import com.java110.po.govVolunteerPersonRel.GovVolunteerPersonRelPo;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govVolunteerServiceRecord.GovVolunteerServiceRecordPo;
import com.java110.intf.gov.IGovVolunteerServiceRecordInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovVolunteerServiceRecordBMOImpl")
public class SaveGovVolunteerServiceRecordBMOImpl implements ISaveGovVolunteerServiceRecordBMO {

    @Autowired
    private IGovVolunteerServiceRecordInnerServiceSMO govVolunteerServiceRecordInnerServiceSMOImpl;

    @Autowired
    private IGovVolunteerPersonRelInnerServiceSMO govVolunteerPersonRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govVolunteerServiceRecordPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovVolunteerServiceRecordPo govVolunteerServiceRecordPo,JSONArray oldPersons,JSONArray volunteers) {
        govVolunteerServiceRecordPo.setServiceRecordId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_serviceRecordId));
        int flag = govVolunteerServiceRecordInnerServiceSMOImpl.saveGovVolunteerServiceRecord(govVolunteerServiceRecordPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }
        if (oldPersons != null && oldPersons.size() > 0) {
            for (int oldIndex = 0; oldIndex < oldPersons.size(); oldIndex++) {
                if (volunteers != null && volunteers.size() > 0) {
                    for (int vIndex = 0; vIndex < volunteers.size(); vIndex++) {
                        GovVolunteerPo  govVolunteerPo = BeanConvertUtil.covertBean(volunteers.get(vIndex), GovVolunteerPo.class);
                        PerGovActivityRelPo perGovActivityRelPo = BeanConvertUtil.covertBean(oldPersons.get(oldIndex), PerGovActivityRelPo.class);
                        GovVolunteerPersonRelPo govVolunteerPersonRelPo = new GovVolunteerPersonRelPo();
                        govVolunteerPersonRelPo.setVolunteerPersonRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_volunteerPersonRelId));
                        govVolunteerPersonRelPo.setGovPersonId(perGovActivityRelPo.getGovPersonId());
                        govVolunteerPersonRelPo.setCaId(perGovActivityRelPo.getCaId());
                        govVolunteerPersonRelPo.setGovCommunityId(perGovActivityRelPo.getGovCommunityId());
                        govVolunteerPersonRelPo.setServiceRecordId(govVolunteerServiceRecordPo.getServiceRecordId());
                        govVolunteerPersonRelPo.setVolunteerId(govVolunteerPo.getVolunteerId());
                        flag = govVolunteerPersonRelInnerServiceSMOImpl.saveGovVolunteerPersonRel(govVolunteerPersonRelPo);
                        if (flag < 1) {
                            throw new IllegalArgumentException("保存志愿服务人员失败");
                        }
                    }
                }
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");

    }

}
