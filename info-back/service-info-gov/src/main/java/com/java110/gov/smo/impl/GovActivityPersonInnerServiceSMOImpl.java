package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govActivityPerson.GovActivityPersonDto;
import com.java110.gov.dao.IGovActivityPersonServiceDao;
import com.java110.intf.gov.IGovActivityPersonInnerServiceSMO;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 活动报名人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivityPersonInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivityPersonInnerServiceSMO {

    @Autowired
    private IGovActivityPersonServiceDao govActivityPersonServiceDaoImpl;


    @Override
    public int saveGovActivityPerson(@RequestBody GovActivityPersonPo govActivityPersonPo) {
        int saveFlag = 1;
        govActivityPersonServiceDaoImpl.saveGovActivityPersonInfo(BeanConvertUtil.beanCovertMap(govActivityPersonPo));
        return saveFlag;
    }

     @Override
    public int updateGovActivityPerson(@RequestBody  GovActivityPersonPo govActivityPersonPo) {
        int saveFlag = 1;
         govActivityPersonServiceDaoImpl.updateGovActivityPersonInfo(BeanConvertUtil.beanCovertMap(govActivityPersonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivityPerson(@RequestBody  GovActivityPersonPo govActivityPersonPo) {
        int saveFlag = 1;
        govActivityPersonPo.setStatusCd("1");
        govActivityPersonServiceDaoImpl.updateGovActivityPersonInfo(BeanConvertUtil.beanCovertMap(govActivityPersonPo));
        return saveFlag;
    }

    @Override
    public List<GovActivityPersonDto> queryGovActivityPersons(@RequestBody  GovActivityPersonDto govActivityPersonDto) {

        //校验是否传了 分页信息

        int page = govActivityPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivityPersonDto.setPage((page - 1) * govActivityPersonDto.getRow());
        }

        List<GovActivityPersonDto> govActivityPersons = BeanConvertUtil.covertBeanList(govActivityPersonServiceDaoImpl.getGovActivityPersonInfo(BeanConvertUtil.beanCovertMap(govActivityPersonDto)), GovActivityPersonDto.class);

        return govActivityPersons;
    }


    @Override
    public int queryGovActivityPersonsCount(@RequestBody GovActivityPersonDto govActivityPersonDto) {
        return govActivityPersonServiceDaoImpl.queryGovActivityPersonsCount(BeanConvertUtil.beanCovertMap(govActivityPersonDto));    }

    public IGovActivityPersonServiceDao getGovActivityPersonServiceDaoImpl() {
        return govActivityPersonServiceDaoImpl;
    }

    public void setGovActivityPersonServiceDaoImpl(IGovActivityPersonServiceDao govActivityPersonServiceDaoImpl) {
        this.govActivityPersonServiceDaoImpl = govActivityPersonServiceDaoImpl;
    }
}
