package com.java110.gov.bmo.govGuideSubscribe.impl;

import com.java110.dto.govGuideSubscribe.GovGuideSubscribeDto;
import com.java110.gov.bmo.govGuideSubscribe.IGetGovGuideSubscribeBMO;
import com.java110.intf.gov.IGovGuideSubscribeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovGuideSubscribeBMOImpl")
public class GetGovGuideSubscribeBMOImpl implements IGetGovGuideSubscribeBMO {

    @Autowired
    private IGovGuideSubscribeInnerServiceSMO govGuideSubscribeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govGuideSubscribeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovGuideSubscribeDto govGuideSubscribeDto) {


        int count = govGuideSubscribeInnerServiceSMOImpl.queryGovGuideSubscribesCount(govGuideSubscribeDto);

        List<GovGuideSubscribeDto> govGuideSubscribeDtos = null;
        if (count > 0) {
            govGuideSubscribeDtos = govGuideSubscribeInnerServiceSMOImpl.queryGovGuideSubscribes(govGuideSubscribeDto);
        } else {
            govGuideSubscribeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govGuideSubscribeDto.getRow()), count, govGuideSubscribeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
