package com.java110.gov.bmo.govAdvert;
import com.alibaba.fastjson.JSONObject;
import com.java110.po.govAdvert.GovAdvertPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovAdvertBMO {


    /**
     * 修改广告
     * add by wuxw
     * @param reqJson
     * @return
     */
    ResponseEntity<String> update(JSONObject reqJson);


}
