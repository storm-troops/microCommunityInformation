package com.java110.gov.bmo.perGovActivities;
import org.springframework.http.ResponseEntity;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
public interface IGetPerGovActivitiesBMO {


    /**
     * 查询生日记录
     * add by wuxw
     * @param  perGovActivitiesDto
     * @return
     */
    ResponseEntity<String> get(PerGovActivitiesDto perGovActivitiesDto);


}
