package com.java110.gov.bmo.govActivities.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govActivities.IDeleteGovActivitiesBMO;
import com.java110.intf.gov.IGovActivitiesInnerServiceSMO;
import com.java110.po.govActivities.GovActivitiesPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovActivitiesBMOImpl")
public class DeleteGovActivitiesBMOImpl implements IDeleteGovActivitiesBMO {

    @Autowired
    private IGovActivitiesInnerServiceSMO govActivitiesInnerServiceSMOImpl;

    /**
     * @param govActivitiesPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovActivitiesPo govActivitiesPo) {

        int flag = govActivitiesInnerServiceSMOImpl.deleteGovActivities(govActivitiesPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
