package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govRenovationCheck.GovRenovationCheckDto;
import com.java110.po.govRenovationCheck.GovRenovationCheckPo;
import com.java110.gov.bmo.govRenovationCheck.IDeleteGovRenovationCheckBMO;
import com.java110.gov.bmo.govRenovationCheck.IGetGovRenovationCheckBMO;
import com.java110.gov.bmo.govRenovationCheck.ISaveGovRenovationCheckBMO;
import com.java110.gov.bmo.govRenovationCheck.IUpdateGovRenovationCheckBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govRenovationCheck")
public class GovRenovationCheckApi {

    @Autowired
    private ISaveGovRenovationCheckBMO saveGovRenovationCheckBMOImpl;
    @Autowired
    private IUpdateGovRenovationCheckBMO updateGovRenovationCheckBMOImpl;
    @Autowired
    private IDeleteGovRenovationCheckBMO deleteGovRenovationCheckBMOImpl;

    @Autowired
    private IGetGovRenovationCheckBMO getGovRenovationCheckBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRenovationCheck/saveGovRenovationCheck
     * @path /app/govRenovationCheck/saveGovRenovationCheck
     */
    @RequestMapping(value = "/saveGovRenovationCheck", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovRenovationCheck(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govRenovationId", "请求报文中未包含govRenovationId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "effectEvaluation", "请求报文中未包含effectEvaluation" );
        Assert.hasKeyAndValue( reqJson, "mainActivities", "请求报文中未包含mainActivities" );
        Assert.hasKeyAndValue( reqJson, "remediationEffect", "请求报文中未包含remediationEffect" );
        Assert.hasKeyAndValue( reqJson, "remediationSummary", "请求报文中未包含remediationSummary" );


        GovRenovationCheckPo govRenovationCheckPo = BeanConvertUtil.covertBean( reqJson, GovRenovationCheckPo.class );
        govRenovationCheckPo.setDatasourceType( "999999" );
        return saveGovRenovationCheckBMOImpl.save( govRenovationCheckPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRenovationCheck/updateGovRenovationCheck
     * @path /app/govRenovationCheck/updateGovRenovationCheck
     */
    @RequestMapping(value = "/updateGovRenovationCheck", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovRenovationCheck(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govCheckId", "请求报文中未包含govCheckId" );
        Assert.hasKeyAndValue( reqJson, "govRenovationId", "请求报文中未包含govRenovationId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "effectEvaluation", "请求报文中未包含effectEvaluation" );
        Assert.hasKeyAndValue( reqJson, "mainActivities", "请求报文中未包含mainActivities" );
        Assert.hasKeyAndValue( reqJson, "remediationEffect", "请求报文中未包含remediationEffect" );
        Assert.hasKeyAndValue( reqJson, "remediationSummary", "请求报文中未包含remediationSummary" );


        GovRenovationCheckPo govRenovationCheckPo = BeanConvertUtil.covertBean( reqJson, GovRenovationCheckPo.class );
        return updateGovRenovationCheckBMOImpl.update( govRenovationCheckPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRenovationCheck/deleteGovRenovationCheck
     * @path /app/govRenovationCheck/deleteGovRenovationCheck
     */
    @RequestMapping(value = "/deleteGovRenovationCheck", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovRenovationCheck(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govCheckId", "govCheckId不能为空" );


        GovRenovationCheckPo govRenovationCheckPo = BeanConvertUtil.covertBean( reqJson, GovRenovationCheckPo.class );
        return deleteGovRenovationCheckBMOImpl.delete( govRenovationCheckPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govRenovationCheck/queryGovRenovationCheck
     * @path /app/govRenovationCheck/queryGovRenovationCheck
     */
    @RequestMapping(value = "/queryGovRenovationCheck", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovRenovationCheck(@RequestParam(value = "caId") String caId,
                                                          @RequestParam(value = "page") int page,
                                                          @RequestParam(value = "row") int row) {
        GovRenovationCheckDto govRenovationCheckDto = new GovRenovationCheckDto();
        govRenovationCheckDto.setPage( page );
        govRenovationCheckDto.setRow( row );
        govRenovationCheckDto.setCaId( caId );
        return getGovRenovationCheckBMOImpl.get( govRenovationCheckDto );
    }
}
