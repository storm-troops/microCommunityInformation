package com.java110.gov.bmo.govReportSetting.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govReportSetting.IUpdateGovReportSettingBMO;
import com.java110.intf.gov.IGovReportSettingInnerServiceSMO;
import com.java110.po.govReportSetting.GovReportSettingPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovReportSettingBMOImpl")
public class UpdateGovReportSettingBMOImpl implements IUpdateGovReportSettingBMO {

    @Autowired
    private IGovReportSettingInnerServiceSMO govReportSettingInnerServiceSMOImpl;

    /**
     * @param govReportSettingPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovReportSettingPo govReportSettingPo) {

        int flag = govReportSettingInnerServiceSMOImpl.updateGovReportSetting(govReportSettingPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
