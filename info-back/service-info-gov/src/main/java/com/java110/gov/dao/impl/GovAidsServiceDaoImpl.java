package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovAidsServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 艾滋病者服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govAidsServiceDaoImpl")
//@Transactional
public class GovAidsServiceDaoImpl extends BaseServiceDao implements IGovAidsServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovAidsServiceDaoImpl.class);





    /**
     * 保存艾滋病者信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovAidsInfo(Map info) throws DAOException {
        logger.debug("保存艾滋病者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govAidsServiceDaoImpl.saveGovAidsInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存艾滋病者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询艾滋病者信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovAidsInfo(Map info) throws DAOException {
        logger.debug("查询艾滋病者信息 入参 info : {}",info);

        List<Map> businessGovAidsInfos = sqlSessionTemplate.selectList("govAidsServiceDaoImpl.getGovAidsInfo",info);

        return businessGovAidsInfos;
    }


    /**
     * 修改艾滋病者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovAidsInfo(Map info) throws DAOException {
        logger.debug("修改艾滋病者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govAidsServiceDaoImpl.updateGovAidsInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改艾滋病者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询艾滋病者数量
     * @param info 艾滋病者信息
     * @return 艾滋病者数量
     */
    @Override
    public int queryGovAidssCount(Map info) {
        logger.debug("查询艾滋病者数据 入参 info : {}",info);

        List<Map> businessGovAidsInfos = sqlSessionTemplate.selectList("govAidsServiceDaoImpl.queryGovAidssCount", info);
        if (businessGovAidsInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovAidsInfos.get(0).get("count").toString());
    }


}
