package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
import com.java110.po.govMentalDisorders.GovMentalDisordersPo;
import com.java110.gov.bmo.govMentalDisorders.IDeleteGovMentalDisordersBMO;
import com.java110.gov.bmo.govMentalDisorders.IGetGovMentalDisordersBMO;
import com.java110.gov.bmo.govMentalDisorders.ISaveGovMentalDisordersBMO;
import com.java110.gov.bmo.govMentalDisorders.IUpdateGovMentalDisordersBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMentalDisorders")
public class GovMentalDisordersApi {

    @Autowired
    private ISaveGovMentalDisordersBMO saveGovMentalDisordersBMOImpl;
    @Autowired
    private IUpdateGovMentalDisordersBMO updateGovMentalDisordersBMOImpl;
    @Autowired
    private IDeleteGovMentalDisordersBMO deleteGovMentalDisordersBMOImpl;

    @Autowired
    private IGetGovMentalDisordersBMO getGovMentalDisordersBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govMentalDisorders/saveGovMentalDisorders
     * @path /app/govMentalDisorders/saveGovMentalDisorders
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovMentalDisorders", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMentalDisorders(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "disordersStartTime", "请求报文中未包含disordersStartTime");
        Assert.hasKeyAndValue(reqJson, "disordersReason", "请求报文中未包含disordersReason");
        Assert.hasKeyAndValue(reqJson, "emergencyPerson", "请求报文中未包含emergencyPerson");
        Assert.hasKeyAndValue(reqJson, "emergencyTel", "请求报文中未包含emergencyTel");


        GovMentalDisordersPo govMentalDisordersPo = BeanConvertUtil.covertBean(reqJson, GovMentalDisordersPo.class);
        return saveGovMentalDisordersBMOImpl.save(govMentalDisordersPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govMentalDisorders/updateGovMentalDisorders
     * @path /app/govMentalDisorders/updateGovMentalDisorders
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovMentalDisorders", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMentalDisorders(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "disordersId", "请求报文中未包含disordersId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "disordersStartTime", "请求报文中未包含disordersStartTime");
        Assert.hasKeyAndValue(reqJson, "disordersReason", "请求报文中未包含disordersReason");
        Assert.hasKeyAndValue(reqJson, "emergencyPerson", "请求报文中未包含emergencyPerson");
        Assert.hasKeyAndValue(reqJson, "emergencyTel", "请求报文中未包含emergencyTel");

        GovMentalDisordersPo govMentalDisordersPo = BeanConvertUtil.covertBean(reqJson, GovMentalDisordersPo.class);
        return updateGovMentalDisordersBMOImpl.update(govMentalDisordersPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govMentalDisorders/deleteGovMentalDisorders
     * @path /app/govMentalDisorders/deleteGovMentalDisorders
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovMentalDisorders", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMentalDisorders(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "disordersId", "disordersId不能为空");


        GovMentalDisordersPo govMentalDisordersPo = BeanConvertUtil.covertBean(reqJson, GovMentalDisordersPo.class);
        return deleteGovMentalDisordersBMOImpl.delete(govMentalDisordersPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govMentalDisorders/queryGovMentalDisorders
     * @path /app/govMentalDisorders/queryGovMentalDisorders
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovMentalDisorders", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMentalDisorders(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "disordersId" , required = false) String disordersId,
                                                      @RequestParam(value = "name" , required = false) String name,
                                                      @RequestParam(value = "tel" , required = false) String tel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovMentalDisordersDto govMentalDisordersDto = new GovMentalDisordersDto();
        govMentalDisordersDto.setPage(page);
        govMentalDisordersDto.setRow(row);
        govMentalDisordersDto.setCaId(caId);
        govMentalDisordersDto.setName(name);
        govMentalDisordersDto.setTel(tel);
        govMentalDisordersDto.setDisordersId(disordersId);
        return getGovMentalDisordersBMOImpl.get(govMentalDisordersDto);
    }
}
