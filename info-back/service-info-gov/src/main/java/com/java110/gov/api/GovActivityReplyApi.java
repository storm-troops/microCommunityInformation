package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivityReply.GovActivityReplyDto;
import com.java110.po.govActivityReply.GovActivityReplyPo;
import com.java110.gov.bmo.govActivityReply.IDeleteGovActivityReplyBMO;
import com.java110.gov.bmo.govActivityReply.IGetGovActivityReplyBMO;
import com.java110.gov.bmo.govActivityReply.ISaveGovActivityReplyBMO;
import com.java110.gov.bmo.govActivityReply.IUpdateGovActivityReplyBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govActivityReply")
public class GovActivityReplyApi {

    @Autowired
    private ISaveGovActivityReplyBMO saveGovActivityReplyBMOImpl;
    @Autowired
    private IUpdateGovActivityReplyBMO updateGovActivityReplyBMOImpl;
    @Autowired
    private IDeleteGovActivityReplyBMO deleteGovActivityReplyBMOImpl;
    @Autowired
    private IGetGovActivityReplyBMO getGovActivityReplyBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govActivityReply/saveGovActivityReply
     * @path /app/govActivityReply/saveGovActivityReply
     */
    @RequestMapping(value = "/saveGovActivityReply", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivityReply(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actId", "请求报文中未包含actId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "userName", "请求报文中未包含userName");

        GovActivityReplyPo govActivityReplyPo = BeanConvertUtil.covertBean(reqJson, GovActivityReplyPo.class);
        return saveGovActivityReplyBMOImpl.save(govActivityReplyPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govActivityReply/updateGovActivityReply
     * @path /app/govActivityReply/updateGovActivityReply
     */
    @RequestMapping(value = "/updateGovActivityReply", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivityReply(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actId", "请求报文中未包含actId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "userName", "请求报文中未包含userName");

        GovActivityReplyPo govActivityReplyPo = BeanConvertUtil.covertBean(reqJson, GovActivityReplyPo.class);
        return updateGovActivityReplyBMOImpl.update(govActivityReplyPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govActivityReply/deleteGovActivityReply
     * @path /app/govActivityReply/deleteGovActivityReply
     */
    @RequestMapping(value = "/deleteGovActivityReply", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivityReply(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "replyId", "replyId不能为空");

        GovActivityReplyPo govActivityReplyPo = BeanConvertUtil.covertBean(reqJson, GovActivityReplyPo.class);
        return deleteGovActivityReplyBMOImpl.delete(govActivityReplyPo);
    }

    /**
     * 微信删除消息模板
     * @return
     * @serviceCode /govActivityReply/queryGovActivityReply
     * @path /app/govActivityReply/queryGovActivityReply
     */
    @RequestMapping(value = "/queryGovActivityReply", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivityReply(
                                                        @RequestParam(value = "actId" , required = false) String actId,
                                                        @RequestParam(value = "userId" , required = false) String userId,
                                                        @RequestParam(value = "userName" , required = false) String userName,
                                                        @RequestParam(value = "sessionId" , required = false) String sessionId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovActivityReplyDto govActivityReplyDto = new GovActivityReplyDto();
        govActivityReplyDto.setPage(page);
        govActivityReplyDto.setRow(row);
        govActivityReplyDto.setUserId(userId);
        govActivityReplyDto.setActId(actId);
        govActivityReplyDto.setUserName(userName);
        govActivityReplyDto.setSessionId(sessionId);
        return getGovActivityReplyBMOImpl.get(govActivityReplyDto);
    }
}
