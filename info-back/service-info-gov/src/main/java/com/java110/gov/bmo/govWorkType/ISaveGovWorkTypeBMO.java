package com.java110.gov.bmo.govWorkType;

import com.java110.po.govWorkType.GovWorkTypePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovWorkTypeBMO {


    /**
     * 添加党内职务
     * add by wuxw
     * @param govWorkTypePo
     * @return
     */
    ResponseEntity<String> save(GovWorkTypePo govWorkTypePo);


}
