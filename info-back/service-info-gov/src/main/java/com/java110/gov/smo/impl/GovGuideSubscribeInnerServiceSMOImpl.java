package com.java110.gov.smo.impl;

import com.java110.gov.dao.IGovGuideSubscribeServiceDao;
import com.java110.intf.gov.IGovGuideSubscribeInnerServiceSMO;
import com.java110.dto.govGuideSubscribe.GovGuideSubscribeDto;
import com.java110.po.govGuideSubscribe.GovGuideSubscribePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 办事预约内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovGuideSubscribeInnerServiceSMOImpl extends BaseServiceSMO implements IGovGuideSubscribeInnerServiceSMO {

    @Autowired
    private IGovGuideSubscribeServiceDao govGuideSubscribeServiceDaoImpl;


    @Override
    public int saveGovGuideSubscribe(@RequestBody GovGuideSubscribePo govGuideSubscribePo) {
        int saveFlag = 1;
        govGuideSubscribeServiceDaoImpl.saveGovGuideSubscribeInfo(BeanConvertUtil.beanCovertMap(govGuideSubscribePo));
        return saveFlag;
    }

     @Override
    public int updateGovGuideSubscribe(@RequestBody  GovGuideSubscribePo govGuideSubscribePo) {
        int saveFlag = 1;
         govGuideSubscribeServiceDaoImpl.updateGovGuideSubscribeInfo(BeanConvertUtil.beanCovertMap(govGuideSubscribePo));
        return saveFlag;
    }

     @Override
    public int deleteGovGuideSubscribe(@RequestBody  GovGuideSubscribePo govGuideSubscribePo) {
        int saveFlag = 1;
        govGuideSubscribePo.setStatusCd("1");
        govGuideSubscribeServiceDaoImpl.updateGovGuideSubscribeInfo(BeanConvertUtil.beanCovertMap(govGuideSubscribePo));
        return saveFlag;
    }

    @Override
    public List<GovGuideSubscribeDto> queryGovGuideSubscribes(@RequestBody  GovGuideSubscribeDto govGuideSubscribeDto) {

        //校验是否传了 分页信息

        int page = govGuideSubscribeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govGuideSubscribeDto.setPage((page - 1) * govGuideSubscribeDto.getRow());
        }

        List<GovGuideSubscribeDto> govGuideSubscribes = BeanConvertUtil.covertBeanList(govGuideSubscribeServiceDaoImpl.getGovGuideSubscribeInfo(BeanConvertUtil.beanCovertMap(govGuideSubscribeDto)), GovGuideSubscribeDto.class);

        return govGuideSubscribes;
    }


    @Override
    public int queryGovGuideSubscribesCount(@RequestBody GovGuideSubscribeDto govGuideSubscribeDto) {
        return govGuideSubscribeServiceDaoImpl.queryGovGuideSubscribesCount(BeanConvertUtil.beanCovertMap(govGuideSubscribeDto));    }

    public IGovGuideSubscribeServiceDao getGovGuideSubscribeServiceDaoImpl() {
        return govGuideSubscribeServiceDaoImpl;
    }

    public void setGovGuideSubscribeServiceDaoImpl(IGovGuideSubscribeServiceDao govGuideSubscribeServiceDaoImpl) {
        this.govGuideSubscribeServiceDaoImpl = govGuideSubscribeServiceDaoImpl;
    }
}
