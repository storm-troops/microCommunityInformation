package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govAdvert.GovAdvertDto;
import com.java110.gov.dao.IGovAdvertServiceDao;
import com.java110.intf.gov.IGovAdvertInnerServiceSMO;
import com.java110.po.govAdvert.GovAdvertPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 广告内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovAdvertInnerServiceSMOImpl extends BaseServiceSMO implements IGovAdvertInnerServiceSMO {

    @Autowired
    private IGovAdvertServiceDao govAdvertServiceDaoImpl;


    @Override
    public int saveGovAdvert(@RequestBody GovAdvertPo govAdvertPo) {
        int saveFlag = 1;
        govAdvertServiceDaoImpl.saveGovAdvertInfo(BeanConvertUtil.beanCovertMap(govAdvertPo));
        return saveFlag;
    }

     @Override
    public int updateGovAdvert(@RequestBody  GovAdvertPo govAdvertPo) {
        int saveFlag = 1;
         govAdvertServiceDaoImpl.updateGovAdvertInfo(BeanConvertUtil.beanCovertMap(govAdvertPo));
        return saveFlag;
    }

     @Override
    public int deleteGovAdvert(@RequestBody  GovAdvertPo govAdvertPo) {
        int saveFlag = 1;
        govAdvertPo.setStatusCd("1");
        govAdvertServiceDaoImpl.updateGovAdvertInfo(BeanConvertUtil.beanCovertMap(govAdvertPo));
        return saveFlag;
    }

    @Override
    public List<GovAdvertDto> queryGovAdverts(@RequestBody  GovAdvertDto govAdvertDto) {

        //校验是否传了 分页信息

        int page = govAdvertDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govAdvertDto.setPage((page - 1) * govAdvertDto.getRow());
        }

        List<GovAdvertDto> govAdverts = BeanConvertUtil.covertBeanList(govAdvertServiceDaoImpl.getGovAdvertInfo(BeanConvertUtil.beanCovertMap(govAdvertDto)), GovAdvertDto.class);

        return govAdverts;
    }

    @Override
    public List<GovAdvertDto> queryAdvertItmes(@RequestBody  GovAdvertDto govAdvertDto) {

        //校验是否传了 分页信息

        int page = govAdvertDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govAdvertDto.setPage((page - 1) * govAdvertDto.getRow());
        }

        List<GovAdvertDto> govAdverts = BeanConvertUtil.covertBeanList(govAdvertServiceDaoImpl.getGovAdvertItmesInfo(BeanConvertUtil.beanCovertMap(govAdvertDto)), GovAdvertDto.class);

        return govAdverts;
    }



    @Override
    public int queryGovAdvertsCount(@RequestBody GovAdvertDto govAdvertDto) {
        return govAdvertServiceDaoImpl.queryGovAdvertsCount(BeanConvertUtil.beanCovertMap(govAdvertDto));    }

    public IGovAdvertServiceDao getGovAdvertServiceDaoImpl() {
        return govAdvertServiceDaoImpl;
    }

    public void setGovAdvertServiceDaoImpl(IGovAdvertServiceDao govAdvertServiceDaoImpl) {
        this.govAdvertServiceDaoImpl = govAdvertServiceDaoImpl;
    }
}
