package com.java110.gov.bmo.govPartyOrgContext.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govPartyOrgContext.ISaveGovPartyOrgContextBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovPartyOrgContextBMOImpl")
public class SaveGovPartyOrgContextBMOImpl implements ISaveGovPartyOrgContextBMO {

    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govPartyOrgContextPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPartyOrgContextPo govPartyOrgContextPo) {

        govPartyOrgContextPo.setOrgId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_orgId));
        int flag = govPartyOrgContextInnerServiceSMOImpl.saveGovPartyOrgContext(govPartyOrgContextPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
