package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPetitionTypeServiceDao;
import com.java110.intf.gov.IGovPetitionTypeInnerServiceSMO;
import com.java110.dto.govPetitionType.GovPetitionTypeDto;
import com.java110.po.govPetitionType.GovPetitionTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 信访类型表内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPetitionTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovPetitionTypeInnerServiceSMO {

    @Autowired
    private IGovPetitionTypeServiceDao govPetitionTypeServiceDaoImpl;


    @Override
    public int saveGovPetitionType(@RequestBody  GovPetitionTypePo govPetitionTypePo) {
        int saveFlag = 1;
        govPetitionTypeServiceDaoImpl.saveGovPetitionTypeInfo(BeanConvertUtil.beanCovertMap(govPetitionTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovPetitionType(@RequestBody  GovPetitionTypePo govPetitionTypePo) {
        int saveFlag = 1;
         govPetitionTypeServiceDaoImpl.updateGovPetitionTypeInfo(BeanConvertUtil.beanCovertMap(govPetitionTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovPetitionType(@RequestBody  GovPetitionTypePo govPetitionTypePo) {
        int saveFlag = 1;
        govPetitionTypePo.setStatusCd("1");
        govPetitionTypeServiceDaoImpl.updateGovPetitionTypeInfo(BeanConvertUtil.beanCovertMap(govPetitionTypePo));
        return saveFlag;
    }

    @Override
    public List<GovPetitionTypeDto> queryGovPetitionTypes(@RequestBody  GovPetitionTypeDto govPetitionTypeDto) {

        //校验是否传了 分页信息

        int page = govPetitionTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPetitionTypeDto.setPage((page - 1) * govPetitionTypeDto.getRow());
        }

        List<GovPetitionTypeDto> govPetitionTypes = BeanConvertUtil.covertBeanList(govPetitionTypeServiceDaoImpl.getGovPetitionTypeInfo(BeanConvertUtil.beanCovertMap(govPetitionTypeDto)), GovPetitionTypeDto.class);

        return govPetitionTypes;
    }


    @Override
    public int queryGovPetitionTypesCount(@RequestBody GovPetitionTypeDto govPetitionTypeDto) {
        return govPetitionTypeServiceDaoImpl.queryGovPetitionTypesCount(BeanConvertUtil.beanCovertMap(govPetitionTypeDto));    }

    public IGovPetitionTypeServiceDao getGovPetitionTypeServiceDaoImpl() {
        return govPetitionTypeServiceDaoImpl;
    }

    public void setGovPetitionTypeServiceDaoImpl(IGovPetitionTypeServiceDao govPetitionTypeServiceDaoImpl) {
        this.govPetitionTypeServiceDaoImpl = govPetitionTypeServiceDaoImpl;
    }
}
