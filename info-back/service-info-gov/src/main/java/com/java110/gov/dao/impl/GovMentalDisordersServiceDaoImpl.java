package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovMentalDisordersServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 障碍者服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMentalDisordersServiceDaoImpl")
//@Transactional
public class GovMentalDisordersServiceDaoImpl extends BaseServiceDao implements IGovMentalDisordersServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMentalDisordersServiceDaoImpl.class);





    /**
     * 保存障碍者信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMentalDisordersInfo(Map info) throws DAOException {
        logger.debug("保存障碍者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMentalDisordersServiceDaoImpl.saveGovMentalDisordersInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存障碍者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询障碍者信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMentalDisordersInfo(Map info) throws DAOException {
        logger.debug("查询障碍者信息 入参 info : {}",info);

        List<Map> businessGovMentalDisordersInfos = sqlSessionTemplate.selectList("govMentalDisordersServiceDaoImpl.getGovMentalDisordersInfo",info);

        return businessGovMentalDisordersInfos;
    }


    /**
     * 修改障碍者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMentalDisordersInfo(Map info) throws DAOException {
        logger.debug("修改障碍者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMentalDisordersServiceDaoImpl.updateGovMentalDisordersInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改障碍者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询障碍者数量
     * @param info 障碍者信息
     * @return 障碍者数量
     */
    @Override
    public int queryGovMentalDisorderssCount(Map info) {
        logger.debug("查询障碍者数据 入参 info : {}",info);

        List<Map> businessGovMentalDisordersInfos = sqlSessionTemplate.selectList("govMentalDisordersServiceDaoImpl.queryGovMentalDisorderssCount", info);
        if (businessGovMentalDisordersInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMentalDisordersInfos.get(0).get("count").toString());
    }


}
