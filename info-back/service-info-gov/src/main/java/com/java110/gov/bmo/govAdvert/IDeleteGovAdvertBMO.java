package com.java110.gov.bmo.govAdvert;
import com.java110.po.govAdvert.GovAdvertPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovAdvertBMO {


    /**
     * 修改广告
     * add by wuxw
     * @param govAdvertPo
     * @return
     */
    ResponseEntity<String> delete(GovAdvertPo govAdvertPo);


}
