package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovReportUserServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 报事类型人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govReportUserServiceDaoImpl")
//@Transactional
public class GovReportUserServiceDaoImpl extends BaseServiceDao implements IGovReportUserServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovReportUserServiceDaoImpl.class);





    /**
     * 保存报事类型人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovReportUserInfo(Map info) throws DAOException {
        logger.debug("保存报事类型人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govReportUserServiceDaoImpl.saveGovReportUserInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存报事类型人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询报事类型人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovReportUserInfo(Map info) throws DAOException {
        logger.debug("查询报事类型人员信息 入参 info : {}",info);

        List<Map> businessGovReportUserInfos = sqlSessionTemplate.selectList("govReportUserServiceDaoImpl.getGovReportUserInfo",info);

        return businessGovReportUserInfos;
    }


    /**
     * 修改报事类型人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovReportUserInfo(Map info) throws DAOException {
        logger.debug("修改报事类型人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govReportUserServiceDaoImpl.updateGovReportUserInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改报事类型人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询报事类型人员数量
     * @param info 报事类型人员信息
     * @return 报事类型人员数量
     */
    @Override
    public int queryGovReportUsersCount(Map info) {
        logger.debug("查询报事类型人员数据 入参 info : {}",info);

        List<Map> businessGovReportUserInfos = sqlSessionTemplate.selectList("govReportUserServiceDaoImpl.queryGovReportUsersCount", info);
        if (businessGovReportUserInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovReportUserInfos.get(0).get("count").toString());
    }


}
