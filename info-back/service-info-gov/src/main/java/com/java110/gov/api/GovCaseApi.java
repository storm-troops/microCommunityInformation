package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govCase.GovCaseDto;
import com.java110.po.govCase.GovCasePo;
import com.java110.gov.bmo.govCase.IDeleteGovCaseBMO;
import com.java110.gov.bmo.govCase.IGetGovCaseBMO;
import com.java110.gov.bmo.govCase.ISaveGovCaseBMO;
import com.java110.gov.bmo.govCase.IUpdateGovCaseBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCase")
public class GovCaseApi {

    @Autowired
    private ISaveGovCaseBMO saveGovCaseBMOImpl;
    @Autowired
    private IUpdateGovCaseBMO updateGovCaseBMOImpl;
    @Autowired
    private IDeleteGovCaseBMO deleteGovCaseBMOImpl;

    @Autowired
    private IGetGovCaseBMO getGovCaseBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCase/saveGovCase
     * @path /app/govCase/saveGovCase
     */
    @RequestMapping(value = "/saveGovCase", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govVictimId", "请求报文中未包含govVictimId" );
        Assert.hasKeyAndValue( reqJson, "victimName", "请求报文中未包含victimName" );
        Assert.hasKeyAndValue( reqJson, "caseName", "请求报文中未包含caseName" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "caseNum", "请求报文中未包含caseNum" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "govSuspectId", "请求报文中未包含govSuspectId" );
        Assert.hasKeyAndValue( reqJson, "suspectName", "请求报文中未包含suspectName" );
        Assert.hasKeyAndValue( reqJson, "settleTime", "请求报文中未包含settleTime" );


        GovCasePo govCasePo = BeanConvertUtil.covertBean( reqJson, GovCasePo.class );
        return saveGovCaseBMOImpl.save( govCasePo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCase/updateGovCase
     * @path /app/govCase/updateGovCase
     */
    @RequestMapping(value = "/updateGovCase", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caseId", "请求报文中未包含caseId" );
        Assert.hasKeyAndValue( reqJson, "govVictimId", "请求报文中未包含govVictimId" );
        Assert.hasKeyAndValue( reqJson, "victimName", "请求报文中未包含victimName" );
        Assert.hasKeyAndValue( reqJson, "caseName", "请求报文中未包含caseName" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "caseNum", "请求报文中未包含caseNum" );
        Assert.hasKeyAndValue( reqJson, "happenTime", "请求报文中未包含happenTime" );
        Assert.hasKeyAndValue( reqJson, "govSuspectId", "请求报文中未包含govSuspectId" );
        Assert.hasKeyAndValue( reqJson, "suspectName", "请求报文中未包含suspectName" );
        Assert.hasKeyAndValue( reqJson, "settleTime", "请求报文中未包含settleTime" );
        Assert.hasKeyAndValue( reqJson, "null", "请求报文中未包含null" );


        GovCasePo govCasePo = BeanConvertUtil.covertBean( reqJson, GovCasePo.class );
        return updateGovCaseBMOImpl.update( govCasePo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCase/deleteGovCase
     * @path /app/govCase/deleteGovCase
     */
    @RequestMapping(value = "/deleteGovCase", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCase(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "caseId", "caseId不能为空" );


        GovCasePo govCasePo = BeanConvertUtil.covertBean( reqJson, GovCasePo.class );
        return deleteGovCaseBMOImpl.delete( govCasePo );
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govCase/queryGovCase
     * @path /app/govCase/queryGovCase
     */
    @RequestMapping(value = "/queryGovCase", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCase(@RequestParam(value = "caId") String caId,
                                               @RequestParam(value = "page") int page,
                                               @RequestParam(value = "row") int row) {
        GovCaseDto govCaseDto = new GovCaseDto();
        govCaseDto.setPage( page );
        govCaseDto.setRow( row );
        govCaseDto.setCaId( caId );
        return getGovCaseBMOImpl.get( govCaseDto );
    }
}
