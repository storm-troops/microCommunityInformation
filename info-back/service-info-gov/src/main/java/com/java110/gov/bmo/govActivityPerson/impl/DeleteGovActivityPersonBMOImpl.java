package com.java110.gov.bmo.govActivityPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govActivityPerson.IDeleteGovActivityPersonBMO;
import com.java110.intf.gov.IGovActivityPersonInnerServiceSMO;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovActivityPersonBMOImpl")
public class DeleteGovActivityPersonBMOImpl implements IDeleteGovActivityPersonBMO {

    @Autowired
    private IGovActivityPersonInnerServiceSMO govActivityPersonInnerServiceSMOImpl;

    /**
     * @param govActivityPersonPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovActivityPersonPo govActivityPersonPo) {

        int flag = govActivityPersonInnerServiceSMOImpl.deleteGovActivityPerson(govActivityPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
