package com.java110.gov.bmo.govActivityReply.impl;

import com.java110.dto.govActivityReply.GovActivityReplyDto;
import com.java110.gov.bmo.govActivityReply.IGetGovActivityReplyBMO;
import com.java110.intf.gov.IGovActivityReplyInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovActivityReplyBMOImpl")
public class GetGovActivityReplyBMOImpl implements IGetGovActivityReplyBMO {

    @Autowired
    private IGovActivityReplyInnerServiceSMO govActivityReplyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govActivityReplyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovActivityReplyDto govActivityReplyDto) {


        int count = govActivityReplyInnerServiceSMOImpl.queryGovActivityReplysCount(govActivityReplyDto);

        List<GovActivityReplyDto> govActivityReplyDtos = null;
        if (count > 0) {
            govActivityReplyDtos = govActivityReplyInnerServiceSMOImpl.queryGovActivityReplys(govActivityReplyDto);
            treeReply(govActivityReplyDtos);
        } else {
            govActivityReplyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govActivityReplyDto.getRow()), count, govActivityReplyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    /**
     * 将回复树桩
     * @param replyDtos
     * @return
     */
    private List<GovActivityReplyDto> treeReply(List<GovActivityReplyDto> replyDtos) {

        if(replyDtos == null || replyDtos.size()<1){
            return replyDtos;
        }

        List<GovActivityReplyDto> replyOnes = new ArrayList<>();
        for(GovActivityReplyDto replyDto: replyDtos){
            if("-1".equals(replyDto.getParentReplyId())){
                replyOnes.add(replyDto);
            }
        }

        List<GovActivityReplyDto> replyTwos = null;
        for(GovActivityReplyDto replyOne: replyOnes){
            replyTwos = new ArrayList<>();
            for(GovActivityReplyDto replyDto: replyDtos){
                if(replyOne.getSessionId().equals(replyDto.getSessionId())
                        && !"-1".equals(replyDto.getParentReplyId())){
                    replyTwos.add(replyDto);
                }
            }
            replyOne.setChilds(replyTwos);
        }
        return replyOnes;
    }

}
