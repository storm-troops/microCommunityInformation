package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govReportSetting.GovReportSettingDto;
import com.java110.gov.bmo.govReportSetting.IDeleteGovReportSettingBMO;
import com.java110.gov.bmo.govReportSetting.IGetGovReportSettingBMO;
import com.java110.gov.bmo.govReportSetting.ISaveGovReportSettingBMO;
import com.java110.gov.bmo.govReportSetting.IUpdateGovReportSettingBMO;
import com.java110.po.govReportSetting.GovReportSettingPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/govReportSetting")
public class GovReportSettingApi {

    @Autowired
    private ISaveGovReportSettingBMO saveGovReportSettingBMOImpl;
    @Autowired
    private IUpdateGovReportSettingBMO updateGovReportSettingBMOImpl;
    @Autowired
    private IDeleteGovReportSettingBMO deleteGovReportSettingBMOImpl;

    @Autowired
    private IGetGovReportSettingBMO getGovReportSettingBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govReportSetting/saveGovReportSetting
     * @path /app/govReportSetting/saveGovReportSetting
     */
    @RequestMapping(value = "/saveGovReportSetting", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovReportSetting(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportTypeName", "请求报文中未包含reportTypeName");
        Assert.hasKeyAndValue(reqJson, "reportWay", "请求报文中未包含reportWay");
        Assert.hasKeyAndValue(reqJson, "returnVisitFlag", "请求报文中未包含returnVisitFlag");


        GovReportSettingPo govReportSettingPo = BeanConvertUtil.covertBean(reqJson, GovReportSettingPo.class);
        return saveGovReportSettingBMOImpl.save(govReportSettingPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govReportSetting/updateGovReportSetting
     * @path /app/govReportSetting/updateGovReportSetting
     */
    @RequestMapping(value = "/updateGovReportSetting", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovReportSetting(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "reportTypeName", "请求报文中未包含reportTypeName");
        Assert.hasKeyAndValue(reqJson, "reportWay", "请求报文中未包含reportWay");
        Assert.hasKeyAndValue(reqJson, "returnVisitFlag", "请求报文中未包含returnVisitFlag");
        Assert.hasKeyAndValue(reqJson, "settingId", "settingId不能为空");


        GovReportSettingPo govReportSettingPo = BeanConvertUtil.covertBean(reqJson, GovReportSettingPo.class);
        return updateGovReportSettingBMOImpl.update(govReportSettingPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govReportSetting/deleteGovReportSetting
     * @path /app/govReportSetting/deleteGovReportSetting
     */
    @RequestMapping(value = "/deleteGovReportSetting", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovReportSetting(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "settingId", "settingId不能为空");


        GovReportSettingPo govReportSettingPo = BeanConvertUtil.covertBean(reqJson, GovReportSettingPo.class);
        return deleteGovReportSettingBMOImpl.delete(govReportSettingPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govReportSetting/queryGovReportSetting
     * @path /app/govReportSetting/queryGovReportSetting
     */
    @RequestMapping(value = "/queryGovReportSetting", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovReportSetting(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovReportSettingDto govReportSettingDto = new GovReportSettingDto();
        govReportSettingDto.setPage(page);
        govReportSettingDto.setRow(row);
        govReportSettingDto.setCaId(caId);
        return getGovReportSettingBMOImpl.get(govReportSettingDto);
    }
}
