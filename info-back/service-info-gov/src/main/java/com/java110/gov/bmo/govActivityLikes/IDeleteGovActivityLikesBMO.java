package com.java110.gov.bmo.govActivityLikes;
import org.springframework.http.ResponseEntity;
import com.java110.po.govActivityLikes.GovActivityLikesPo;

public interface IDeleteGovActivityLikesBMO {


    /**
     * 修改活动点赞
     * add by wuxw
     * @param govActivityLikesPo
     * @return
     */
    ResponseEntity<String> delete(GovActivityLikesPo govActivityLikesPo);


}
