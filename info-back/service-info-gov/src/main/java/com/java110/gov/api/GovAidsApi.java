package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govAids.GovAidsDto;
import com.java110.po.govAids.GovAidsPo;
import com.java110.gov.bmo.govAids.IDeleteGovAidsBMO;
import com.java110.gov.bmo.govAids.IGetGovAidsBMO;
import com.java110.gov.bmo.govAids.ISaveGovAidsBMO;
import com.java110.gov.bmo.govAids.IUpdateGovAidsBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govAids")
public class GovAidsApi {

    @Autowired
    private ISaveGovAidsBMO saveGovAidsBMOImpl;
    @Autowired
    private IUpdateGovAidsBMO updateGovAidsBMOImpl;
    @Autowired
    private IDeleteGovAidsBMO deleteGovAidsBMOImpl;

    @Autowired
    private IGetGovAidsBMO getGovAidsBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govAids/saveGovAids
     * @path /app/govAids/saveGovAids
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovAids", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovAids(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "aidsStartTime", "请求报文中未包含aidsStartTime");
        Assert.hasKeyAndValue(reqJson, "aidsReason", "请求报文中未包含aidsReason");
        Assert.hasKeyAndValue(reqJson, "emergencyPerson", "请求报文中未包含emergencyPerson");
        Assert.hasKeyAndValue(reqJson, "emergencyTel", "请求报文中未包含emergencyTel");

        GovAidsPo govAidsPo = BeanConvertUtil.covertBean(reqJson, GovAidsPo.class);
        return saveGovAidsBMOImpl.save(govAidsPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govAids/updateGovAids
     * @path /app/govAids/updateGovAids
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovAids", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovAids(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "aidsStartTime", "请求报文中未包含aidsStartTime");
        Assert.hasKeyAndValue(reqJson, "aidsReason", "请求报文中未包含aidsReason");
        Assert.hasKeyAndValue(reqJson, "emergencyPerson", "请求报文中未包含emergencyPerson");
        Assert.hasKeyAndValue(reqJson, "emergencyTel", "请求报文中未包含emergencyTel");
        Assert.hasKeyAndValue(reqJson, "aidsId", "aidsId不能为空");


        GovAidsPo govAidsPo = BeanConvertUtil.covertBean(reqJson, GovAidsPo.class);
        return updateGovAidsBMOImpl.update(govAidsPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govAids/deleteGovAids
     * @path /app/govAids/deleteGovAids
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovAids", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovAids(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "aidsId", "aidsId不能为空");
        GovAidsPo govAidsPo = BeanConvertUtil.covertBean(reqJson, GovAidsPo.class);
        return deleteGovAidsBMOImpl.delete(govAidsPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govAids/queryGovAids
     * @path /app/govAids/queryGovAids
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovAids", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovAids(@RequestParam(value = "caId") String caId,
                                               @RequestParam(value = "aidsId" , required = false) String aidsId,
                                               @RequestParam(value = "name" , required = false) String name,
                                               @RequestParam(value = "tel" , required = false) String tel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovAidsDto govAidsDto = new GovAidsDto();
        govAidsDto.setPage(page);
        govAidsDto.setRow(row);
        govAidsDto.setCaId(caId);
        govAidsDto.setName(name);
        govAidsDto.setTel(tel);
        govAidsDto.setAidsId(aidsId);
        return getGovAidsBMOImpl.get(govAidsDto);
    }
}
