package com.java110.gov.bmo.govSpecialFollow;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govSpecialFollow.GovSpecialFollowPo;
public interface ISaveGovSpecialFollowBMO {


    /**
     * 添加特殊人员跟进记录
     * add by wuxw
     * @param govSpecialFollowPo
     * @return
     */
    ResponseEntity<String> save(GovSpecialFollowPo govSpecialFollowPo);

    /**
     * 添加特殊人员跟进记录
     * add by wuxw
     * @param govSpecialFollowPo
     * @return
     */
    ResponseEntity<String> savePhone(GovSpecialFollowPo govSpecialFollowPo,JSONArray photos);

}
