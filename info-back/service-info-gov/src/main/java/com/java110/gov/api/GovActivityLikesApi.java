package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivityLikes.GovActivityLikesDto;
import com.java110.po.govActivityLikes.GovActivityLikesPo;
import com.java110.gov.bmo.govActivityLikes.IDeleteGovActivityLikesBMO;
import com.java110.gov.bmo.govActivityLikes.IGetGovActivityLikesBMO;
import com.java110.gov.bmo.govActivityLikes.ISaveGovActivityLikesBMO;
import com.java110.gov.bmo.govActivityLikes.IUpdateGovActivityLikesBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govActivityLikes")
public class GovActivityLikesApi {

    @Autowired
    private ISaveGovActivityLikesBMO saveGovActivityLikesBMOImpl;
    @Autowired
    private IUpdateGovActivityLikesBMO updateGovActivityLikesBMOImpl;
    @Autowired
    private IDeleteGovActivityLikesBMO deleteGovActivityLikesBMOImpl;
    @Autowired
    private IGetGovActivityLikesBMO getGovActivityLikesBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govActivityLikes/saveGovActivityLikes
     * @path /app/govActivityLikes/saveGovActivityLikes
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovActivityLikes", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivityLikes(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actId", "请求报文中未包含actId");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "userName", "请求报文中未包含userName");
        Assert.hasKeyAndValue(reqJson, "flag", "请求报文中未包含flag");


        GovActivityLikesPo govActivityLikesPo = BeanConvertUtil.covertBean(reqJson, GovActivityLikesPo.class);
        return saveGovActivityLikesBMOImpl.save(govActivityLikesPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govActivityLikes/updateGovActivityLikes
     * @path /app/govActivityLikes/updateGovActivityLikes
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovActivityLikes", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivityLikes(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actId", "请求报文中未包含actId");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "userName", "请求报文中未包含userName");
        Assert.hasKeyAndValue(reqJson, "flag", "请求报文中未包含flag");

        GovActivityLikesPo govActivityLikesPo = BeanConvertUtil.covertBean(reqJson, GovActivityLikesPo.class);
        return updateGovActivityLikesBMOImpl.update(govActivityLikesPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivityLikes/deleteGovActivityLikes
     * @path /app/govActivityLikes/deleteGovActivityLikes
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovActivityLikes", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivityLikes(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "likeId", "likeId不能为空");
        GovActivityLikesPo govActivityLikesPo = BeanConvertUtil.covertBean(reqJson, GovActivityLikesPo.class);
        return deleteGovActivityLikesBMOImpl.delete(govActivityLikesPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivityLikes/queryGovActivityLikes
     * @path /app/govActivityLikes/queryGovActivityLikes
     * @return
     */
    @RequestMapping(value = "/queryGovActivityLikes", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivityLikes(
                                                        @RequestParam(value = "userId" , required = false) String userId,
                                                        @RequestParam(value = "actId" , required = false) String actId,
                                                        @RequestParam(value = "likeId" , required = false) String likeId,
                                                        @RequestParam(value = "flag" , required = false) String flag,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovActivityLikesDto govActivityLikesDto = new GovActivityLikesDto();
        govActivityLikesDto.setPage(page);
        govActivityLikesDto.setRow(row);
        govActivityLikesDto.setUserId(userId);
        govActivityLikesDto.setActId(actId);
        govActivityLikesDto.setLikeId(likeId);
        govActivityLikesDto.setFlag(flag);
        return getGovActivityLikesBMOImpl.get(govActivityLikesDto);
    }
}
