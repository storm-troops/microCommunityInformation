package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPartyOrgContextServiceDao;
import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 党组织简介内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPartyOrgContextInnerServiceSMOImpl extends BaseServiceSMO implements IGovPartyOrgContextInnerServiceSMO {

    @Autowired
    private IGovPartyOrgContextServiceDao govPartyOrgContextServiceDaoImpl;


    @Override
    public int saveGovPartyOrgContext(@RequestBody GovPartyOrgContextPo govPartyOrgContextPo) {
        int saveFlag = 1;
        govPartyOrgContextServiceDaoImpl.saveGovPartyOrgContextInfo(BeanConvertUtil.beanCovertMap(govPartyOrgContextPo));
        return saveFlag;
    }

     @Override
    public int updateGovPartyOrgContext(@RequestBody  GovPartyOrgContextPo govPartyOrgContextPo) {
        int saveFlag = 1;
         govPartyOrgContextServiceDaoImpl.updateGovPartyOrgContextInfo(BeanConvertUtil.beanCovertMap(govPartyOrgContextPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPartyOrgContext(@RequestBody  GovPartyOrgContextPo govPartyOrgContextPo) {
        int saveFlag = 1;
        govPartyOrgContextPo.setStatusCd("1");
        govPartyOrgContextServiceDaoImpl.updateGovPartyOrgContextInfo(BeanConvertUtil.beanCovertMap(govPartyOrgContextPo));
        return saveFlag;
    }

    @Override
    public List<GovPartyOrgContextDto> queryGovPartyOrgContexts(@RequestBody  GovPartyOrgContextDto govPartyOrgContextDto) {

        //校验是否传了 分页信息

        int page = govPartyOrgContextDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPartyOrgContextDto.setPage((page - 1) * govPartyOrgContextDto.getRow());
        }

        List<GovPartyOrgContextDto> govPartyOrgContexts = BeanConvertUtil.covertBeanList(govPartyOrgContextServiceDaoImpl.getGovPartyOrgContextInfo(BeanConvertUtil.beanCovertMap(govPartyOrgContextDto)), GovPartyOrgContextDto.class);

        return govPartyOrgContexts;
    }


    @Override
    public int queryGovPartyOrgContextsCount(@RequestBody GovPartyOrgContextDto govPartyOrgContextDto) {
        return govPartyOrgContextServiceDaoImpl.queryGovPartyOrgContextsCount(BeanConvertUtil.beanCovertMap(govPartyOrgContextDto));    }

    public IGovPartyOrgContextServiceDao getGovPartyOrgContextServiceDaoImpl() {
        return govPartyOrgContextServiceDaoImpl;
    }

    public void setGovPartyOrgContextServiceDaoImpl(IGovPartyOrgContextServiceDao govPartyOrgContextServiceDaoImpl) {
        this.govPartyOrgContextServiceDaoImpl = govPartyOrgContextServiceDaoImpl;
    }
}
