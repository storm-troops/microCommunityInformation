package com.java110.gov.bmo.govPersonLabelRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.gov.bmo.govPersonLabelRel.ISaveGovPersonLabelRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovPersonLabelRelBMOImpl")
public class SaveGovPersonLabelRelBMOImpl implements ISaveGovPersonLabelRelBMO {

    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;
    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govPersonLabelRelPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPersonLabelRelPo govPersonLabelRelPo, String labels) {
        GovPersonLabelRelPo labelRelPo = new GovPersonLabelRelPo();
        labelRelPo.setGovPersonId( govPersonLabelRelPo.getGovPersonId() );
        govPersonLabelRelInnerServiceSMOImpl.deleteGovPersonLabelRel( labelRelPo );
        int flag = 0;
        String[] labelArray = labels.split( "," );
        for (String labelCd : labelArray) {
            govPersonLabelRelPo.setLabelRelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_labelRelId ) );
            govPersonLabelRelPo.setLabelCd( labelCd );
            flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel( govPersonLabelRelPo );
        }
        if (flag > 0) {
            return ResultVo.createResponseEntity( ResultVo.CODE_OK, "保存成功" );
        }
        return ResultVo.createResponseEntity( ResultVo.CODE_ERROR, "保存失败" );
    }
    /**
     * 添加小区信息
     *
     * @param govPersonLabelRelPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> savePersonLabelRel(GovPersonLabelRelPo govPersonLabelRelPo,String lableCd,String lableName,String lableType) {

        isGovLabelNull(lableCd,lableType,govPersonLabelRelPo.getCaId(),lableName);
        isGovLabelRelNull(govPersonLabelRelPo.getGovPersonId(),lableCd,govPersonLabelRelPo.getCaId());
        return ResultVo.createResponseEntity( ResultVo.CODE_OK, "保存成功" );
    }

    protected void isGovLabelNull(String lableCd, String labelType, String caId, String lableName) {
        //处理标签信息
        GovLabelDto govLabelDto = new GovLabelDto();
        govLabelDto.setLabelCd( lableCd );
        govLabelDto.setLabelType( labelType );
        govLabelDto.setCaId( caId );
        List<GovLabelDto> govLabelDtos = govLabelInnerServiceSMOImpl.queryGovLabels( govLabelDto );
        if (govLabelDtos == null || govLabelDtos.size() < 1) {
            GovLabelPo govLabelPo = new GovLabelPo();
            govLabelPo.setGovLabelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govLabelPo.setLabelCd( lableCd );
            govLabelPo.setLabelType( labelType );
            govLabelPo.setCaId( caId );
            govLabelPo.setLabelName( lableName );
            int flag = govLabelInnerServiceSMOImpl.saveGovLabel( govLabelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "保存" + lableName + "信息失败" );
            }
        }
    }

    protected void isGovLabelRelNull(String govPersonId, String lableCd, String caId) {

        //处理人员与标签关系
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setGovPersonId( govPersonId );
        govPersonLabelRelDto.setLabelCd( lableCd );
        govPersonLabelRelDto.setCaId( caId );
        List<GovPersonLabelRelDto> govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels( govPersonLabelRelDto );
        if (govPersonLabelRelDtos == null || govPersonLabelRelDtos.size() < 1) {
            GovPersonLabelRelPo govPersonLabelRelPo = new GovPersonLabelRelPo();
            govPersonLabelRelPo.setLabelRelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govPersonLabelRelPo.setGovPersonId( govPersonId );
            govPersonLabelRelPo.setLabelCd( lableCd );
            govPersonLabelRelPo.setCaId( caId );
            int flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel( govPersonLabelRelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "打标签嫌疑人失败" );
            }
        }
    }
}