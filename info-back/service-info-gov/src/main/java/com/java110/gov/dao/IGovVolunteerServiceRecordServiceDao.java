package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 服务记录表组件内部之间使用，没有给外围系统提供服务能力
 * 服务记录表服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovVolunteerServiceRecordServiceDao {


    /**
     * 保存 服务记录表信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovVolunteerServiceRecordInfo(Map info) throws DAOException;




    /**
     * 查询服务记录表信息（instance过程）
     * 根据bId 查询服务记录表信息
     * @param info bId 信息
     * @return 服务记录表信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovVolunteerServiceRecordInfo(Map info) throws DAOException;



    /**
     * 修改服务记录表信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovVolunteerServiceRecordInfo(Map info) throws DAOException;


    /**
     * 查询服务记录表总数
     *
     * @param info 服务记录表信息
     * @return 服务记录表数量
     */
    int queryGovVolunteerServiceRecordsCount(Map info);

}
