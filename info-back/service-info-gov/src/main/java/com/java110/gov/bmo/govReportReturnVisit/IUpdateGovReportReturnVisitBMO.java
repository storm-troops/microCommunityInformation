package com.java110.gov.bmo.govReportReturnVisit;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovReportReturnVisitBMO {


    /**
     * 修改回访管理
     * add by wuxw
     * @param govReportReturnVisitPo
     * @return
     */
    ResponseEntity<String> update(GovReportReturnVisitPo govReportReturnVisitPo);


}
