package com.java110.gov.bmo.govCarInout;
import com.java110.dto.govCarInout.GovCarInoutDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCarInoutBMO {


    /**
     * 查询车辆进出记录
     * add by wuxw
     * @param  govCarInoutDto
     * @return
     */
    ResponseEntity<String> get(GovCarInoutDto govCarInoutDto);


}
