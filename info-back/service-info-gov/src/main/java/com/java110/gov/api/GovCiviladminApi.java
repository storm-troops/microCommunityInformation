package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govCiviladmin.GovCiviladminDto;
import com.java110.gov.bmo.govCiviladmin.IDeleteGovCiviladminBMO;
import com.java110.gov.bmo.govCiviladmin.IGetGovCiviladminBMO;
import com.java110.gov.bmo.govCiviladmin.ISaveGovCiviladminBMO;
import com.java110.gov.bmo.govCiviladmin.IUpdateGovCiviladminBMO;
import com.java110.po.govCiviladmin.GovCiviladminPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCiviladmin")
public class GovCiviladminApi {

    @Autowired
    private ISaveGovCiviladminBMO saveGovCiviladminBMOImpl;
    @Autowired
    private IUpdateGovCiviladminBMO updateGovCiviladminBMOImpl;
    @Autowired
    private IDeleteGovCiviladminBMO deleteGovCiviladminBMOImpl;

    @Autowired
    private IGetGovCiviladminBMO getGovCiviladminBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCiviladmin/saveGovCiviladmin
     * @path /app/govCiviladmin/saveGovCiviladmin
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCiviladmin", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCiviladmin(@RequestHeader(value = "user-id") String userId,
                                                    @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");

        reqJson.put( "userId",userId );
        GovCiviladminPo govCiviladminPo = BeanConvertUtil.covertBean(reqJson, GovCiviladminPo.class);
        return saveGovCiviladminBMOImpl.save(govCiviladminPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCiviladmin/updateGovCiviladmin
     * @path /app/govCiviladmin/updateGovCiviladmin
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCiviladmin", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCiviladmin(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        GovCiviladminPo govCiviladminPo = BeanConvertUtil.covertBean(reqJson, GovCiviladminPo.class);
        return updateGovCiviladminBMOImpl.update(govCiviladminPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCiviladmin/deleteGovCiviladmin
     * @path /app/govCiviladmin/deleteGovCiviladmin
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCiviladmin", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCiviladmin(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "civiladminId", "civiladminId不能为空");


        GovCiviladminPo govCiviladminPo = BeanConvertUtil.covertBean(reqJson, GovCiviladminPo.class);
        return deleteGovCiviladminBMOImpl.delete(govCiviladminPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCiviladmin/queryGovCiviladmin
     * @path /app/govCiviladmin/queryGovCiviladmin
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovCiviladmin", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCiviladmin(@RequestParam(value = "caId") String caId,
                                                     @RequestParam(value = "title",required = false) String title,
                                                     @RequestParam(value = "civiladminId",required = false) String civiladminId,
                                                     @RequestParam(value = "typeCd",required = false) String typeCd,
                                                     @RequestParam(value = "page") int page,
                                                     @RequestParam(value = "row") int row) {
        GovCiviladminDto govCiviladminDto = new GovCiviladminDto();
        govCiviladminDto.setPage(page);
        govCiviladminDto.setRow(row);
        govCiviladminDto.setCaId(caId);
        govCiviladminDto.setCiviladminId(civiladminId);
        govCiviladminDto.setTitle(title);
        govCiviladminDto.setTypeCd(typeCd);
        return getGovCiviladminBMOImpl.get(govCiviladminDto);
    }
}
