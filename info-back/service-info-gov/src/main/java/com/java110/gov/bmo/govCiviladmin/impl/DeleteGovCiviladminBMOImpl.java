package com.java110.gov.bmo.govCiviladmin.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govCiviladmin.IDeleteGovCiviladminBMO;
import com.java110.intf.gov.IGovCiviladminInnerServiceSMO;
import com.java110.po.govCiviladmin.GovCiviladminPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovCiviladminBMOImpl")
public class DeleteGovCiviladminBMOImpl implements IDeleteGovCiviladminBMO {

    @Autowired
    private IGovCiviladminInnerServiceSMO govCiviladminInnerServiceSMOImpl;

    /**
     * @param govCiviladminPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovCiviladminPo govCiviladminPo) {

        int flag = govCiviladminInnerServiceSMOImpl.deleteGovCiviladmin(govCiviladminPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
