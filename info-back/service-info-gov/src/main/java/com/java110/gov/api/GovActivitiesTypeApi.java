package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivitiesType.GovActivitiesTypeDto;
import com.java110.gov.bmo.govActivitiesType.IDeleteGovActivitiesTypeBMO;
import com.java110.gov.bmo.govActivitiesType.IGetGovActivitiesTypeBMO;
import com.java110.gov.bmo.govActivitiesType.ISaveGovActivitiesTypeBMO;
import com.java110.gov.bmo.govActivitiesType.IUpdateGovActivitiesTypeBMO;
import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govActivitiesType")
public class GovActivitiesTypeApi {

    @Autowired
    private ISaveGovActivitiesTypeBMO saveGovActivitiesTypeBMOImpl;
    @Autowired
    private IUpdateGovActivitiesTypeBMO updateGovActivitiesTypeBMOImpl;
    @Autowired
    private IDeleteGovActivitiesTypeBMO deleteGovActivitiesTypeBMOImpl;

    @Autowired
    private IGetGovActivitiesTypeBMO getGovActivitiesTypeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govActivitiesType/saveGovActivitiesType
     * @path /app/govActivitiesType/saveGovActivitiesType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovActivitiesType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivitiesType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "defalutShow", "请求报文中未包含defalutShow");


        GovActivitiesTypePo govActivitiesTypePo = BeanConvertUtil.covertBean(reqJson, GovActivitiesTypePo.class);
        return saveGovActivitiesTypeBMOImpl.save(govActivitiesTypePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govActivitiesType/updateGovActivitiesType
     * @path /app/govActivitiesType/updateGovActivitiesType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovActivitiesType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivitiesType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "defalutShow", "请求报文中未包含defalutShow");
        Assert.hasKeyAndValue(reqJson, "typeCd", "typeCd不能为空");


        GovActivitiesTypePo govActivitiesTypePo = BeanConvertUtil.covertBean(reqJson, GovActivitiesTypePo.class);
        return updateGovActivitiesTypeBMOImpl.update(govActivitiesTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivitiesType/deleteGovActivitiesType
     * @path /app/govActivitiesType/deleteGovActivitiesType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovActivitiesType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivitiesType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "typeCd", "typeCd不能为空");


        GovActivitiesTypePo govActivitiesTypePo = BeanConvertUtil.covertBean(reqJson, GovActivitiesTypePo.class);
        return deleteGovActivitiesTypeBMOImpl.delete(govActivitiesTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivitiesType/queryGovActivitiesType
     * @path /app/govActivitiesType/queryGovActivitiesType
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovActivitiesType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivitiesType(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovActivitiesTypeDto govActivitiesTypeDto = new GovActivitiesTypeDto();
        govActivitiesTypeDto.setPage(page);
        govActivitiesTypeDto.setRow(row);
        govActivitiesTypeDto.setCaId(caId);
        return getGovActivitiesTypeBMOImpl.get(govActivitiesTypeDto);
    }
}
