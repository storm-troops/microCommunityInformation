package com.java110.gov.bmo.govReportUser;
import com.java110.po.govReportUser.GovReportUserPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovReportUserBMO {


    /**
     * 修改报事类型人员
     * add by wuxw
     * @param govReportUserPo
     * @return
     */
    ResponseEntity<String> update(GovReportUserPo govReportUserPo);


}
