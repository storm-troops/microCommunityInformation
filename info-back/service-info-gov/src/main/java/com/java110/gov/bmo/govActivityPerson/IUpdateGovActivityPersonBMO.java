package com.java110.gov.bmo.govActivityPerson;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovActivityPersonBMO {


    /**
     * 修改活动报名人员
     * add by wuxw
     * @param govActivityPersonPo
     * @return
     */
    ResponseEntity<String> update(GovActivityPersonPo govActivityPersonPo);


}
