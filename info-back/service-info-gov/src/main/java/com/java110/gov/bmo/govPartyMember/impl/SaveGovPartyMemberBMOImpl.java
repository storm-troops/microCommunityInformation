package com.java110.gov.bmo.govPartyMember.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govPartyMember.ISaveGovPartyMemberBMO;
import com.java110.intf.gov.IGovPartyMemberInnerServiceSMO;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovPartyMemberBMOImpl")
public class SaveGovPartyMemberBMOImpl implements ISaveGovPartyMemberBMO {

    @Autowired
    private IGovPartyMemberInnerServiceSMO govPartyMemberInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govPartyMemberPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPartyMemberPo govPartyMemberPo) {

        govPartyMemberPo.setGovMemberId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govMemberId));
        int flag = govPartyMemberInnerServiceSMOImpl.saveGovPartyMember(govPartyMemberPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
