package com.java110.gov.bmo.govMeetingMemberRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govMeetingMemberRel.IGetGovMeetingMemberRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovMeetingMemberRelInnerServiceSMO;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMeetingMemberRelBMOImpl")
public class GetGovMeetingMemberRelBMOImpl implements IGetGovMeetingMemberRelBMO {

    @Autowired
    private IGovMeetingMemberRelInnerServiceSMO govMeetingMemberRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMeetingMemberRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMeetingMemberRelDto govMeetingMemberRelDto) {


        int count = govMeetingMemberRelInnerServiceSMOImpl.queryGovMeetingMemberRelsCount(govMeetingMemberRelDto);

        List<GovMeetingMemberRelDto> govMeetingMemberRelDtos = null;
        if (count > 0) {
            govMeetingMemberRelDtos = govMeetingMemberRelInnerServiceSMOImpl.queryGovMeetingMemberRels(govMeetingMemberRelDto);
        } else {
            govMeetingMemberRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMeetingMemberRelDto.getRow()), count, govMeetingMemberRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
