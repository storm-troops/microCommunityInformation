package com.java110.gov.bmo.perGovActivities;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
public interface ISavePerGovActivitiesBMO {


    /**
     * 添加生日记录
     * add by wuxw
     * @param perGovActivitiesPo
     * @return
     */
    ResponseEntity<String> save(PerGovActivitiesPo perGovActivitiesPo,JSONArray oldPersons);


}
