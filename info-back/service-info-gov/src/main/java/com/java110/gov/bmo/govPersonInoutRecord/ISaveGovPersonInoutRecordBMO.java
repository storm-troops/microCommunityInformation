package com.java110.gov.bmo.govPersonInoutRecord;

import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovPersonInoutRecordBMO {


    /**
     * 添加开门记录
     * add by wuxw
     * @param govPersonInoutRecordPo
     * @return
     */
    ResponseEntity<String> save(GovPersonInoutRecordPo govPersonInoutRecordPo);


}
