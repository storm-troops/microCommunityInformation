package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govHelpPolicyList.GovHelpPolicyListDto;
import com.java110.po.govHelpPolicyList.GovHelpPolicyListPo;
import com.java110.gov.bmo.govHelpPolicyList.IDeleteGovHelpPolicyListBMO;
import com.java110.gov.bmo.govHelpPolicyList.IGetGovHelpPolicyListBMO;
import com.java110.gov.bmo.govHelpPolicyList.ISaveGovHelpPolicyListBMO;
import com.java110.gov.bmo.govHelpPolicyList.IUpdateGovHelpPolicyListBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHelpPolicyList")
public class GovHelpPolicyListApi {

    @Autowired
    private ISaveGovHelpPolicyListBMO saveGovHelpPolicyListBMOImpl;
    @Autowired
    private IUpdateGovHelpPolicyListBMO updateGovHelpPolicyListBMOImpl;
    @Autowired
    private IDeleteGovHelpPolicyListBMO deleteGovHelpPolicyListBMOImpl;

    @Autowired
    private IGetGovHelpPolicyListBMO getGovHelpPolicyListBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHelpPolicyList/saveGovHelpPolicyList
     * @path /app/govHelpPolicyList/saveGovHelpPolicyList
     */
    @RequestMapping(value = "/saveGovHelpPolicyList", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHelpPolicyList(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "govHelpId", "请求报文中未包含helpId" );
        Assert.hasKeyAndValue( reqJson, "helpName", "请求报文中未包含helpName" );
        Assert.hasKeyAndValue( reqJson, "listName", "请求报文中未包含listName" );
        Assert.hasKeyAndValue( reqJson, "poorPersonId", "请求报文中未包含poorPersonId" );
        Assert.hasKeyAndValue( reqJson, "poorPersonName", "请求报文中未包含poorPersonName" );
        Assert.hasKeyAndValue( reqJson, "cadrePersonId", "请求报文中未包含cadrePersonId" );
        Assert.hasKeyAndValue( reqJson, "cadrePersonName", "请求报文中未包含cadrePersonName" );
        Assert.hasKeyAndValue( reqJson, "startTime", "请求报文中未包含startTime" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );


        GovHelpPolicyListPo govHelpPolicyListPo = BeanConvertUtil.covertBean( reqJson, GovHelpPolicyListPo.class );
        return saveGovHelpPolicyListBMOImpl.save( govHelpPolicyListPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHelpPolicyList/updateGovHelpPolicyList
     * @path /app/govHelpPolicyList/updateGovHelpPolicyList
     */
    @RequestMapping(value = "/updateGovHelpPolicyList", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHelpPolicyList(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govHelpListId", "请求报文中未包含govHelpListId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "govHelpId", "请求报文中未包含helpId" );
        Assert.hasKeyAndValue( reqJson, "helpName", "请求报文中未包含helpName" );
        Assert.hasKeyAndValue( reqJson, "listName", "请求报文中未包含listName" );
        Assert.hasKeyAndValue( reqJson, "poorPersonId", "请求报文中未包含poorPersonId" );
        Assert.hasKeyAndValue( reqJson, "poorPersonName", "请求报文中未包含poorPersonName" );
        Assert.hasKeyAndValue( reqJson, "cadrePersonId", "请求报文中未包含cadrePersonId" );
        Assert.hasKeyAndValue( reqJson, "cadrePersonName", "请求报文中未包含cadrePersonName" );
        Assert.hasKeyAndValue( reqJson, "startTime", "请求报文中未包含startTime" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );

        GovHelpPolicyListPo govHelpPolicyListPo = BeanConvertUtil.covertBean( reqJson, GovHelpPolicyListPo.class );
        return updateGovHelpPolicyListBMOImpl.update( govHelpPolicyListPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHelpPolicyList/deleteGovHelpPolicyList
     * @path /app/govHelpPolicyList/deleteGovHelpPolicyList
     */
    @RequestMapping(value = "/deleteGovHelpPolicyList", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHelpPolicyList(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govHelpListId", "govHelpListId不能为空" );


        GovHelpPolicyListPo govHelpPolicyListPo = BeanConvertUtil.covertBean( reqJson, GovHelpPolicyListPo.class );
        return deleteGovHelpPolicyListBMOImpl.delete( govHelpPolicyListPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govHelpPolicyList/queryGovHelpPolicyList
     * @path /app/govHelpPolicyList/queryGovHelpPolicyList
     */
    @RequestMapping(value = "/queryGovHelpPolicyList", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHelpPolicyList(@RequestParam(value = "caId",required = false) String caId,
                                                         @RequestParam(value = "poorPersonNameLike",required = false) String poorPersonNameLike,
                                                         @RequestParam(value = "cadrePersonNameLike",required = false) String cadrePersonNameLike,
                                                         @RequestParam(value = "govHelpId",required = false) String govHelpId,
                                                         @RequestParam(value = "govHelpListId",required = false) String govHelpListId,
                                                         @RequestParam(value = "listNameLike",required = false) String listNameLike,
                                                         @RequestParam(value = "cadrePersonId",required = false) String cadrePersonId,
                                                         @RequestParam(value = "poorPersonId",required = false) String poorPersonId,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {

        GovHelpPolicyListDto govHelpPolicyListDto = new GovHelpPolicyListDto();
        govHelpPolicyListDto.setPage( page );
        govHelpPolicyListDto.setRow( row );
        govHelpPolicyListDto.setCaId( caId );
        govHelpPolicyListDto.setGovHelpId( govHelpId );
        govHelpPolicyListDto.setPoorPersonNameLike( poorPersonNameLike );
        govHelpPolicyListDto.setCadrePersonNameLike( cadrePersonNameLike );
        govHelpPolicyListDto.setListNameLike( listNameLike );
        govHelpPolicyListDto.setGovHelpListId( govHelpListId );
        govHelpPolicyListDto.setCadrePersonId( cadrePersonId );
        govHelpPolicyListDto.setPoorPersonId( poorPersonId );
        return getGovHelpPolicyListBMOImpl.get( govHelpPolicyListDto );
    }
}
