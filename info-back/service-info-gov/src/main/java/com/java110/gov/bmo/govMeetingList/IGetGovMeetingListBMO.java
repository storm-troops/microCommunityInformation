package com.java110.gov.bmo.govMeetingList;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMeetingList.GovMeetingListDto;
public interface IGetGovMeetingListBMO {


    /**
     * 查询会议列表
     * add by wuxw
     * @param  govMeetingListDto
     * @return
     */
    ResponseEntity<String> get(GovMeetingListDto govMeetingListDto);


    ResponseEntity<String> getPersonIdMeetingList(GovMeetingListDto govMeetingListDto, String govMemberId);
}
