package com.java110.gov.bmo.govWorkType.impl;

import com.java110.gov.bmo.govWorkType.IGetGovWorkTypeBMO;
import com.java110.intf.gov.IGovWorkTypeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govWorkType.GovWorkTypeDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovWorkTypeBMOImpl")
public class GetGovWorkTypeBMOImpl implements IGetGovWorkTypeBMO {

    @Autowired
    private IGovWorkTypeInnerServiceSMO govWorkTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govWorkTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovWorkTypeDto govWorkTypeDto) {


        int count = govWorkTypeInnerServiceSMOImpl.queryGovWorkTypesCount(govWorkTypeDto);

        List<GovWorkTypeDto> govWorkTypeDtos = null;
        if (count > 0) {
            govWorkTypeDtos = govWorkTypeInnerServiceSMOImpl.queryGovWorkTypes(govWorkTypeDto);
        } else {
            govWorkTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govWorkTypeDto.getRow()), count, govWorkTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
