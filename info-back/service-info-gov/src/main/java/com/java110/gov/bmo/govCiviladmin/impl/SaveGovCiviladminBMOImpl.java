package com.java110.gov.bmo.govCiviladmin.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.user.UserDto;
import com.java110.gov.bmo.govCiviladmin.ISaveGovCiviladminBMO;
import com.java110.intf.cust.IUserInnerServiceSMO;
import com.java110.intf.gov.IGovCiviladminInnerServiceSMO;
import com.java110.po.govCiviladmin.GovCiviladminPo;
import com.java110.utils.util.Assert;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovCiviladminBMOImpl")
public class SaveGovCiviladminBMOImpl implements ISaveGovCiviladminBMO {

    @Autowired
    private IGovCiviladminInnerServiceSMO govCiviladminInnerServiceSMOImpl;

    @Autowired
    private IUserInnerServiceSMO userInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govCiviladminPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCiviladminPo govCiviladminPo) {
        UserDto userDto = new UserDto();
        userDto.setUserId(govCiviladminPo.getUserId());
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);
        Assert.listOnlyOne(userDtos, "用户不存在");
        govCiviladminPo.setUserName( userDtos.get( 0 ).getUserName() );
        govCiviladminPo.setCiviladminId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_civiladminId));
        int flag = govCiviladminInnerServiceSMOImpl.saveGovCiviladmin(govCiviladminPo);
        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
