package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 障碍者组件内部之间使用，没有给外围系统提供服务能力
 * 障碍者服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovMentalDisordersServiceDao {


    /**
     * 保存 障碍者信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovMentalDisordersInfo(Map info) throws DAOException;




    /**
     * 查询障碍者信息（instance过程）
     * 根据bId 查询障碍者信息
     * @param info bId 信息
     * @return 障碍者信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovMentalDisordersInfo(Map info) throws DAOException;



    /**
     * 修改障碍者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovMentalDisordersInfo(Map info) throws DAOException;


    /**
     * 查询障碍者总数
     *
     * @param info 障碍者信息
     * @return 障碍者数量
     */
    int queryGovMentalDisorderssCount(Map info);

}
