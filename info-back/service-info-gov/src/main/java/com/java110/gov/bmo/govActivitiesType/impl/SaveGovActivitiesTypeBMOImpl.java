package com.java110.gov.bmo.govActivitiesType.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govActivitiesType.ISaveGovActivitiesTypeBMO;
import com.java110.intf.gov.IGovActivitiesTypeInnerServiceSMO;
import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovActivitiesTypeBMOImpl")
public class SaveGovActivitiesTypeBMOImpl implements ISaveGovActivitiesTypeBMO {

    @Autowired
    private IGovActivitiesTypeInnerServiceSMO govActivitiesTypeInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govActivitiesTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovActivitiesTypePo govActivitiesTypePo) {

        govActivitiesTypePo.setTypeCd(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_typeCd));
        int flag = govActivitiesTypeInnerServiceSMOImpl.saveGovActivitiesType(govActivitiesTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
