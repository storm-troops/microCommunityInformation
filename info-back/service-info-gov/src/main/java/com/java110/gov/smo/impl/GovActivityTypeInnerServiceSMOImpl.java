package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govActivityType.GovActivityTypeDto;
import com.java110.gov.dao.IGovActivityTypeServiceDao;
import com.java110.intf.gov.IGovActivityTypeInnerServiceSMO;
import com.java110.po.govActivityType.GovActivityTypePo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 活动类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivityTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivityTypeInnerServiceSMO {

    @Autowired
    private IGovActivityTypeServiceDao govActivityTypeServiceDaoImpl;


    @Override
    public int saveGovActivityType(@RequestBody GovActivityTypePo govActivityTypePo) {
        int saveFlag = 1;
        govActivityTypeServiceDaoImpl.saveGovActivityTypeInfo(BeanConvertUtil.beanCovertMap(govActivityTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovActivityType(@RequestBody  GovActivityTypePo govActivityTypePo) {
        int saveFlag = 1;
         govActivityTypeServiceDaoImpl.updateGovActivityTypeInfo(BeanConvertUtil.beanCovertMap(govActivityTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivityType(@RequestBody  GovActivityTypePo govActivityTypePo) {
        int saveFlag = 1;
        govActivityTypePo.setStatusCd("1");
        govActivityTypeServiceDaoImpl.updateGovActivityTypeInfo(BeanConvertUtil.beanCovertMap(govActivityTypePo));
        return saveFlag;
    }

    @Override
    public List<GovActivityTypeDto> queryGovActivityTypes(@RequestBody  GovActivityTypeDto govActivityTypeDto) {

        //校验是否传了 分页信息

        int page = govActivityTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivityTypeDto.setPage((page - 1) * govActivityTypeDto.getRow());
        }

        List<GovActivityTypeDto> govActivityTypes = BeanConvertUtil.covertBeanList(govActivityTypeServiceDaoImpl.getGovActivityTypeInfo(BeanConvertUtil.beanCovertMap(govActivityTypeDto)), GovActivityTypeDto.class);

        return govActivityTypes;
    }


    @Override
    public int queryGovActivityTypesCount(@RequestBody GovActivityTypeDto govActivityTypeDto) {
        return govActivityTypeServiceDaoImpl.queryGovActivityTypesCount(BeanConvertUtil.beanCovertMap(govActivityTypeDto));    }

    public IGovActivityTypeServiceDao getGovActivityTypeServiceDaoImpl() {
        return govActivityTypeServiceDaoImpl;
    }

    public void setGovActivityTypeServiceDaoImpl(IGovActivityTypeServiceDao govActivityTypeServiceDaoImpl) {
        this.govActivityTypeServiceDaoImpl = govActivityTypeServiceDaoImpl;
    }
}
