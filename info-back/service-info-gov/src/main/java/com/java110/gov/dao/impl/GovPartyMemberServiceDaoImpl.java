package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovPartyMemberServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 党员管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPartyMemberServiceDaoImpl")
//@Transactional
public class GovPartyMemberServiceDaoImpl extends BaseServiceDao implements IGovPartyMemberServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPartyMemberServiceDaoImpl.class);





    /**
     * 保存党员管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPartyMemberInfo(Map info) throws DAOException {
        logger.debug("保存党员管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPartyMemberServiceDaoImpl.saveGovPartyMemberInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存党员管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询党员管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPartyMemberInfo(Map info) throws DAOException {
        logger.debug("查询党员管理信息 入参 info : {}",info);

        List<Map> businessGovPartyMemberInfos = sqlSessionTemplate.selectList("govPartyMemberServiceDaoImpl.getGovPartyMemberInfo",info);

        return businessGovPartyMemberInfos;
    }


    /**
     * 修改党员管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPartyMemberInfo(Map info) throws DAOException {
        logger.debug("修改党员管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPartyMemberServiceDaoImpl.updateGovPartyMemberInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改党员管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询党员管理数量
     * @param info 党员管理信息
     * @return 党员管理数量
     */
    @Override
    public int queryGovPartyMembersCount(Map info) {
        logger.debug("查询党员管理数据 入参 info : {}",info);

        List<Map> businessGovPartyMemberInfos = sqlSessionTemplate.selectList("govPartyMemberServiceDaoImpl.queryGovPartyMembersCount", info);
        if (businessGovPartyMemberInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPartyMemberInfos.get(0).get("count").toString());
    }


}
