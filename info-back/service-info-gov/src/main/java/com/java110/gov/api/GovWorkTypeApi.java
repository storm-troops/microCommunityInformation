package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govWorkType.GovWorkTypeDto;
import com.java110.gov.bmo.govWorkType.IDeleteGovWorkTypeBMO;
import com.java110.gov.bmo.govWorkType.IGetGovWorkTypeBMO;
import com.java110.gov.bmo.govWorkType.ISaveGovWorkTypeBMO;
import com.java110.gov.bmo.govWorkType.IUpdateGovWorkTypeBMO;
import com.java110.po.govWorkType.GovWorkTypePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govWorkType")
public class GovWorkTypeApi {

    @Autowired
    private ISaveGovWorkTypeBMO saveGovWorkTypeBMOImpl;
    @Autowired
    private IUpdateGovWorkTypeBMO updateGovWorkTypeBMOImpl;
    @Autowired
    private IDeleteGovWorkTypeBMO deleteGovWorkTypeBMOImpl;

    @Autowired
    private IGetGovWorkTypeBMO getGovWorkTypeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govWorkType/saveGovWorkType
     * @path /app/govWorkType/saveGovWorkType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovWorkType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovWorkType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "workTypeName", "请求报文中未包含workTypeName");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");


        GovWorkTypePo govWorkTypePo = BeanConvertUtil.covertBean(reqJson, GovWorkTypePo.class);
        return saveGovWorkTypeBMOImpl.save(govWorkTypePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govWorkType/updateGovWorkType
     * @path /app/govWorkType/updateGovWorkType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovWorkType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovWorkType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "workTypeName", "请求报文中未包含workTypeName");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "govTypeId", "govTypeId不能为空");


        GovWorkTypePo govWorkTypePo = BeanConvertUtil.covertBean(reqJson, GovWorkTypePo.class);
        return updateGovWorkTypeBMOImpl.update(govWorkTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govWorkType/deleteGovWorkType
     * @path /app/govWorkType/deleteGovWorkType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovWorkType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovWorkType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govTypeId", "govTypeId不能为空");


        GovWorkTypePo govWorkTypePo = BeanConvertUtil.covertBean(reqJson, GovWorkTypePo.class);
        return deleteGovWorkTypeBMOImpl.delete(govWorkTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govWorkType/queryGovWorkType
     * @path /app/govWorkType/queryGovWorkType
     * @param govTypeId ID
     * @return
     */
    @RequestMapping(value = "/queryGovWorkType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovWorkType(@RequestParam(value = "govTypeId", required = false) String govTypeId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovWorkTypeDto govWorkTypeDto = new GovWorkTypeDto();
        govWorkTypeDto.setPage(page);
        govWorkTypeDto.setRow(row);
        govWorkTypeDto.setGovTypeId(govTypeId);
        return getGovWorkTypeBMOImpl.get(govWorkTypeDto);
    }
}
