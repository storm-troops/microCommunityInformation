package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govCiviladminType.GovCiviladminTypeDto;
import com.java110.gov.bmo.govCiviladminType.IDeleteGovCiviladminTypeBMO;
import com.java110.gov.bmo.govCiviladminType.IGetGovCiviladminTypeBMO;
import com.java110.gov.bmo.govCiviladminType.ISaveGovCiviladminTypeBMO;
import com.java110.gov.bmo.govCiviladminType.IUpdateGovCiviladminTypeBMO;
import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCiviladminType")
public class GovCiviladminTypeApi {

    @Autowired
    private ISaveGovCiviladminTypeBMO saveGovCiviladminTypeBMOImpl;
    @Autowired
    private IUpdateGovCiviladminTypeBMO updateGovCiviladminTypeBMOImpl;
    @Autowired
    private IDeleteGovCiviladminTypeBMO deleteGovCiviladminTypeBMOImpl;

    @Autowired
    private IGetGovCiviladminTypeBMO getGovCiviladminTypeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCiviladminType/saveGovCiviladminType
     * @path /app/govCiviladminType/saveGovCiviladminType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCiviladminType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCiviladminType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "defalutShow", "请求报文中未包含defalutShow");


        GovCiviladminTypePo govCiviladminTypePo = BeanConvertUtil.covertBean(reqJson, GovCiviladminTypePo.class);
        return saveGovCiviladminTypeBMOImpl.save(govCiviladminTypePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCiviladminType/updateGovCiviladminType
     * @path /app/govCiviladminType/updateGovCiviladminType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCiviladminType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCiviladminType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "defalutShow", "请求报文中未包含defalutShow");


        GovCiviladminTypePo govCiviladminTypePo = BeanConvertUtil.covertBean(reqJson, GovCiviladminTypePo.class);
        return updateGovCiviladminTypeBMOImpl.update(govCiviladminTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCiviladminType/deleteGovCiviladminType
     * @path /app/govCiviladminType/deleteGovCiviladminType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCiviladminType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCiviladminType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "typeCd", "typeCd不能为空");


        GovCiviladminTypePo govCiviladminTypePo = BeanConvertUtil.covertBean(reqJson, GovCiviladminTypePo.class);
        return deleteGovCiviladminTypeBMOImpl.delete(govCiviladminTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCiviladminType/queryGovCiviladminType
     * @path /app/govCiviladminType/queryGovCiviladminType
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovCiviladminType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCiviladminType(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCiviladminTypeDto govCiviladminTypeDto = new GovCiviladminTypeDto();
        govCiviladminTypeDto.setPage(page);
        govCiviladminTypeDto.setRow(row);
        govCiviladminTypeDto.setCaId(caId);
        return getGovCiviladminTypeBMOImpl.get(govCiviladminTypeDto);
    }
}
