package com.java110.gov.bmo.govAreaType;
import org.springframework.http.ResponseEntity;
import com.java110.po.govAreaType.GovAreaTypePo;

public interface IDeleteGovAreaTypeBMO {


    /**
     * 修改涉及区域类型
     * add by wuxw
     * @param govAreaTypePo
     * @return
     */
    ResponseEntity<String> delete(GovAreaTypePo govAreaTypePo);


}
