package com.java110.gov.bmo.govDrug;
import org.springframework.http.ResponseEntity;
import com.java110.po.govDrug.GovDrugPo;

public interface IDeleteGovDrugBMO {


    /**
     * 修改吸毒者
     * add by wuxw
     * @param govDrugPo
     * @return
     */
    ResponseEntity<String> delete(GovDrugPo govDrugPo);


}
