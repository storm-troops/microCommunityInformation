package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovCommunityarCorrectionServiceDao;
import com.java110.intf.gov.IGovCommunityarCorrectionInnerServiceSMO;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
import com.java110.po.govCommunityarCorrection.GovCommunityarCorrectionPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 矫正者内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCommunityarCorrectionInnerServiceSMOImpl extends BaseServiceSMO implements IGovCommunityarCorrectionInnerServiceSMO {

    @Autowired
    private IGovCommunityarCorrectionServiceDao govCommunityarCorrectionServiceDaoImpl;


    @Override
    public int saveGovCommunityarCorrection(@RequestBody  GovCommunityarCorrectionPo govCommunityarCorrectionPo) {
        int saveFlag = 1;
        govCommunityarCorrectionServiceDaoImpl.saveGovCommunityarCorrectionInfo(BeanConvertUtil.beanCovertMap(govCommunityarCorrectionPo));
        return saveFlag;
    }

     @Override
    public int updateGovCommunityarCorrection(@RequestBody  GovCommunityarCorrectionPo govCommunityarCorrectionPo) {
        int saveFlag = 1;
         govCommunityarCorrectionServiceDaoImpl.updateGovCommunityarCorrectionInfo(BeanConvertUtil.beanCovertMap(govCommunityarCorrectionPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCommunityarCorrection(@RequestBody  GovCommunityarCorrectionPo govCommunityarCorrectionPo) {
        int saveFlag = 1;
        govCommunityarCorrectionPo.setStatusCd("1");
        govCommunityarCorrectionServiceDaoImpl.updateGovCommunityarCorrectionInfo(BeanConvertUtil.beanCovertMap(govCommunityarCorrectionPo));
        return saveFlag;
    }

    @Override
    public List<GovCommunityarCorrectionDto> queryGovCommunityarCorrections(@RequestBody  GovCommunityarCorrectionDto govCommunityarCorrectionDto) {

        //校验是否传了 分页信息

        int page = govCommunityarCorrectionDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityarCorrectionDto.setPage((page - 1) * govCommunityarCorrectionDto.getRow());
        }

        List<GovCommunityarCorrectionDto> govCommunityarCorrections = BeanConvertUtil.covertBeanList(govCommunityarCorrectionServiceDaoImpl.getGovCommunityarCorrectionInfo(BeanConvertUtil.beanCovertMap(govCommunityarCorrectionDto)), GovCommunityarCorrectionDto.class);

        return govCommunityarCorrections;
    }


    @Override
    public int queryGovCommunityarCorrectionsCount(@RequestBody GovCommunityarCorrectionDto govCommunityarCorrectionDto) {
        return govCommunityarCorrectionServiceDaoImpl.queryGovCommunityarCorrectionsCount(BeanConvertUtil.beanCovertMap(govCommunityarCorrectionDto));    }

    public IGovCommunityarCorrectionServiceDao getGovCommunityarCorrectionServiceDaoImpl() {
        return govCommunityarCorrectionServiceDaoImpl;
    }

    public void setGovCommunityarCorrectionServiceDaoImpl(IGovCommunityarCorrectionServiceDao govCommunityarCorrectionServiceDaoImpl) {
        this.govCommunityarCorrectionServiceDaoImpl = govCommunityarCorrectionServiceDaoImpl;
    }
}
