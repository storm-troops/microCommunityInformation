package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * 广告组件内部之间使用，没有给外围系统提供服务能力
 * 广告服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovAdvertServiceDao {


    /**
     * 保存 广告信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovAdvertInfo(Map info) throws DAOException;




    /**
     * 查询广告信息（instance过程）
     * 根据bId 查询广告信息
     * @param info bId 信息
     * @return 广告信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovAdvertInfo(Map info) throws DAOException;

    /**
     * 查询广告信息（instance过程）
     * 根据bId 查询广告信息
     * @param info bId 信息
     * @return 广告信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovAdvertItmesInfo(Map info) throws DAOException;

    /**
     * 修改广告信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovAdvertInfo(Map info) throws DAOException;


    /**
     * 查询广告总数
     *
     * @param info 广告信息
     * @return 广告数量
     */
    int queryGovAdvertsCount(Map info);

}
