package com.java110.gov.bmo.govPetitionLetter;

import org.springframework.http.ResponseEntity;
import com.java110.po.govPetitionLetter.GovPetitionLetterPo;
public interface ISaveGovPetitionLetterBMO {


    /**
     * 添加信访管理
     * add by wuxw
     * @param govPetitionLetterPo
     * @return
     */
    ResponseEntity<String> save(GovPetitionLetterPo govPetitionLetterPo);


}
