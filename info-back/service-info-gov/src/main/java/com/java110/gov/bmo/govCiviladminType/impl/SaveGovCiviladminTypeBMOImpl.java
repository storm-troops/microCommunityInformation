package com.java110.gov.bmo.govCiviladminType.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govCiviladminType.ISaveGovCiviladminTypeBMO;
import com.java110.intf.gov.IGovCiviladminTypeInnerServiceSMO;
import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovCiviladminTypeBMOImpl")
public class SaveGovCiviladminTypeBMOImpl implements ISaveGovCiviladminTypeBMO {

    @Autowired
    private IGovCiviladminTypeInnerServiceSMO govCiviladminTypeInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govCiviladminTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCiviladminTypePo govCiviladminTypePo) {

        govCiviladminTypePo.setTypeCd(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_civiladminTypeCd));
        int flag = govCiviladminTypeInnerServiceSMOImpl.saveGovCiviladminType(govCiviladminTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
