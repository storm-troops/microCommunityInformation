package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPetitionLetterServiceDao;
import com.java110.intf.gov.IGovPetitionLetterInnerServiceSMO;
import com.java110.dto.govPetitionLetter.GovPetitionLetterDto;
import com.java110.po.govPetitionLetter.GovPetitionLetterPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 信访管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPetitionLetterInnerServiceSMOImpl extends BaseServiceSMO implements IGovPetitionLetterInnerServiceSMO {

    @Autowired
    private IGovPetitionLetterServiceDao govPetitionLetterServiceDaoImpl;


    @Override
    public int saveGovPetitionLetter(@RequestBody  GovPetitionLetterPo govPetitionLetterPo) {
        int saveFlag = 1;
        govPetitionLetterServiceDaoImpl.saveGovPetitionLetterInfo(BeanConvertUtil.beanCovertMap(govPetitionLetterPo));
        return saveFlag;
    }

     @Override
    public int updateGovPetitionLetter(@RequestBody  GovPetitionLetterPo govPetitionLetterPo) {
        int saveFlag = 1;
         govPetitionLetterServiceDaoImpl.updateGovPetitionLetterInfo(BeanConvertUtil.beanCovertMap(govPetitionLetterPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPetitionLetter(@RequestBody  GovPetitionLetterPo govPetitionLetterPo) {
        int saveFlag = 1;
        govPetitionLetterPo.setStatusCd("1");
        govPetitionLetterServiceDaoImpl.updateGovPetitionLetterInfo(BeanConvertUtil.beanCovertMap(govPetitionLetterPo));
        return saveFlag;
    }

    @Override
    public List<GovPetitionLetterDto> queryGovPetitionLetters(@RequestBody  GovPetitionLetterDto govPetitionLetterDto) {

        //校验是否传了 分页信息

        int page = govPetitionLetterDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPetitionLetterDto.setPage((page - 1) * govPetitionLetterDto.getRow());
        }

        List<GovPetitionLetterDto> govPetitionLetters = BeanConvertUtil.covertBeanList(govPetitionLetterServiceDaoImpl.getGovPetitionLetterInfo(BeanConvertUtil.beanCovertMap(govPetitionLetterDto)), GovPetitionLetterDto.class);

        return govPetitionLetters;
    }


    @Override
    public int queryGovPetitionLettersCount(@RequestBody GovPetitionLetterDto govPetitionLetterDto) {
        return govPetitionLetterServiceDaoImpl.queryGovPetitionLettersCount(BeanConvertUtil.beanCovertMap(govPetitionLetterDto));    }

    public IGovPetitionLetterServiceDao getGovPetitionLetterServiceDaoImpl() {
        return govPetitionLetterServiceDaoImpl;
    }

    public void setGovPetitionLetterServiceDaoImpl(IGovPetitionLetterServiceDao govPetitionLetterServiceDaoImpl) {
        this.govPetitionLetterServiceDaoImpl = govPetitionLetterServiceDaoImpl;
    }
}
