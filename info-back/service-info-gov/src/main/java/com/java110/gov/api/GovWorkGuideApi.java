package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govWorkGuide.GovWorkGuideDto;
import com.java110.gov.bmo.govWorkGuide.IDeleteGovWorkGuideBMO;
import com.java110.gov.bmo.govWorkGuide.IGetGovWorkGuideBMO;
import com.java110.gov.bmo.govWorkGuide.ISaveGovWorkGuideBMO;
import com.java110.gov.bmo.govWorkGuide.IUpdateGovWorkGuideBMO;
import com.java110.po.govWorkGuide.GovWorkGuidePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govWorkGuide")
public class GovWorkGuideApi {

    @Autowired
    private ISaveGovWorkGuideBMO saveGovWorkGuideBMOImpl;
    @Autowired
    private IUpdateGovWorkGuideBMO updateGovWorkGuideBMOImpl;
    @Autowired
    private IDeleteGovWorkGuideBMO deleteGovWorkGuideBMOImpl;

    @Autowired
    private IGetGovWorkGuideBMO getGovWorkGuideBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govWorkGuide/saveGovWorkGuide
     * @path /app/govWorkGuide/saveGovWorkGuide
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovWorkGuide", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovWorkGuide(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "guideName", "请求报文中未包含guideName");
        Assert.hasKeyAndValue(reqJson, "guideType", "请求报文中未包含guideType");
        Assert.hasKeyAndValue(reqJson, "flow", "请求报文中未包含flow");
        Assert.hasKeyAndValue(reqJson, "data", "请求报文中未包含data");
        Assert.hasKeyAndValue(reqJson, "person", "请求报文中未包含person");
        Assert.hasKeyAndValue(reqJson, "link", "请求报文中未包含link");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");


        GovWorkGuidePo govWorkGuidePo = BeanConvertUtil.covertBean(reqJson, GovWorkGuidePo.class);
        return saveGovWorkGuideBMOImpl.save(govWorkGuidePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govWorkGuide/updateGovWorkGuide
     * @path /app/govWorkGuide/updateGovWorkGuide
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovWorkGuide", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovWorkGuide(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "guideName", "请求报文中未包含guideName");
        Assert.hasKeyAndValue(reqJson, "guideType", "请求报文中未包含guideType");
        Assert.hasKeyAndValue(reqJson, "flow", "请求报文中未包含flow");
        Assert.hasKeyAndValue(reqJson, "data", "请求报文中未包含data");
        Assert.hasKeyAndValue(reqJson, "person", "请求报文中未包含person");
        Assert.hasKeyAndValue(reqJson, "link", "请求报文中未包含link");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "wgId", "wgId不能为空");


        GovWorkGuidePo govWorkGuidePo = BeanConvertUtil.covertBean(reqJson, GovWorkGuidePo.class);
        return updateGovWorkGuideBMOImpl.update(govWorkGuidePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govWorkGuide/deleteGovWorkGuide
     * @path /app/govWorkGuide/deleteGovWorkGuide
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovWorkGuide", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovWorkGuide(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "wgId", "wgId不能为空");
        GovWorkGuidePo govWorkGuidePo = BeanConvertUtil.covertBean(reqJson, GovWorkGuidePo.class);
        return deleteGovWorkGuideBMOImpl.delete(govWorkGuidePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govWorkGuide/queryGovWorkGuide
     * @path /app/govWorkGuide/queryGovWorkGuide
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovWorkGuide", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovWorkGuide(@RequestParam(value = "caId") String caId,
                                                    @RequestParam(value = "guideName" , required = false) String guideName,
                                                    @RequestParam(value = "guideType" , required = false) String guideType,
                                                    @RequestParam(value = "state" , required = false) String state,
                                                    @RequestParam(value = "flow" , required = false) String flow,
                                                    @RequestParam(value = "person" , required = false) String person,
                                                    @RequestParam(value = "link" , required = false) String link,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovWorkGuideDto govWorkGuideDto = new GovWorkGuideDto();
        govWorkGuideDto.setPage(page);
        govWorkGuideDto.setRow(row);
        govWorkGuideDto.setCaId(caId);
        govWorkGuideDto.setGuideName(guideName);
        govWorkGuideDto.setGuideType(guideType);
        govWorkGuideDto.setState(state);
        govWorkGuideDto.setFlow(flow);
        govWorkGuideDto.setPerson(person);
        govWorkGuideDto.setLink(link);
        return getGovWorkGuideBMOImpl.get(govWorkGuideDto);
    }
}
