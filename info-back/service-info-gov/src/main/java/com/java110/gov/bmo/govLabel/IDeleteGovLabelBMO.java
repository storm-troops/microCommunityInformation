package com.java110.gov.bmo.govLabel;
import org.springframework.http.ResponseEntity;
import com.java110.po.govLabel.GovLabelPo;

public interface IDeleteGovLabelBMO {


    /**
     * 修改标签管理
     * add by wuxw
     * @param govLabelPo
     * @return
     */
    ResponseEntity<String> delete(GovLabelPo govLabelPo);


}
