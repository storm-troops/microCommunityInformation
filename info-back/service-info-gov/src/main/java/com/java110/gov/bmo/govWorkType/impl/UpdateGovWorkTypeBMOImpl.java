package com.java110.gov.bmo.govWorkType.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govWorkType.IUpdateGovWorkTypeBMO;
import com.java110.intf.gov.IGovWorkTypeInnerServiceSMO;
import com.java110.po.govWorkType.GovWorkTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovWorkTypeBMOImpl")
public class UpdateGovWorkTypeBMOImpl implements IUpdateGovWorkTypeBMO {

    @Autowired
    private IGovWorkTypeInnerServiceSMO govWorkTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param govWorkTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovWorkTypePo govWorkTypePo) {

        int flag = govWorkTypeInnerServiceSMOImpl.updateGovWorkType(govWorkTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
