package com.java110.gov.bmo.govActivityPerson;

import com.java110.po.govActivityPerson.GovActivityPersonPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovActivityPersonBMO {


    /**
     * 添加活动报名人员
     * add by wuxw
     * @param govActivityPersonPo
     * @return
     */
    ResponseEntity<String> save(GovActivityPersonPo govActivityPersonPo);


}
