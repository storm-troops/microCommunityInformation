package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * 报事管理组件内部之间使用，没有给外围系统提供服务能力
 * 报事管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IReportPoolServiceDao {


    /**
     * 保存 报事管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveReportPoolInfo(Map info) throws DAOException;




    /**
     * 查询报事管理信息（instance过程）
     * 根据bId 查询报事管理信息
     * @param info bId 信息
     * @return 报事管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getReportPoolInfo(Map info) throws DAOException;


    /**
     * 查询报事管理信息（instance过程）
     * 根据bId 查询报事管理信息
     * @param info bId 信息
     * @return 报事管理信息
     * @throws DAOException DAO异常
     */
    List<Map> quseryReportStaffs(Map info) throws DAOException;
    /**
     * 查询报事管理信息（instance过程）
     * 根据bId 查询报事管理信息
     * @param info bId 信息
     * @return 报事管理信息
     * @throws DAOException DAO异常
     */
    List<Map> queryStaffFinishReports(Map info) throws DAOException;

    /**
     * 查询报事管理信息（instance过程）
     * 根据bId 查询报事管理信息
     * @param info bId 信息
     * @return 报事管理信息
     * @throws DAOException DAO异常
     */
    List<Map> quseryReportFinishStaffs(Map info) throws DAOException;



    /**
     * 修改报事管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateReportPoolInfo(Map info) throws DAOException;


    /**
     * 查询报事管理总数
     *
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    int queryReportPoolsCount(Map info);
    /**
     * 查询报事管理总数
     *
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    int quseryReportStaffCount(Map info);
    /**
     * 查询报事管理总数
     *
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    int queryStaffFinishReportCount(Map info);
    /**
     * 查询报事管理总数
     *
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    int quseryReportFinishStaffCount(Map info);

}
