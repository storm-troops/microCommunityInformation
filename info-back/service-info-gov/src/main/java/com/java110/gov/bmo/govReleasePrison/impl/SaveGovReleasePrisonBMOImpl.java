package com.java110.gov.bmo.govReleasePrison.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.gov.bmo.govReleasePrison.ISaveGovReleasePrisonBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govReleasePrison.GovReleasePrisonPo;
import com.java110.intf.gov.IGovReleasePrisonInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovReleasePrisonBMOImpl")
public class SaveGovReleasePrisonBMOImpl implements ISaveGovReleasePrisonBMO {

    @Autowired
    private IGovReleasePrisonInnerServiceSMO govReleasePrisonInnerServiceSMOImpl;

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govReleasePrisonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovReleasePrisonPo govReleasePrisonPo) {

        govReleasePrisonPo.setReleasePrisonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_releasePrisonId));
        //往人口表加一条数据
        GovPersonPo govPersonPo = new GovPersonPo();
        govPersonPo.setPersonName(govReleasePrisonPo.getName());
        govPersonPo.setPersonTel(govReleasePrisonPo.getTel());
        govPersonPo.setCaId(govReleasePrisonPo.getCaId());
        govPersonPo.setIsWeb("F");
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govGridId));
        govPersonPo.setPersonType("8008");//特殊人员
        govPersonPo.setRamark("系統自动生成");
        govPersonPo.setReligiousBelief("无");
        govPersonPo.setNativePlace("中国");
        govPersonPo.setIdType("1");
        govPersonPo.setBirthday(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_B));
        govPersonPo.setDatasourceType("999999");
        govPersonPo.setPrePersonName(govPersonPo.getPersonName());
        govPersonPo.setPoliticalOutlook("5012");

        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPersonTel(govPersonPo.getPersonTel());
        govPersonDto.setPersonName(govPersonPo.getPersonName());
        govPersonDto.setCaId(govPersonPo.getCaId());
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
            if (flag < 1) {
                return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存固定人口失败");
            }
        } else {
            govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
            int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
            if (flag < 1) {
                return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "修改固定人口失败");
            }
        }
        int flag = govReleasePrisonInnerServiceSMOImpl.saveGovReleasePrison(govReleasePrisonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
