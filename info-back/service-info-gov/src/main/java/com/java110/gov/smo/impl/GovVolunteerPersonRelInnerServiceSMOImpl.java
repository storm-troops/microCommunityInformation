package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovVolunteerPersonRelServiceDao;
import com.java110.intf.gov.IGovVolunteerPersonRelInnerServiceSMO;
import com.java110.dto.govVolunteerPersonRel.GovVolunteerPersonRelDto;
import com.java110.po.govVolunteerPersonRel.GovVolunteerPersonRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 服务记录人员关系表内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovVolunteerPersonRelInnerServiceSMOImpl extends BaseServiceSMO implements IGovVolunteerPersonRelInnerServiceSMO {

    @Autowired
    private IGovVolunteerPersonRelServiceDao govVolunteerPersonRelServiceDaoImpl;


    @Override
    public int saveGovVolunteerPersonRel(@RequestBody  GovVolunteerPersonRelPo govVolunteerPersonRelPo) {
        int saveFlag = 1;
        govVolunteerPersonRelServiceDaoImpl.saveGovVolunteerPersonRelInfo(BeanConvertUtil.beanCovertMap(govVolunteerPersonRelPo));
        return saveFlag;
    }

     @Override
    public int updateGovVolunteerPersonRel(@RequestBody  GovVolunteerPersonRelPo govVolunteerPersonRelPo) {
        int saveFlag = 1;
         govVolunteerPersonRelServiceDaoImpl.updateGovVolunteerPersonRelInfo(BeanConvertUtil.beanCovertMap(govVolunteerPersonRelPo));
        return saveFlag;
    }

     @Override
    public int deleteGovVolunteerPersonRel(@RequestBody  GovVolunteerPersonRelPo govVolunteerPersonRelPo) {
        int saveFlag = 1;
        govVolunteerPersonRelPo.setStatusCd("1");
        govVolunteerPersonRelServiceDaoImpl.updateGovVolunteerPersonRelInfo(BeanConvertUtil.beanCovertMap(govVolunteerPersonRelPo));
        return saveFlag;
    }

    @Override
    public List<GovVolunteerPersonRelDto> queryGovVolunteerPersonRels(@RequestBody  GovVolunteerPersonRelDto govVolunteerPersonRelDto) {

        //校验是否传了 分页信息

        int page = govVolunteerPersonRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govVolunteerPersonRelDto.setPage((page - 1) * govVolunteerPersonRelDto.getRow());
        }

        List<GovVolunteerPersonRelDto> govVolunteerPersonRels = BeanConvertUtil.covertBeanList(govVolunteerPersonRelServiceDaoImpl.getGovVolunteerPersonRelInfo(BeanConvertUtil.beanCovertMap(govVolunteerPersonRelDto)), GovVolunteerPersonRelDto.class);

        return govVolunteerPersonRels;
    }


    @Override
    public int queryGovVolunteerPersonRelsCount(@RequestBody GovVolunteerPersonRelDto govVolunteerPersonRelDto) {
        return govVolunteerPersonRelServiceDaoImpl.queryGovVolunteerPersonRelsCount(BeanConvertUtil.beanCovertMap(govVolunteerPersonRelDto));    }

    public IGovVolunteerPersonRelServiceDao getGovVolunteerPersonRelServiceDaoImpl() {
        return govVolunteerPersonRelServiceDaoImpl;
    }

    public void setGovVolunteerPersonRelServiceDaoImpl(IGovVolunteerPersonRelServiceDao govVolunteerPersonRelServiceDaoImpl) {
        this.govVolunteerPersonRelServiceDaoImpl = govVolunteerPersonRelServiceDaoImpl;
    }
}
