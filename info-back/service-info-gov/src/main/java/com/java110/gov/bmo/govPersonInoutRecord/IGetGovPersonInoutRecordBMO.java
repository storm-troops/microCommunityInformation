package com.java110.gov.bmo.govPersonInoutRecord;
import com.java110.dto.govPersonInoutRecord.GovPersonInoutRecordDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPersonInoutRecordBMO {


    /**
     * 查询开门记录
     * add by wuxw
     * @param  govPersonInoutRecordDto
     * @return
     */
    ResponseEntity<String> get(GovPersonInoutRecordDto govPersonInoutRecordDto);


}
