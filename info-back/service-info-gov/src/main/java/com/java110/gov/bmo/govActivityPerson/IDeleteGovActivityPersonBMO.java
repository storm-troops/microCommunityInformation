package com.java110.gov.bmo.govActivityPerson;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovActivityPersonBMO {


    /**
     * 修改活动报名人员
     * add by wuxw
     * @param govActivityPersonPo
     * @return
     */
    ResponseEntity<String> delete(GovActivityPersonPo govActivityPersonPo);


}
