package com.java110.gov.bmo.govAreaRenovation.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govAreaRenovation.IGetGovAreaRenovationBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovAreaRenovationInnerServiceSMO;
import com.java110.dto.govAreaRenovation.GovAreaRenovationDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovAreaRenovationBMOImpl")
public class GetGovAreaRenovationBMOImpl implements IGetGovAreaRenovationBMO {

    @Autowired
    private IGovAreaRenovationInnerServiceSMO govAreaRenovationInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govAreaRenovationDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovAreaRenovationDto govAreaRenovationDto) {


        int count = govAreaRenovationInnerServiceSMOImpl.queryGovAreaRenovationsCount(govAreaRenovationDto);

        List<GovAreaRenovationDto> govAreaRenovationDtos = null;
        if (count > 0) {
            govAreaRenovationDtos = govAreaRenovationInnerServiceSMOImpl.queryGovAreaRenovations(govAreaRenovationDto);
        } else {
            govAreaRenovationDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govAreaRenovationDto.getRow()), count, govAreaRenovationDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
