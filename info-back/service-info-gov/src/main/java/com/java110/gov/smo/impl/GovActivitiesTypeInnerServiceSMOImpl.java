package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovActivitiesTypeServiceDao;
import com.java110.dto.govActivitiesType.GovActivitiesTypeDto;
import com.java110.intf.gov.IGovActivitiesTypeInnerServiceSMO;
import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 公告类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivitiesTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivitiesTypeInnerServiceSMO {

    @Autowired
    private IGovActivitiesTypeServiceDao govActivitiesTypeServiceDaoImpl;


    @Override
    public int saveGovActivitiesType(@RequestBody GovActivitiesTypePo govActivitiesTypePo) {
        int saveFlag = 1;
        govActivitiesTypeServiceDaoImpl.saveGovActivitiesTypeInfo(BeanConvertUtil.beanCovertMap(govActivitiesTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovActivitiesType(@RequestBody  GovActivitiesTypePo govActivitiesTypePo) {
        int saveFlag = 1;
         govActivitiesTypeServiceDaoImpl.updateGovActivitiesTypeInfo(BeanConvertUtil.beanCovertMap(govActivitiesTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivitiesType(@RequestBody  GovActivitiesTypePo govActivitiesTypePo) {
        int saveFlag = 1;
        govActivitiesTypePo.setStatusCd("1");
        govActivitiesTypeServiceDaoImpl.updateGovActivitiesTypeInfo(BeanConvertUtil.beanCovertMap(govActivitiesTypePo));
        return saveFlag;
    }

    @Override
    public List<GovActivitiesTypeDto> queryGovActivitiesTypes(@RequestBody  GovActivitiesTypeDto govActivitiesTypeDto) {

        //校验是否传了 分页信息

        int page = govActivitiesTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivitiesTypeDto.setPage((page - 1) * govActivitiesTypeDto.getRow());
        }

        List<GovActivitiesTypeDto> govActivitiesTypes = BeanConvertUtil.covertBeanList(govActivitiesTypeServiceDaoImpl.getGovActivitiesTypeInfo(BeanConvertUtil.beanCovertMap(govActivitiesTypeDto)), GovActivitiesTypeDto.class);

        return govActivitiesTypes;
    }


    @Override
    public int queryGovActivitiesTypesCount(@RequestBody GovActivitiesTypeDto govActivitiesTypeDto) {
        return govActivitiesTypeServiceDaoImpl.queryGovActivitiesTypesCount(BeanConvertUtil.beanCovertMap(govActivitiesTypeDto));    }

    public IGovActivitiesTypeServiceDao getGovActivitiesTypeServiceDaoImpl() {
        return govActivitiesTypeServiceDaoImpl;
    }

    public void setGovActivitiesTypeServiceDaoImpl(IGovActivitiesTypeServiceDao govActivitiesTypeServiceDaoImpl) {
        this.govActivitiesTypeServiceDaoImpl = govActivitiesTypeServiceDaoImpl;
    }
}
