package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 党组织组件内部之间使用，没有给外围系统提供服务能力
 * 党组织服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovPartyOrgServiceDao {


    /**
     * 保存 党组织信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovPartyOrgInfo(Map info) throws DAOException;




    /**
     * 查询党组织信息（instance过程）
     * 根据bId 查询党组织信息
     * @param info bId 信息
     * @return 党组织信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovPartyOrgInfo(Map info) throws DAOException;

    /**
     * 查询党组织信息（instance过程）
     * 根据bId 查询党组织信息
     * @param info bId 信息
     * @return 党组织信息
     * @throws DAOException DAO异常
     */
    List<Map> getNotGovPartyOrgInfo(Map info) throws DAOException;



    /**
     * 修改党组织信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovPartyOrgInfo(Map info) throws DAOException;


    /**
     * 查询党组织总数
     *
     * @param info 党组织信息
     * @return 党组织数量
     */
    int queryGovPartyOrgsCount(Map info);

}
