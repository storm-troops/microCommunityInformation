package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovWorkTypeServiceDao;
import com.java110.dto.govWorkType.GovWorkTypeDto;
import com.java110.intf.gov.IGovWorkTypeInnerServiceSMO;
import com.java110.po.govWorkType.GovWorkTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 党内职务内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovWorkTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovWorkTypeInnerServiceSMO {

    @Autowired
    private IGovWorkTypeServiceDao govWorkTypeServiceDaoImpl;


    @Override
    public int saveGovWorkType(@RequestBody GovWorkTypePo govWorkTypePo) {
        int saveFlag = 1;
        govWorkTypeServiceDaoImpl.saveGovWorkTypeInfo(BeanConvertUtil.beanCovertMap(govWorkTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovWorkType(@RequestBody  GovWorkTypePo govWorkTypePo) {
        int saveFlag = 1;
         govWorkTypeServiceDaoImpl.updateGovWorkTypeInfo(BeanConvertUtil.beanCovertMap(govWorkTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovWorkType(@RequestBody  GovWorkTypePo govWorkTypePo) {
        int saveFlag = 1;
        govWorkTypePo.setStatusCd("1");
        govWorkTypeServiceDaoImpl.updateGovWorkTypeInfo(BeanConvertUtil.beanCovertMap(govWorkTypePo));
        return saveFlag;
    }

    @Override
    public List<GovWorkTypeDto> queryGovWorkTypes(@RequestBody  GovWorkTypeDto govWorkTypeDto) {

        //校验是否传了 分页信息

        int page = govWorkTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govWorkTypeDto.setPage((page - 1) * govWorkTypeDto.getRow());
        }

        List<GovWorkTypeDto> govWorkTypes = BeanConvertUtil.covertBeanList(govWorkTypeServiceDaoImpl.getGovWorkTypeInfo(BeanConvertUtil.beanCovertMap(govWorkTypeDto)), GovWorkTypeDto.class);

        return govWorkTypes;
    }


    @Override
    public int queryGovWorkTypesCount(@RequestBody GovWorkTypeDto govWorkTypeDto) {
        return govWorkTypeServiceDaoImpl.queryGovWorkTypesCount(BeanConvertUtil.beanCovertMap(govWorkTypeDto));    }

    public IGovWorkTypeServiceDao getGovWorkTypeServiceDaoImpl() {
        return govWorkTypeServiceDaoImpl;
    }

    public void setGovWorkTypeServiceDaoImpl(IGovWorkTypeServiceDao govWorkTypeServiceDaoImpl) {
        this.govWorkTypeServiceDaoImpl = govWorkTypeServiceDaoImpl;
    }
}
