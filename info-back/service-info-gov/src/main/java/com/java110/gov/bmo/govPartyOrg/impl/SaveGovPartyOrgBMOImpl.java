package com.java110.gov.bmo.govPartyOrg.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govPartyOrg.ISaveGovPartyOrgBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.intf.gov.IGovPartyOrgInnerServiceSMO;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovPartyOrgBMOImpl")
public class SaveGovPartyOrgBMOImpl implements ISaveGovPartyOrgBMO {

    @Autowired
    private IGovPartyOrgInnerServiceSMO govPartyOrgInnerServiceSMOImpl;
    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govPartyOrgPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPartyOrgPo govPartyOrgPo) {

        govPartyOrgPo.setOrgId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_orgId));
        int flag = govPartyOrgInnerServiceSMOImpl.saveGovPartyOrg(govPartyOrgPo);

        if (flag > 0) {
            GovPartyOrgContextPo govPartyOrgContextPo = new GovPartyOrgContextPo();
            govPartyOrgContextPo.setOrgId( govPartyOrgPo.getOrgId() );
            govPartyOrgContextPo.setContext( govPartyOrgPo.getContext() );
            int flagCon = govPartyOrgContextInnerServiceSMOImpl.saveGovPartyOrgContext(govPartyOrgContextPo);
            if (flagCon > 0) {
                return ResultVo.createResponseEntity( ResultVo.CODE_OK, "保存成功" );
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
