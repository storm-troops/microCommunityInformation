package com.java110.gov.bmo.govReleasePrison.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govReleasePrison.IGetGovReleasePrisonBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovReleasePrisonInnerServiceSMO;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovReleasePrisonBMOImpl")
public class GetGovReleasePrisonBMOImpl implements IGetGovReleasePrisonBMO {

    @Autowired
    private IGovReleasePrisonInnerServiceSMO govReleasePrisonInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govReleasePrisonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovReleasePrisonDto govReleasePrisonDto) {


        int count = govReleasePrisonInnerServiceSMOImpl.queryGovReleasePrisonsCount(govReleasePrisonDto);

        List<GovReleasePrisonDto> govReleasePrisonDtos = null;
        if (count > 0) {
            govReleasePrisonDtos = govReleasePrisonInnerServiceSMOImpl.queryGovReleasePrisons(govReleasePrisonDto);
        } else {
            govReleasePrisonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govReleasePrisonDto.getRow()), count, govReleasePrisonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
