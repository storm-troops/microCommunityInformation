package com.java110.gov.bmo.govPetitionType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govPetitionType.IDeleteGovPetitionTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import com.java110.po.govPetitionType.GovPetitionTypePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovPetitionTypeInnerServiceSMO;
import com.java110.dto.govPetitionType.GovPetitionTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovPetitionTypeBMOImpl")
public class DeleteGovPetitionTypeBMOImpl implements IDeleteGovPetitionTypeBMO {

    @Autowired
    private IGovPetitionTypeInnerServiceSMO govPetitionTypeInnerServiceSMOImpl;

    /**
     * @param govPetitionTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovPetitionTypePo govPetitionTypePo) {

        int flag = govPetitionTypeInnerServiceSMOImpl.deleteGovPetitionType(govPetitionTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
