package com.java110.gov.bmo.perGovActivities.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.perGovActivities.IUpdatePerGovActivitiesBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.gov.IPerGovActivityRelInnerServiceSMO;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IPerGovActivitiesInnerServiceSMO;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
import java.util.List;

@Service("updatePerGovActivitiesBMOImpl")
public class UpdatePerGovActivitiesBMOImpl implements IUpdatePerGovActivitiesBMO {

    @Autowired
    private IPerGovActivitiesInnerServiceSMO perGovActivitiesInnerServiceSMOImpl;

    @Autowired
    private IPerGovActivityRelInnerServiceSMO perGovActivityRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param perGovActivitiesPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(PerGovActivitiesPo perGovActivitiesPo, JSONArray oldPersons) {

        int flag = perGovActivitiesInnerServiceSMOImpl.updatePerGovActivities(perGovActivitiesPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }
        PerGovActivityRelPo relPo = new PerGovActivityRelPo();
        relPo.setPerActivitiesId(perGovActivitiesPo.getPerActivitiesId());
        perGovActivityRelInnerServiceSMOImpl.deletePerGovActivityRel(relPo);
        if (oldPersons != null && oldPersons.size() > 0) {
            for (int oldIndex = 0; oldIndex < oldPersons.size(); oldIndex++) {
                PerGovActivityRelPo perGovActivityRelPo = BeanConvertUtil.covertBean(oldPersons.get(oldIndex), PerGovActivityRelPo.class);
                perGovActivityRelPo.setPerActivitiesId(perGovActivitiesPo.getPerActivitiesId());
                perGovActivityRelPo.setCaId(perGovActivitiesPo.getCaId());
                perGovActivityRelPo.setActRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_actRelId));
                flag = perGovActivityRelInnerServiceSMOImpl.savePerGovActivityRel(perGovActivityRelPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存老人信息失败");
                }
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
