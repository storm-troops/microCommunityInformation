package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovSpecialFollowServiceDao;
import com.java110.intf.gov.IGovSpecialFollowInnerServiceSMO;
import com.java110.dto.govSpecialFollow.GovSpecialFollowDto;
import com.java110.po.govSpecialFollow.GovSpecialFollowPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 特殊人员跟进记录内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovSpecialFollowInnerServiceSMOImpl extends BaseServiceSMO implements IGovSpecialFollowInnerServiceSMO {

    @Autowired
    private IGovSpecialFollowServiceDao govSpecialFollowServiceDaoImpl;


    @Override
    public int saveGovSpecialFollow(@RequestBody  GovSpecialFollowPo govSpecialFollowPo) {
        int saveFlag = 1;
        govSpecialFollowServiceDaoImpl.saveGovSpecialFollowInfo(BeanConvertUtil.beanCovertMap(govSpecialFollowPo));
        return saveFlag;
    }

     @Override
    public int updateGovSpecialFollow(@RequestBody  GovSpecialFollowPo govSpecialFollowPo) {
        int saveFlag = 1;
         govSpecialFollowServiceDaoImpl.updateGovSpecialFollowInfo(BeanConvertUtil.beanCovertMap(govSpecialFollowPo));
        return saveFlag;
    }

     @Override
    public int deleteGovSpecialFollow(@RequestBody  GovSpecialFollowPo govSpecialFollowPo) {
        int saveFlag = 1;
        govSpecialFollowPo.setStatusCd("1");
        govSpecialFollowServiceDaoImpl.updateGovSpecialFollowInfo(BeanConvertUtil.beanCovertMap(govSpecialFollowPo));
        return saveFlag;
    }

    @Override
    public List<GovSpecialFollowDto> queryGovSpecialFollows(@RequestBody  GovSpecialFollowDto govSpecialFollowDto) {

        //校验是否传了 分页信息

        int page = govSpecialFollowDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govSpecialFollowDto.setPage((page - 1) * govSpecialFollowDto.getRow());
        }

        List<GovSpecialFollowDto> govSpecialFollows = BeanConvertUtil.covertBeanList(govSpecialFollowServiceDaoImpl.getGovSpecialFollowInfo(BeanConvertUtil.beanCovertMap(govSpecialFollowDto)), GovSpecialFollowDto.class);

        return govSpecialFollows;
    }


    @Override
    public int queryGovSpecialFollowsCount(@RequestBody GovSpecialFollowDto govSpecialFollowDto) {
        return govSpecialFollowServiceDaoImpl.queryGovSpecialFollowsCount(BeanConvertUtil.beanCovertMap(govSpecialFollowDto));    }

    public IGovSpecialFollowServiceDao getGovSpecialFollowServiceDaoImpl() {
        return govSpecialFollowServiceDaoImpl;
    }

    public void setGovSpecialFollowServiceDaoImpl(IGovSpecialFollowServiceDao govSpecialFollowServiceDaoImpl) {
        this.govSpecialFollowServiceDaoImpl = govSpecialFollowServiceDaoImpl;
    }
}
