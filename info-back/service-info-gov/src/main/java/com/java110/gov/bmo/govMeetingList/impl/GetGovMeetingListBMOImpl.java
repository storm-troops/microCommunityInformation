package com.java110.gov.bmo.govMeetingList.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.gov.bmo.govMeetingList.IGetGovMeetingListBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.gov.IGovMeetingMemberRelInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovMeetingListInnerServiceSMO;
import com.java110.dto.govMeetingList.GovMeetingListDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMeetingListBMOImpl")
public class GetGovMeetingListBMOImpl implements IGetGovMeetingListBMO {

    @Autowired
    private IGovMeetingListInnerServiceSMO govMeetingListInnerServiceSMOImpl;
    @Autowired
    private IGovMeetingMemberRelInnerServiceSMO govMeetingMemberRelInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     *
     *
     * @param  govMeetingListDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMeetingListDto govMeetingListDto) {


        int count = govMeetingListInnerServiceSMOImpl.queryGovMeetingListsCount(govMeetingListDto);

        List<GovMeetingListDto> govMeetingListDtos = null;
        if (count > 0) {
            govMeetingListDtos = govMeetingListInnerServiceSMOImpl.queryGovMeetingLists(govMeetingListDto);
            resfGovMeetingMemberRel(govMeetingListDtos);
        } else {
            govMeetingListDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMeetingListDto.getRow()), count, govMeetingListDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    @Override
    public ResponseEntity<String> getPersonIdMeetingList(GovMeetingListDto govMeetingListDto, String govMemberId) {

        GovMeetingMemberRelDto govMeetingMemberRelDto = new GovMeetingMemberRelDto();
        govMeetingMemberRelDto.setGovMemberId(govMemberId);
        govMeetingMemberRelDto.setCaId(govMeetingListDto.getCaId());
        List<GovMeetingMemberRelDto>  GovMeetingMemberRelDtos
                = govMeetingMemberRelInnerServiceSMOImpl.queryGovMeetingMemberRels(govMeetingMemberRelDto);
        Assert.isNotNull( GovMeetingMemberRelDtos,"没有开会记录" );

        govMeetingListDto.setGovMeetingId( GovMeetingMemberRelDtos.get( 0 ).getGovMeetingId() );
        int count = govMeetingListInnerServiceSMOImpl.queryGovMeetingListsCount(govMeetingListDto);

        List<GovMeetingListDto> govMeetingListDtos = null;
        if (count > 0) {
            govMeetingListDtos = govMeetingListInnerServiceSMOImpl.queryGovMeetingLists(govMeetingListDto);
            resfGovMeetingMemberRel(govMeetingListDtos);
        } else {
            govMeetingListDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMeetingListDto.getRow()), count, govMeetingListDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void resfGovMeetingMemberRel(List<GovMeetingListDto> govMeetingListDtos) {
        if (govMeetingListDtos == null || govMeetingListDtos.size() < 1) {
            return;
        }
        List<String> govMeetingIds = new ArrayList<>();
        for (GovMeetingListDto govMeetingListDto : govMeetingListDtos) {
            govMeetingIds.add(govMeetingListDto.getGovMeetingId());
        }

        GovMeetingMemberRelDto govMeetingMemberRelDto = new GovMeetingMemberRelDto();
        govMeetingMemberRelDto.setGovMeetingIds(govMeetingIds.toArray(new String[govMeetingIds.size()]));
        govMeetingMemberRelDto.setCaId(govMeetingListDtos.get(0).getCaId());
        List<GovMeetingMemberRelDto>  GovMeetingMemberRelDtos
                = govMeetingMemberRelInnerServiceSMOImpl.queryGovMeetingMemberRels(govMeetingMemberRelDto);

        FileRelDto fileRelDto = new FileRelDto();
        fileRelDto.setObjIds(govMeetingIds.toArray(new String[govMeetingIds.size()]));
        fileRelDto.setRelType(FileRelDto.REL_MEETING_TYPE);
        List<FileRelDto> fileRelDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);


        List<GovMeetingMemberRelDto> tmpGovMeetingMemberRelDto = null;
        List<String> meetingImgs = null;
        for (GovMeetingListDto govMeetingListDto : govMeetingListDtos) {
            tmpGovMeetingMemberRelDto = new ArrayList<>();
            for (GovMeetingMemberRelDto govMeetingMemberRel : GovMeetingMemberRelDtos) {
                if (govMeetingListDto.getGovMeetingId().equals(govMeetingMemberRel.getGovMeetingId())) {
                    tmpGovMeetingMemberRelDto.add(govMeetingMemberRel);
                }
            }
            govMeetingListDto.setGovMeetingMemberRelDtos(tmpGovMeetingMemberRelDto);

            meetingImgs = new ArrayList<>();
            for (FileRelDto fileRel : fileRelDtos) {
                if (govMeetingListDto.getGovMeetingId().equals(fileRel.getObjId())) {
                    meetingImgs.add(fileRel.getFileName());
                }
            }

            govMeetingListDto.setMeetingImgs(meetingImgs);
        }
    }

}
