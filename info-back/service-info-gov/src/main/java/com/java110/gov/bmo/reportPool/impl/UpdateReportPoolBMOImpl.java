package com.java110.gov.bmo.reportPool.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.reportPool.IUpdateReportPoolBMO;
import com.java110.intf.gov.IReportPoolInnerServiceSMO;
import com.java110.po.reportPool.ReportPoolPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateReportPoolBMOImpl")
public class UpdateReportPoolBMOImpl implements IUpdateReportPoolBMO {

    @Autowired
    private IReportPoolInnerServiceSMO reportPoolInnerServiceSMOImpl;

    /**
     *
     *
     * @param reportPoolPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(ReportPoolPo reportPoolPo) {

        int flag = reportPoolInnerServiceSMOImpl.updateReportPool(reportPoolPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
