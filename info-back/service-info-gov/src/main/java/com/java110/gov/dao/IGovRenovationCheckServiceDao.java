package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 重点地区整治情况组件内部之间使用，没有给外围系统提供服务能力
 * 重点地区整治情况服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovRenovationCheckServiceDao {


    /**
     * 保存 重点地区整治情况信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovRenovationCheckInfo(Map info) throws DAOException;




    /**
     * 查询重点地区整治情况信息（instance过程）
     * 根据bId 查询重点地区整治情况信息
     * @param info bId 信息
     * @return 重点地区整治情况信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovRenovationCheckInfo(Map info) throws DAOException;



    /**
     * 修改重点地区整治情况信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovRenovationCheckInfo(Map info) throws DAOException;


    /**
     * 查询重点地区整治情况总数
     *
     * @param info 重点地区整治情况信息
     * @return 重点地区整治情况数量
     */
    int queryGovRenovationChecksCount(Map info);

}
