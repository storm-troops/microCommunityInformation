package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IPerGovActivitiesServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 生日记录服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("perGovActivitiesServiceDaoImpl")
//@Transactional
public class PerGovActivitiesServiceDaoImpl extends BaseServiceDao implements IPerGovActivitiesServiceDao {

    private static Logger logger = LoggerFactory.getLogger(PerGovActivitiesServiceDaoImpl.class);





    /**
     * 保存生日记录信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void savePerGovActivitiesInfo(Map info) throws DAOException {
        logger.debug("保存生日记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("perGovActivitiesServiceDaoImpl.savePerGovActivitiesInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存生日记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询生日记录信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getPerGovActivitiesInfo(Map info) throws DAOException {
        logger.debug("查询生日记录信息 入参 info : {}",info);

        List<Map> businessPerGovActivitiesInfos = sqlSessionTemplate.selectList("perGovActivitiesServiceDaoImpl.getPerGovActivitiesInfo",info);

        return businessPerGovActivitiesInfos;
    }


    /**
     * 修改生日记录信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updatePerGovActivitiesInfo(Map info) throws DAOException {
        logger.debug("修改生日记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("perGovActivitiesServiceDaoImpl.updatePerGovActivitiesInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改生日记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询生日记录数量
     * @param info 生日记录信息
     * @return 生日记录数量
     */
    @Override
    public int queryPerGovActivitiessCount(Map info) {
        logger.debug("查询生日记录数据 入参 info : {}",info);

        List<Map> businessPerGovActivitiesInfos = sqlSessionTemplate.selectList("perGovActivitiesServiceDaoImpl.queryPerGovActivitiessCount", info);
        if (businessPerGovActivitiesInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessPerGovActivitiesInfos.get(0).get("count").toString());
    }


}
