package com.java110.gov.bmo.govActivitiesType.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govActivitiesType.IDeleteGovActivitiesTypeBMO;
import com.java110.intf.gov.IGovActivitiesTypeInnerServiceSMO;
import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovActivitiesTypeBMOImpl")
public class DeleteGovActivitiesTypeBMOImpl implements IDeleteGovActivitiesTypeBMO {

    @Autowired
    private IGovActivitiesTypeInnerServiceSMO govActivitiesTypeInnerServiceSMOImpl;

    /**
     * @param govActivitiesTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovActivitiesTypePo govActivitiesTypePo) {

        int flag = govActivitiesTypeInnerServiceSMOImpl.deleteGovActivitiesType(govActivitiesTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
