package com.java110.gov.bmo.govActivityLikes.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govActivityLikes.IUpdateGovActivityLikesBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovActivityLikesInnerServiceSMO;
import com.java110.dto.govActivityLikes.GovActivityLikesDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govActivityLikes.GovActivityLikesPo;
import java.util.List;

@Service("updateGovActivityLikesBMOImpl")
public class UpdateGovActivityLikesBMOImpl implements IUpdateGovActivityLikesBMO {

    @Autowired
    private IGovActivityLikesInnerServiceSMO govActivityLikesInnerServiceSMOImpl;

    /**
     *
     *
     * @param govActivityLikesPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovActivityLikesPo govActivityLikesPo) {

        int flag = govActivityLikesInnerServiceSMOImpl.updateGovActivityLikes(govActivityLikesPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
