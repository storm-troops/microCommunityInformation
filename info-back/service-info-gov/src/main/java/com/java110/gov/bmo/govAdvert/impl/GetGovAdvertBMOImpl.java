package com.java110.gov.bmo.govAdvert.impl;

import com.java110.dto.govAdvert.GovAdvertDto;
import com.java110.gov.bmo.govAdvert.IGetGovAdvertBMO;
import com.java110.intf.gov.IGovAdvertInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovAdvertBMOImpl")
public class GetGovAdvertBMOImpl implements IGetGovAdvertBMO {

    @Autowired
    private IGovAdvertInnerServiceSMO govAdvertInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govAdvertDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovAdvertDto govAdvertDto) {


        int count = govAdvertInnerServiceSMOImpl.queryGovAdvertsCount(govAdvertDto);

        List<GovAdvertDto> govAdvertDtos = null;
        if (count > 0) {
            govAdvertDtos = govAdvertInnerServiceSMOImpl.queryGovAdverts(govAdvertDto);
        } else {
            govAdvertDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govAdvertDto.getRow()), count, govAdvertDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }


    /**
     *
     *
     * @param  govAdvertDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getAdvertItmes(GovAdvertDto govAdvertDto) {

        int count = govAdvertInnerServiceSMOImpl.queryGovAdvertsCount(govAdvertDto);

        List<GovAdvertDto> govAdvertDtos = null;
        if (count > 0) {
            govAdvertDtos = govAdvertInnerServiceSMOImpl.queryAdvertItmes(govAdvertDto);
        } else {
            govAdvertDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govAdvertDto.getRow()), count, govAdvertDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
