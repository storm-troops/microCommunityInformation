package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovVolunteerServiceRecordServiceDao;
import com.java110.intf.gov.IGovVolunteerServiceRecordInnerServiceSMO;
import com.java110.dto.govVolunteerServiceRecord.GovVolunteerServiceRecordDto;
import com.java110.po.govVolunteerServiceRecord.GovVolunteerServiceRecordPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 服务记录表内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovVolunteerServiceRecordInnerServiceSMOImpl extends BaseServiceSMO implements IGovVolunteerServiceRecordInnerServiceSMO {

    @Autowired
    private IGovVolunteerServiceRecordServiceDao govVolunteerServiceRecordServiceDaoImpl;


    @Override
    public int saveGovVolunteerServiceRecord(@RequestBody  GovVolunteerServiceRecordPo govVolunteerServiceRecordPo) {
        int saveFlag = 1;
        govVolunteerServiceRecordServiceDaoImpl.saveGovVolunteerServiceRecordInfo(BeanConvertUtil.beanCovertMap(govVolunteerServiceRecordPo));
        return saveFlag;
    }

     @Override
    public int updateGovVolunteerServiceRecord(@RequestBody  GovVolunteerServiceRecordPo govVolunteerServiceRecordPo) {
        int saveFlag = 1;
         govVolunteerServiceRecordServiceDaoImpl.updateGovVolunteerServiceRecordInfo(BeanConvertUtil.beanCovertMap(govVolunteerServiceRecordPo));
        return saveFlag;
    }

     @Override
    public int deleteGovVolunteerServiceRecord(@RequestBody  GovVolunteerServiceRecordPo govVolunteerServiceRecordPo) {
        int saveFlag = 1;
        govVolunteerServiceRecordPo.setStatusCd("1");
        govVolunteerServiceRecordServiceDaoImpl.updateGovVolunteerServiceRecordInfo(BeanConvertUtil.beanCovertMap(govVolunteerServiceRecordPo));
        return saveFlag;
    }

    @Override
    public List<GovVolunteerServiceRecordDto> queryGovVolunteerServiceRecords(@RequestBody  GovVolunteerServiceRecordDto govVolunteerServiceRecordDto) {

        //校验是否传了 分页信息

        int page = govVolunteerServiceRecordDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govVolunteerServiceRecordDto.setPage((page - 1) * govVolunteerServiceRecordDto.getRow());
        }

        List<GovVolunteerServiceRecordDto> govVolunteerServiceRecords = BeanConvertUtil.covertBeanList(govVolunteerServiceRecordServiceDaoImpl.getGovVolunteerServiceRecordInfo(BeanConvertUtil.beanCovertMap(govVolunteerServiceRecordDto)), GovVolunteerServiceRecordDto.class);

        return govVolunteerServiceRecords;
    }


    @Override
    public int queryGovVolunteerServiceRecordsCount(@RequestBody GovVolunteerServiceRecordDto govVolunteerServiceRecordDto) {
        return govVolunteerServiceRecordServiceDaoImpl.queryGovVolunteerServiceRecordsCount(BeanConvertUtil.beanCovertMap(govVolunteerServiceRecordDto));    }

    public IGovVolunteerServiceRecordServiceDao getGovVolunteerServiceRecordServiceDaoImpl() {
        return govVolunteerServiceRecordServiceDaoImpl;
    }

    public void setGovVolunteerServiceRecordServiceDaoImpl(IGovVolunteerServiceRecordServiceDao govVolunteerServiceRecordServiceDaoImpl) {
        this.govVolunteerServiceRecordServiceDaoImpl = govVolunteerServiceRecordServiceDaoImpl;
    }
}
