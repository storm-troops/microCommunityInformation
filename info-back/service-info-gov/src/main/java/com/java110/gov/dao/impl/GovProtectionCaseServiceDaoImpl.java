package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovProtectionCaseServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 周边重点人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govProtectionCaseServiceDaoImpl")
//@Transactional
public class GovProtectionCaseServiceDaoImpl extends BaseServiceDao implements IGovProtectionCaseServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovProtectionCaseServiceDaoImpl.class);





    /**
     * 保存周边重点人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovProtectionCaseInfo(Map info) throws DAOException {
        logger.debug("保存周边重点人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govProtectionCaseServiceDaoImpl.saveGovProtectionCaseInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存周边重点人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询周边重点人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovProtectionCaseInfo(Map info) throws DAOException {
        logger.debug("查询周边重点人员信息 入参 info : {}",info);

        List<Map> businessGovProtectionCaseInfos = sqlSessionTemplate.selectList("govProtectionCaseServiceDaoImpl.getGovProtectionCaseInfo",info);

        return businessGovProtectionCaseInfos;
    }


    /**
     * 修改周边重点人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovProtectionCaseInfo(Map info) throws DAOException {
        logger.debug("修改周边重点人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govProtectionCaseServiceDaoImpl.updateGovProtectionCaseInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改周边重点人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询周边重点人员数量
     * @param info 周边重点人员信息
     * @return 周边重点人员数量
     */
    @Override
    public int queryGovProtectionCasesCount(Map info) {
        logger.debug("查询周边重点人员数据 入参 info : {}",info);

        List<Map> businessGovProtectionCaseInfos = sqlSessionTemplate.selectList("govProtectionCaseServiceDaoImpl.queryGovProtectionCasesCount", info);
        if (businessGovProtectionCaseInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovProtectionCaseInfos.get(0).get("count").toString());
    }


}
