package com.java110.gov.bmo.govCiviladminType.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govCiviladminType.IDeleteGovCiviladminTypeBMO;
import com.java110.intf.gov.IGovCiviladminTypeInnerServiceSMO;
import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovCiviladminTypeBMOImpl")
public class DeleteGovCiviladminTypeBMOImpl implements IDeleteGovCiviladminTypeBMO {

    @Autowired
    private IGovCiviladminTypeInnerServiceSMO govCiviladminTypeInnerServiceSMOImpl;

    /**
     * @param govCiviladminTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovCiviladminTypePo govCiviladminTypePo) {

        int flag = govCiviladminTypeInnerServiceSMOImpl.deleteGovCiviladminType(govCiviladminTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
