package com.java110.gov.bmo.govActivityReply;
import org.springframework.http.ResponseEntity;
import com.java110.po.govActivityReply.GovActivityReplyPo;

public interface IDeleteGovActivityReplyBMO {


    /**
     * 修改活动评论
     * add by wuxw
     * @param govActivityReplyPo
     * @return
     */
    ResponseEntity<String> delete(GovActivityReplyPo govActivityReplyPo);


}
