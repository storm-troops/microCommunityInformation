package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivities.GovActivitiesDto;
import com.java110.gov.bmo.govActivities.IDeleteGovActivitiesBMO;
import com.java110.gov.bmo.govActivities.IGetGovActivitiesBMO;
import com.java110.gov.bmo.govActivities.ISaveGovActivitiesBMO;
import com.java110.gov.bmo.govActivities.IUpdateGovActivitiesBMO;
import com.java110.po.govActivities.GovActivitiesPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govActivities")
public class GovActivitiesApi {

    @Autowired
    private ISaveGovActivitiesBMO saveGovActivitiesBMOImpl;
    @Autowired
    private IUpdateGovActivitiesBMO updateGovActivitiesBMOImpl;
    @Autowired
    private IDeleteGovActivitiesBMO deleteGovActivitiesBMOImpl;

    @Autowired
    private IGetGovActivitiesBMO getGovActivitiesBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govActivities/saveGovActivities
     * @path /app/govActivities/saveGovActivities
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovActivities", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivities(@RequestHeader(value = "user-id") String userId,
                                                    @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reqJson, "headerImg", "请求报文中未包含headerImg");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");

        reqJson.put( "userId",userId );
        GovActivitiesPo govActivitiesPo = BeanConvertUtil.covertBean(reqJson, GovActivitiesPo.class);
        return saveGovActivitiesBMOImpl.save(govActivitiesPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govActivities/updateGovActivities
     * @path /app/govActivities/updateGovActivities
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovActivities", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivities(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "activitiesId", "请求报文中未包含activitiesId");
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reqJson, "headerImg", "请求报文中未包含headerImg");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");


        GovActivitiesPo govActivitiesPo = BeanConvertUtil.covertBean(reqJson, GovActivitiesPo.class);
        return updateGovActivitiesBMOImpl.update(govActivitiesPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivities/deleteGovActivities
     * @path /app/govActivities/deleteGovActivities
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovActivities", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivities(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "activitiesId", "activitiesId不能为空");


        GovActivitiesPo govActivitiesPo = BeanConvertUtil.covertBean(reqJson, GovActivitiesPo.class);
        return deleteGovActivitiesBMOImpl.delete(govActivitiesPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivities/queryGovActivities
     * @path /app/govActivities/queryGovActivities
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovActivities", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivities(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "title",required = false) String title,
                                                      @RequestParam(value = "typeCd",required = false) String typeCd,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovActivitiesDto govActivitiesDto = new GovActivitiesDto();
        govActivitiesDto.setPage(page);
        govActivitiesDto.setRow(row);
        govActivitiesDto.setCaId(caId);
        govActivitiesDto.setTitle(title);
        govActivitiesDto.setTypeCd(typeCd);
        return getGovActivitiesBMOImpl.get(govActivitiesDto);
    }
}
