package com.java110.gov.bmo.govProtectionCase.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.cityArea.CityAreaDto;
import com.java110.dto.govSchool.GovSchoolDto;
import com.java110.gov.bmo.govProtectionCase.IGetGovProtectionCaseBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.ICityAreaInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovProtectionCaseInnerServiceSMO;
import com.java110.dto.govProtectionCase.GovProtectionCaseDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovProtectionCaseBMOImpl")
public class GetGovProtectionCaseBMOImpl implements IGetGovProtectionCaseBMO {

    @Autowired
    private IGovProtectionCaseInnerServiceSMO govProtectionCaseInnerServiceSMOImpl;

    @Autowired
    private ICityAreaInnerServiceSMO cityAreaInnerServiceSMOImpl;
    /**
     *
     *
     * @param  govProtectionCaseDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovProtectionCaseDto govProtectionCaseDto) {


        int count = govProtectionCaseInnerServiceSMOImpl.queryGovProtectionCasesCount(govProtectionCaseDto);

        List<GovProtectionCaseDto> govProtectionCaseDtos = null;
        if (count > 0) {
            govProtectionCaseDtos = govProtectionCaseInnerServiceSMOImpl.queryGovProtectionCases(govProtectionCaseDto);
            reFreshAreaCode(govProtectionCaseDtos);
        } else {
            govProtectionCaseDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govProtectionCaseDto.getRow()), count, govProtectionCaseDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    /**
     * @param govProtectionCaseDtos
     */
    private void reFreshAreaCode(List<GovProtectionCaseDto> govProtectionCaseDtos) {
        for (GovProtectionCaseDto caseDto : govProtectionCaseDtos) {
            CityAreaDto cityAreaDto = new CityAreaDto();
            cityAreaDto.setAreaCode(caseDto.getAreaCode());
            List<CityAreaDto> cityAreaDtos = cityAreaInnerServiceSMOImpl.queryCityAreas(cityAreaDto);
            if (cityAreaDtos != null && cityAreaDtos.size() > 0) {
                caseDto.setAreaName(cityAreaDtos.get(0).getAreaName());
            }
        }
    }
}
