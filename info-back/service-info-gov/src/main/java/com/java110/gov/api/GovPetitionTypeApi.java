package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPetitionType.GovPetitionTypeDto;
import com.java110.po.govPetitionType.GovPetitionTypePo;
import com.java110.gov.bmo.govPetitionType.IDeleteGovPetitionTypeBMO;
import com.java110.gov.bmo.govPetitionType.IGetGovPetitionTypeBMO;
import com.java110.gov.bmo.govPetitionType.ISaveGovPetitionTypeBMO;
import com.java110.gov.bmo.govPetitionType.IUpdateGovPetitionTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPetitionType")
public class GovPetitionTypeApi {

    @Autowired
    private ISaveGovPetitionTypeBMO saveGovPetitionTypeBMOImpl;
    @Autowired
    private IUpdateGovPetitionTypeBMO updateGovPetitionTypeBMOImpl;
    @Autowired
    private IDeleteGovPetitionTypeBMO deleteGovPetitionTypeBMOImpl;

    @Autowired
    private IGetGovPetitionTypeBMO getGovPetitionTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPetitionType/saveGovPetitionType
     * @path /app/govPetitionType/saveGovPetitionType
     */
    @RequestMapping(value = "/saveGovPetitionType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPetitionType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");

        GovPetitionTypePo govPetitionTypePo = BeanConvertUtil.covertBean(reqJson, GovPetitionTypePo.class);
        return saveGovPetitionTypeBMOImpl.save(govPetitionTypePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPetitionType/updateGovPetitionType
     * @path /app/govPetitionType/updateGovPetitionType
     */
    @RequestMapping(value = "/updateGovPetitionType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPetitionType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovPetitionTypePo govPetitionTypePo = BeanConvertUtil.covertBean(reqJson, GovPetitionTypePo.class);
        return updateGovPetitionTypeBMOImpl.update(govPetitionTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPetitionType/deleteGovPetitionType
     * @path /app/govPetitionType/deleteGovPetitionType
     */
    @RequestMapping(value = "/deleteGovPetitionType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPetitionType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");
        GovPetitionTypePo govPetitionTypePo = BeanConvertUtil.covertBean(reqJson, GovPetitionTypePo.class);
        return deleteGovPetitionTypeBMOImpl.delete(govPetitionTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govPetitionType/queryGovPetitionType
     * @path /app/govPetitionType/queryGovPetitionType
     */
    @RequestMapping(value = "/queryGovPetitionType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPetitionType(@RequestParam(value = "caId") String caId,
                                                       @RequestParam(value = "typeName" , required = false) String typeName,
                                                       @RequestParam(value = "seq" , required = false) String seq,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "row") int row) {
        GovPetitionTypeDto govPetitionTypeDto = new GovPetitionTypeDto();
        govPetitionTypeDto.setPage(page);
        govPetitionTypeDto.setRow(row);
        govPetitionTypeDto.setCaId(caId);
        govPetitionTypeDto.setTypeName(typeName);
        govPetitionTypeDto.setSeq(seq);
        return getGovPetitionTypeBMOImpl.get(govPetitionTypeDto);
    }
}
