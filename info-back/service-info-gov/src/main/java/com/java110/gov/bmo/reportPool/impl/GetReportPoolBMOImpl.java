package com.java110.gov.bmo.reportPool.impl;

import com.java110.dto.govReportReturnVisit.GovReportReturnVisitDto;
import com.java110.dto.reportPool.LargeReportPoolDto;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.gov.bmo.reportPool.IGetReportPoolBMO;
import com.java110.intf.gov.IGovReportReturnVisitInnerServiceSMO;
import com.java110.intf.gov.IReportPoolInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getReportPoolBMOImpl")
public class GetReportPoolBMOImpl implements IGetReportPoolBMO {

    @Autowired
    private IReportPoolInnerServiceSMO reportPoolInnerServiceSMOImpl;
    @Autowired
    private IGovReportReturnVisitInnerServiceSMO govReportReturnVisitInnerServiceSMOImpl;
    /**
     *
     *
     * @param  reportPoolDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(ReportPoolDto reportPoolDto) {


        int count = reportPoolInnerServiceSMOImpl.queryReportPoolsCount(reportPoolDto);

        List<ReportPoolDto> reportPoolDtos = null;
        if (count > 0) {
            reportPoolDtos = reportPoolInnerServiceSMOImpl.queryReportPools(reportPoolDto);
        } else {
            reportPoolDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reportPoolDto.getRow()), count, reportPoolDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    /**
     *
     *
     * @param  reportPoolDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> quseryReportStaff(ReportPoolDto reportPoolDto) {


        int count = reportPoolInnerServiceSMOImpl.quseryReportStaffCount(reportPoolDto);

        List<ReportPoolDto> reportPoolDtos = null;
        if (count > 0) {
            reportPoolDtos = reportPoolInnerServiceSMOImpl.quseryReportStaffs(reportPoolDto);
        } else {
            reportPoolDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reportPoolDto.getRow()), count, reportPoolDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  reportPoolDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> queryStaffFinishReport(ReportPoolDto reportPoolDto) {


        int count = reportPoolInnerServiceSMOImpl.queryStaffFinishReportCount(reportPoolDto);

        List<ReportPoolDto> reportPoolDtos = null;
        if (count > 0) {
            reportPoolDtos = reportPoolInnerServiceSMOImpl.queryStaffFinishReports(reportPoolDto);
        } else {
            reportPoolDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reportPoolDto.getRow()), count, reportPoolDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  reportPoolDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> quseryReportFinishStaffs(ReportPoolDto reportPoolDto) {


        int count = reportPoolInnerServiceSMOImpl.quseryReportFinishStaffCount(reportPoolDto);

        List<ReportPoolDto> reportPoolDtos = null;
        if (count > 0) {
            reportPoolDtos = reportPoolInnerServiceSMOImpl.quseryReportFinishStaffs(reportPoolDto);
        } else {
            reportPoolDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reportPoolDto.getRow()), count, reportPoolDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  reportPoolDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> quseryLargeReportCount(ReportPoolDto reportPoolDto) {

        GovReportReturnVisitDto govReportReturnVisitDto = new GovReportReturnVisitDto();
        govReportReturnVisitDto.setCaId(reportPoolDto.getCaId());
        govReportReturnVisitDto.setState(reportPoolDto.getState());
        int finishCoun = govReportReturnVisitInnerServiceSMOImpl.queryGovReportReturnVisitsCount(govReportReturnVisitDto);
        reportPoolDto.setState("");
        int totalCoun = reportPoolInnerServiceSMOImpl.queryReportPoolsCount(reportPoolDto);
        int withCoun = reportPoolInnerServiceSMOImpl.quseryReportStaffCount(reportPoolDto);
        int doneCoun = reportPoolInnerServiceSMOImpl.quseryReportFinishStaffCount(reportPoolDto);


        LargeReportPoolDto largereportPoolDtos = new LargeReportPoolDto();

        largereportPoolDtos.setTotalCoun(Integer.toString(totalCoun));
        largereportPoolDtos.setWithCoun(Integer.toString(withCoun));
        largereportPoolDtos.setDoneCoun(Integer.toString(doneCoun));
        largereportPoolDtos.setFinishCoun(Integer.toString(finishCoun));
        ResultVo resultVo = new ResultVo((int) Math.ceil((double) 1 / (double) reportPoolDto.getRow()), 1, largereportPoolDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
}
