package com.java110.gov.bmo.govActivitiesType;
import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovActivitiesTypeBMO {


    /**
     * 修改公告类型
     * add by wuxw
     * @param govActivitiesTypePo
     * @return
     */
    ResponseEntity<String> update(GovActivitiesTypePo govActivitiesTypePo);


}
