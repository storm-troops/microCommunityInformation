package com.java110.gov.bmo.govActivity;

import com.java110.po.govActivity.GovActivityPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovActivityBMO {


    /**
     * 添加活动
     * add by wuxw
     * @param govActivityPo
     * @return
     */
    ResponseEntity<String> save(GovActivityPo govActivityPo);


}
