package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.gov.bmo.govLabel.IDeleteGovLabelBMO;
import com.java110.gov.bmo.govLabel.IGetGovLabelBMO;
import com.java110.gov.bmo.govLabel.ISaveGovLabelBMO;
import com.java110.gov.bmo.govLabel.IUpdateGovLabelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govLabel")
public class GovLabelApi {

    @Autowired
    private ISaveGovLabelBMO saveGovLabelBMOImpl;
    @Autowired
    private IUpdateGovLabelBMO updateGovLabelBMOImpl;
    @Autowired
    private IDeleteGovLabelBMO deleteGovLabelBMOImpl;

    @Autowired
    private IGetGovLabelBMO getGovLabelBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govLabel/saveGovLabel
     * @path /app/govLabel/saveGovLabel
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovLabel", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovLabel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "labelName", "请求报文中未包含labelName");
        Assert.hasKeyAndValue(reqJson, "labelCd", "请求报文中未包含labelCd");
        Assert.hasKeyAndValue(reqJson, "labelType", "请求报文中未包含labelType");


        GovLabelPo govLabelPo = BeanConvertUtil.covertBean(reqJson, GovLabelPo.class);
        return saveGovLabelBMOImpl.save(govLabelPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govLabel/updateGovLabel
     * @path /app/govLabel/updateGovLabel
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovLabel", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovLabel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "labelName", "请求报文中未包含labelName");
        Assert.hasKeyAndValue(reqJson, "labelCd", "请求报文中未包含labelCd");
        Assert.hasKeyAndValue(reqJson, "labelType", "请求报文中未包含labelType");
        Assert.hasKeyAndValue(reqJson, "govLabelId", "govLabelId不能为空");
        GovLabelPo govLabelPo = BeanConvertUtil.covertBean(reqJson, GovLabelPo.class);
        return updateGovLabelBMOImpl.update(govLabelPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govLabel/deleteGovLabel
     * @path /app/govLabel/deleteGovLabel
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovLabel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovLabel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "govLabelId", "govLabelId不能为空");
        GovLabelPo govLabelPo = BeanConvertUtil.covertBean(reqJson, GovLabelPo.class);
        return deleteGovLabelBMOImpl.delete(govLabelPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govLabel/queryGovLabel
     * @path /app/govLabel/queryGovLabel
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovLabel", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovLabel(@RequestParam(value = "caId") String caId,
                                                @RequestParam(value = "labelName",required = false) String labelName,
                                                @RequestParam(value = "labelType",required = false) String labelType,
                                                @RequestParam(value = "labelCd",required = false) String labelCd,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovLabelDto govLabelDto = new GovLabelDto();
        govLabelDto.setPage(page);
        govLabelDto.setRow(row);
        govLabelDto.setCaId(caId);
        govLabelDto.setLabelName(labelName);
        govLabelDto.setLabelCd(labelCd);
        govLabelDto.setLabelType(labelType);
        return getGovLabelBMOImpl.get(govLabelDto);
    }
}
