package com.java110.gov.bmo.govReportSetting.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govReportSetting.ISaveGovReportSettingBMO;
import com.java110.intf.gov.IGovReportSettingInnerServiceSMO;
import com.java110.po.govReportSetting.GovReportSettingPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovReportSettingBMOImpl")
public class SaveGovReportSettingBMOImpl implements ISaveGovReportSettingBMO {

    @Autowired
    private IGovReportSettingInnerServiceSMO govReportSettingInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govReportSettingPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovReportSettingPo govReportSettingPo) {

        govReportSettingPo.setSettingId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_settingId));
        govReportSettingPo.setReportType(govReportSettingPo.getSettingId());
        int flag = govReportSettingInnerServiceSMOImpl.saveGovReportSetting(govReportSettingPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
