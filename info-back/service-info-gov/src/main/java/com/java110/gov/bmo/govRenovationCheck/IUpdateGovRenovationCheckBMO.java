package com.java110.gov.bmo.govRenovationCheck;
import org.springframework.http.ResponseEntity;
import com.java110.po.govRenovationCheck.GovRenovationCheckPo;

public interface IUpdateGovRenovationCheckBMO {


    /**
     * 修改重点地区整治情况
     * add by wuxw
     * @param govRenovationCheckPo
     * @return
     */
    ResponseEntity<String> update(GovRenovationCheckPo govRenovationCheckPo);


}
