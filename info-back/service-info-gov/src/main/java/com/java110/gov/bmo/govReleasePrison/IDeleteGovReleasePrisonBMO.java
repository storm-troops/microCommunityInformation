package com.java110.gov.bmo.govReleasePrison;
import org.springframework.http.ResponseEntity;
import com.java110.po.govReleasePrison.GovReleasePrisonPo;

public interface IDeleteGovReleasePrisonBMO {


    /**
     * 修改刑满释放人员
     * add by wuxw
     * @param govReleasePrisonPo
     * @return
     */
    ResponseEntity<String> delete(GovReleasePrisonPo govReleasePrisonPo);


}
