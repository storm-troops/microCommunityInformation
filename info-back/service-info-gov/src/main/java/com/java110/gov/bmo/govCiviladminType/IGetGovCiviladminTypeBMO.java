package com.java110.gov.bmo.govCiviladminType;
import com.java110.dto.govCiviladminType.GovCiviladminTypeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCiviladminTypeBMO {


    /**
     * 查询民政服务宣传类型
     * add by wuxw
     * @param  govCiviladminTypeDto
     * @return
     */
    ResponseEntity<String> get(GovCiviladminTypeDto govCiviladminTypeDto);


}
