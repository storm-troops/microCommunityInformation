package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovCiviladminTypeServiceDao;
import com.java110.intf.gov.IGovCiviladminTypeInnerServiceSMO;
import com.java110.dto.govCiviladminType.GovCiviladminTypeDto;
import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 民政服务宣传类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCiviladminTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovCiviladminTypeInnerServiceSMO {

    @Autowired
    private IGovCiviladminTypeServiceDao govCiviladminTypeServiceDaoImpl;


    @Override
    public int saveGovCiviladminType(@RequestBody  GovCiviladminTypePo govCiviladminTypePo) {
        int saveFlag = 1;
        govCiviladminTypeServiceDaoImpl.saveGovCiviladminTypeInfo(BeanConvertUtil.beanCovertMap(govCiviladminTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovCiviladminType(@RequestBody  GovCiviladminTypePo govCiviladminTypePo) {
        int saveFlag = 1;
         govCiviladminTypeServiceDaoImpl.updateGovCiviladminTypeInfo(BeanConvertUtil.beanCovertMap(govCiviladminTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovCiviladminType(@RequestBody  GovCiviladminTypePo govCiviladminTypePo) {
        int saveFlag = 1;
        govCiviladminTypePo.setStatusCd("1");
        govCiviladminTypeServiceDaoImpl.updateGovCiviladminTypeInfo(BeanConvertUtil.beanCovertMap(govCiviladminTypePo));
        return saveFlag;
    }

    @Override
    public List<GovCiviladminTypeDto> queryGovCiviladminTypes(@RequestBody  GovCiviladminTypeDto govCiviladminTypeDto) {

        //校验是否传了 分页信息

        int page = govCiviladminTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCiviladminTypeDto.setPage((page - 1) * govCiviladminTypeDto.getRow());
        }

        List<GovCiviladminTypeDto> govCiviladminTypes = BeanConvertUtil.covertBeanList(govCiviladminTypeServiceDaoImpl.getGovCiviladminTypeInfo(BeanConvertUtil.beanCovertMap(govCiviladminTypeDto)), GovCiviladminTypeDto.class);

        return govCiviladminTypes;
    }


    @Override
    public int queryGovCiviladminTypesCount(@RequestBody GovCiviladminTypeDto govCiviladminTypeDto) {
        return govCiviladminTypeServiceDaoImpl.queryGovCiviladminTypesCount(BeanConvertUtil.beanCovertMap(govCiviladminTypeDto));    }

    public IGovCiviladminTypeServiceDao getGovCiviladminTypeServiceDaoImpl() {
        return govCiviladminTypeServiceDaoImpl;
    }

    public void setGovCiviladminTypeServiceDaoImpl(IGovCiviladminTypeServiceDao govCiviladminTypeServiceDaoImpl) {
        this.govCiviladminTypeServiceDaoImpl = govCiviladminTypeServiceDaoImpl;
    }
}
