package com.java110.gov.bmo.govActivityType.impl;

import com.java110.dto.govActivityType.GovActivityTypeDto;
import com.java110.gov.bmo.govActivityType.IGetGovActivityTypeBMO;
import com.java110.intf.gov.IGovActivityTypeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovActivityTypeBMOImpl")
public class GetGovActivityTypeBMOImpl implements IGetGovActivityTypeBMO {

    @Autowired
    private IGovActivityTypeInnerServiceSMO govActivityTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govActivityTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovActivityTypeDto govActivityTypeDto) {


        int count = govActivityTypeInnerServiceSMOImpl.queryGovActivityTypesCount(govActivityTypeDto);

        List<GovActivityTypeDto> govActivityTypeDtos = null;
        if (count > 0) {
            govActivityTypeDtos = govActivityTypeInnerServiceSMOImpl.queryGovActivityTypes(govActivityTypeDto);
        } else {
            govActivityTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govActivityTypeDto.getRow()), count, govActivityTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
