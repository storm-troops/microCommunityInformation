package com.java110.gov.bmo.govPartyMember.impl;


import com.java110.core.annotation.Java110Transactional;

import com.java110.gov.bmo.govPartyMember.IUpdateGovPartyMemberBMO;
import com.java110.intf.gov.IGovPartyMemberInnerServiceSMO;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("updateGovPartyMemberBMOImpl")
public class UpdateGovPartyMemberBMOImpl implements IUpdateGovPartyMemberBMO {

    @Autowired
    private IGovPartyMemberInnerServiceSMO govPartyMemberInnerServiceSMOImpl;

    /**
     *
     *
     * @param govPartyMemberPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovPartyMemberPo govPartyMemberPo) {

        int flag = govPartyMemberInnerServiceSMOImpl.updateGovPartyMember(govPartyMemberPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
