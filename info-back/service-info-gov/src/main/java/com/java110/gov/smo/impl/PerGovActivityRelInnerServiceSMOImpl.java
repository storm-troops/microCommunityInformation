package com.java110.gov.smo.impl;


import com.java110.gov.dao.IPerGovActivityRelServiceDao;
import com.java110.intf.gov.IPerGovActivityRelInnerServiceSMO;
import com.java110.dto.perGovActivityRel.PerGovActivityRelDto;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 生日记录关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class PerGovActivityRelInnerServiceSMOImpl extends BaseServiceSMO implements IPerGovActivityRelInnerServiceSMO {

    @Autowired
    private IPerGovActivityRelServiceDao perGovActivityRelServiceDaoImpl;


    @Override
    public int savePerGovActivityRel(@RequestBody  PerGovActivityRelPo perGovActivityRelPo) {
        int saveFlag = 1;
        perGovActivityRelServiceDaoImpl.savePerGovActivityRelInfo(BeanConvertUtil.beanCovertMap(perGovActivityRelPo));
        return saveFlag;
    }

     @Override
    public int updatePerGovActivityRel(@RequestBody  PerGovActivityRelPo perGovActivityRelPo) {
        int saveFlag = 1;
         perGovActivityRelServiceDaoImpl.updatePerGovActivityRelInfo(BeanConvertUtil.beanCovertMap(perGovActivityRelPo));
        return saveFlag;
    }

     @Override
    public int deletePerGovActivityRel(@RequestBody  PerGovActivityRelPo perGovActivityRelPo) {
        int saveFlag = 1;
        perGovActivityRelPo.setStatusCd("1");
        perGovActivityRelServiceDaoImpl.updatePerGovActivityRelInfo(BeanConvertUtil.beanCovertMap(perGovActivityRelPo));
        return saveFlag;
    }

    @Override
    public List<PerGovActivityRelDto> queryPerGovActivityRels(@RequestBody  PerGovActivityRelDto perGovActivityRelDto) {

        //校验是否传了 分页信息

        int page = perGovActivityRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            perGovActivityRelDto.setPage((page - 1) * perGovActivityRelDto.getRow());
        }

        List<PerGovActivityRelDto> perGovActivityRels = BeanConvertUtil.covertBeanList(perGovActivityRelServiceDaoImpl.getPerGovActivityRelInfo(BeanConvertUtil.beanCovertMap(perGovActivityRelDto)), PerGovActivityRelDto.class);

        return perGovActivityRels;
    }


    @Override
    public int queryPerGovActivityRelsCount(@RequestBody PerGovActivityRelDto perGovActivityRelDto) {
        return perGovActivityRelServiceDaoImpl.queryPerGovActivityRelsCount(BeanConvertUtil.beanCovertMap(perGovActivityRelDto));    }

    public IPerGovActivityRelServiceDao getPerGovActivityRelServiceDaoImpl() {
        return perGovActivityRelServiceDaoImpl;
    }

    public void setPerGovActivityRelServiceDaoImpl(IPerGovActivityRelServiceDao perGovActivityRelServiceDaoImpl) {
        this.perGovActivityRelServiceDaoImpl = perGovActivityRelServiceDaoImpl;
    }
}
