package com.java110.gov.bmo.govPetitionType;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPetitionType.GovPetitionTypePo;

public interface IUpdateGovPetitionTypeBMO {


    /**
     * 修改信访类型表
     * add by wuxw
     * @param govPetitionTypePo
     * @return
     */
    ResponseEntity<String> update(GovPetitionTypePo govPetitionTypePo);


}
