package com.java110.gov.bmo.govRenovationCheck;
import org.springframework.http.ResponseEntity;
import com.java110.po.govRenovationCheck.GovRenovationCheckPo;

public interface IDeleteGovRenovationCheckBMO {


    /**
     * 修改重点地区整治情况
     * add by wuxw
     * @param govRenovationCheckPo
     * @return
     */
    ResponseEntity<String> delete(GovRenovationCheckPo govRenovationCheckPo);


}
