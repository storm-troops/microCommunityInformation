package com.java110.gov.bmo.govMentalDisorders.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govMentalDisorders.IGetGovMentalDisordersBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovMentalDisordersInnerServiceSMO;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMentalDisordersBMOImpl")
public class GetGovMentalDisordersBMOImpl implements IGetGovMentalDisordersBMO {

    @Autowired
    private IGovMentalDisordersInnerServiceSMO govMentalDisordersInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMentalDisordersDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMentalDisordersDto govMentalDisordersDto) {


        int count = govMentalDisordersInnerServiceSMOImpl.queryGovMentalDisorderssCount(govMentalDisordersDto);

        List<GovMentalDisordersDto> govMentalDisordersDtos = null;
        if (count > 0) {
            govMentalDisordersDtos = govMentalDisordersInnerServiceSMOImpl.queryGovMentalDisorderss(govMentalDisordersDto);
        } else {
            govMentalDisordersDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMentalDisordersDto.getRow()), count, govMentalDisordersDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
