package com.java110.gov.bmo.govAids.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govAids.IGetGovAidsBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovAidsInnerServiceSMO;
import com.java110.dto.govAids.GovAidsDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovAidsBMOImpl")
public class GetGovAidsBMOImpl implements IGetGovAidsBMO {

    @Autowired
    private IGovAidsInnerServiceSMO govAidsInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govAidsDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovAidsDto govAidsDto) {


        int count = govAidsInnerServiceSMOImpl.queryGovAidssCount(govAidsDto);

        List<GovAidsDto> govAidsDtos = null;
        if (count > 0) {
            govAidsDtos = govAidsInnerServiceSMOImpl.queryGovAidss(govAidsDto);
        } else {
            govAidsDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govAidsDto.getRow()), count, govAidsDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
