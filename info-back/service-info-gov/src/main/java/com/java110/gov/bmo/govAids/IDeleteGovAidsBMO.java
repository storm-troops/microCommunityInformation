package com.java110.gov.bmo.govAids;
import org.springframework.http.ResponseEntity;
import com.java110.po.govAids.GovAidsPo;

public interface IDeleteGovAidsBMO {


    /**
     * 修改艾滋病者
     * add by wuxw
     * @param govAidsPo
     * @return
     */
    ResponseEntity<String> delete(GovAidsPo govAidsPo);


}
