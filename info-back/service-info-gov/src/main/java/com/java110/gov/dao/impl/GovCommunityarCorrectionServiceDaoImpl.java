package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovCommunityarCorrectionServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 矫正者服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCommunityarCorrectionServiceDaoImpl")
//@Transactional
public class GovCommunityarCorrectionServiceDaoImpl extends BaseServiceDao implements IGovCommunityarCorrectionServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCommunityarCorrectionServiceDaoImpl.class);





    /**
     * 保存矫正者信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCommunityarCorrectionInfo(Map info) throws DAOException {
        logger.debug("保存矫正者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCommunityarCorrectionServiceDaoImpl.saveGovCommunityarCorrectionInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存矫正者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询矫正者信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityarCorrectionInfo(Map info) throws DAOException {
        logger.debug("查询矫正者信息 入参 info : {}",info);

        List<Map> businessGovCommunityarCorrectionInfos = sqlSessionTemplate.selectList("govCommunityarCorrectionServiceDaoImpl.getGovCommunityarCorrectionInfo",info);

        return businessGovCommunityarCorrectionInfos;
    }


    /**
     * 修改矫正者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCommunityarCorrectionInfo(Map info) throws DAOException {
        logger.debug("修改矫正者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCommunityarCorrectionServiceDaoImpl.updateGovCommunityarCorrectionInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改矫正者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询矫正者数量
     * @param info 矫正者信息
     * @return 矫正者数量
     */
    @Override
    public int queryGovCommunityarCorrectionsCount(Map info) {
        logger.debug("查询矫正者数据 入参 info : {}",info);

        List<Map> businessGovCommunityarCorrectionInfos = sqlSessionTemplate.selectList("govCommunityarCorrectionServiceDaoImpl.queryGovCommunityarCorrectionsCount", info);
        if (businessGovCommunityarCorrectionInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityarCorrectionInfos.get(0).get("count").toString());
    }


}
