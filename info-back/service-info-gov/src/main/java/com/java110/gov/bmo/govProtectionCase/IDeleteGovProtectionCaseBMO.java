package com.java110.gov.bmo.govProtectionCase;
import org.springframework.http.ResponseEntity;
import com.java110.po.govProtectionCase.GovProtectionCasePo;

public interface IDeleteGovProtectionCaseBMO {


    /**
     * 修改周边重点人员
     * add by wuxw
     * @param govProtectionCasePo
     * @return
     */
    ResponseEntity<String> delete(GovProtectionCasePo govProtectionCasePo);


}
