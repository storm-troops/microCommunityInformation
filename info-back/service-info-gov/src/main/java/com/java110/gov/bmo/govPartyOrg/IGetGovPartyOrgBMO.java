package com.java110.gov.bmo.govPartyOrg;
import com.java110.dto.govPartyOrg.GovPartyOrgDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPartyOrgBMO {


    /**
     * 查询党组织
     * add by wuxw
     * @param  govPartyOrgDto
     * @return
     */
    ResponseEntity<String> get(GovPartyOrgDto govPartyOrgDto);

    ResponseEntity<String> queryNotGovPartyOrg(GovPartyOrgDto govPartyOrgDto);


}
