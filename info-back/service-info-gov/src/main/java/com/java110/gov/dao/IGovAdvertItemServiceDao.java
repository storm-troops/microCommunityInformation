package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * 广告明细组件内部之间使用，没有给外围系统提供服务能力
 * 广告明细服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovAdvertItemServiceDao {


    /**
     * 保存 广告明细信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovAdvertItemInfo(Map info) throws DAOException;




    /**
     * 查询广告明细信息（instance过程）
     * 根据bId 查询广告明细信息
     * @param info bId 信息
     * @return 广告明细信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovAdvertItemInfo(Map info) throws DAOException;



    /**
     * 修改广告明细信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovAdvertItemInfo(Map info) throws DAOException;


    /**
     * 查询广告明细总数
     *
     * @param info 广告明细信息
     * @return 广告明细数量
     */
    int queryGovAdvertItemsCount(Map info);

}
