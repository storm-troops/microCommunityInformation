package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovPersonLabelRelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 标签与人口关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPersonLabelRelServiceDaoImpl")
//@Transactional
public class GovPersonLabelRelServiceDaoImpl extends BaseServiceDao implements IGovPersonLabelRelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPersonLabelRelServiceDaoImpl.class);





    /**
     * 保存标签与人口关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPersonLabelRelInfo(Map info) throws DAOException {
        logger.debug("保存标签与人口关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPersonLabelRelServiceDaoImpl.saveGovPersonLabelRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存标签与人口关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询标签与人口关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPersonLabelRelInfo(Map info) throws DAOException {
        logger.debug("查询标签与人口关系信息 入参 info : {}",info);

        List<Map> businessGovPersonLabelRelInfos = sqlSessionTemplate.selectList("govPersonLabelRelServiceDaoImpl.getGovPersonLabelRelInfo",info);

        return businessGovPersonLabelRelInfos;
    }


    /**
     * 修改标签与人口关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPersonLabelRelInfo(Map info) throws DAOException {
        logger.debug("修改标签与人口关系信息Instance 入参 info : {}",info);

        sqlSessionTemplate.update("govPersonLabelRelServiceDaoImpl.updateGovPersonLabelRelInfo",info);

//        if(saveFlag < 1){
//            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改标签与人口关系信息Instance数据失败："+ JSONObject.toJSONString(info));
//        }
    }

     /**
     * 查询标签与人口关系数量
     * @param info 标签与人口关系信息
     * @return 标签与人口关系数量
     */
    @Override
    public int queryGovPersonLabelRelsCount(Map info) {
        logger.debug("查询标签与人口关系数据 入参 info : {}",info);

        List<Map> businessGovPersonLabelRelInfos = sqlSessionTemplate.selectList("govPersonLabelRelServiceDaoImpl.queryGovPersonLabelRelsCount", info);
        if (businessGovPersonLabelRelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPersonLabelRelInfos.get(0).get("count").toString());
    }


}
