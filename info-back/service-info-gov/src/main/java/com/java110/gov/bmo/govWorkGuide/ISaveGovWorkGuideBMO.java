package com.java110.gov.bmo.govWorkGuide;

import com.java110.po.govWorkGuide.GovWorkGuidePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovWorkGuideBMO {


    /**
     * 添加办事指南
     * add by wuxw
     * @param govWorkGuidePo
     * @return
     */
    ResponseEntity<String> save(GovWorkGuidePo govWorkGuidePo);


}
