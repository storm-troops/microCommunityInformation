package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovRoadProtectionCaseServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 路案事件服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govRoadProtectionCaseServiceDaoImpl")
//@Transactional
public class GovRoadProtectionCaseServiceDaoImpl extends BaseServiceDao implements IGovRoadProtectionCaseServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovRoadProtectionCaseServiceDaoImpl.class);





    /**
     * 保存路案事件信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovRoadProtectionCaseInfo(Map info) throws DAOException {
        logger.debug("保存路案事件信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govRoadProtectionCaseServiceDaoImpl.saveGovRoadProtectionCaseInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存路案事件信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询路案事件信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovRoadProtectionCaseInfo(Map info) throws DAOException {
        logger.debug("查询路案事件信息 入参 info : {}",info);

        List<Map> businessGovRoadProtectionCaseInfos = sqlSessionTemplate.selectList("govRoadProtectionCaseServiceDaoImpl.getGovRoadProtectionCaseInfo",info);

        return businessGovRoadProtectionCaseInfos;
    }


    /**
     * 修改路案事件信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovRoadProtectionCaseInfo(Map info) throws DAOException {
        logger.debug("修改路案事件信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govRoadProtectionCaseServiceDaoImpl.updateGovRoadProtectionCaseInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改路案事件信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询路案事件数量
     * @param info 路案事件信息
     * @return 路案事件数量
     */
    @Override
    public int queryGovRoadProtectionCasesCount(Map info) {
        logger.debug("查询路案事件数据 入参 info : {}",info);

        List<Map> businessGovRoadProtectionCaseInfos = sqlSessionTemplate.selectList("govRoadProtectionCaseServiceDaoImpl.queryGovRoadProtectionCasesCount", info);
        if (businessGovRoadProtectionCaseInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovRoadProtectionCaseInfos.get(0).get("count").toString());
    }


}
