package com.java110.gov.bmo.govMeetingType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govMeetingType.IGetGovMeetingTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovMeetingTypeInnerServiceSMO;
import com.java110.dto.govMeetingType.GovMeetingTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMeetingTypeBMOImpl")
public class GetGovMeetingTypeBMOImpl implements IGetGovMeetingTypeBMO {

    @Autowired
    private IGovMeetingTypeInnerServiceSMO govMeetingTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMeetingTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMeetingTypeDto govMeetingTypeDto) {


        int count = govMeetingTypeInnerServiceSMOImpl.queryGovMeetingTypesCount(govMeetingTypeDto);

        List<GovMeetingTypeDto> govMeetingTypeDtos = null;
        if (count > 0) {
            govMeetingTypeDtos = govMeetingTypeInnerServiceSMOImpl.queryGovMeetingTypes(govMeetingTypeDto);
        } else {
            govMeetingTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMeetingTypeDto.getRow()), count, govMeetingTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
