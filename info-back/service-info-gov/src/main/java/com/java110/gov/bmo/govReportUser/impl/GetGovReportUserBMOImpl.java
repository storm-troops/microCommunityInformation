package com.java110.gov.bmo.govReportUser.impl;

import com.java110.gov.bmo.govReportUser.IGetGovReportUserBMO;
import com.java110.intf.gov.IGovReportUserInnerServiceSMO;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govReportUser.GovReportUserDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovReportUserBMOImpl")
public class GetGovReportUserBMOImpl implements IGetGovReportUserBMO {

    @Autowired
    private IGovReportUserInnerServiceSMO govReportUserInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govReportUserDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovReportUserDto govReportUserDto) {


        int count = govReportUserInnerServiceSMOImpl.queryGovReportUsersCount(govReportUserDto);

        List<GovReportUserDto> govReportUserDtos = null;
        if (count > 0) {
            govReportUserDtos = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
            refreshPoolUser(govReportUserDtos);
        } else {
            govReportUserDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govReportUserDto.getRow()), count, govReportUserDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void refreshPoolUser(List<GovReportUserDto> govReportUserDtos) {
        long duration = 0;
        for (GovReportUserDto govReportUserDto : govReportUserDtos) {
            if (govReportUserDto.getEndTime() == null) {
                duration = DateUtil.getCurrentDate().getTime() - govReportUserDto.getStartTime().getTime();
            } else {
                duration = govReportUserDto.getEndTime().getTime() - govReportUserDto.getStartTime().getTime();
            }
            govReportUserDto.setDuration(getCostTime(duration));
        }
    }

    public String getCostTime(Long time) {
        if (time == null) {
            return "00:00";
        }
        long hours = time / (1000 * 60 * 60);
        long minutes = (time - hours * (1000 * 60 * 60)) / (1000 * 60);
        String diffTime = "";
        if (minutes < 10) {
            diffTime = hours + ":0" + minutes;
        } else {
            diffTime = hours + ":" + minutes;
        }
        return diffTime;
    }

}
