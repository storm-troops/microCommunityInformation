package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovCiviladminServiceDao;
import com.java110.intf.gov.IGovCiviladminInnerServiceSMO;
import com.java110.dto.govCiviladmin.GovCiviladminDto;
import com.java110.po.govCiviladmin.GovCiviladminPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 民政服务宣传内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCiviladminInnerServiceSMOImpl extends BaseServiceSMO implements IGovCiviladminInnerServiceSMO {

    @Autowired
    private IGovCiviladminServiceDao govCiviladminServiceDaoImpl;


    @Override
    public int saveGovCiviladmin(@RequestBody  GovCiviladminPo govCiviladminPo) {
        int saveFlag = 1;
        govCiviladminServiceDaoImpl.saveGovCiviladminInfo(BeanConvertUtil.beanCovertMap(govCiviladminPo));
        return saveFlag;
    }

     @Override
    public int updateGovCiviladmin(@RequestBody  GovCiviladminPo govCiviladminPo) {
        int saveFlag = 1;
         govCiviladminServiceDaoImpl.updateGovCiviladminInfo(BeanConvertUtil.beanCovertMap(govCiviladminPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCiviladmin(@RequestBody  GovCiviladminPo govCiviladminPo) {
        int saveFlag = 1;
        govCiviladminPo.setStatusCd("1");
        govCiviladminServiceDaoImpl.updateGovCiviladminInfo(BeanConvertUtil.beanCovertMap(govCiviladminPo));
        return saveFlag;
    }

    @Override
    public List<GovCiviladminDto> queryGovCiviladmins(@RequestBody  GovCiviladminDto govCiviladminDto) {

        //校验是否传了 分页信息

        int page = govCiviladminDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCiviladminDto.setPage((page - 1) * govCiviladminDto.getRow());
        }

        List<GovCiviladminDto> govCiviladmins = BeanConvertUtil.covertBeanList(govCiviladminServiceDaoImpl.getGovCiviladminInfo(BeanConvertUtil.beanCovertMap(govCiviladminDto)), GovCiviladminDto.class);

        return govCiviladmins;
    }


    @Override
    public int queryGovCiviladminsCount(@RequestBody GovCiviladminDto govCiviladminDto) {
        return govCiviladminServiceDaoImpl.queryGovCiviladminsCount(BeanConvertUtil.beanCovertMap(govCiviladminDto));    }

    public IGovCiviladminServiceDao getGovCiviladminServiceDaoImpl() {
        return govCiviladminServiceDaoImpl;
    }

    public void setGovCiviladminServiceDaoImpl(IGovCiviladminServiceDao govCiviladminServiceDaoImpl) {
        this.govCiviladminServiceDaoImpl = govCiviladminServiceDaoImpl;
    }
}
