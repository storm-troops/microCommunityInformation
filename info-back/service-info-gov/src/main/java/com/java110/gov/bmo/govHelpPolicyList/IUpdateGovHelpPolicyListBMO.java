package com.java110.gov.bmo.govHelpPolicyList;
import org.springframework.http.ResponseEntity;
import com.java110.po.govHelpPolicyList.GovHelpPolicyListPo;

public interface IUpdateGovHelpPolicyListBMO {


    /**
     * 修改帮扶记录
     * add by wuxw
     * @param govHelpPolicyListPo
     * @return
     */
    ResponseEntity<String> update(GovHelpPolicyListPo govHelpPolicyListPo);


}
