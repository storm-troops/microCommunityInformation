package com.java110.gov.bmo.govActivityType;
import com.java110.dto.govActivityType.GovActivityTypeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovActivityTypeBMO {


    /**
     * 查询活动类型
     * add by wuxw
     * @param  govActivityTypeDto
     * @return
     */
    ResponseEntity<String> get(GovActivityTypeDto govActivityTypeDto);


}
