package com.java110.gov.bmo.govLabel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govLabel.ISaveGovLabelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovLabelBMOImpl")
public class SaveGovLabelBMOImpl implements ISaveGovLabelBMO {

    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govLabelPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovLabelPo govLabelPo) {

        govLabelPo.setGovLabelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govLabelId));
        int flag = govLabelInnerServiceSMOImpl.saveGovLabel(govLabelPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
