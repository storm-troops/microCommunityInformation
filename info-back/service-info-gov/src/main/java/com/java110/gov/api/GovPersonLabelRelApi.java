package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.gov.bmo.govPersonLabelRel.IDeleteGovPersonLabelRelBMO;
import com.java110.gov.bmo.govPersonLabelRel.IGetGovPersonLabelRelBMO;
import com.java110.gov.bmo.govPersonLabelRel.ISaveGovPersonLabelRelBMO;
import com.java110.gov.bmo.govPersonLabelRel.IUpdateGovPersonLabelRelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPersonLabelRel")
public class GovPersonLabelRelApi {

    @Autowired
    private ISaveGovPersonLabelRelBMO saveGovPersonLabelRelBMOImpl;
    @Autowired
    private IUpdateGovPersonLabelRelBMO updateGovPersonLabelRelBMOImpl;
    @Autowired
    private IDeleteGovPersonLabelRelBMO deleteGovPersonLabelRelBMOImpl;

    @Autowired
    private IGetGovPersonLabelRelBMO getGovPersonLabelRelBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonLabelRel/saveGovPersonLabelRel
     * @path /app/govPersonLabelRel/saveGovPersonLabelRel
     */
    @RequestMapping(value = "/saveGovPersonLabelRel", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPersonLabelRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govLabelRelString", "请求报文中未包含标签信息");
        String labels = reqJson.getString("govLabelRelString");
        GovPersonLabelRelPo govPersonLabelRelPo = BeanConvertUtil.covertBean(reqJson, GovPersonLabelRelPo.class);
        return saveGovPersonLabelRelBMOImpl.save(govPersonLabelRelPo,labels);
    }
    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonLabelRel/savePersonLabelRel
     * @path /app/govPersonLabelRel/saveGovPersonLabelRel
     */
    @RequestMapping(value = "/savePersonLabelRel", method = RequestMethod.POST)
    public ResponseEntity<String> savePersonLabelRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "lableCd", "请求报文中未包含标签信息");
        Assert.hasKeyAndValue(reqJson, "lableName", "请求报文中未包含标签信息");
        Assert.hasKeyAndValue(reqJson, "lableType", "请求报文中未包含标签信息");
        String lableCd = reqJson.getString("lableCd");
        String lableName = reqJson.getString("lableName");
        String lableType = reqJson.getString("lableType");
        GovPersonLabelRelPo govPersonLabelRelPo = BeanConvertUtil.covertBean(reqJson, GovPersonLabelRelPo.class);
        return saveGovPersonLabelRelBMOImpl.savePersonLabelRel(govPersonLabelRelPo,lableCd,lableName,lableType);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonLabelRel/updateGovPersonLabelRel
     * @path /app/govPersonLabelRel/updateGovPersonLabelRel
     */
    @RequestMapping(value = "/updateGovPersonLabelRel", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPersonLabelRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govLabelRelString", "请求报文中未包含标签信息");
        String labels = reqJson.getString("govLabelRelString");
        GovPersonLabelRelPo govPersonLabelRelPo = BeanConvertUtil.covertBean(reqJson, GovPersonLabelRelPo.class);
        return updateGovPersonLabelRelBMOImpl.update(govPersonLabelRelPo,labels);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonLabelRel/deleteGovPersonLabelRel
     * @path /app/govPersonLabelRel/deleteGovPersonLabelRel
     */
    @RequestMapping(value = "/deleteGovPersonLabelRel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPersonLabelRel(@RequestBody JSONObject reqJson) {
        String labelRelId = reqJson.getString("labelRelId");
        String govPersonId = reqJson.getString("govPersonId");
        if ((labelRelId+govPersonId+"").length() < 1){
            Assert.hasKeyAndValue(reqJson, "xxxxx", "请求报文中未包含labelRelId或govPersonId");
        }
        GovPersonLabelRelPo govPersonLabelRelPo = BeanConvertUtil.covertBean(reqJson, GovPersonLabelRelPo.class);
        return deleteGovPersonLabelRelBMOImpl.delete(govPersonLabelRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govPersonLabelRel/queryGovPersonLabelRel
     * @path /app/govPersonLabelRel/queryGovPersonLabelRel
     */
    @RequestMapping(value = "/queryGovPersonLabelRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPersonLabelRel(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "govPersonId") String govPersonId,
                                                         @RequestParam(value = "labelCd",required = false) String labelCd,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setPage(page);
        govPersonLabelRelDto.setRow(row);
        govPersonLabelRelDto.setCaId(caId);
        govPersonLabelRelDto.setGovPersonId(govPersonId);
        govPersonLabelRelDto.setLabelCd(labelCd);
        return getGovPersonLabelRelBMOImpl.get(govPersonLabelRelDto);
    }
}
