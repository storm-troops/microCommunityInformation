package com.java110.gov.bmo.govActivities;
import com.java110.dto.govActivities.GovActivitiesDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovActivitiesBMO {


    /**
     * 查询公告管理
     * add by wuxw
     * @param  govActivitiesDto
     * @return
     */
    ResponseEntity<String> get(GovActivitiesDto govActivitiesDto);


}
