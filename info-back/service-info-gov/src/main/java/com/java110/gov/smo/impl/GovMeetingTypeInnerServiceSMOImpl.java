package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovMeetingTypeServiceDao;
import com.java110.intf.gov.IGovMeetingTypeInnerServiceSMO;
import com.java110.dto.govMeetingType.GovMeetingTypeDto;
import com.java110.po.govMeetingType.GovMeetingTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 会议类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMeetingTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovMeetingTypeInnerServiceSMO {

    @Autowired
    private IGovMeetingTypeServiceDao govMeetingTypeServiceDaoImpl;


    @Override
    public int saveGovMeetingType(@RequestBody  GovMeetingTypePo govMeetingTypePo) {
        int saveFlag = 1;
        govMeetingTypeServiceDaoImpl.saveGovMeetingTypeInfo(BeanConvertUtil.beanCovertMap(govMeetingTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovMeetingType(@RequestBody  GovMeetingTypePo govMeetingTypePo) {
        int saveFlag = 1;
         govMeetingTypeServiceDaoImpl.updateGovMeetingTypeInfo(BeanConvertUtil.beanCovertMap(govMeetingTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovMeetingType(@RequestBody  GovMeetingTypePo govMeetingTypePo) {
        int saveFlag = 1;
        govMeetingTypePo.setStatusCd("1");
        govMeetingTypeServiceDaoImpl.updateGovMeetingTypeInfo(BeanConvertUtil.beanCovertMap(govMeetingTypePo));
        return saveFlag;
    }

    @Override
    public List<GovMeetingTypeDto> queryGovMeetingTypes(@RequestBody  GovMeetingTypeDto govMeetingTypeDto) {

        //校验是否传了 分页信息

        int page = govMeetingTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMeetingTypeDto.setPage((page - 1) * govMeetingTypeDto.getRow());
        }

        List<GovMeetingTypeDto> govMeetingTypes = BeanConvertUtil.covertBeanList(govMeetingTypeServiceDaoImpl.getGovMeetingTypeInfo(BeanConvertUtil.beanCovertMap(govMeetingTypeDto)), GovMeetingTypeDto.class);

        return govMeetingTypes;
    }


    @Override
    public int queryGovMeetingTypesCount(@RequestBody GovMeetingTypeDto govMeetingTypeDto) {
        return govMeetingTypeServiceDaoImpl.queryGovMeetingTypesCount(BeanConvertUtil.beanCovertMap(govMeetingTypeDto));    }

    public IGovMeetingTypeServiceDao getGovMeetingTypeServiceDaoImpl() {
        return govMeetingTypeServiceDaoImpl;
    }

    public void setGovMeetingTypeServiceDaoImpl(IGovMeetingTypeServiceDao govMeetingTypeServiceDaoImpl) {
        this.govMeetingTypeServiceDaoImpl = govMeetingTypeServiceDaoImpl;
    }
}
