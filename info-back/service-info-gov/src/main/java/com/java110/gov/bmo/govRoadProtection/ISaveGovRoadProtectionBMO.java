package com.java110.gov.bmo.govRoadProtection;

import org.springframework.http.ResponseEntity;
import com.java110.po.govRoadProtection.GovRoadProtectionPo;
public interface ISaveGovRoadProtectionBMO {


    /**
     * 添加护路护线
     * add by wuxw
     * @param govRoadProtectionPo
     * @return
     */
    ResponseEntity<String> save(GovRoadProtectionPo govRoadProtectionPo);


}
