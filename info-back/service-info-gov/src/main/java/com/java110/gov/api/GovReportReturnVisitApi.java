package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govReportReturnVisit.GovReportReturnVisitDto;
import com.java110.gov.bmo.govReportReturnVisit.IDeleteGovReportReturnVisitBMO;
import com.java110.gov.bmo.govReportReturnVisit.IGetGovReportReturnVisitBMO;
import com.java110.gov.bmo.govReportReturnVisit.ISaveGovReportReturnVisitBMO;
import com.java110.gov.bmo.govReportReturnVisit.IUpdateGovReportReturnVisitBMO;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govReportReturnVisit")
public class GovReportReturnVisitApi {

    @Autowired
    private ISaveGovReportReturnVisitBMO saveGovReportReturnVisitBMOImpl;
    @Autowired
    private IUpdateGovReportReturnVisitBMO updateGovReportReturnVisitBMOImpl;
    @Autowired
    private IDeleteGovReportReturnVisitBMO deleteGovReportReturnVisitBMOImpl;

    @Autowired
    private IGetGovReportReturnVisitBMO getGovReportReturnVisitBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govReportReturnVisit/saveReportReturnVisit
     * @path /app/govReportReturnVisit/saveGovReportReturnVisit
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveReportReturnVisit", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovReportReturnVisit(@RequestHeader("user-id") String userId,
                                                           @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "reportId", "请求报文中未包含reportId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "visitType", "请求报文中未包含visitType");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        reqJson.put("visitPersonId",userId);

        GovReportReturnVisitPo govReportReturnVisitPo = BeanConvertUtil.covertBean(reqJson, GovReportReturnVisitPo.class);
        return saveGovReportReturnVisitBMOImpl.save(govReportReturnVisitPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govReportReturnVisit/updateGovReportReturnVisit
     * @path /app/govReportReturnVisit/updateGovReportReturnVisit
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovReportReturnVisit", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovReportReturnVisit(@RequestHeader("user-id") String userId,
                                                             @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "reportId", "请求报文中未包含reportId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "visitType", "请求报文中未包含visitType");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");


        GovReportReturnVisitPo govReportReturnVisitPo = BeanConvertUtil.covertBean(reqJson, GovReportReturnVisitPo.class);
        return updateGovReportReturnVisitBMOImpl.update(govReportReturnVisitPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReportReturnVisit/deleteGovReportReturnVisit
     * @path /app/govReportReturnVisit/deleteGovReportReturnVisit
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovReportReturnVisit", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovReportReturnVisit(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "visitId", "visitId不能为空");


        GovReportReturnVisitPo govReportReturnVisitPo = BeanConvertUtil.covertBean(reqJson, GovReportReturnVisitPo.class);
        return deleteGovReportReturnVisitBMOImpl.delete(govReportReturnVisitPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReportReturnVisit/queryReportReturnVisit
     * @path /app/govReportReturnVisit/queryReportReturnVisit
     * @param
     * @return
     */
    @RequestMapping(value = "/queryReportReturnVisit", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovReportReturnVisit(@RequestParam(value = "visitId",required = false) String visitId,
                                                            @RequestParam(value = "reportId",required = false) String reportId,
                                                            @RequestParam(value = "caId",required = false) String caId,
                                                            @RequestParam(value = "reportType",required = false) String reportType,
                                                            @RequestParam(value = "tel",required = false) String tel,
                                                            @RequestParam(value = "state",required = false) String state,
                                                            @RequestParam(value = "visitType",required = false) String visitType,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovReportReturnVisitDto govReportReturnVisitDto = new GovReportReturnVisitDto();
        govReportReturnVisitDto.setPage(page);
        govReportReturnVisitDto.setRow(row);
        govReportReturnVisitDto.setVisitId(visitId);
        govReportReturnVisitDto.setReportId(reportId);
        govReportReturnVisitDto.setCaId(caId);
        govReportReturnVisitDto.setReportId(reportType);
        govReportReturnVisitDto.setTel(tel);
        govReportReturnVisitDto.setState(state);
        govReportReturnVisitDto.setVisitType(visitType);
        return getGovReportReturnVisitBMOImpl.get(govReportReturnVisitDto);
    }
}
