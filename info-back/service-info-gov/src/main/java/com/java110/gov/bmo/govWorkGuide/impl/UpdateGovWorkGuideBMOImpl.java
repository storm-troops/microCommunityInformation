package com.java110.gov.bmo.govWorkGuide.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govWorkGuide.IUpdateGovWorkGuideBMO;
import com.java110.intf.gov.IGovWorkGuideInnerServiceSMO;
import com.java110.po.govWorkGuide.GovWorkGuidePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovWorkGuideBMOImpl")
public class UpdateGovWorkGuideBMOImpl implements IUpdateGovWorkGuideBMO {

    @Autowired
    private IGovWorkGuideInnerServiceSMO govWorkGuideInnerServiceSMOImpl;

    /**
     *
     *
     * @param govWorkGuidePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovWorkGuidePo govWorkGuidePo) {

        int flag = govWorkGuideInnerServiceSMOImpl.updateGovWorkGuide(govWorkGuidePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
