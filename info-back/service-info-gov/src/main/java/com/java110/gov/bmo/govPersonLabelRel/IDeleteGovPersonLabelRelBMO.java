package com.java110.gov.bmo.govPersonLabelRel;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;

public interface IDeleteGovPersonLabelRelBMO {


    /**
     * 修改标签与人口关系
     * add by wuxw
     * @param govPersonLabelRelPo
     * @return
     */
    ResponseEntity<String> delete(GovPersonLabelRelPo govPersonLabelRelPo);


}
