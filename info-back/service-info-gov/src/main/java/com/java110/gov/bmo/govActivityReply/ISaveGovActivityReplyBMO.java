package com.java110.gov.bmo.govActivityReply;

import org.springframework.http.ResponseEntity;
import com.java110.po.govActivityReply.GovActivityReplyPo;
public interface ISaveGovActivityReplyBMO {


    /**
     * 添加活动评论
     * add by wuxw
     * @param govActivityReplyPo
     * @return
     */
    ResponseEntity<String> save(GovActivityReplyPo govActivityReplyPo);


}
