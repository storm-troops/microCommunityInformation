package com.java110.gov.bmo.govSchool;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govSchool.GovSchoolDto;
public interface IGetGovSchoolBMO {


    /**
     * 查询学校
     * add by wuxw
     * @param  govSchoolDto
     * @return
     */
    ResponseEntity<String> get(GovSchoolDto govSchoolDto);


}
