package com.java110.gov.bmo.govProtectionCase;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govProtectionCase.GovProtectionCaseDto;
public interface IGetGovProtectionCaseBMO {


    /**
     * 查询周边重点人员
     * add by wuxw
     * @param  govProtectionCaseDto
     * @return
     */
    ResponseEntity<String> get(GovProtectionCaseDto govProtectionCaseDto);


}
