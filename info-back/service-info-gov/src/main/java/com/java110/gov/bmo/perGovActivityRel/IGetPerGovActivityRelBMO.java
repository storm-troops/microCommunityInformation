package com.java110.gov.bmo.perGovActivityRel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.perGovActivityRel.PerGovActivityRelDto;
public interface IGetPerGovActivityRelBMO {


    /**
     * 查询生日记录关系
     * add by wuxw
     * @param  perGovActivityRelDto
     * @return
     */
    ResponseEntity<String> get(PerGovActivityRelDto perGovActivityRelDto);


}
