package com.java110.gov.bmo.govHelpPolicy.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govHelpPolicy.IGetGovHelpPolicyBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovHelpPolicyInnerServiceSMO;
import com.java110.dto.govHelpPolicy.GovHelpPolicyDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHelpPolicyBMOImpl")
public class GetGovHelpPolicyBMOImpl implements IGetGovHelpPolicyBMO {

    @Autowired
    private IGovHelpPolicyInnerServiceSMO govHelpPolicyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHelpPolicyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHelpPolicyDto govHelpPolicyDto) {


        int count = govHelpPolicyInnerServiceSMOImpl.queryGovHelpPolicysCount(govHelpPolicyDto);

        List<GovHelpPolicyDto> govHelpPolicyDtos = null;
        if (count > 0) {
            govHelpPolicyDtos = govHelpPolicyInnerServiceSMOImpl.queryGovHelpPolicys(govHelpPolicyDto);
        } else {
            govHelpPolicyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHelpPolicyDto.getRow()), count, govHelpPolicyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
