package com.java110.gov.bmo.govActivityReply.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govActivity.GovActivityDto;
import com.java110.dto.govActivityReply.GovActivityReplyDto;
import com.java110.gov.bmo.govActivityReply.ISaveGovActivityReplyBMO;
import com.java110.intf.gov.IGovActivityInnerServiceSMO;
import com.java110.intf.gov.IGovActivityReplyInnerServiceSMO;
import com.java110.po.govActivityReply.GovActivityReplyPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("saveGovActivityReplyBMOImpl")
public class SaveGovActivityReplyBMOImpl implements ISaveGovActivityReplyBMO {
    public static final String CODE_PREFIX_ID = "10";

    @Autowired
    private IGovActivityReplyInnerServiceSMO govActivityReplyInnerServiceSMOImpl;

    @Autowired
    private IGovActivityInnerServiceSMO govActivityInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govActivityReplyPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovActivityReplyPo govActivityReplyPo) {
        String sessionId = "";
        String preUserName = "";
        String preAuthorId = "";
        GovActivityDto govActivityDto = new GovActivityDto();
        govActivityDto.setActId(govActivityReplyPo.getActId());
        List<GovActivityDto> govActivityDtos = govActivityInnerServiceSMOImpl.queryGovActivitys(govActivityDto);
        Assert.listOnlyOne(govActivityDtos, "活动不存在");

        if (govActivityReplyPo.getParentReplyId() != null && !govActivityReplyPo.getParentReplyId().equals("-1")) {
            GovActivityReplyDto replyDto = new GovActivityReplyDto();
            replyDto.setReplyId(govActivityReplyPo.getParentReplyId());
            List<GovActivityReplyDto> replyDtos = govActivityReplyInnerServiceSMOImpl.queryGovActivityReplys(replyDto);

            Assert.listOnlyOne(replyDtos, "要回复的评论不存在");
            sessionId = replyDtos.get(0).getSessionId();
            preUserName = replyDtos.get(0).getUserName();
            preAuthorId = replyDtos.get(0).getUserId();
        } else {
            sessionId = GenerateCodeFactory.getGeneratorId(CODE_PREFIX_ID);
            preUserName = govActivityReplyPo.getPreUserName();
            preAuthorId = govActivityReplyPo.getPreAuthorId();
        }
        govActivityReplyPo.setReplyId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_replyId));
        govActivityReplyPo.setCreateTime(new Date());
        govActivityReplyPo.setSessionId(sessionId);
        govActivityReplyPo.setPreUserName(preUserName);
        govActivityReplyPo.setPreAuthorId(preAuthorId);
        int flag = govActivityReplyInnerServiceSMOImpl.saveGovActivityReply(govActivityReplyPo);
        if (flag > 0) {
            ResultVo resultVo = new ResultVo(ResultVo.CODE_OK, "保存成功",govActivityReplyPo);
            ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);
            return responseEntity;
        }
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
