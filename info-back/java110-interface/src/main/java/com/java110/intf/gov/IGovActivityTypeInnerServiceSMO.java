package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivityType.GovActivityTypeDto;
import com.java110.po.govActivityType.GovActivityTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivityTypeInnerServiceSMO
 * @Description 活动类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivityTypeApi")
public interface IGovActivityTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivityType", method = RequestMethod.POST)
    public int saveGovActivityType(@RequestBody GovActivityTypePo govActivityTypePo);

    @RequestMapping(value = "/updateGovActivityType", method = RequestMethod.POST)
    public int updateGovActivityType(@RequestBody  GovActivityTypePo govActivityTypePo);

    @RequestMapping(value = "/deleteGovActivityType", method = RequestMethod.POST)
    public int deleteGovActivityType(@RequestBody  GovActivityTypePo govActivityTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivityTypeDto 数据对象分享
     * @return GovActivityTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivityTypes", method = RequestMethod.POST)
    List<GovActivityTypeDto> queryGovActivityTypes(@RequestBody GovActivityTypeDto govActivityTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivityTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivityTypesCount", method = RequestMethod.POST)
    int queryGovActivityTypesCount(@RequestBody GovActivityTypeDto govActivityTypeDto);
}
