package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govAdvert.GovAdvertDto;
import com.java110.po.govAdvert.GovAdvertPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovAdvertInnerServiceSMO
 * @Description 广告接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govAdvertApi")
public interface IGovAdvertInnerServiceSMO {


    @RequestMapping(value = "/saveGovAdvert", method = RequestMethod.POST)
    public int saveGovAdvert(@RequestBody GovAdvertPo govAdvertPo);

    @RequestMapping(value = "/updateGovAdvert", method = RequestMethod.POST)
    public int updateGovAdvert(@RequestBody  GovAdvertPo govAdvertPo);

    @RequestMapping(value = "/deleteGovAdvert", method = RequestMethod.POST)
    public int deleteGovAdvert(@RequestBody  GovAdvertPo govAdvertPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govAdvertDto 数据对象分享
     * @return GovAdvertDto 对象数据
     */
    @RequestMapping(value = "/queryGovAdverts", method = RequestMethod.POST)
    List<GovAdvertDto> queryGovAdverts(@RequestBody GovAdvertDto govAdvertDto);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govAdvertDto 数据对象分享
     * @return GovAdvertDto 对象数据
     */
    @RequestMapping(value = "/queryAdvertItmes", method = RequestMethod.POST)
    List<GovAdvertDto> queryAdvertItmes(@RequestBody GovAdvertDto govAdvertDto);


    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govAdvertDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovAdvertsCount", method = RequestMethod.POST)
    int queryGovAdvertsCount(@RequestBody GovAdvertDto govAdvertDto);
}
