package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHelpPolicy.GovHelpPolicyDto;
import com.java110.po.govHelpPolicy.GovHelpPolicyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHelpPolicyInnerServiceSMO
 * @Description 帮扶政策接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govHelpPolicyApi")
public interface IGovHelpPolicyInnerServiceSMO {


    @RequestMapping(value = "/saveGovHelpPolicy", method = RequestMethod.POST)
    public int saveGovHelpPolicy(@RequestBody  GovHelpPolicyPo govHelpPolicyPo);

    @RequestMapping(value = "/updateGovHelpPolicy", method = RequestMethod.POST)
    public int updateGovHelpPolicy(@RequestBody  GovHelpPolicyPo govHelpPolicyPo);

    @RequestMapping(value = "/deleteGovHelpPolicy", method = RequestMethod.POST)
    public int deleteGovHelpPolicy(@RequestBody  GovHelpPolicyPo govHelpPolicyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHelpPolicyDto 数据对象分享
     * @return GovHelpPolicyDto 对象数据
     */
    @RequestMapping(value = "/queryGovHelpPolicys", method = RequestMethod.POST)
    List<GovHelpPolicyDto> queryGovHelpPolicys(@RequestBody GovHelpPolicyDto govHelpPolicyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHelpPolicyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHelpPolicysCount", method = RequestMethod.POST)
    int queryGovHelpPolicysCount(@RequestBody GovHelpPolicyDto govHelpPolicyDto);
}
