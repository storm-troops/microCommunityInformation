package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHealth.GovHealthDto;
import com.java110.po.govHealth.GovHealthPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHealthInnerServiceSMO
 * @Description 体检项目接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govHealthApi")
public interface IGovHealthInnerServiceSMO {


    @RequestMapping(value = "/saveGovHealth", method = RequestMethod.POST)
    public int saveGovHealth(@RequestBody  GovHealthPo govHealthPo);

    @RequestMapping(value = "/updateGovHealth", method = RequestMethod.POST)
    public int updateGovHealth(@RequestBody  GovHealthPo govHealthPo);

    @RequestMapping(value = "/deleteGovHealth", method = RequestMethod.POST)
    public int deleteGovHealth(@RequestBody  GovHealthPo govHealthPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHealthDto 数据对象分享
     * @return GovHealthDto 对象数据
     */
    @RequestMapping(value = "/queryGovHealths", method = RequestMethod.POST)
    List<GovHealthDto> queryGovHealths(@RequestBody GovHealthDto govHealthDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHealthDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHealthsCount", method = RequestMethod.POST)
    int queryGovHealthsCount(@RequestBody GovHealthDto govHealthDto);
}
