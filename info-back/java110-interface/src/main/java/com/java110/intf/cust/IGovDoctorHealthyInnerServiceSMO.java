package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govDoctorHealthy.GovDoctorHealthyDto;
import com.java110.po.govDoctorHealthy.GovDoctorHealthyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovDoctorHealthyInnerServiceSMO
 * @Description 档案医生接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govDoctorHealthyApi")
public interface IGovDoctorHealthyInnerServiceSMO {


    @RequestMapping(value = "/saveGovDoctorHealthy", method = RequestMethod.POST)
    public int saveGovDoctorHealthy(@RequestBody  GovDoctorHealthyPo govDoctorHealthyPo);

    @RequestMapping(value = "/updateGovDoctorHealthy", method = RequestMethod.POST)
    public int updateGovDoctorHealthy(@RequestBody  GovDoctorHealthyPo govDoctorHealthyPo);

    @RequestMapping(value = "/deleteGovDoctorHealthy", method = RequestMethod.POST)
    public int deleteGovDoctorHealthy(@RequestBody  GovDoctorHealthyPo govDoctorHealthyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govDoctorHealthyDto 数据对象分享
     * @return GovDoctorHealthyDto 对象数据
     */
    @RequestMapping(value = "/queryGovDoctorHealthys", method = RequestMethod.POST)
    List<GovDoctorHealthyDto> queryGovDoctorHealthys(@RequestBody GovDoctorHealthyDto govDoctorHealthyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govDoctorHealthyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovDoctorHealthysCount", method = RequestMethod.POST)
    int queryGovDoctorHealthysCount(@RequestBody GovDoctorHealthyDto govDoctorHealthyDto);
}
