package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govFloor.GovFloorDto;
import com.java110.po.govFloor.GovFloorPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovFloorInnerServiceSMO
 * @Description 建筑物管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govFloorApi")
public interface IGovFloorInnerServiceSMO {


    @RequestMapping(value = "/saveGovFloor", method = RequestMethod.POST)
    public int saveGovFloor(@RequestBody GovFloorPo govFloorPo);

    @RequestMapping(value = "/updateGovFloor", method = RequestMethod.POST)
    public int updateGovFloor(@RequestBody  GovFloorPo govFloorPo);

    @RequestMapping(value = "/deleteGovFloor", method = RequestMethod.POST)
    public int deleteGovFloor(@RequestBody  GovFloorPo govFloorPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govFloorDto 数据对象分享
     * @return GovFloorDto 对象数据
     */
    @RequestMapping(value = "/queryGovFloors", method = RequestMethod.POST)
    List<GovFloorDto> queryGovFloors(@RequestBody GovFloorDto govFloorDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govFloorDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovFloorsCount", method = RequestMethod.POST)
    int queryGovFloorsCount(@RequestBody GovFloorDto govFloorDto);
}
