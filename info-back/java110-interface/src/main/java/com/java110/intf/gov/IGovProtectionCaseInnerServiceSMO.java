package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govProtectionCase.GovProtectionCaseDto;
import com.java110.po.govProtectionCase.GovProtectionCasePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovProtectionCaseInnerServiceSMO
 * @Description 周边重点人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govProtectionCaseApi")
public interface IGovProtectionCaseInnerServiceSMO {


    @RequestMapping(value = "/saveGovProtectionCase", method = RequestMethod.POST)
    public int saveGovProtectionCase(@RequestBody  GovProtectionCasePo govProtectionCasePo);

    @RequestMapping(value = "/updateGovProtectionCase", method = RequestMethod.POST)
    public int updateGovProtectionCase(@RequestBody  GovProtectionCasePo govProtectionCasePo);

    @RequestMapping(value = "/deleteGovProtectionCase", method = RequestMethod.POST)
    public int deleteGovProtectionCase(@RequestBody  GovProtectionCasePo govProtectionCasePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govProtectionCaseDto 数据对象分享
     * @return GovProtectionCaseDto 对象数据
     */
    @RequestMapping(value = "/queryGovProtectionCases", method = RequestMethod.POST)
    List<GovProtectionCaseDto> queryGovProtectionCases(@RequestBody GovProtectionCaseDto govProtectionCaseDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govProtectionCaseDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovProtectionCasesCount", method = RequestMethod.POST)
    int queryGovProtectionCasesCount(@RequestBody GovProtectionCaseDto govProtectionCaseDto);
}
