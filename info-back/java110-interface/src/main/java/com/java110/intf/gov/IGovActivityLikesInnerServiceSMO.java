package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivityLikes.GovActivityLikesDto;
import com.java110.po.govActivityLikes.GovActivityLikesPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivityLikesInnerServiceSMO
 * @Description 活动点赞接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivityLikesApi")
public interface IGovActivityLikesInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivityLikes", method = RequestMethod.POST)
    public int saveGovActivityLikes(@RequestBody  GovActivityLikesPo govActivityLikesPo);

    @RequestMapping(value = "/updateGovActivityLikes", method = RequestMethod.POST)
    public int updateGovActivityLikes(@RequestBody  GovActivityLikesPo govActivityLikesPo);

    @RequestMapping(value = "/deleteGovActivityLikes", method = RequestMethod.POST)
    public int deleteGovActivityLikes(@RequestBody  GovActivityLikesPo govActivityLikesPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivityLikesDto 数据对象分享
     * @return GovActivityLikesDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivityLikess", method = RequestMethod.POST)
    List<GovActivityLikesDto> queryGovActivityLikess(@RequestBody GovActivityLikesDto govActivityLikesDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivityLikesDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivityLikessCount", method = RequestMethod.POST)
    int queryGovActivityLikessCount(@RequestBody GovActivityLikesDto govActivityLikesDto);
}
