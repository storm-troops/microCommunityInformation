package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHealthTitleInnerServiceSMO
 * @Description 体检项接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govHealthTitleApi")
public interface IGovHealthTitleInnerServiceSMO {


    @RequestMapping(value = "/saveGovHealthTitle", method = RequestMethod.POST)
    public int saveGovHealthTitle(@RequestBody  GovHealthTitlePo govHealthTitlePo);

    @RequestMapping(value = "/updateGovHealthTitle", method = RequestMethod.POST)
    public int updateGovHealthTitle(@RequestBody  GovHealthTitlePo govHealthTitlePo);

    @RequestMapping(value = "/deleteGovHealthTitle", method = RequestMethod.POST)
    public int deleteGovHealthTitle(@RequestBody  GovHealthTitlePo govHealthTitlePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHealthTitleDto 数据对象分享
     * @return GovHealthTitleDto 对象数据
     */
    @RequestMapping(value = "/queryGovHealthTitles", method = RequestMethod.POST)
    List<GovHealthTitleDto> queryGovHealthTitles(@RequestBody GovHealthTitleDto govHealthTitleDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHealthTitleDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHealthTitlesCount", method = RequestMethod.POST)
    int queryGovHealthTitlesCount(@RequestBody GovHealthTitleDto govHealthTitleDto);
}
