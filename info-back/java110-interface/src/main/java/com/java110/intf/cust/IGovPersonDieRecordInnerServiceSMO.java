package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPersonDieRecord.GovPersonDieRecordDto;
import com.java110.po.govPersonDieRecord.GovPersonDieRecordPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPersonDieRecordInnerServiceSMO
 * @Description 临终送别接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govPersonDieRecordApi")
public interface IGovPersonDieRecordInnerServiceSMO {


    @RequestMapping(value = "/saveGovPersonDieRecord", method = RequestMethod.POST)
    public int saveGovPersonDieRecord(@RequestBody  GovPersonDieRecordPo govPersonDieRecordPo);

    @RequestMapping(value = "/updateGovPersonDieRecord", method = RequestMethod.POST)
    public int updateGovPersonDieRecord(@RequestBody  GovPersonDieRecordPo govPersonDieRecordPo);

    @RequestMapping(value = "/deleteGovPersonDieRecord", method = RequestMethod.POST)
    public int deleteGovPersonDieRecord(@RequestBody  GovPersonDieRecordPo govPersonDieRecordPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonDieRecordDto 数据对象分享
     * @return GovPersonDieRecordDto 对象数据
     */
    @RequestMapping(value = "/queryGovPersonDieRecords", method = RequestMethod.POST)
    List<GovPersonDieRecordDto> queryGovPersonDieRecords(@RequestBody GovPersonDieRecordDto govPersonDieRecordDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonDieRecordDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPersonDieRecordsCount", method = RequestMethod.POST)
    int queryGovPersonDieRecordsCount(@RequestBody GovPersonDieRecordDto govPersonDieRecordDto);
}
