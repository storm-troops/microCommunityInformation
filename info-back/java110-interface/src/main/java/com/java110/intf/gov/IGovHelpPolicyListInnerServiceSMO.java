package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHelpPolicyList.GovHelpPolicyListDto;
import com.java110.po.govHelpPolicyList.GovHelpPolicyListPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHelpPolicyListInnerServiceSMO
 * @Description 帮扶记录接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govHelpPolicyListApi")
public interface IGovHelpPolicyListInnerServiceSMO {


    @RequestMapping(value = "/saveGovHelpPolicyList", method = RequestMethod.POST)
    public int saveGovHelpPolicyList(@RequestBody  GovHelpPolicyListPo govHelpPolicyListPo);

    @RequestMapping(value = "/updateGovHelpPolicyList", method = RequestMethod.POST)
    public int updateGovHelpPolicyList(@RequestBody  GovHelpPolicyListPo govHelpPolicyListPo);

    @RequestMapping(value = "/deleteGovHelpPolicyList", method = RequestMethod.POST)
    public int deleteGovHelpPolicyList(@RequestBody  GovHelpPolicyListPo govHelpPolicyListPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHelpPolicyListDto 数据对象分享
     * @return GovHelpPolicyListDto 对象数据
     */
    @RequestMapping(value = "/queryGovHelpPolicyLists", method = RequestMethod.POST)
    List<GovHelpPolicyListDto> queryGovHelpPolicyLists(@RequestBody GovHelpPolicyListDto govHelpPolicyListDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHelpPolicyListDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHelpPolicyListsCount", method = RequestMethod.POST)
    int queryGovHelpPolicyListsCount(@RequestBody GovHelpPolicyListDto govHelpPolicyListDto);
}
