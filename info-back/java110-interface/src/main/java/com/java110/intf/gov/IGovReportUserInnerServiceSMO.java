package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govReportUser.GovReportUserDto;
import com.java110.po.govReportUser.GovReportUserPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovReportUserInnerServiceSMO
 * @Description 报事类型人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "gov-service", configuration = {FeignConfiguration.class})
@RequestMapping("/govReportUserApi")
public interface IGovReportUserInnerServiceSMO {


    @RequestMapping(value = "/saveGovReportUser", method = RequestMethod.POST)
    public int saveGovReportUser(@RequestBody GovReportUserPo govReportUserPo);

    @RequestMapping(value = "/updateGovReportUser", method = RequestMethod.POST)
    public int updateGovReportUser(@RequestBody  GovReportUserPo govReportUserPo);

    @RequestMapping(value = "/deleteGovReportUser", method = RequestMethod.POST)
    public int deleteGovReportUser(@RequestBody  GovReportUserPo govReportUserPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govReportUserDto 数据对象分享
     * @return GovReportUserDto 对象数据
     */
    @RequestMapping(value = "/queryGovReportUsers", method = RequestMethod.POST)
    List<GovReportUserDto> queryGovReportUsers(@RequestBody GovReportUserDto govReportUserDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govReportUserDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovReportUsersCount", method = RequestMethod.POST)
    int queryGovReportUsersCount(@RequestBody GovReportUserDto govReportUserDto);
}
