package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivityPerson.GovActivityPersonDto;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivityPersonInnerServiceSMO
 * @Description 活动报名人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivityPersonApi")
public interface IGovActivityPersonInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivityPerson", method = RequestMethod.POST)
    public int saveGovActivityPerson(@RequestBody GovActivityPersonPo govActivityPersonPo);

    @RequestMapping(value = "/updateGovActivityPerson", method = RequestMethod.POST)
    public int updateGovActivityPerson(@RequestBody  GovActivityPersonPo govActivityPersonPo);

    @RequestMapping(value = "/deleteGovActivityPerson", method = RequestMethod.POST)
    public int deleteGovActivityPerson(@RequestBody  GovActivityPersonPo govActivityPersonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivityPersonDto 数据对象分享
     * @return GovActivityPersonDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivityPersons", method = RequestMethod.POST)
    List<GovActivityPersonDto> queryGovActivityPersons(@RequestBody GovActivityPersonDto govActivityPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivityPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivityPersonsCount", method = RequestMethod.POST)
    int queryGovActivityPersonsCount(@RequestBody GovActivityPersonDto govActivityPersonDto);
}
