package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govWorkGuide.GovWorkGuideDto;
import com.java110.po.govWorkGuide.GovWorkGuidePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovWorkGuideInnerServiceSMO
 * @Description 办事指南接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govWorkGuideApi")
public interface IGovWorkGuideInnerServiceSMO {


    @RequestMapping(value = "/saveGovWorkGuide", method = RequestMethod.POST)
    public int saveGovWorkGuide(@RequestBody GovWorkGuidePo govWorkGuidePo);

    @RequestMapping(value = "/updateGovWorkGuide", method = RequestMethod.POST)
    public int updateGovWorkGuide(@RequestBody  GovWorkGuidePo govWorkGuidePo);

    @RequestMapping(value = "/deleteGovWorkGuide", method = RequestMethod.POST)
    public int deleteGovWorkGuide(@RequestBody  GovWorkGuidePo govWorkGuidePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govWorkGuideDto 数据对象分享
     * @return GovWorkGuideDto 对象数据
     */
    @RequestMapping(value = "/queryGovWorkGuides", method = RequestMethod.POST)
    List<GovWorkGuideDto> queryGovWorkGuides(@RequestBody GovWorkGuideDto govWorkGuideDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govWorkGuideDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovWorkGuidesCount", method = RequestMethod.POST)
    int queryGovWorkGuidesCount(@RequestBody GovWorkGuideDto govWorkGuideDto);
}
