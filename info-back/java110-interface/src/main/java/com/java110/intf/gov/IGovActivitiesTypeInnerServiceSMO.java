package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivitiesType.GovActivitiesTypeDto;
import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivitiesTypeInnerServiceSMO
 * @Description 公告类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "community-service", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivitiesTypeApi")
public interface IGovActivitiesTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivitiesType", method = RequestMethod.POST)
    public int saveGovActivitiesType(@RequestBody GovActivitiesTypePo govActivitiesTypePo);

    @RequestMapping(value = "/updateGovActivitiesType", method = RequestMethod.POST)
    public int updateGovActivitiesType(@RequestBody  GovActivitiesTypePo govActivitiesTypePo);

    @RequestMapping(value = "/deleteGovActivitiesType", method = RequestMethod.POST)
    public int deleteGovActivitiesType(@RequestBody  GovActivitiesTypePo govActivitiesTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivitiesTypeDto 数据对象分享
     * @return GovActivitiesTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivitiesTypes", method = RequestMethod.POST)
    List<GovActivitiesTypeDto> queryGovActivitiesTypes(@RequestBody GovActivitiesTypeDto govActivitiesTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivitiesTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivitiesTypesCount", method = RequestMethod.POST)
    int queryGovActivitiesTypesCount(@RequestBody GovActivitiesTypeDto govActivitiesTypeDto);
}
