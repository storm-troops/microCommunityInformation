package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govSchool.GovSchoolDto;
import com.java110.po.govSchool.GovSchoolPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovSchoolInnerServiceSMO
 * @Description 学校接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govSchoolApi")
public interface IGovSchoolInnerServiceSMO {


    @RequestMapping(value = "/saveGovSchool", method = RequestMethod.POST)
    public int saveGovSchool(@RequestBody  GovSchoolPo govSchoolPo);

    @RequestMapping(value = "/updateGovSchool", method = RequestMethod.POST)
    public int updateGovSchool(@RequestBody  GovSchoolPo govSchoolPo);

    @RequestMapping(value = "/deleteGovSchool", method = RequestMethod.POST)
    public int deleteGovSchool(@RequestBody  GovSchoolPo govSchoolPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govSchoolDto 数据对象分享
     * @return GovSchoolDto 对象数据
     */
    @RequestMapping(value = "/queryGovSchools", method = RequestMethod.POST)
    List<GovSchoolDto> queryGovSchools(@RequestBody GovSchoolDto govSchoolDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govSchoolDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovSchoolsCount", method = RequestMethod.POST)
    int queryGovSchoolsCount(@RequestBody GovSchoolDto govSchoolDto);
}
