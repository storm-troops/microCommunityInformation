package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govWorkType.GovWorkTypeDto;
import com.java110.po.govWorkType.GovWorkTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovWorkTypeInnerServiceSMO
 * @Description 党内职务接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govWorkTypeApi")
public interface IGovWorkTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovWorkType", method = RequestMethod.POST)
    public int saveGovWorkType(@RequestBody GovWorkTypePo govWorkTypePo);

    @RequestMapping(value = "/updateGovWorkType", method = RequestMethod.POST)
    public int updateGovWorkType(@RequestBody  GovWorkTypePo govWorkTypePo);

    @RequestMapping(value = "/deleteGovWorkType", method = RequestMethod.POST)
    public int deleteGovWorkType(@RequestBody  GovWorkTypePo govWorkTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govWorkTypeDto 数据对象分享
     * @return GovWorkTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovWorkTypes", method = RequestMethod.POST)
    List<GovWorkTypeDto> queryGovWorkTypes(@RequestBody GovWorkTypeDto govWorkTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govWorkTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovWorkTypesCount", method = RequestMethod.POST)
    int queryGovWorkTypesCount(@RequestBody GovWorkTypeDto govWorkTypeDto);
}
