package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govVolunteerServiceRecord.GovVolunteerServiceRecordDto;
import com.java110.po.govVolunteerServiceRecord.GovVolunteerServiceRecordPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovVolunteerServiceRecordInnerServiceSMO
 * @Description 服务记录表接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govVolunteerServiceRecordApi")
public interface IGovVolunteerServiceRecordInnerServiceSMO {


    @RequestMapping(value = "/saveGovVolunteerServiceRecord", method = RequestMethod.POST)
    public int saveGovVolunteerServiceRecord(@RequestBody  GovVolunteerServiceRecordPo govVolunteerServiceRecordPo);

    @RequestMapping(value = "/updateGovVolunteerServiceRecord", method = RequestMethod.POST)
    public int updateGovVolunteerServiceRecord(@RequestBody  GovVolunteerServiceRecordPo govVolunteerServiceRecordPo);

    @RequestMapping(value = "/deleteGovVolunteerServiceRecord", method = RequestMethod.POST)
    public int deleteGovVolunteerServiceRecord(@RequestBody  GovVolunteerServiceRecordPo govVolunteerServiceRecordPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govVolunteerServiceRecordDto 数据对象分享
     * @return GovVolunteerServiceRecordDto 对象数据
     */
    @RequestMapping(value = "/queryGovVolunteerServiceRecords", method = RequestMethod.POST)
    List<GovVolunteerServiceRecordDto> queryGovVolunteerServiceRecords(@RequestBody GovVolunteerServiceRecordDto govVolunteerServiceRecordDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govVolunteerServiceRecordDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovVolunteerServiceRecordsCount", method = RequestMethod.POST)
    int queryGovVolunteerServiceRecordsCount(@RequestBody GovVolunteerServiceRecordDto govVolunteerServiceRecordDto);
}
