package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govVolunteerPersonRel.GovVolunteerPersonRelDto;
import com.java110.po.govVolunteerPersonRel.GovVolunteerPersonRelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovVolunteerPersonRelInnerServiceSMO
 * @Description 服务记录人员关系表接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govVolunteerPersonRelApi")
public interface IGovVolunteerPersonRelInnerServiceSMO {


    @RequestMapping(value = "/saveGovVolunteerPersonRel", method = RequestMethod.POST)
    public int saveGovVolunteerPersonRel(@RequestBody  GovVolunteerPersonRelPo govVolunteerPersonRelPo);

    @RequestMapping(value = "/updateGovVolunteerPersonRel", method = RequestMethod.POST)
    public int updateGovVolunteerPersonRel(@RequestBody  GovVolunteerPersonRelPo govVolunteerPersonRelPo);

    @RequestMapping(value = "/deleteGovVolunteerPersonRel", method = RequestMethod.POST)
    public int deleteGovVolunteerPersonRel(@RequestBody  GovVolunteerPersonRelPo govVolunteerPersonRelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govVolunteerPersonRelDto 数据对象分享
     * @return GovVolunteerPersonRelDto 对象数据
     */
    @RequestMapping(value = "/queryGovVolunteerPersonRels", method = RequestMethod.POST)
    List<GovVolunteerPersonRelDto> queryGovVolunteerPersonRels(@RequestBody GovVolunteerPersonRelDto govVolunteerPersonRelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govVolunteerPersonRelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovVolunteerPersonRelsCount", method = RequestMethod.POST)
    int queryGovVolunteerPersonRelsCount(@RequestBody GovVolunteerPersonRelDto govVolunteerPersonRelDto);
}
