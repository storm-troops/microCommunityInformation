package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMedicalGroup.GovMedicalGroupDto;
import com.java110.po.govMedicalGroup.GovMedicalGroupPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMedicalGroupInnerServiceSMO
 * @Description 医疗团队接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govMedicalGroupApi")
public interface IGovMedicalGroupInnerServiceSMO {


    @RequestMapping(value = "/saveGovMedicalGroup", method = RequestMethod.POST)
    public int saveGovMedicalGroup(@RequestBody  GovMedicalGroupPo govMedicalGroupPo);

    @RequestMapping(value = "/updateGovMedicalGroup", method = RequestMethod.POST)
    public int updateGovMedicalGroup(@RequestBody  GovMedicalGroupPo govMedicalGroupPo);

    @RequestMapping(value = "/deleteGovMedicalGroup", method = RequestMethod.POST)
    public int deleteGovMedicalGroup(@RequestBody  GovMedicalGroupPo govMedicalGroupPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMedicalGroupDto 数据对象分享
     * @return GovMedicalGroupDto 对象数据
     */
    @RequestMapping(value = "/queryGovMedicalGroups", method = RequestMethod.POST)
    List<GovMedicalGroupDto> queryGovMedicalGroups(@RequestBody GovMedicalGroupDto govMedicalGroupDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMedicalGroupDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMedicalGroupsCount", method = RequestMethod.POST)
    int queryGovMedicalGroupsCount(@RequestBody GovMedicalGroupDto govMedicalGroupDto);
}
