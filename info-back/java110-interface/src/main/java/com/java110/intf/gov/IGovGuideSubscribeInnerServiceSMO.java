package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govGuideSubscribe.GovGuideSubscribeDto;
import com.java110.po.govGuideSubscribe.GovGuideSubscribePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovGuideSubscribeInnerServiceSMO
 * @Description 办事预约接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govGuideSubscribeApi")
public interface IGovGuideSubscribeInnerServiceSMO {


    @RequestMapping(value = "/saveGovGuideSubscribe", method = RequestMethod.POST)
    public int saveGovGuideSubscribe(@RequestBody GovGuideSubscribePo govGuideSubscribePo);

    @RequestMapping(value = "/updateGovGuideSubscribe", method = RequestMethod.POST)
    public int updateGovGuideSubscribe(@RequestBody  GovGuideSubscribePo govGuideSubscribePo);

    @RequestMapping(value = "/deleteGovGuideSubscribe", method = RequestMethod.POST)
    public int deleteGovGuideSubscribe(@RequestBody  GovGuideSubscribePo govGuideSubscribePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govGuideSubscribeDto 数据对象分享
     * @return GovGuideSubscribeDto 对象数据
     */
    @RequestMapping(value = "/queryGovGuideSubscribes", method = RequestMethod.POST)
    List<GovGuideSubscribeDto> queryGovGuideSubscribes(@RequestBody GovGuideSubscribeDto govGuideSubscribeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govGuideSubscribeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovGuideSubscribesCount", method = RequestMethod.POST)
    int queryGovGuideSubscribesCount(@RequestBody GovGuideSubscribeDto govGuideSubscribeDto);
}
