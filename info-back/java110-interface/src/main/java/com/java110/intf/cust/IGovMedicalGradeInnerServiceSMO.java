package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMedicalGrade.GovMedicalGradeDto;
import com.java110.po.govMedicalGrade.GovMedicalGradePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMedicalGradeInnerServiceSMO
 * @Description 医疗分级接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govMedicalGradeApi")
public interface IGovMedicalGradeInnerServiceSMO {


    @RequestMapping(value = "/saveGovMedicalGrade", method = RequestMethod.POST)
    public int saveGovMedicalGrade(@RequestBody  GovMedicalGradePo govMedicalGradePo);

    @RequestMapping(value = "/updateGovMedicalGrade", method = RequestMethod.POST)
    public int updateGovMedicalGrade(@RequestBody  GovMedicalGradePo govMedicalGradePo);

    @RequestMapping(value = "/deleteGovMedicalGrade", method = RequestMethod.POST)
    public int deleteGovMedicalGrade(@RequestBody  GovMedicalGradePo govMedicalGradePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMedicalGradeDto 数据对象分享
     * @return GovMedicalGradeDto 对象数据
     */
    @RequestMapping(value = "/queryGovMedicalGrades", method = RequestMethod.POST)
    List<GovMedicalGradeDto> queryGovMedicalGrades(@RequestBody GovMedicalGradeDto govMedicalGradeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMedicalGradeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMedicalGradesCount", method = RequestMethod.POST)
    int queryGovMedicalGradesCount(@RequestBody GovMedicalGradeDto govMedicalGradeDto);
}
