package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.car.CarDto;
import com.java110.po.car.CarPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName ICarInnerServiceSMO
 * @Description 车辆接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "assets-service", configuration = {FeignConfiguration.class})
@RequestMapping("/carApi")
public interface ICarInnerServiceSMO {


    @RequestMapping(value = "/saveCar", method = RequestMethod.POST)
    public int saveCar(@RequestBody CarPo carPo);

    @RequestMapping(value = "/updateCar", method = RequestMethod.POST)
    public int updateCar(@RequestBody  CarPo carPo);

    @RequestMapping(value = "/deleteCar", method = RequestMethod.POST)
    public int deleteCar(@RequestBody  CarPo carPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param carDto 数据对象分享
     * @return CarDto 对象数据
     */
    @RequestMapping(value = "/queryCars", method = RequestMethod.POST)
    List<CarDto> queryCars(@RequestBody CarDto carDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param carDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryCarsCount", method = RequestMethod.POST)
    int queryCarsCount(@RequestBody CarDto carDto);
}
