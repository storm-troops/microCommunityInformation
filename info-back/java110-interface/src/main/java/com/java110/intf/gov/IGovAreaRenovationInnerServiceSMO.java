package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govAreaRenovation.GovAreaRenovationDto;
import com.java110.po.govAreaRenovation.GovAreaRenovationPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovAreaRenovationInnerServiceSMO
 * @Description 重点地区排查整治接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govAreaRenovationApi")
public interface IGovAreaRenovationInnerServiceSMO {


    @RequestMapping(value = "/saveGovAreaRenovation", method = RequestMethod.POST)
    public int saveGovAreaRenovation(@RequestBody  GovAreaRenovationPo govAreaRenovationPo);

    @RequestMapping(value = "/updateGovAreaRenovation", method = RequestMethod.POST)
    public int updateGovAreaRenovation(@RequestBody  GovAreaRenovationPo govAreaRenovationPo);

    @RequestMapping(value = "/deleteGovAreaRenovation", method = RequestMethod.POST)
    public int deleteGovAreaRenovation(@RequestBody  GovAreaRenovationPo govAreaRenovationPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govAreaRenovationDto 数据对象分享
     * @return GovAreaRenovationDto 对象数据
     */
    @RequestMapping(value = "/queryGovAreaRenovations", method = RequestMethod.POST)
    List<GovAreaRenovationDto> queryGovAreaRenovations(@RequestBody GovAreaRenovationDto govAreaRenovationDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govAreaRenovationDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovAreaRenovationsCount", method = RequestMethod.POST)
    int queryGovAreaRenovationsCount(@RequestBody GovAreaRenovationDto govAreaRenovationDto);
}
