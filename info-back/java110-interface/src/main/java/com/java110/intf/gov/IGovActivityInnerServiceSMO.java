package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivity.GovActivityDto;
import com.java110.po.govActivity.GovActivityPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivityInnerServiceSMO
 * @Description 活动接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivityApi")
public interface IGovActivityInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivity", method = RequestMethod.POST)
    public int saveGovActivity(@RequestBody GovActivityPo govActivityPo);

    @RequestMapping(value = "/updateGovActivity", method = RequestMethod.POST)
    public int updateGovActivity(@RequestBody  GovActivityPo govActivityPo);

    @RequestMapping(value = "/deleteGovActivity", method = RequestMethod.POST)
    public int deleteGovActivity(@RequestBody  GovActivityPo govActivityPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivityDto 数据对象分享
     * @return GovActivityDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivitys", method = RequestMethod.POST)
    List<GovActivityDto> queryGovActivitys(@RequestBody GovActivityDto govActivityDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivityDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivitysCount", method = RequestMethod.POST)
    int queryGovActivitysCount(@RequestBody GovActivityDto govActivityDto);
}
