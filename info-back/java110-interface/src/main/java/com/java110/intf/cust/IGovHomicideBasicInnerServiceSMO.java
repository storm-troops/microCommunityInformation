package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHomicideBasic.GovHomicideBasicDto;
import com.java110.po.govHomicideBasic.GovHomicideBasicPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHomicideBasicInnerServiceSMO
 * @Description 命案基本信息接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govHomicideBasicApi")
public interface IGovHomicideBasicInnerServiceSMO {


    @RequestMapping(value = "/saveGovHomicideBasic", method = RequestMethod.POST)
    public int saveGovHomicideBasic(@RequestBody  GovHomicideBasicPo govHomicideBasicPo);

    @RequestMapping(value = "/updateGovHomicideBasic", method = RequestMethod.POST)
    public int updateGovHomicideBasic(@RequestBody  GovHomicideBasicPo govHomicideBasicPo);

    @RequestMapping(value = "/deleteGovHomicideBasic", method = RequestMethod.POST)
    public int deleteGovHomicideBasic(@RequestBody  GovHomicideBasicPo govHomicideBasicPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHomicideBasicDto 数据对象分享
     * @return GovHomicideBasicDto 对象数据
     */
    @RequestMapping(value = "/queryGovHomicideBasics", method = RequestMethod.POST)
    List<GovHomicideBasicDto> queryGovHomicideBasics(@RequestBody GovHomicideBasicDto govHomicideBasicDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHomicideBasicDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHomicideBasicsCount", method = RequestMethod.POST)
    int queryGovHomicideBasicsCount(@RequestBody GovHomicideBasicDto govHomicideBasicDto);
}
