package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govAids.GovAidsDto;
import com.java110.po.govAids.GovAidsPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovAidsInnerServiceSMO
 * @Description 艾滋病者接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govAidsApi")
public interface IGovAidsInnerServiceSMO {


    @RequestMapping(value = "/saveGovAids", method = RequestMethod.POST)
    public int saveGovAids(@RequestBody  GovAidsPo govAidsPo);

    @RequestMapping(value = "/updateGovAids", method = RequestMethod.POST)
    public int updateGovAids(@RequestBody  GovAidsPo govAidsPo);

    @RequestMapping(value = "/deleteGovAids", method = RequestMethod.POST)
    public int deleteGovAids(@RequestBody  GovAidsPo govAidsPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govAidsDto 数据对象分享
     * @return GovAidsDto 对象数据
     */
    @RequestMapping(value = "/queryGovAidss", method = RequestMethod.POST)
    List<GovAidsDto> queryGovAidss(@RequestBody GovAidsDto govAidsDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govAidsDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovAidssCount", method = RequestMethod.POST)
    int queryGovAidssCount(@RequestBody GovAidsDto govAidsDto);
}
