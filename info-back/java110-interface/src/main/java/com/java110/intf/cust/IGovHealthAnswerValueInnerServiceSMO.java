package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHealthAnswerValueInnerServiceSMO
 * @Description 体检项目答案接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govHealthAnswerValueApi")
public interface IGovHealthAnswerValueInnerServiceSMO {


    @RequestMapping(value = "/saveGovHealthAnswerValue", method = RequestMethod.POST)
    public int saveGovHealthAnswerValue(@RequestBody  GovHealthAnswerValuePo govHealthAnswerValuePo);

    @RequestMapping(value = "/updateGovHealthAnswerValue", method = RequestMethod.POST)
    public int updateGovHealthAnswerValue(@RequestBody  GovHealthAnswerValuePo govHealthAnswerValuePo);

    @RequestMapping(value = "/deleteGovHealthAnswerValue", method = RequestMethod.POST)
    public int deleteGovHealthAnswerValue(@RequestBody  GovHealthAnswerValuePo govHealthAnswerValuePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHealthAnswerValueDto 数据对象分享
     * @return GovHealthAnswerValueDto 对象数据
     */
    @RequestMapping(value = "/queryGovHealthAnswerValues", method = RequestMethod.POST)
    List<GovHealthAnswerValueDto> queryGovHealthAnswerValues(@RequestBody GovHealthAnswerValueDto govHealthAnswerValueDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHealthAnswerValueDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHealthAnswerValuesCount", method = RequestMethod.POST)
    int queryGovHealthAnswerValuesCount(@RequestBody GovHealthAnswerValueDto govHealthAnswerValueDto);
}
