package com.java110.databus.reportData.car;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.databus.reportData.IReportDataAdapt;
import com.java110.dto.car.CarDto;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.dto.parkingArea.ParkingAreaDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.ICarInnerServiceSMO;
import com.java110.intf.assets.IParkingAreaInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.car.CarPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.parkingArea.ParkingAreaPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 添加车辆功能
 * add by 吴学文 2021-09-17
 */
@Service(value = "ADD_CAR")
public class ReportAddCarDataAdapt implements IReportDataAdapt {
    private final static Logger logger = LoggerFactory.getLogger(ReportAddCarDataAdapt.class);
    /**
     * 停车场处理类
     */
    @Autowired
    private IParkingAreaInnerServiceSMO parkingAreaInnerServiceSMOImpl;

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    @Autowired
    private ICarInnerServiceSMO carInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));

        JSONObject reqJson = reportDataDto.getReportDataBodyDto();

        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "carNum", "请求报文中未包含carNum");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "carBrand", "请求报文中未包含carBrand");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "carType", "请求报文中未包含carType");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "carColor", "请求报文中未包含carColor");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "extPaId", "请求报文中未包含extPaId");

        //判断停车场是存在
        ParkingAreaDto parkingAreaDto = new ParkingAreaDto();
        parkingAreaDto.setNum(reqJson.getString("extPaId"));
        parkingAreaDto.setCaId(govCommunity.getCaId());
        parkingAreaDto.setGovCommunityId(govCommunity.getGovCommunityId());
        List<ParkingAreaDto> parkingAreaDtos = parkingAreaInnerServiceSMOImpl.queryParkingAreas(parkingAreaDto);

        Assert.listOnlyOne(parkingAreaDtos, "停车场不存在");

        //人口是否存在
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setCaId(govCommunity.getCaId());
        govPersonDto.setPersonName(reqJson.getString("personName"));
        govPersonDto.setPersonTel(reqJson.getString("personTel"));
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        String personId = "";
        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            personId = savePerson(reqJson, govCommunity);
        } else {
            personId = govPersonDtos.get(0).getGovPersonId();
        }

        CarPo carPo = BeanConvertUtil.covertBean(reqJson,CarPo.class);
        carPo.setGovPersonId(personId);
        carPo.setCarId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_carId));
        carPo.setPaId(parkingAreaDtos.get(0).getPaId());

        CarDto carDto = new CarDto();
        carDto.setCarNum(reportDataDto.getReportDataBodyDto().getString("carNum"));
        carDto.setCaId(govCommunity.getCaId());
        carDto.setGovCommunityId(govCommunity.getGovCommunityId());
        List<CarDto> carDtos = carInnerServiceSMOImpl.queryCars(carDto);
        if (carDtos == null || carDtos.size() < 1) {
            int flag = carInnerServiceSMOImpl.saveCar(carPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存停车场信息失败");
            }
        } else {
            carPo.setCarId(carDtos.get(0).getCarId());
            int flag = carInnerServiceSMOImpl.updateCar(carPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改停车场信息失败");
            }
        }

        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extCarId", carPo.getCarId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }

    /**
     * 保存person
     *
     * @param reqJson
     * @param govCommunity
     */
    private String savePerson(JSONObject reqJson, GovCommunityDto govCommunity) {

        GovPersonPo govPersonPo = new GovPersonPo();
        govPersonPo.setBirthday(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govPersonPo.setCaId(govCommunity.getCaId());
        govPersonPo.setDatasourceType(GovPersonDto.DATA_SOURCE_TYPE_COMMUNITY_TYPE);
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
        govPersonPo.setIdCard("-1");
        govPersonPo.setIdType("1");
        govPersonPo.setMaritalStatus("N");
        govPersonPo.setNation("1");
        govPersonPo.setNativePlace("中国");
        govPersonPo.setPersonSex("1");
        govPersonPo.setPersonName(reqJson.getString("personName"));
        govPersonPo.setPersonTel(reqJson.getString("personTel"));
        govPersonPo.setPersonType("2002");
        govPersonPo.setPoliticalOutlook("5012");
        govPersonPo.setReligiousBelief("未知");
        govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
        return govPersonPo.getGovPersonId();
    }
}
