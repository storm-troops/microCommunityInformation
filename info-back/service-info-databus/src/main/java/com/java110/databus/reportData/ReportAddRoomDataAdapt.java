package com.java110.databus.reportData;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.govFloor.GovFloorDto;
import com.java110.dto.govRoom.GovRoomDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.DateUtil;
import com.java110.utils.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 保存房屋
 */
@Service(value = "ADD_ROOM")
public class ReportAddRoomDataAdapt implements IReportDataAdapt {

    private final static Logger logger = LoggerFactory.getLogger( ReportAddRoomDataAdapt.class );
    @Autowired
    private IGovRoomInnerServiceSMO govRoomInnerServiceSMOImpl;
    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info( JSONObject.toJSONString( reportDataDto ) );

        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "roomNum", "请求报文中未包含roomNum" );
        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "builtUpArea", "请求报文中未包含builtUpArea" );
        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "layer", "请求报文中未包含layer" );
        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "roomArea", "请求报文中未包含roomArea" );
        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "extFloorId", "请求报文中未包含extFloorId" );
        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "state", "请求报文中未包含state" );
        Assert.hasKeyAndValue( reportDataDto.getReportDataBodyDto(), "roomSubType", "请求报文中未包含roomSubType" );

        GovRoomPo govRoomPo = new GovRoomPo();
        govRoomPo.setGovRoomId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govRoomId ) );
        govRoomPo.setGovCommunityId( govCommunity.getGovCommunityId() );
        govRoomPo.setRoomNum( reportDataDto.getReportDataBodyDto().getString( "roomNum" ) );
        govRoomPo.setRoomArea( reportDataDto.getReportDataBodyDto().getString( "roomArea" ) );
        govRoomPo.setRoomType( reportDataDto.getReportDataBodyDto().getString( "state" ) );
        govRoomPo.setDatasourceType( ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE );
        govRoomPo.setGovFloorId( reportDataDto.getReportDataBodyDto().getString( "extFloorId" ) );
        govRoomPo.setCaId( govCommunity.getCaId() );
        govRoomPo.setLayer( reportDataDto.getReportDataBodyDto().getString( "layer" ) );
        govRoomPo.setIsConservati( "N" );
        if (!StringUtil.isNullOrNone( reportDataDto.getReportDataBodyDto().getString( "isConservati" ) )) {
            govRoomPo.setIsConservati( "Y" );
        }
        govRoomPo.setIsSettle( "N" );
        if (!StringUtil.isNullOrNone( reportDataDto.getReportDataBodyDto().getString( "isSettle" ) )) {
            govRoomPo.setIsSettle( "Y" );
        }
        govRoomPo.setRoomRight("1001");
        if (!StringUtil.isNullOrNone( reportDataDto.getReportDataBodyDto().getString( "roomRight" ) )) {
            govRoomPo.setRoomRight( reportDataDto.getReportDataBodyDto().getString( "roomRight" ) );
        }
        govRoomPo.setRoomAddress("无");
        if (!StringUtil.isNullOrNone( reportDataDto.getReportDataBodyDto().getString( "roomAddress" ) )) {
            govRoomPo.setRoomAddress( reportDataDto.getReportDataBodyDto().getString( "roomAddress" ) );
        }
        govRoomPo.setOwnerId( reportDataDto.getReportDataBodyDto().getString( "userId" ) );
        govRoomPo.setRamark( "物业系统自动同步数据" );

        GovRoomDto govRoomDto = new GovRoomDto();
        govRoomDto.setRoomNum( reportDataDto.getReportDataBodyDto().getString( "roomNum" ) );
        govRoomDto.setLayer( reportDataDto.getReportDataBodyDto().getString( "layer" ) );
        govRoomDto.setGovFloorId( reportDataDto.getReportDataBodyDto().getString( "extFloorId" ) );
        govRoomDto.setCaId( govCommunity.getCaId() );
        govRoomDto.setGovCommunityId( govCommunity.getGovCommunityId() );
        List<GovRoomDto> govRoomDtoList = govRoomInnerServiceSMOImpl.queryGovRooms( govRoomDto );
        if (govRoomDtoList == null || govRoomDtoList.size() < 1) {
            int flag = govRoomInnerServiceSMOImpl.saveGovRoom( govRoomPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "保存房屋信息失败" );
            }
        } else {
            govRoomPo.setGovRoomId( govRoomDtoList.get( 0 ).getGovRoomId() );
            int flag = govRoomInnerServiceSMOImpl.updateGovRoom( govRoomPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "修改房屋信息失败" );
            }
        }

        GovFloorDto govFloorDto = new GovFloorDto();
        govFloorDto.setGovFloorId(govRoomPo.getGovFloorId());
        List<GovFloorDto>  govFloorDtoList = govFloorInnerServiceSMOImpl.queryGovFloors(govFloorDto);
        if (govFloorDtoList != null && govFloorDtoList.size() > 0) {

            GovFloorPo govFloorPo = new GovFloorPo();
            govFloorPo.setGovFloorId( govRoomPo.getGovFloorId() );
            if (!StringUtil.isNullOrNone( reportDataDto.getReportDataBodyDto().getString( "layerCount" ) )) {
                govFloorPo.setLayerCount( reportDataDto.getReportDataBodyDto().getString( "layerCount" ) );
            }
            if (!StringUtil.isNullOrNone( reportDataDto.getReportDataBodyDto().getString( "unitCount" ) )) {
                govFloorPo.setUnitCount( reportDataDto.getReportDataBodyDto().getString( "unitCount" ) );
            }
            govFloorPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
            int flag = govFloorInnerServiceSMOImpl.updateGovFloor( govFloorPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "修改楼栋信息失败" );
            }
        }


        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode( ReportDataHeaderDto.CODE_SUCCESS );
        returnReportHeader.setMsg( ReportDataHeaderDto.MSG_SUCCESS );
        returnReportHeader.setResTime( DateUtil.getNow( DateUtil.DATE_FORMATE_STRING_DEFAULT ) );
        returnReportHeader.setReqTime( null );
        JSONObject body = new JSONObject();
        body.put( "extRoomId", govRoomPo.getGovRoomId() );
        returnReportData.setReportDataHeaderDto( returnReportHeader );
        returnReportData.setReportDataBodyDto( body );
        return returnReportData;
    }
}
