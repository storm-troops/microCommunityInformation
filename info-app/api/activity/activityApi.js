import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';


/**
 * 删除评论
 * @param {Object} _data 保存用户报名
 */
export function deleteGovActivityReply(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.deleteGovActivityReply,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 删除点赞
 * @param {Object} _data 保存用户报名
 */
export function deleteGovActivityLikes(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.deleteGovActivityLikes,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
	
/**
 * 保存点赞
 * @param {Object} _data 保存用户报名
 */
export function saveGovActivityLikes(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveGovActivityLikes,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 保存评论
 * @param {Object} _data 保存用户报名
 */
export function saveGovActivityReply(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveGovActivityReply,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 查询点赞
 * @param {Object} _data 
 */
export function queryGovActivityLikes(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovActivityLikes,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}


/**
 * 查询评论
 * @param {Object} _data 
 */
export function queryGovActivityReply(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovActivityReply,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}


/**
 * 查询报名类型
 * @param {Object} _data 
 */
export function queryGovActivityType(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovActivityType,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存用户报名
 */
export function saveGovActivityPerson(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveGovActivityPerson,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}


/**
 * add by wuxw
 * @param {Object} _data 删除报名记录
 */
export function deleteGovActivityPerson(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.deleteGovActivityPerson,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 查询活动
 * @param {Object} _data 
 */
export function queryGovActivity(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovActivity,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 查询活动报名人员
 * @param {Object} _data 
 */
export function queryGovActivityPerson(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovActivityPerson,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}