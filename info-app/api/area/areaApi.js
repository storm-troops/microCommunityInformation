import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';

import {getJson,saveJson} from '../../lib/java110/utils/StorageUtil.js'
	import {
		isEmpty
	} from '../../lib/java110/utils/StringUtil.js'
import conf from "../../conf/config.js"
const AREA = "HC_AREA"

/**
 * 获取区域ID
 */
export function getAreaId(){
	let caId = "";
	let area = uni.getStorageSync("_selectArea");
	if(isEmpty(area)){
		caId = conf.DEFAULT_AREA_ID;
	}else{
		caId =area.caId ;
	}
	return caId;
}

/**
 * 获取区域名称
 */
export function getAreaById(communityId){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryArea,
			method: "GET",
			data: {communityId:communityId},
			success: function(res) {
				resolve(res.data.data);
				return ;
			},
			fail: function(res) {
				reject(res);
			}
		});
	})
}

/**
 * 获取区域信息
 */
export function getArea(){
	let _communityInfo = getJson(AREA);
	if(_communityInfo){
		return _communityInfo;
	}
	return null;
}

/**
 * 查询区域列表
 */
export function getAreas(dataObj){
	return new Promise(
		(resolve, reject) => {
		requestNoAuth({
			url: url.listAreas,
			method: "GET",
			data: dataObj,
			success: function(res) {
				resolve(res.data);
				return ;
			},
			fail: function(res) {
				reject(res);
			}
		});
	})
}

