import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';

import mapping from '../../lib/java110/java110Mapping.js'



export function getUserAddress(_data) {
	return new Promise((resolve, reject) => {
		request({
			url: url.queryUserAddress,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function saveUserVisit(_data) {
	return new Promise((resolve, reject) => {
		request({
			url: url.saveUserVisitRecord,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				return;
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function saveUpdateUserAddress(_data) {

	return new Promise((resolve, reject) => {

		if (_data.userId == '') {
			reject("用户不能为空");
			return;
		} else if (_data.areaCode == '') {
			reject("地区不能为空");
			return;
		} else if (_data.tel == '') {
			reject("手机号不能为空");
			return;
		} else if (_data.address == '') {
			reject("地址不能为空");
			return;
		} else if (_data.isDefault == '') {
			reject("默认地址不能为空");
			return;
		}
		let moreRooms = [];
		request({
			url: url.saveUserAddress,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function deleteUserAddress(_data) {

	return new Promise((resolve, reject) => {

		if (_data.userId == '') {
			reject("用户不能为空");
			return;
		} else if (_data.addressId == '') {
			reject("地址不能为空");
			return;
		}
		request({
			url: url.deleteUserAddress,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
/**
 * address: "青海省西宁市城中区申宁路"
	age: 30
	email: "9"
	name: ""
	sex: ""
	tel: ""
	token: ""
	total: 0
	userId: ""
	userName: "吴学文"
 * 获取用户信息
 */
export function getUserInfo() {
	//return uni.getStorageSync(mapping.USER_INFO);
	return {
			age: 30,
			email: "9",
			name: "",
			sex: "",
			tel: "15302365896",
			token: "",
			total: 0,
			userId: "302021082485390003",
			userName: "李逍遥"
	};
}
/**
 * 用户ID
 */
export function getUserId() {
	return getUserInfo().userId;
}
/**
 * 用户名称
 */
export function getUserName() {
	return getUserInfo().userName;
}
/**
 * 用户手机号
 */
export function getUserTel() {
	return getUserInfo().tel;
}


/**
 * 注册用户
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function registerUser(_data) {

	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.register,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return;
				}
				reject(_data);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 注册用户
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function loginUser(_data) {

	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.login,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return;
				}
				reject(_data);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

export function sendMsgCode(_tel) {
	uni.showLoading();
	return new Promise((resolve, reject) => {

		requestNoAuth({
			url: url.userSendSms,
			method: "POST",
			data: {
				tel: _tel
			}, //动态数据
			success: function(res) {
				console.log(res);
				uni.hideLoading();
				//成功情况下跳转
				if (res.statusCode == 200) {
					wx.showToast({
						title: '验证码下发成功',
						icon: 'none',
						duration: 2000
					});
					wx.hideLoading();
					resolve();
					return;
				}
				wx.hideLoading();
				wx.showToast({
					title: res.data,
					icon: 'none',
					duration: 2000
				});
				reject();
			},
			fail: function(e) {
				wx.hideLoading();
				wx.showToast({
					title: "服务器异常了",
					icon: 'none',
					duration: 2000
				})
			}
		});
	})
}


/**
 * 
 * @param {Object} _data
 */
export function queryShopUserAccountAndVip(_data) {
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		request({
			url: url.queryShopUserAccountAndVip,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

export function queryGovPerson(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryGovPerson,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
