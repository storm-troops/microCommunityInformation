import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';


/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function saveGovPersonDie(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveGovPersonDie,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
