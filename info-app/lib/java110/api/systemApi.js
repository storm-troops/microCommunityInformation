import {
	requestNoAuth,
	request
} from '../java110Request.js';

import conf from '../../../conf/config.js'

const refreshUserByTokenUrl = conf.baseUrl + 'app/userAuth/refreshUserByToken';

const refreshOpenIdUrl = conf.baseUrl + 'app/userAuth/refreshToken';
/**
 *  根据HCcode 刷商城回话
 */
export function refreshUserByToken(dataObj) {
		return new Promise(
			(resolve, reject) => {
				requestNoAuth({
					url: refreshUserByTokenUrl,
					method: "POST",
					data: dataObj,
					//动态数据
					success: function(res) {
						if (res.statusCode == 200) {
							resolve(res.data);
							return;
						}
						reject(res);
					},
					fail: function(e) {
						reject(e);
					}
		});
	})
}


/**
 *  根据HCcode 刷商城回话
 */
export function refreshOpenId(dataObj) {
		return new Promise(
			(resolve, reject) => {
				request({
					url: refreshOpenIdUrl,
					method: "GET",
					data: dataObj,
					//动态数据
					success: function(res) {
						if (res.statusCode == 200) {
							resolve(res.data.data);
							return;
						}
						reject(res);
					},
					fail: function(e) {
						reject(e);
					}
		});
	})
}