/**
 * 英文包
 */
(function (window) {
    window.lang = {
        "systemName": "HC社区政务",
        "systemSimpleName": "HC",
        "subSystemName": "社区政务",
        "companyTeam": "java110 团队",
        "welcome": "欢迎访问HC社区政务",
        "signOut": "退出",
        "signIn":"登录",
        "register":"入驻平台",
        "moreCommunity":"更多小区",
        "moreMsg":"查看全部消息",
        "title":"HC社区政务",
        "noAccount":"还没有账号？ ",
        "areyouhasaccount":"已经有账户了？" 
    }
})(window)