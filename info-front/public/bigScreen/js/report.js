function _loadReport() {

    let param = {
        params: {
            caId: vc.getCurrentCommunity().caId,
            state: 'finish',
            page: 1,
            row: 1
        }
    }
    vc.http.apiGet(
        '/reportPool/quseryLargeReportCount',
        param,
        function (json, res) {
            //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
            let _json = JSON.parse(json);
            console.log(_json);
            if (_json.code == 0) {
                let _data = _json.data;
                let totalCoun = document.getElementById("totalCoun");
                totalCoun.innerHTML = _data.totalCoun;
                let doneCoun = document.getElementById("doneCoun");
                doneCoun.innerHTML = _data.doneCoun;
                let withCoun = document.getElementById("withCoun");
                withCoun.innerHTML = _data.withCoun;
                let finishCoun = document.getElementById("finishCoun");
                finishCoun.innerHTML = _data.finishCoun;
                //initChart(_data.freeRoomCount,_data.sellRoomCount);
                return;
            }
        },
        function (errInfo, error) {
            console.log('请求失败处理');

            vc.toast(errInfo);

        });
}
_loadReport();
function _listReportPools() {
    let context = '';
    var param = {
        params: {
            caId: vc.getCurrentCommunity().caId,
            page: 1,
            row: 10
        }
    };

    //发送get请求
    vc.http.apiGet('/reportPool/queryReportPool',
        param,
        function (json, res) {
            var _reportPoolManageInfo = JSON.parse(json);
            var reportPools = _reportPoolManageInfo.data;
            console.log(reportPools);
            let baoshi = document.getElementById("baoshi");
            reportPools.forEach(element => {
                context = context + '<li><p class="fl"><b>' + element.reportTypeName + '</b><br>'
                    + element.context + '<b class="fr"><a href="/admin.html#/pages/gov/reportPoolDetail?reportId=' + element.reportId + '">' + element.stateName + '</a></b></p><p class="fr pt17">' + element.appointmentTime + '</p></li>';
            });

            console.log(baoshi);
            baoshi.innerHTML = context;
        }, function (errInfo, error) {
            console.log('请求失败处理');
        }
    );
}
_listReportPools();