/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govEventsTypeManageInfo: {
                govEventsTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    caId: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovEventsTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govEventsTypeManage', 'listGovEventsType', function (_param) {
                vc.component._listGovEventsTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovEventsTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovEventsTypes: function (_page, _rows) {

                vc.component.govEventsTypeManageInfo.conditions.page = _page;
                vc.component.govEventsTypeManageInfo.conditions.row = _rows;
                vc.component.govEventsTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govEventsTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govEventsType/queryGovEventsType',
                    param,
                    function (json, res) {
                        var _govEventsTypeManageInfo = JSON.parse(json);
                        vc.component.govEventsTypeManageInfo.total = _govEventsTypeManageInfo.total;
                        vc.component.govEventsTypeManageInfo.records = _govEventsTypeManageInfo.records;
                        vc.component.govEventsTypeManageInfo.govEventsTypes = _govEventsTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govEventsTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovEventsTypeModal: function () {
                vc.emit('addGovEventsType', 'openAddGovEventsTypeModal', {});
            },
            _openEditGovEventsTypeModel: function (_govEventsType) {
                vc.emit('editGovEventsType', 'openEditGovEventsTypeModal', _govEventsType);
            },
            _openDeleteGovEventsTypeModel: function (_govEventsType) {
                vc.emit('deleteGovEventsType', 'openDeleteGovEventsTypeModal', _govEventsType);
            },
            _queryGovEventsTypeMethod: function () {
                vc.component._listGovEventsTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govEventsTypeManageInfo.moreCondition) {
                    vc.component.govEventsTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govEventsTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
