/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govHelpPolicyManageInfo: {
                govHelpPolicys: [],
                total: 0,
                records: 1,
                componentShow: 'govHelpPolicyManage',
                moreCondition: false,
                caId: '',
                conditions: {
                    caId: '',
                    helpNameLike: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovHelpPolicys(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govHelpPolicyManage', 'listGovHelpPolicy', function (_param) {
                $that.govHelpPolicyManageInfo.componentShow ='govHelpPolicyManage';
                vc.component._listGovHelpPolicys(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovHelpPolicys(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovHelpPolicys: function (_page, _rows) {

                vc.component.govHelpPolicyManageInfo.conditions.page = _page;
                vc.component.govHelpPolicyManageInfo.conditions.row = _rows;
                vc.component.govHelpPolicyManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govHelpPolicyManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govHelpPolicy/queryGovHelpPolicy',
                    param,
                    function (json, res) {
                        var _govHelpPolicyManageInfo = JSON.parse(json);
                        vc.component.govHelpPolicyManageInfo.total = _govHelpPolicyManageInfo.total;
                        vc.component.govHelpPolicyManageInfo.records = _govHelpPolicyManageInfo.records;
                        vc.component.govHelpPolicyManageInfo.govHelpPolicys = _govHelpPolicyManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govHelpPolicyManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovHelpPolicyModal: function () {
                $that.govHelpPolicyManageInfo.componentShow = 'addGovHelpPolicy';
                vc.emit('addGovHelpPolicy', 'openAddGovHelpPolicyModal', {});
            },
            _openEditGovHelpPolicyModel: function (_govHelpPolicy) {
                $that.govHelpPolicyManageInfo.componentShow = 'editGovHelpPolicy';
                vc.emit('editGovHelpPolicy', 'openEditGovHelpPolicyModal', _govHelpPolicy);
            },
            _openDeleteGovHelpPolicyModel: function (_govHelpPolicy) {
                vc.emit('deleteGovHelpPolicy', 'openDeleteGovHelpPolicyModal', _govHelpPolicy);
            },
            _queryGovHelpPolicyMethod: function () {
                vc.component._listGovHelpPolicys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govHelpPolicyManageInfo.moreCondition) {
                    vc.component.govHelpPolicyManageInfo.moreCondition = false;
                } else {
                    vc.component.govHelpPolicyManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
