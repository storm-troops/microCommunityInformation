/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPartyMemberChangeManageInfo: {
                govPartyMemberChanges: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govChangeId: '',
                componentShow: 'govPartyMemberChangeManage',
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    govMemberId: '',
                    chagneType: '',
                    state: '',

                }
            }
        },
        _initMethod: function () {
            $that.govPartyMemberChangeManageInfo.conditions.chagneType = vc.getParam('chagneType');
            vc.component._listGovPartyMemberChanges(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govPartyMemberChangeManage', 'listGovPartyMemberChange', function (_param) {
                $that.govPartyMemberChangeManageInfo.componentShow ='govPartyMemberChangeManage';
                vc.component._listGovPartyMemberChanges(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPartyMemberChanges(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPartyMemberChanges: function (_page, _rows) {

                vc.component.govPartyMemberChangeManageInfo.conditions.page = _page;
                vc.component.govPartyMemberChangeManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPartyMemberChangeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPartyMemberChange/queryGovPartyMemberChange',
                    param,
                    function (json, res) {
                        var _govPartyMemberChangeManageInfo = JSON.parse(json);
                        vc.component.govPartyMemberChangeManageInfo.total = _govPartyMemberChangeManageInfo.total;
                        vc.component.govPartyMemberChangeManageInfo.records = _govPartyMemberChangeManageInfo.records;
                        vc.component.govPartyMemberChangeManageInfo.govPartyMemberChanges = _govPartyMemberChangeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPartyMemberChangeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPartyMemberChangeModal: function () {
                
               $that.govPartyMemberChangeManageInfo.componentShow = 'addGovPartyMemberChange';
               vc.emit('addGovPartyMemberChange', 'openAddGovPartyMemberChangeModal', {});
            },
            _openEditGovPartyMemberChangeModel: function (_govPartyMemberChange) {
                $that.govPartyMemberChangeManageInfo.componentShow = 'editGovPartyMemberChange';
                vc.emit('editGovPartyMemberChange', 'openEditGovPartyMemberChangeModal', _govPartyMemberChange);
            },
            _openDeleteGovPartyMemberChangeModel: function (_govPartyMemberChange) {
                vc.emit('deleteGovPartyMemberChange', 'openDeleteGovPartyMemberChangeModal', _govPartyMemberChange);
            },
            _queryGovPartyMemberChangeMethod: function () {
                vc.component._listGovPartyMemberChanges(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPartyMemberChangeManageInfo.moreCondition) {
                    vc.component.govPartyMemberChangeManageInfo.moreCondition = false;
                } else {
                    vc.component.govPartyMemberChangeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
