/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govOldPersonTypeManageInfo: {
                govOldPersonTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
              
                    typeName: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovOldPersonTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govOldPersonTypeManage', 'listGovOldPersonType', function (_param) {
                vc.component._listGovOldPersonTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovOldPersonTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovOldPersonTypes: function (_page, _rows) {

                vc.component.govOldPersonTypeManageInfo.conditions.page = _page;
                vc.component.govOldPersonTypeManageInfo.conditions.row = _rows;
                vc.component.govOldPersonTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govOldPersonTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                    param,
                    function (json, res) {
                        var _govOldPersonTypeManageInfo = JSON.parse(json);
                        vc.component.govOldPersonTypeManageInfo.total = _govOldPersonTypeManageInfo.total;
                        vc.component.govOldPersonTypeManageInfo.records = _govOldPersonTypeManageInfo.records;
                        vc.component.govOldPersonTypeManageInfo.govOldPersonTypes = _govOldPersonTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govOldPersonTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovOldPersonTypeModal: function () {
                vc.emit('addGovOldPersonType', 'openAddGovOldPersonTypeModal', {});
            },
            _openEditGovOldPersonTypeModel: function (_govOldPersonType) {
                vc.emit('editGovOldPersonType', 'openEditGovOldPersonTypeModal', _govOldPersonType);
            },
            _openDeleteGovOldPersonTypeModel: function (_govOldPersonType) {
                vc.emit('deleteGovOldPersonType', 'openDeleteGovOldPersonTypeModal', _govOldPersonType);
            },
            _queryGovOldPersonTypeMethod: function () {
                vc.component._listGovOldPersonTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govOldPersonTypeManageInfo.moreCondition) {
                    vc.component.govOldPersonTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govOldPersonTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
