/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPetitionLetterManageInfo: {
                govPetitionLetters: [],
                petitionPurposes: [],
                stateNames: [],
                petitionForms: [],
                govPetitionTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                componentShow: 'govPetitionLetterManage',
                petitionLetterId: '',
                conditions: {
                    petitionLetterId: '',
                    caId: '',
                    govCommunityId: '',
                    letterCode: '',
                    queryCode: '',
                    petitionTime: '',
                    petitionPersonName: '',
                    petitionType: '',
                    petitionForm: '',
                    petitionPurpose: '',
                    context: '',
                    state: '',
                    dealDepartmentName: '',
                    dealUserName: '',
                    dealOpinion: '',
                    receiveUserName: '',
                    receiveDepartmentName: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            vc.getDict('gov_petition_letter', "petition_type", function (_data) {
                vc.component.govPetitionLetterManageInfo.petitionForms = _data;
            });
            vc.getDict('gov_petition_letter', "state", function (_data) {
                vc.component.govPetitionLetterManageInfo.stateNames = _data;
            });
            vc.getDict('gov_petition_letter', "petition_purpose", function (_data) {
                vc.component.govPetitionLetterManageInfo.petitionPurposes = _data;
            });
            vc.initDate('qPetitionTime', function (_value) {
                $that.govPetitionLetterManageInfo.conditions.petitionTime = _value;
            });
            vc.component._listGovPetitionLetters(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovPetitionTypes();
        },
        _initEvent: function () {
            vc.on('govPetitionLetterManage', 'listGovPetitionLetter', function (_param) {
                $that.govPetitionLetterManageInfo.componentShow ='govPetitionLetterManage';
                vc.component._listGovPetitionLetters(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPetitionLetters(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPetitionTypes: function () {
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page:1,
                        row:50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govPetitionType/queryGovPetitionType',
                    param,
                    function (json, res) {
                        var _govPetitionTypeManageInfo = JSON.parse(json);
                        vc.component.govPetitionLetterManageInfo.govPetitionTypes = _govPetitionTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovPetitionLetters: function (_page, _rows) {
                vc.component.govPetitionLetterManageInfo.conditions.page = _page;
                vc.component.govPetitionLetterManageInfo.conditions.row = _rows;
                vc.component.govPetitionLetterManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govPetitionLetterManageInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/govPetitionLetter/queryGovPetitionLetter',
                    param,
                    function (json, res) {
                        var _govPetitionLetterManageInfo = JSON.parse(json);
                        vc.component.govPetitionLetterManageInfo.total = _govPetitionLetterManageInfo.total;
                        vc.component.govPetitionLetterManageInfo.records = _govPetitionLetterManageInfo.records;
                        vc.component.govPetitionLetterManageInfo.govPetitionLetters = _govPetitionLetterManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPetitionLetterManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPetitionLetterModal: function () {
                //vc.emit('addGovPetitionLetter', 'openAddGovPetitionLetterModal', {});
                $that.govPetitionLetterManageInfo.componentShow = 'addGovPetitionLetter';
            },
            _openEditGovPetitionLetterModel: function (_govPetitionLetter) {
                $that.govPetitionLetterManageInfo.componentShow = 'editGovPetitionLetter';
                vc.emit('editGovPetitionLetter', 'openEditGovPetitionLetterModal', _govPetitionLetter);
            },
            _openAuditGovPetitionLetterModel: function (_govPetitionLetter) {
                $that.govPetitionLetterManageInfo.componentShow = 'auditGovPetitionLetter';
                vc.emit('auditGovPetitionLetter', 'openAuditGovPetitionLetterModal', _govPetitionLetter);
            },
             _getPetitionType: function (_value) {
                let _retutest = '';
                $that.govPetitionLetterManageInfo.petitionForms.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _getPetitionPurpose: function (_value) {
                let _retutest = '';
                $that.govPetitionLetterManageInfo.petitionPurposes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _getStateName: function (_value) {
                let _retutest = '';
                $that.govPetitionLetterManageInfo.stateNames.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _openDeleteGovPetitionLetterModel: function (_govPetitionLetter) {
                vc.emit('deleteGovPetitionLetter', 'openDeleteGovPetitionLetterModal', _govPetitionLetter);
            },
            _queryGovPetitionLetterMethod: function () {
                vc.component._listGovPetitionLetters(DEFAULT_PAGE, DEFAULT_ROWS);
            }
        }
    });
})(window.vc);
