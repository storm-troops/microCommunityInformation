/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govReleasePrisonManageInfo:{
                govReleasePrisons:[],
                total:0,
                records:1,
                moreCondition: false,
                releasePrisonId:'',
                conditions:{
                    releasePrisonId:'',
                    name:'',
                    caId:'',
                    address:'',
                    tel:'',
                    releaseTime:'',
                    sentenceStartTime:'',
                    sentenceEndTime:'',
                    sentenceReason:'',

                }
            }
        },
        _initMethod:function(){
            vc.component._listGovReleasePrisons(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govReleasePrisonManage','listGovReleasePrison',function(_param){
                  vc.component._listGovReleasePrisons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovReleasePrisons(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovReleasePrisons:function(_page, _rows){

                vc.component.govReleasePrisonManageInfo.conditions.page = _page;
                vc.component.govReleasePrisonManageInfo.conditions.row = _rows;
                vc.component.govReleasePrisonManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params:vc.component.govReleasePrisonManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govReleasePrison/queryGovReleasePrison',
                             param,
                             function(json,res){
                                var _govReleasePrisonManageInfo=JSON.parse(json);
                                vc.component.govReleasePrisonManageInfo.total = _govReleasePrisonManageInfo.total;
                                vc.component.govReleasePrisonManageInfo.records = _govReleasePrisonManageInfo.records;
                                vc.component.govReleasePrisonManageInfo.govReleasePrisons = _govReleasePrisonManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govReleasePrisonManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovReleasePrisonModal:function(){
                vc.emit('addGovReleasePrison','openAddGovReleasePrisonModal',{});
            },
            _openEditGovReleasePrisonModel:function(_govReleasePrison){
                vc.emit('editGovReleasePrison','openEditGovReleasePrisonModal',_govReleasePrison);
            },
            _openDeleteGovReleasePrisonModel:function(_govReleasePrison){
                vc.emit('deleteGovReleasePrison','openDeleteGovReleasePrisonModal',_govReleasePrison);
            },
            _queryGovReleasePrisonMethod:function(){
                vc.component._listGovReleasePrisons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openGovSpecialFolloModel: function (personInfo) {
                vc.jumpToPage('/admin.html#/pages/gov/govSpecialFollowManage?pId=' + personInfo.releasePrisonId +'&pName='+ personInfo.name);
            },
            _moreCondition:function(){
                if(vc.component.govReleasePrisonManageInfo.moreCondition){
                    vc.component.govReleasePrisonManageInfo.moreCondition = false;
                }else{
                    vc.component.govReleasePrisonManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
