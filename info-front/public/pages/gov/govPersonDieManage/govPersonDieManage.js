/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPersonDieManageInfo: {
                govPersonDies: [],
                total: 0,
                records: 1,
                moreCondition: false,
                componentShow: 'govPersonDieManage',
                dieId: '',
                conditions: {
                    dieId: '',
                    govPersonId: '',
                    caId: '',
                    personName: '',
                    govCommunityId: '',
                    diePlace: '',
                    dieTime: '',
                    contactPerson: '',
                    contactTel: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            vc.initDate('qDieTime', function (_value) {
                $that.govPersonDieManageInfo.conditions.dieTime = _value;
            });
            vc.component._listGovPersonDies(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {
            vc.on('govPersonDieManage', 'listGovPersonDie', function (_param) {
                $that.govPersonDieManageInfo.componentShow ='govPersonDieManage';
                vc.component._listGovPersonDies(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPersonDies(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPersonDies: function (_page, _rows) {

                vc.component.govPersonDieManageInfo.conditions.page = _page;
                vc.component.govPersonDieManageInfo.conditions.row = _rows;
                vc.component.govPersonDieManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govPersonDieManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPersonDie/queryGovPersonDie',
                    param,
                    function (json, res) {
                        var _govPersonDieManageInfo = JSON.parse(json);
                        vc.component.govPersonDieManageInfo.total = _govPersonDieManageInfo.total;
                        vc.component.govPersonDieManageInfo.records = _govPersonDieManageInfo.records;
                        vc.component.govPersonDieManageInfo.govPersonDies = _govPersonDieManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPersonDieManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openPersonDieRecordManage: function (_govPersonDie) {
                vc.jumpToPage("/admin.html#/pages/gov/govPersonDieRecordManage?dieId="+_govPersonDie.dieId+"&govPersonId="+_govPersonDie.govPersonId+"&caId="+_govPersonDie.caId);
            },
            _openAddGovPersonDieModal: function () {
                //vc.emit('addGovPersonDie', 'openAddGovPersonDieModal', {});
                $that.govPersonDieManageInfo.componentShow = 'addGovPersonDie';
            },
            _openEditGovPersonDieModel: function (_govPersonDie) {
                $that.govPersonDieManageInfo.componentShow = 'editGovPersonDie';
                vc.emit('editGovPersonDie', 'openEditGovPersonDieModal', _govPersonDie);
            },
            _openDeleteGovPersonDieModel: function (_govPersonDie) {
                vc.emit('deleteGovPersonDie', 'openDeleteGovPersonDieModal', _govPersonDie);
            },
            _queryGovPersonDieMethod: function () {
                vc.component._listGovPersonDies(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPersonDieManageInfo.moreCondition) {
                    vc.component.govPersonDieManageInfo.moreCondition = false;
                } else {
                    vc.component.govPersonDieManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
