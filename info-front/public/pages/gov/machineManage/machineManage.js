/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            machineManageInfo: {
                machines: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                machineTypes: [],
                govCommunitys:[],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    machineCode: '',
                    machineTypeCd: '',
                    govCommunityId: '',
                    machineName: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listMachines(DEFAULT_PAGE, DEFAULT_ROWS);
             vc.getDict('machine', "machine_type_cd", function (_data) {
                 vc.component.machineManageInfo.machineTypes = _data;
             });
            $that._listGovCommunitys(vc.getCurrentCommunity().caId);
        },
        _initEvent: function () {

            vc.on('machineManage', 'listMachine', function (_param) {
                vc.component._listMachines(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listMachines(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listMachines: function (_page, _rows) {

                vc.component.machineManageInfo.conditions.page = _page;
                vc.component.machineManageInfo.conditions.row = _rows;
                vc.component.machineManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.machineManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/machine/queryMachine',
                    param,
                    function (json, res) {
                        var _machineManageInfo = JSON.parse(json);
                        vc.component.machineManageInfo.total = _machineManageInfo.total;
                        vc.component.machineManageInfo.records = _machineManageInfo.records;
                        vc.component.machineManageInfo.machines = _machineManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.machineManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            // _listMachineTypes: function () {

            //     var param = {
            //         params: {
            //             caId = vc.getCurrentCommunity().caId,
            //             page: 1,
            //             row: 50
            //         }
            //     };

            //     //发送get请求
            //     vc.http.apiGet('machineType.listMachineType',
            //         param,
            //         function (json, res) {
            //             var _machineTypeManageInfo = JSON.parse(json);
            //             vc.component.machineManageInfo.machineTypes = _machineTypeManageInfo.data;
            //         }, function (errInfo, error) {
            //             console.log('请求失败处理');
            //         }
            //     );
            // },
            _listGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.machineManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddMachineModal: function () {
                vc.emit('addMachine', 'openAddMachineModal', {});
            },
            _openEditMachineModel: function (_machine) {
                vc.emit('editMachine', 'openEditMachineModal', _machine);
            },
            _openDeleteMachineModel: function (_machine) {
                vc.emit('deleteMachine', 'openDeleteMachineModal', _machine);
            },
            _queryMachineMethod: function () {
                vc.component._listMachines(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.machineManageInfo.moreCondition) {
                    vc.component.machineManageInfo.moreCondition = false;
                } else {
                    vc.component.machineManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
