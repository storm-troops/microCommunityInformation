/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govHomicideBasicManageInfo: {
                govHomicideBasics: [],
                total: 0,
                records: 1,
                moreCondition: false,
                componentShow: 'govHomicideBasicManage',
                caId: '',
                conditions: {
                    homicideNum: '',
                    homicideName: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovHomicideBasics(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govHomicideBasicManage', 'listGovHomicideBasic', function (_param) {
                $that.govHomicideBasicManageInfo.componentShow ='govHomicideBasicManage';
                vc.component._listGovHomicideBasics(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovHomicideBasics(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovHomicideBasics: function (_page, _rows) {

                vc.component.govHomicideBasicManageInfo.conditions.page = _page;
                vc.component.govHomicideBasicManageInfo.conditions.row = _rows;
                vc.component.govHomicideBasicManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govHomicideBasicManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govHomicideBasic/queryGovHomicideBasic',
                    param,
                    function (json, res) {
                        var _govHomicideBasicManageInfo = JSON.parse(json);
                        vc.component.govHomicideBasicManageInfo.total = _govHomicideBasicManageInfo.total;
                        vc.component.govHomicideBasicManageInfo.records = _govHomicideBasicManageInfo.records;
                        vc.component.govHomicideBasicManageInfo.govHomicideBasics = _govHomicideBasicManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govHomicideBasicManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovHomicideBasicModal: function () {
                $that.govHomicideBasicManageInfo.componentShow = 'addGovHomicideBasic';
                vc.emit('addGovHomicideBasic', 'openAddGovHomicideBasicModal', {});
            },
            _openEditGovHomicideBasicModel: function (_govHomicideBasic) {
                $that.govHomicideBasicManageInfo.componentShow = 'editGovHomicideBasic';
                vc.emit('editGovHomicideBasic', 'openEditGovHomicideBasicModal', _govHomicideBasic);
            },
            _openDeleteGovHomicideBasicModel: function (_govHomicideBasic) {
                vc.emit('deleteGovHomicideBasic', 'openDeleteGovHomicideBasicModal', _govHomicideBasic);
            },
            _queryGovHomicideBasicMethod: function () {
                vc.component._listGovHomicideBasics(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govHomicideBasicManageInfo.moreCondition) {
                    vc.component.govHomicideBasicManageInfo.moreCondition = false;
                } else {
                    vc.component.govHomicideBasicManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
