/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCadrePersonManageInfo: {
                govPersons: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govPersonId: '',
                componentShow: 'govCadrePersonManage',
                politicalOutlooks:[],
                isWeb: '',
                govCommunityAreas: [],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    idCardLike: '',
                    personNameLike: '',
                    personTelLike: '',
                    labelCd: '6010'
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCadrePersons(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.govCadrePersonManageInfo.politicalOutlooks = _data;
            });
        },
        _initEvent: function () {

            vc.on('govCadrePersonManage', 'listGovPerson', function (_param) {
                $that.govCadrePersonManageInfo.componentShow ='govCadrePersonManage';
                vc.component._listGovCadrePersons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCadrePersons(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCadrePersons: function (_page, _rows) {

                vc.component.govCadrePersonManageInfo.conditions.page = _page;
                vc.component.govCadrePersonManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCadrePersonManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPerson/getGovPersonHelpPolicy',
                    param,
                    function (json, res) {
                        var _govCadrePersonManageInfo = JSON.parse(json);
                        vc.component.govCadrePersonManageInfo.total = _govCadrePersonManageInfo.total;
                        vc.component.govCadrePersonManageInfo.records = _govCadrePersonManageInfo.records;
                        vc.component.govCadrePersonManageInfo.govPersons = _govCadrePersonManageInfo.data;
                        console.log(vc.component.govCadrePersonManageInfo.govPersons);
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCadrePersonManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSpecName: function (_politicalOutlook) {
                let _retutest = '';
                $that.govCadrePersonManageInfo.politicalOutlooks.forEach(_item => {
                    if (_item.statusCd == _politicalOutlook) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _openViewGovPersons: function (_govPerson) {

                vc.jumpToPage('/admin.html#/gov/viewGovCadrePerson?govPersonId='+_govPerson.govPersonId+'&govOwnerId='
                +_govPerson.govOwnerId+'&govHelpListId='+_govPerson.govHelpListId);
            },
            _queryGovPersonMethod: function () {
                vc.component._listGovCadrePersons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openDeleteGovCadrPerson: function (_govPerson) {

                vc.emit('deleteGovCadrePerson', 'openDeleteGovPersonModal', _govPerson);
            },
            _openAddGovCadrePerson: function () {
                $that.govCadrePersonManageInfo.componentShow = 'addGovCadrePerson';
                vc.emit('addGovCadrePerson', 'openaddGovCadrePersonModal', {});
            },
            _moreCondition: function () {
                if (vc.component.govCadrePersonManageInfo.moreCondition) {
                    vc.component.govCadrePersonManageInfo.moreCondition = false;
                } else {
                    vc.component.govCadrePersonManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
