/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govGridTypeManageInfo: {
                govGridTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeName: '',
                conditions: {
                    caId:vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovGridTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govGridTypeManage', 'listGovGridType', function (_param) {
                vc.component._listGovGridTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovGridTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovGridTypes: function (_page, _rows) {

                vc.component.govGridTypeManageInfo.conditions.page = _page;
                vc.component.govGridTypeManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govGridTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govGridType/queryGovGridType',
                    param,
                    function (json, res) {
                        var _govGridTypeManageInfo = JSON.parse(json);
                        vc.component.govGridTypeManageInfo.total = _govGridTypeManageInfo.total;
                        vc.component.govGridTypeManageInfo.records = _govGridTypeManageInfo.records;
                        vc.component.govGridTypeManageInfo.govGridTypes = _govGridTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govGridTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovGridTypeModal: function () {
                vc.emit('addGovGridType', 'openAddGovGridTypeModal', {});
            },
            _openEditGovGridTypeModel: function (_govGridType) {
                vc.emit('editGovGridType', 'openEditGovGridTypeModal', _govGridType);
            },
            _openDeleteGovGridTypeModel: function (_govGridType) {
                vc.emit('deleteGovGridType', 'openDeleteGovGridTypeModal', _govGridType);
            },
            _queryGovGridTypeMethod: function () {
                vc.component._listGovGridTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govGridTypeManageInfo.moreCondition) {
                    vc.component.govGridTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govGridTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
