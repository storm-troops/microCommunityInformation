/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govGuideTitleManageInfo: {
                govGuideTitles: [],
                total: 0,
                records: 1,
                moreCondition: false,
                titleId: '',
                conditions: {
                    titleType: '',
                    title: '',
                    titleId: '',
                    healthId: '',
                    caId:vc.getCurrentCommunity().caId

                }
            }
        },
        _initMethod: function () {
            let _healthId = vc.getParam('healthId');
            $that.govGuideTitleManageInfo.conditions.healthId = _healthId;
            vc.component._listgovGuideTitles(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {
            
            vc.on('govGuideTitleManage', 'listGovGuideTitle', function (_param) {
                vc.component._listgovGuideTitles(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listgovGuideTitles(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listgovGuideTitles: function (_page, _rows) {

                vc.component.govGuideTitleManageInfo.conditions.page = _page;
                vc.component.govGuideTitleManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govGuideTitleManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govHealthTitle/queryGovHealthTitle',
                    param,
                    function (json, res) {
                        var _govGuideTitleManageInfo = JSON.parse(json);
                        vc.component.govGuideTitleManageInfo.total = _govGuideTitleManageInfo.total;
                        vc.component.govGuideTitleManageInfo.records = _govGuideTitleManageInfo.records;
                        vc.component.govGuideTitleManageInfo.govGuideTitles = _govGuideTitleManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govGuideTitleManageInfo.records,
                            dataCount: vc.component.govGuideTitleManageInfo.total,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddgovGuideTitleModal: function () {
                vc.emit('addGovGuideTitle', 'openAddGovGuideTitleModal', {
                    healthId: $that.govGuideTitleManageInfo.conditions.healthId,
                });
            },
            _openEditgovGuideTitleModel: function (_govGuideTitle) {
                
                vc.emit('editGovGuideTitle', 'openEditGovGuideTitleModal', _govGuideTitle);
            },
            _openDeletegovGuideTitleModel: function (_govGuideTitle) {
                vc.emit('deleteGovGuideTitle', 'openDeleteGovGuideTitleModal', _govGuideTitle);
            },
            _querygovGuideTitleMethod: function () {
                vc.component._listgovGuideTitles(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _goBack: function () {
                vc.goBack();
            },
            _moreCondition: function () {
                if (vc.component.govGuideTitleManageInfo.moreCondition) {
                    vc.component.govGuideTitleManageInfo.moreCondition = false;
                } else {
                    vc.component.govGuideTitleManageInfo.moreCondition = true;
                }
            },
            _getTitleTypeName:function(_titleType){
                if(_titleType == '1001'){
                    return '单选';
                }else if(_titleType == '2002'){
                    return '多选';
                }else{
                    return '简答';
                }
            },
            _openQuestionValueModel:function(_govGuideTitle){
                vc.emit('govGuideTitleValue','opengovGuideTitleValueModel',_govGuideTitle);
            },
            _toQuestionValueModel:function(_govGuideTitle){
                vc.jumpToPage('/admin.html#/pages/property/reportInfoAnswerValueManage?titleId='+_govGuideTitle.titleId)
            }


        }
    });
})(window.vc);
