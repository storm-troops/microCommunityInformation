/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govRoadProtectionManageInfo: {
                govRoadProtections: [],
                roadTypes: [],
                safeHiddenGrades: [],
                total: 0,
                records: 1,
                moreCondition: false,
                roadProtectionId: '',
                conditions: {
                    roadProtectionId: '',
                    caId: '',
                    govCommunityId: '',
                    roadType: '',
                    roadName: '',
                    belongToDepartment: '',
                    departmentTel: '',
                    departmentAddress: '',
                    leadingCadreName: '',
                    leaderLink: '',
                    manageDepartment: '',
                    manageDepTel: '',
                    manageDepAddress: '',
                    branchLeaders: '',
                    branchLeadersTel: '',
                    safeHiddenGrade: '',
                    safeTrouble: '',
                    statusCd: '',
                }
            }
        },
        _initMethod: function (){
            vc.component._listGovRoadProtections(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.getDict('gov_road_protection', "road_type", function (_data) {
                vc.component.govRoadProtectionManageInfo.roadTypes = _data;
            });
            vc.getDict('gov_road_protection', "safe_hidden_grade", function (_data) {
                vc.component.govRoadProtectionManageInfo.safeHiddenGrades = _data;
            });
        },
        _initEvent: function () {
            vc.on('govRoadProtectionManage', 'listGovRoadProtection', function (_param) {
                vc.component._listGovRoadProtections(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovRoadProtections(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovRoadProtections: function (_page, _rows) {
                vc.component.govRoadProtectionManageInfo.conditions.page = _page;
                vc.component.govRoadProtectionManageInfo.conditions.row = _rows;
                vc.component.govRoadProtectionManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govRoadProtectionManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govRoadProtection/queryGovRoadProtection',
                    param,
                    function (json, res) {
                        var _govRoadProtectionManageInfo = JSON.parse(json);
                        vc.component.govRoadProtectionManageInfo.total = _govRoadProtectionManageInfo.total;
                        vc.component.govRoadProtectionManageInfo.records = _govRoadProtectionManageInfo.records;
                        vc.component.govRoadProtectionManageInfo.govRoadProtections = _govRoadProtectionManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govRoadProtectionManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getRoadTypes: function (_value) {
                let _retutest = '';
                $that.govRoadProtectionManageInfo.roadTypes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _openAddGovRoadProtectionModal: function () {
                vc.emit('addGovRoadProtection', 'openAddGovRoadProtectionModal', {});
            },
            _openEditGovRoadProtectionModel: function (_govRoadProtection) {
                vc.emit('editGovRoadProtection', 'openEditGovRoadProtectionModal', _govRoadProtection);
            },
            _openDeleteGovRoadProtectionModel: function (_govRoadProtection) {
                vc.emit('deleteGovRoadProtection', 'openDeleteGovRoadProtectionModal', _govRoadProtection);
            },
            _queryGovRoadProtectionMethod: function () {
                vc.component._listGovRoadProtections(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govRoadProtectionManageInfo.moreCondition) {
                    vc.component.govRoadProtectionManageInfo.moreCondition = false;
                } else {
                    vc.component.govRoadProtectionManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
