/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govActivityReplyManageInfo:{
                govActivityReplys:[],
                total:0,
                records:1,
                moreCondition:false,
                caId:'',
                conditions:{
                    replyId:'',
actId:'',
parentReplyId:'',
context:'',
userId:'',
userName:'',
statusCd:'',

                }
            }
        },
        _initMethod:function(){
            vc.component._listGovActivityReplys(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govActivityReplyManage','listGovActivityReply',function(_param){
                  vc.component._listGovActivityReplys(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovActivityReplys(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovActivityReplys:function(_page, _rows){

                vc.component.govActivityReplyManageInfo.conditions.page = _page;
                vc.component.govActivityReplyManageInfo.conditions.row = _rows;
                vc.component.govActivityReplyManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params:vc.component.govActivityReplyManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govActivityReply/queryGovActivityReply',
                             param,
                             function(json,res){
                                var _govActivityReplyManageInfo=JSON.parse(json);
                                vc.component.govActivityReplyManageInfo.total = _govActivityReplyManageInfo.total;
                                vc.component.govActivityReplyManageInfo.records = _govActivityReplyManageInfo.records;
                                vc.component.govActivityReplyManageInfo.govActivityReplys = _govActivityReplyManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govActivityReplyManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovActivityReplyModal:function(){
                vc.emit('addGovActivityReply','openAddGovActivityReplyModal',{});
            },
            _openEditGovActivityReplyModel:function(_govActivityReply){
                vc.emit('editGovActivityReply','openEditGovActivityReplyModal',_govActivityReply);
            },
            _openDeleteGovActivityReplyModel:function(_govActivityReply){
                vc.emit('deleteGovActivityReply','openDeleteGovActivityReplyModal',_govActivityReply);
            },
            _queryGovActivityReplyMethod:function(){
                vc.component._listGovActivityReplys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.govActivityReplyManageInfo.moreCondition){
                    vc.component.govActivityReplyManageInfo.moreCondition = false;
                }else{
                    vc.component.govActivityReplyManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
