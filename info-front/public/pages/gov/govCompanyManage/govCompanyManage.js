/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCompanyManageInfo: {
                govCompanys: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govCompanyId: '',
                govCommunityAreas: [],
                govCommunitys: [],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    companyName: '',
                    companyType: '',
                    idCard: '',
                    govCommunityId:'',
                    artificialPerson: '',

                }
            }
        },
        _initMethod: function () {
            $that.govCompanyManageInfo.conditions.companyType = vc.getParam('companyType');
            vc.component._listGovCompanys(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listCompanyGovCommunitys();
        },
        _initEvent: function () {

            vc.on('govCompanyManage', 'listGovCompany', function (_param) {
                vc.component._listGovCompanys(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCompanys(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCompanys: function (_page, _rows) {

                vc.component.govCompanyManageInfo.conditions.page = _page;
                vc.component.govCompanyManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCompanyManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govCompany/queryGovCompany',
                    param,
                    function (json, res) {
                        var _govCompanyManageInfo = JSON.parse(json);
                        vc.component.govCompanyManageInfo.total = _govCompanyManageInfo.total;
                        vc.component.govCompanyManageInfo.records = _govCompanyManageInfo.records;
                        vc.component.govCompanyManageInfo.govCompanys = _govCompanyManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCompanyManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govCompanyManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govCompanyManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govCompanyManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
             _listCompanyGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govCompanyManageInfo.total = _govCommunityManageInfo.total;
                        vc.component.govCompanyManageInfo.records = _govCommunityManageInfo.records;
                        vc.component.govCompanyManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovCompanyModal: function () {
                let _companyType = $that.govCompanyManageInfo.conditions.companyType;
                vc.emit('addGovCompany', 'openAddGovCompanyModal', {_companyType});
            },
            _openEditGovCompanyModel: function (_govCompany) {
                vc.emit('editGovCompany', 'openEditGovCompanyModal', _govCompany);
            },
            _openDeleteGovCompanyModel: function (_govCompany) {
                vc.emit('deleteGovCompany', 'openDeleteGovCompanyModal', _govCompany);
            },
            _queryGovCompanyMethod: function () {
                vc.component._listGovCompanys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govCompanyManageInfo.moreCondition) {
                    vc.component.govCompanyManageInfo.moreCondition = false;
                } else {
                    vc.component.govCompanyManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
