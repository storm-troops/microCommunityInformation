/**
    生日记录 组件
**/
(function (vc) {

    vc.extends({
        data: {
            viewPerGovActivitiesInfo: {
                index: 0,
                perActivitiesId: '',
                title: '',
                typeCd: '',
                headerImg: '',
                caId: '',
                context: '',
                userId: '',
                userName: '',
                startTime: '',
                endTime: '',
                activityTime: '',
                state: '',
                statusCd: '',
            },
            viewOldBirthInfo: {
                oldPersons: [],
            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            let _perActivitiesId = vc.getParam("perActivitiesId");
            vc.component._loadPerGovActivitiesInfoData(_perActivitiesId);
            vc.component._loadPerGovOldInfoData(_perActivitiesId);
        },
        _initEvent: function () {
            vc.on('viewPerGovActivitiesInfo', 'choosePerGovActivities', function (_app) {
                vc.copyObject(_app, vc.component.viewPerGovActivitiesInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewPerGovActivitiesInfo);
            });

            vc.on('viewPerGovActivitiesInfo', 'onIndex', function (_index) {
                vc.component.viewPerGovActivitiesInfo.index = _index;
            });

        },
        methods: {
            _openSelectPerGovActivitiesInfoModel() {
                vc.emit('choosePerGovActivities', 'openChoosePerGovActivitiesModel', {});
            },
            _openAddPerGovActivitiesInfoModel() {
                vc.emit('addPerGovActivities', 'openAddPerGovActivitiesModal', {});
            },
            _loadPerGovActivitiesInfoData: function (_perActivitiesId) {
                var param = {
                    params: {page:1,
                            row:1,
                            caId: vc.getCurrentCommunity().caId,
                            perActivitiesId:_perActivitiesId
                        }
                };
                //发送get请求
                vc.http.apiGet('/perGovActivities/queryPerGovActivities',
                    param,
                    function (json, res) {
                        var _perGovActivitiesManageInfo = JSON.parse(json);
                        vc.component.viewPerGovActivitiesInfo = _perGovActivitiesManageInfo.data[0];
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _goBack:function(){
                vc.goBack();
            },
            _loadPerGovOldInfoData: function (_perActivitiesId) {
                var param = {
                    params: {page:1,
                            row:50,
                            caId: vc.getCurrentCommunity().caId,
                            perActivitiesId:_perActivitiesId
                        }
                };
                //发送get请求
                vc.http.apiGet('/perGovActivityRel/queryPerGovActivityRel',
                    param,
                    function (json, res) {
                        var _perGovActivitiesManageInfo = JSON.parse(json);
                        vc.component.viewOldBirthInfo.oldPersons = _perGovActivitiesManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            }
        }
    });

})(window.vc);
