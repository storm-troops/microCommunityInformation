/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCarManageInfo: {
                govCars: [],
                total: 0,
                records: 1,
                moreCondition: false,
                carId: '',
                govCommunitys: [],
                govParkingAreas: [],
                conditions: {
                    govCommunityId: '',
                    paId: '',
                    carNum: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCars(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunitys();
            $that._listGovParkingAreas();
        },
        _initEvent: function () {

            vc.on('govCarManage', 'listGovCar', function (_param) {
                vc.component._listGovCars(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCars(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCars: function (_page, _rows) {

                vc.component.govCarManageInfo.conditions.page = _page;
                vc.component.govCarManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCarManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/car/queryCar',
                    param,
                    function (json, res) {
                        var _govCarManageInfo = JSON.parse(json);
                        vc.component.govCarManageInfo.total = _govCarManageInfo.total;
                        vc.component.govCarManageInfo.records = _govCarManageInfo.records;
                        vc.component.govCarManageInfo.govCars = _govCarManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCarManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovCarModal: function () {
                vc.emit('addGovCar', 'openAddGovCarModal', {});
            },
            _openEditGovCarModel: function (_govCar) {
                vc.emit('editGovCar', 'openEditGovCarModal', _govCar);
            },
            _openDeleteGovCarModel: function (_govCar) {
                vc.emit('deleteGovCar', 'openDeleteGovCarModal', _govCar);
            },
            _queryGovCarMethod: function () {
                vc.component._listGovCars(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govCarManageInfo.moreCondition) {
                    vc.component.govCarManageInfo.moreCondition = false;
                } else {
                    vc.component.govCarManageInfo.moreCondition = true;
                }
            },
            _listGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govCarManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },

            _listGovParkingAreas: function (_page, _rows) {

                vc.component.govParkingAreaManageInfo.conditions.page = _page;
                vc.component.govParkingAreaManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govParkingAreaManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/parkingArea/queryParkingArea',
                    param,
                    function (json, res) {
                        var _govParkingAreaManageInfo = JSON.parse(json);
                        let govParkingAreas = _govParkingAreaManageInfo.data;
                        $that.govCarManageInfo.govParkingAreas = govParkingAreas;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            }


        }
    });
})(window.vc);
