/**
 入驻小区
 **/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            reportPoolDispatchManageInfo: {
                pools: [],
                total: 0,
                records: 1,
                moreCondition: false,
                states: [],
                conditions: {
                    reportId: '',
                    reportName: '',
                    tel:''
                }
            }
        },
        _initMethod: function () {
            $that._listHousekeepingPools(DEFAULT_PAGE, DEFAULT_ROWS);
            //$that._validateParam();
            //与字典表关联
            vc.getDict('gov_report_pool', "state", function (_data) {
                $that.reportPoolDispatchManageInfo.states = _data;
            });
        },
        _initEvent: function () {
            vc.on('reportPoolDispatchManage', 'listHousekeepingPool', function (_param) {
                location.reload();
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                $that._listHousekeepingPools(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listHousekeepingPools: function (_page, _rows) {
                $that.reportPoolDispatchManageInfo.conditions.page = _page;
                $that.reportPoolDispatchManageInfo.conditions.row = _rows;
                $that.reportPoolDispatchManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: $that.reportPoolDispatchManageInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/reportPool/quseryReportStaff',
                    param,
                    function (json, res) {
                        var _reportPoolDispatchManageInfo = JSON.parse(json);
                        $that.reportPoolDispatchManageInfo.total = _reportPoolDispatchManageInfo.total;
                        $that.reportPoolDispatchManageInfo.records = _reportPoolDispatchManageInfo.records;
                        $that.reportPoolDispatchManageInfo.pools = _reportPoolDispatchManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: $that.reportPoolDispatchManageInfo.records,
                            dataCount: $that.reportPoolDispatchManageInfo.total,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryHousekeepingPoolMethod: function () {
                $that._listHousekeepingPools(DEFAULT_PAGE, DEFAULT_ROWS);
            },
            //办结
            _openFinishRepair: function (_pool) {
                // vc.emit('finishRepair', 'openFinishRepairModal', _pool);
                vc.jumpToPage('/admin.html#/pages/gov/finishReportPool?reportId=' + _pool.reportId+"&ruId="+_pool.ruId);
            },
            //评价
            _openFinishRepairHui: function (_pool) {
                // vc.emit('finishRepair', 'openFinishRepairModal', _pool);
                vc.jumpToPage('/admin.html#/pages/gov/finishReportPoolHui?reportId=' + _pool.reportId+"&ruId="+_pool.ruId);
            },
            _openDispatchRepairDetail: function (_pool) {
                //vc.emit('poolDetail','openHousekeepingPoolDetailModal',_pool);
                vc.jumpToPage('/admin.html#/pages/gov/poolDetail?repairId=' + _pool.repairId);
            },
            _moreCondition: function () {
                if ($that.reportPoolDispatchManageInfo.moreCondition) {
                    $that.reportPoolDispatchManageInfo.moreCondition = false;
                } else {
                    $that.reportPoolDispatchManageInfo.moreCondition = true;
                }
            },
            _openDispatchRepairModel: function (_pool) {
                _pool.action = "TRANSFER";
                vc.emit('dispatchReportPoolDispatch', 'opendispatchReportPoolDispatchModal', _pool);
            },
            _openBackRepairModel: function (_pool) {
                _pool.action = "BACK";
                vc.emit('dispatchReportPoolDispatch', 'opendispatchReportPoolDispatchModal', _pool);
            },
            //回访
            _openAppraiseRepair: function (_pool) {
                vc.emit('appraiseRepair', 'openAppraiseRepairModal', _pool);
            },
            _openDetail: function (_pool) {
                vc.jumpToPage('/admin.html#/pages/gov/reportPoolDetail?reportId=' + _pool.reportId+"&ruId="+_pool.ruId)
            }
        }
    });
})(window.vc);
