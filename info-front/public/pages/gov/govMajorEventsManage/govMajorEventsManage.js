/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMajorEventsManageInfo: {
                govMajorEventss: [],
                govEventsTypes: [],
                total: 0,
                records: 1,
                levelCds: [],
                moreCondition: false,
                caId: '',
                conditions: {
                    caId: '',
                    eventsName: '',
                    levelCd: '',
                    typeId: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMajorEventss(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovEventsTypes();
            vc.getDict('gov_major_events', "level_cd", function (_data) {
                vc.component.govMajorEventsManageInfo.levelCds = _data;
            });
        },
        _initEvent: function () {

            vc.on('govMajorEventsManage', 'listGovMajorEvents', function (_param) {
                vc.component._listGovMajorEventss(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMajorEventss(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMajorEventss: function (_page, _rows) {

                vc.component.govMajorEventsManageInfo.conditions.page = _page;
                vc.component.govMajorEventsManageInfo.conditions.row = _rows;
                vc.component.govMajorEventsManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMajorEventsManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMajorEvents/queryGovMajorEvents',
                    param,
                    function (json, res) {
                        var _govMajorEventsManageInfo = JSON.parse(json);
                        vc.component.govMajorEventsManageInfo.total = _govMajorEventsManageInfo.total;
                        vc.component.govMajorEventsManageInfo.records = _govMajorEventsManageInfo.records;
                        vc.component.govMajorEventsManageInfo.govMajorEventss = _govMajorEventsManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMajorEventsManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovEventsTypes: function () {

                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govEventsType/queryGovEventsType',
                    param,
                    function (json, res) {
                        var _govEventsTypeManageInfo = JSON.parse(json);
                        vc.component.govMajorEventsManageInfo.govEventsTypes = _govEventsTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMajorEventsModal: function () {
                vc.emit('addGovMajorEvents', 'openAddGovMajorEventsModal', {});
            },
            _openEditGovMajorEventsModel: function (_govMajorEvents) {
                vc.emit('editGovMajorEvents', 'openEditGovMajorEventsModal', _govMajorEvents);
            },
            _openDeleteGovMajorEventsModel: function (_govMajorEvents) {
                vc.emit('deleteGovMajorEvents', 'openDeleteGovMajorEventsModal', _govMajorEvents);
            },
            _queryGovMajorEventsMethod: function () {
                vc.component._listGovMajorEventss(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMajorEventsManageInfo.moreCondition) {
                    vc.component.govMajorEventsManageInfo.moreCondition = false;
                } else {
                    vc.component.govMajorEventsManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
