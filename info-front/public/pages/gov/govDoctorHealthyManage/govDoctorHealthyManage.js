/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govDoctorHealthyManageInfo: {
                govDoctorHealthys: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govMedicalGroups: [],
                titleNames: [],
                jobNames: [],
                caId: '',
                conditions: {
                    caId: '',
                    personNameLike: '',
                    groupId: '',
                    labelCd: '',
                    doctorNum: '',
                    jobName: '',
                    titleName: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovDoctorHealthys(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovMedicalGroups();
            vc.getDict('gov_doctor_healthy', "title_name", function (_data) {
                vc.component.govDoctorHealthyManageInfo.titleNames = _data;
            });
            vc.getDict('gov_doctor_healthy', "job_name", function (_data) {
                vc.component.govDoctorHealthyManageInfo.jobNames = _data;
            });
        },
        _initEvent: function () {

            vc.on('govDoctorHealthyManage', 'listGovDoctorHealthy', function (_param) {
                vc.component._listGovDoctorHealthys(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovDoctorHealthys(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovDoctorHealthys: function (_page, _rows) {

                vc.component.govDoctorHealthyManageInfo.conditions.page = _page;
                vc.component.govDoctorHealthyManageInfo.conditions.row = _rows;
                vc.component.govDoctorHealthyManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govDoctorHealthyManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govDoctorHealthy/queryGovDoctorHealthy',
                    param,
                    function (json, res) {
                        var _govDoctorHealthyManageInfo = JSON.parse(json);
                        vc.component.govDoctorHealthyManageInfo.total = _govDoctorHealthyManageInfo.total;
                        vc.component.govDoctorHealthyManageInfo.records = _govDoctorHealthyManageInfo.records;
                        vc.component.govDoctorHealthyManageInfo.govDoctorHealthys = _govDoctorHealthyManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govDoctorHealthyManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovMedicalGroups: function () {

                var param = {
                    params: {
                        page : 1,
                        row: 50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMedicalGroup/queryGovMedicalGroup',
                    param,
                    function (json, res) {
                        var _govMedicalGroupManageInfo = JSON.parse(json);
                        vc.component.govDoctorHealthyManageInfo.govMedicalGroups = _govMedicalGroupManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovDoctorHealthyModal: function () {
                vc.emit('addGovDoctorHealthy', 'openAddGovDoctorHealthyModal', {});
            },
            _openEditGovDoctorHealthyModel: function (_govDoctorHealthy) {
                vc.emit('editGovDoctorHealthy', 'openEditGovDoctorHealthyModal', _govDoctorHealthy);
            },
            _openDeleteGovDoctorHealthyModel: function (_govDoctorHealthy) {
                vc.emit('deleteGovDoctorHealthy', 'openDeleteGovDoctorHealthyModal', _govDoctorHealthy);
            },
            _queryGovDoctorHealthyMethod: function () {
                vc.component._listGovDoctorHealthys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govDoctorHealthyManageInfo.moreCondition) {
                    vc.component.govDoctorHealthyManageInfo.moreCondition = false;
                } else {
                    vc.component.govDoctorHealthyManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
