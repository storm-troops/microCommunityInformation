/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govFloorManageInfo: {
                govFloors: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govFloorId: '',
                govCommunityAreas:[],
                govCommunitys:[],
                govBuildingTypes:[],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    govCommunityId: '',
                    floorName: '',
                    floorType: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovFloors(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunitys(vc.getCurrentCommunity().caId);
            $that._listGovBuildingTypes();
        },
        _initEvent: function () {

            vc.on('govFloorManage', 'listGovFloor', function (_param) {
                 location.reload();
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovFloors(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovFloors: function (_page, _rows) {

                vc.component.govFloorManageInfo.conditions.page = _page;
                vc.component.govFloorManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govFloorManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        vc.component.govFloorManageInfo.total = _govFloorManageInfo.total;
                        vc.component.govFloorManageInfo.records = _govFloorManageInfo.records;
                        vc.component.govFloorManageInfo.govFloors = _govFloorManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govFloorManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govFloorManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govFloorManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govFloorManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govFloorManageInfo.total = _govCommunityManageInfo.total;
                        vc.component.govFloorManageInfo.records = _govCommunityManageInfo.records;
                        vc.component.govFloorManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovBuildingTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govBuildingType/queryGovBuildingType',
                    param,
                    function (json, res) {
                        var _govBuildingTypeManageInfo = JSON.parse(json);
                        vc.component.govFloorManageInfo.total = _govBuildingTypeManageInfo.total;
                        vc.component.govFloorManageInfo.records = _govBuildingTypeManageInfo.records;
                        vc.component.govFloorManageInfo.govBuildingTypes = _govBuildingTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovFloorModal: function () {
                vc.emit('addGovFloor', 'openAddGovFloorModal', {});
            },
            _openEditGovFloorModel: function (_govFloor) {
                vc.emit('editGovFloor', 'openEditGovFloorModal', _govFloor);
            },
            _openDeleteGovFloorModel: function (_govFloor) {
                vc.emit('deleteGovFloor', 'openDeleteGovFloorModal', _govFloor);
            },
            _queryGovFloorMethod: function () {
                vc.component._listGovFloors(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govFloorManageInfo.moreCondition) {
                    vc.component.govFloorManageInfo.moreCondition = false;
                } else {
                    vc.component.govFloorManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
