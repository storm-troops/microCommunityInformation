/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govParkingAreaManageInfo: {
                govParkingAreas: [],
                total: 0,
                records: 1,
                moreCondition: false,
                paId: '',
                conditions: {
                    govCommunityId: '',
                    num: '',
                    typeCd: '',
                    caId:vc.getCurrentCommunity().caId
                }   
            }
        },
        _initMethod: function () {
            vc.component._listGovParkingAreas(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunitys();
        },
        _initEvent: function () {

            vc.on('govParkingAreaManage', 'listGovParkingArea', function (_param) {
                vc.component._listGovParkingAreas(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovParkingAreas(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovParkingAreas: function (_page, _rows) {

                vc.component.govParkingAreaManageInfo.conditions.page = _page;
                vc.component.govParkingAreaManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govParkingAreaManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/parkingArea/queryParkingArea',
                    param,
                    function (json, res) {
                        var _govParkingAreaManageInfo = JSON.parse(json);
                        vc.component.govParkingAreaManageInfo.total = _govParkingAreaManageInfo.total;
                        vc.component.govParkingAreaManageInfo.records = _govParkingAreaManageInfo.records;
                        vc.component.govParkingAreaManageInfo.govParkingAreas = _govParkingAreaManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govParkingAreaManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govParkingAreaManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovParkingAreaModal: function () {
                vc.emit('addGovParkingArea', 'openAddGovParkingAreaModal', {});
            },
            _openEditGovParkingAreaModel: function (_govParkingArea) {
                vc.emit('editGovParkingArea', 'openEditGovParkingAreaModal', _govParkingArea);
            },
            _openDeleteGovParkingAreaModel: function (_govParkingArea) {
                vc.emit('deleteGovParkingArea', 'openDeleteGovParkingAreaModal', _govParkingArea);
            },
            _queryGovParkingAreaMethod: function () {
                vc.component._listGovParkingAreas(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govParkingAreaManageInfo.moreCondition) {
                    vc.component.govParkingAreaManageInfo.moreCondition = false;
                } else {
                    vc.component.govParkingAreaManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
