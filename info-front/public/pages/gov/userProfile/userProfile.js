/**
 入驻小区
 **/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    var TEMP_SEARCH = "simplifyAcceptanceSearch";
    vc.extends({
        data: {
            simplifyAcceptanceInfo: {
                searchType: '1',
                searchValue: '',
                searchPlaceholder: '请输入姓名',
                ownerPhoto: '',
                _currentTab: 'viewPersonLabel',
                caId: '',
                personType: '',
                govPersonId: '',
                idType: '',
                idCard: '',
                personName: '',
                personTel: '',
                personSex: '',
                prePersonName: '',
                birthday: '',
                nation: '',
                nativePlace: '',
                politicalOutlook: '',
                maritalStatus: '',
                religiousBelief: '',
                labelCd: '',
                ramark: '',
                politicalOutlooks:[]
            }
        },
        _initMethod: function () {
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.simplifyAcceptanceInfo.politicalOutlooks = _data;
            });
            
            let _govPersonId = vc.getParam('govPersonId');
            if (_govPersonId) {
                $that.simplifyAcceptanceInfo.searchType = "0";
                $that.simplifyAcceptanceInfo.searchValue = _govPersonId;
                $that.simplifyAcceptanceInfo.searchPlaceholder = "请输入人员Id";
                $that._doSearch();
            }
            if (!vc.isBack()) {
                return;
            }
            //检查是否有缓存数据
            let _tempData = vc.getData(TEMP_SEARCH);
            if (_tempData == null) {
                return;
            }

            $that.simplifyAcceptanceInfo.searchType = _tempData.searchType;
            $that.simplifyAcceptanceInfo.searchValue = _tempData.searchValue;
            $that.simplifyAcceptanceInfo.searchPlaceholder = _tempData.searchPlaceholder;
            $that._doSearch();
        },
        _initEvent: function () {
            vc.on('simplifyAcceptance', 'chooseRoom', function (_room) {
                vc.copyObject(_room, $that.simplifyAcceptanceInfo);
                vc.emit('personLable', 'switch', $that.simplifyAcceptanceInfo)
            });
        },
        methods: {
            _changeSearchType: function () {
                switch ($that.simplifyAcceptanceInfo.searchType) {
                    case '1':
                        $that.simplifyAcceptanceInfo.searchPlaceholder = '请输入姓名';
                        $that.simplifyAcceptanceInfo.searchValue = "";
                        break;
                    case '2':
                        $that.simplifyAcceptanceInfo.searchPlaceholder = '请输入手机号';
                        $that.simplifyAcceptanceInfo.searchValue = "";
                        break;
                    case '3':
                        $that.simplifyAcceptanceInfo.searchPlaceholder = '请输入身份证';
                        $that.simplifyAcceptanceInfo.searchValue = "";
                        break;
                }
            },
            _doSearch: function () {
                if (!vc.isNotEmpty($that.simplifyAcceptanceInfo.searchValue)) {
                    vc.toast('请输入查询条件');
                    return;
                }
               let params = {
                    page: 1,
                    row: 1,
                    caId: vc.getCurrentCommunity().caId,
                    searchType: $that.simplifyAcceptanceInfo.searchType
                }
                switch ($that.simplifyAcceptanceInfo.searchType) {
                    case '1':
                        params['personName'] = $that.simplifyAcceptanceInfo.searchValue;
                        break;
                    case '2':
                        params['personTel'] = $that.simplifyAcceptanceInfo.searchValue;
                        break;
                    case '3':
                        params['idCard'] = $that.simplifyAcceptanceInfo.searchValue;
                        break;
                }

                // 清理信息
                $that._clearData();
                let _param = {params};
                vc.http.apiGet('/govPerson/userProfile',
                    _param,
                    function (json, res) {
                        let _ownerJson = JSON.parse(json);
                        if (_ownerJson.code != 0 && _ownerJson.total != 0) {
                            vc.toast(_ownerJson.msg);
                            return;
                        }
                        $that.saveTempSearchData();
                        let _owner = _ownerJson.data[0];
                        vc.copyObject(_owner, $that.simplifyAcceptanceInfo);
                        $that.simplifyAcceptanceInfo.personSex = $that.simplifyAcceptanceInfo.personSex == '0'?'女':'男';
                        $that.simplifyAcceptanceInfo.maritalStatus = $that.simplifyAcceptanceInfo.maritalStatus == 'N'?'未婚':'已婚';

                        $that.simplifyAcceptanceInfo._currentTab = 'viewPersonLabel';
                        vc.emit('viewPersonLabelInit', 'switch', {
                            govPersonId: $that.simplifyAcceptanceInfo
                        })
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSpecName: function (_politicalOutlook) {
                if(!_politicalOutlook){
                    return;
                }
                let _retutest = '';
                vc.component.simplifyAcceptanceInfo.politicalOutlooks.forEach(_item => {
                    if (_item.statusCd == _politicalOutlook) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            saveTempSearchData: function () {
                let _searchType = $that.simplifyAcceptanceInfo.searchType;
                let _searchValue = $that.simplifyAcceptanceInfo.searchValue;
                let _searchPlaceholder = $that.simplifyAcceptanceInfo.searchPlaceholder;
                //缓存起来
                vc.saveData(TEMP_SEARCH, {
                    searchType: _searchType,
                    searchValue: _searchValue,
                    searchPlaceholder: _searchPlaceholder
                });
            },
            changeTab: function (_tab) {
                $that.simplifyAcceptanceInfo._currentTab = _tab;
                let tmpGovPersonId= $that.simplifyAcceptanceInfo.govPersonId;
                if(!tmpGovPersonId){
                    return;
                }
                if('inoutRecordInfo' == _tab || 'carInoutRecordInfo' == _tab){
                    tmpGovPersonId = $that.simplifyAcceptanceInfo.personTel;
                }
                if('homicideInfo' == _tab || 'helpPolicyListInfo' == _tab){
                    tmpGovPersonId = $that.simplifyAcceptanceInfo.govPersonId+'&'+$that.simplifyAcceptanceInfo.labelCd;
                }
                vc.emit(_tab, 'switch', {
                    govPersonId: tmpGovPersonId
                })

            },
            errorLoadImg: function () {
                vc.component.simplifyAcceptanceInfo.ownerPhoto = "/img/noPhoto.jpg";
            },
            _clearData: function () {
                let _searchType = $that.simplifyAcceptanceInfo.searchType;
                let _searchValue = $that.simplifyAcceptanceInfo.searchValue;
                let _searchPlaceholder = $that.simplifyAcceptanceInfo.searchPlaceholder;
                let _politicalOutlooks = $that.simplifyAcceptanceInfo.politicalOutlooks;

                $that.simplifyAcceptanceInfo = {
                    searchType: _searchType,
                    searchValue: _searchValue,
                    searchPlaceholder: _searchPlaceholder,
                    ownerPhoto: '',
                    _currentTab: 'personLable',
                    caId: '',
                    personType: '',
                    idType: '',
                    govPersonId: '',
                    idCard: '',
                    personName: '',
                    personTel: '',
                    personSex: '',
                    prePersonName: '',
                    birthday: '',
                    nation: '',
                    nativePlace: '',
                    politicalOutlook: '',
                    maritalStatus: '',
                    religiousBelief: '',
                    ramark: '',
                    politicalOutlooks: _politicalOutlooks
                }
            }
        }
    });
})(window.vc);
