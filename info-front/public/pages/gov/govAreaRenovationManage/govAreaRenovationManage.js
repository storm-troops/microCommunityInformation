/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govAreaRenovationManageInfo: {
                govAreaRenovations: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    typeId: '',
                    securityKey: '',
                    leadCompany: '',
                    leadParticipate: '',
                    name: '',
                    tel: '',
                    startTime: '',
                    endTime: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovAreaRenovations(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govAreaRenovationManage', 'listGovAreaRenovation', function (_param) {
                vc.component._listGovAreaRenovations(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovAreaRenovations(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovAreaRenovations: function (_page, _rows) {

                vc.component.govAreaRenovationManageInfo.conditions.page = _page;
                vc.component.govAreaRenovationManageInfo.conditions.row = _rows;
                vc.component.govAreaRenovationManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govAreaRenovationManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govAreaRenovation/queryGovAreaRenovation',
                    param,
                    function (json, res) {
                        var _govAreaRenovationManageInfo = JSON.parse(json);
                        vc.component.govAreaRenovationManageInfo.total = _govAreaRenovationManageInfo.total;
                        vc.component.govAreaRenovationManageInfo.records = _govAreaRenovationManageInfo.records;
                        vc.component.govAreaRenovationManageInfo.govAreaRenovations = _govAreaRenovationManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govAreaRenovationManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovAreaRenovationModal: function () {
                vc.emit('addGovAreaRenovation', 'openAddGovAreaRenovationModal', {});
            },
            _openEditGovAreaRenovationModel: function (_govAreaRenovation) {
                vc.emit('editGovAreaRenovation', 'openEditGovAreaRenovationModal', _govAreaRenovation);
            },
            _openDeleteGovAreaRenovationModel: function (_govAreaRenovation) {
                vc.emit('deleteGovAreaRenovation', 'openDeleteGovAreaRenovationModal', _govAreaRenovation);
            },
            _queryGovAreaRenovationMethod: function () {
                vc.component._listGovAreaRenovations(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govAreaRenovationManageInfo.moreCondition) {
                    vc.component.govAreaRenovationManageInfo.moreCondition = false;
                } else {
                    vc.component.govAreaRenovationManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
