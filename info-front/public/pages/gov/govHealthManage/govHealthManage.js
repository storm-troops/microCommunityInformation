/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govHealthManageInfo: {
                govHealths: [],
                total: 0,
                records: 1,
                caName:'',
                moreCondition: false,
                conditions: {
                    caId: vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovHealths(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govHealthManage', 'listGovHealth', function (_param) {
                vc.component._listGovHealths(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovHealths(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovHealths: function (_page, _rows) {

                vc.component.govHealthManageInfo.conditions.page = _page;
                vc.component.govHealthManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govHealthManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govHealth/queryGovHealth',
                    param,
                    function (json, res) {
                        var _govHealthManageInfo = JSON.parse(json);
                        vc.component.govHealthManageInfo.total = _govHealthManageInfo.total;
                        vc.component.govHealthManageInfo.records = _govHealthManageInfo.records;
                        vc.component.govHealthManageInfo.govHealths = _govHealthManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govHealthManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovHealthModal: function () {
                vc.emit('addGovHealth', 'openAddGovHealthModal', {});
            },
            _openEditGovHealthModel: function (_govHealth) {
                vc.emit('editGovHealth', 'openEditGovHealthModal', _govHealth);
            },
            _openDeleteGovHealthModel: function (_govHealth) {
                vc.emit('deleteGovHealth', 'openDeleteGovHealthModal', _govHealth);
            },
            _queryGovHealthMethod: function () {
                vc.component._listGovHealths(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _togovGuideTitle:function(_govGuide){
                vc.jumpToPage('/admin.html#/pages/gov/govGuideTitleManage?healthId='+_govGuide.healthId)
            },
            _moreCondition: function () {
                if (vc.component.govHealthManageInfo.moreCondition) {
                    vc.component.govHealthManageInfo.moreCondition = false;
                } else {
                    vc.component.govHealthManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
