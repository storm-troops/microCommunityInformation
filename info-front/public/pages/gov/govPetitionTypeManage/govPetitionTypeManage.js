/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPetitionTypeManageInfo: {
                govPetitionTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeId: '',
                conditions: {
                    typeId: '',
                    typeName: '',
                    seq: '',
                    caId: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPetitionTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govPetitionTypeManage', 'listGovPetitionType', function (_param) {
                vc.component._listGovPetitionTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPetitionTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPetitionTypes: function (_page, _rows) {

                vc.component.govPetitionTypeManageInfo.conditions.page = _page;
                vc.component.govPetitionTypeManageInfo.conditions.row = _rows;
                vc.component.govPetitionTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govPetitionTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPetitionType/queryGovPetitionType',
                    param,
                    function (json, res) {
                        var _govPetitionTypeManageInfo = JSON.parse(json);
                        vc.component.govPetitionTypeManageInfo.total = _govPetitionTypeManageInfo.total;
                        vc.component.govPetitionTypeManageInfo.records = _govPetitionTypeManageInfo.records;
                        vc.component.govPetitionTypeManageInfo.govPetitionTypes = _govPetitionTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPetitionTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPetitionTypeModal: function () {
                vc.emit('addGovPetitionType', 'openAddGovPetitionTypeModal', {});
            },
            _openEditGovPetitionTypeModel: function (_govPetitionType) {
                vc.emit('editGovPetitionType', 'openEditGovPetitionTypeModal', _govPetitionType);
            },
            _openDeleteGovPetitionTypeModel: function (_govPetitionType) {
                vc.emit('deleteGovPetitionType', 'openDeleteGovPetitionTypeModal', _govPetitionType);
            },
            _queryGovPetitionTypeMethod: function () {
                vc.component._listGovPetitionTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPetitionTypeManageInfo.moreCondition) {
                    vc.component.govPetitionTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govPetitionTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
