/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMedicalClassifyManageInfo: {
                govMedicalClassifys: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    medicalClassifyId: '',
                    caId: '',
                    classifyName: '',
                    classifyType: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMedicalClassifys(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govMedicalClassifyManage', 'listGovMedicalClassify', function (_param) {
                vc.component._listGovMedicalClassifys(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMedicalClassifys(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMedicalClassifys: function (_page, _rows) {

                vc.component.govMedicalClassifyManageInfo.conditions.page = _page;
                vc.component.govMedicalClassifyManageInfo.conditions.row = _rows;
                vc.component.govMedicalClassifyManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMedicalClassifyManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMedicalClassify/queryGovMedicalClassify',
                    param,
                    function (json, res) {
                        var _govMedicalClassifyManageInfo = JSON.parse(json);
                        vc.component.govMedicalClassifyManageInfo.total = _govMedicalClassifyManageInfo.total;
                        vc.component.govMedicalClassifyManageInfo.records = _govMedicalClassifyManageInfo.records;
                        vc.component.govMedicalClassifyManageInfo.govMedicalClassifys = _govMedicalClassifyManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMedicalClassifyManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMedicalClassifyModal: function () {
                vc.emit('addGovMedicalClassify', 'openAddGovMedicalClassifyModal', {});
            },
            _openEditGovMedicalClassifyModel: function (_govMedicalClassify) {
                vc.emit('editGovMedicalClassify', 'openEditGovMedicalClassifyModal', _govMedicalClassify);
            },
            _openDeleteGovMedicalClassifyModel: function (_govMedicalClassify) {
                vc.emit('deleteGovMedicalClassify', 'openDeleteGovMedicalClassifyModal', _govMedicalClassify);
            },
            _queryGovMedicalClassifyMethod: function () {
                vc.component._listGovMedicalClassifys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMedicalClassifyManageInfo.moreCondition) {
                    vc.component.govMedicalClassifyManageInfo.moreCondition = false;
                } else {
                    vc.component.govMedicalClassifyManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
