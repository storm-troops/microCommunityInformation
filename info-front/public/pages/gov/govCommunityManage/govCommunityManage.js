/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCommunityManageInfo: {
                govCommunitys: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govCommunityId: '',
                govCommunityAreas: [],
                conditions: {
                    communityName: '',
                    propertyType: '',
                    managerName: '',
                    caId: vc.getCurrentCommunity().caId

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCommunitys(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._queryGovCommunityAreas();
        },
        _initEvent: function () {

            vc.on('govCommunityManage', 'listGovCommunity', function (_param) {
                 location.reload();
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCommunitys(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCommunitys: function (_page, _rows) {

                vc.component.govCommunityManageInfo.conditions.page = _page;
                vc.component.govCommunityManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCommunityManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govCommunityManageInfo.total = _govCommunityManageInfo.total;
                        vc.component.govCommunityManageInfo.records = _govCommunityManageInfo.records;
                        vc.component.govCommunityManageInfo.govCommunitys = _govCommunityManageInfo.data;
                        vc.component.govCommunityManageInfo.govCommunitys.forEach(comItem => {
                            if (comItem.govCommunityAttrDtos.length > 0) {
                                comItem.govCommunityAttrDtos.forEach(item => {
                                    if (item.specCd == 'map_x') {
                                        comItem.mapX = item.value;
                                    }
                                    if (item.specCd == 'map_y') {
                                        comItem.mapY = item.value;
                                    }
                                });
                            }

                        });
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCommunityManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govCommunityManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govCommunityManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govCommunityManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovCommunityModal: function () {
                vc.emit('addGovCommunity', 'openAddGovCommunityModal', {});
            },
            _openEditGovCommunityModel: function (_govCommunity) {
                vc.emit('editGovCommunity', 'openEditGovCommunityModal', _govCommunity);
            },
            _openDeleteGovCommunityModel: function (_govCommunity) {
                vc.emit('deleteGovCommunity', 'openDeleteGovCommunityModal', _govCommunity);
            },
            _queryGovCommunityMethod: function () {
                vc.component._listGovCommunitys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govCommunityManageInfo.moreCondition) {
                    vc.component.govCommunityManageInfo.moreCondition = false;
                } else {
                    vc.component.govCommunityManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
