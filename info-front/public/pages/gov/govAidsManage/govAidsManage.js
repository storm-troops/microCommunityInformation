/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govAidsManageInfo: {
                govAidss: [],
                total: 0,
                records: 1,
                moreCondition: false,
                aidsId: '',
                conditions: {
                    aidsId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    aidsStartTime: '',
                    aidsReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    statusCd: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovAidss(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govAidsManage', 'listGovAids', function (_param) {
                vc.component._listGovAidss(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovAidss(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovAidss: function (_page, _rows) {
                vc.component.govAidsManageInfo.conditions.page = _page;
                vc.component.govAidsManageInfo.conditions.row = _rows;
                vc.component.govAidsManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govAidsManageInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/govAids/queryGovAids',
                    param,
                    function (json, res) {
                        var _govAidsManageInfo = JSON.parse(json);
                        vc.component.govAidsManageInfo.total = _govAidsManageInfo.total;
                        vc.component.govAidsManageInfo.records = _govAidsManageInfo.records;
                        vc.component.govAidsManageInfo.govAidss = _govAidsManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govAidsManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovAidsModal: function () {
                vc.emit('addGovAids', 'openAddGovAidsModal', {});
            },
            _openEditGovAidsModel: function (_govAids) {
                vc.emit('editGovAids', 'openEditGovAidsModal', _govAids);
            },
            _openDeleteGovAidsModel: function (_govAids) {
                vc.emit('deleteGovAids', 'openDeleteGovAidsModal', _govAids);
            },
            _queryGovAidsMethod: function () {
                vc.component._listGovAidss(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openGovSpecialFolloModel: function (personInfo) {
                vc.jumpToPage('/admin.html#/pages/gov/govSpecialFollowManage?pId=' + personInfo.aidsId +'&pName='+ personInfo.name);
            },
            _moreCondition: function () {
                if (vc.component.govAidsManageInfo.moreCondition) {
                    vc.component.govAidsManageInfo.moreCondition = false;
                } else {
                    vc.component.govAidsManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
