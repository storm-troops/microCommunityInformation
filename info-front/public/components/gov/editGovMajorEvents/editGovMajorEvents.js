(function (vc, vm) {

    vc.extends({
        data: {
            editGovMajorEventsInfo: {
                govEventsId: '',
                caId: '',
                eventsName: '',
                happenTime: '',
                levelCd: '',
                levelCds: [],
                typeId: '',
                govEventsTypes: [],
                areaCode: '',
                eventsAddress: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            $that._listEditGovEventsTypes();
            vc.initDate('happenTimes', function (_value) {
                $that.editGovMajorEventsInfo.happenTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('editGovMajorEvents', 'openEditGovMajorEventsModal', function (_params) {
                vc.component.refreshEditGovMajorEventsInfo();
                $('#editGovMajorEventsModel').modal('show');
                vc.copyObject(_params, vc.component.editGovMajorEventsInfo);
                vc.component.editGovMajorEventsInfo.caId = vc.getCurrentCommunity().caId;

                vc.getDict('gov_major_events', "level_cd", function (_data) {
                    vc.component.editGovMajorEventsInfo.levelCds = _data;
                });
            });
        },
        methods: {
            editGovMajorEventsValidate: function () {
                return vc.validate.validate({
                    editGovMajorEventsInfo: vc.component.editGovMajorEventsInfo
                }, {
                    'editGovMajorEventsInfo.govEventsId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "重特大ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "重特大ID不能超过30"
                        },
                    ],
                    'editGovMajorEventsInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovMajorEventsInfo.eventsName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "名称不能超过64"
                        },
                    ],
                    'editGovMajorEventsInfo.happenTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生时间不能超过时间类型"
                        },
                    ],
                    'editGovMajorEventsInfo.levelCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "事件分级不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "事件分级不能超过30"
                        },
                    ],
                    'editGovMajorEventsInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "事件类型Id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "事件类型Id不能超过102"
                        },
                    ],
                    'editGovMajorEventsInfo.eventsAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "详细地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "详细地址不能超过1024"
                        },
                    ],
                    'editGovMajorEventsInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovMajorEvents: function () {
                if (!vc.component.editGovMajorEventsValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.component.editGovMajorEventsInfo.areaCode = vc.component.editGovMajorEventsInfo.eventsAddress;
                vc.http.apiPost(
                    '/govMajorEvents/updateGovMajorEvents',
                    JSON.stringify(vc.component.editGovMajorEventsInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMajorEventsModel').modal('hide');
                            vc.emit('govMajorEventsManage', 'listGovMajorEvents', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovEventsTypes: function () {

                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govEventsType/queryGovEventsType',
                    param,
                    function (json, res) {
                        var _govEventsTypeManageInfo = JSON.parse(json);
                        vc.component.editGovMajorEventsInfo.govEventsTypes = _govEventsTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovMajorEventsInfo: function () {
                let _levelCds = vc.component.editGovMajorEventsInfo.levelCds;
                let _govEventsTypes = vc.component.editGovMajorEventsInfo.govEventsTypes;
                vc.component.editGovMajorEventsInfo = {
                    govEventsId: '',
                    caId: '',
                    eventsName: '',
                    happenTime: '',
                    levelCd: '',
                    levelCds: _levelCds,
                    typeId: '',
                    govEventsTypes: _govEventsTypes,
                    areaCode: '',
                    eventsAddress: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
