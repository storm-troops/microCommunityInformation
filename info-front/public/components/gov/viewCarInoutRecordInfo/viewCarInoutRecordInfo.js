(function (vc) {
    vc.extends({
        data: {
            viewCarInoutRecordInfo: {
                orgId: '',
                govPersonId: '',
                govInouts: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('carInoutRecordInfo', 'switch', function (_param) {
                vc.component._refreshviewCarInoutRecordInfo();
                console.log(_param);
                vc.component._loadCarInoutRecordInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadCarInoutRecordInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        personLink: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCarInout/queryGovCarInout',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewCarInoutRecordInfo.govInouts = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewCarInoutRecordInfo: function () {
                $that.viewCarInoutRecordInfo = {
                    orgId: '',
                    govPersonId: '',
                    govInouts: []
                }
            }
        }

    });
})(window.vc);
