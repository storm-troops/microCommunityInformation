(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCadrePersonInfo: {
                govPersonId: '',
                lableCd: '6010',
                lableName: '扶贫干部',
                lableType: 'P',
                caId: '',
                personName: ''

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovCadrePerson', 'openaddGovCadrePersonModal', function () {
                $('#addGovCadrePersonModel').modal('show');
            });

            vc.on('addGovCadrePerson','chooseGovPerson', function (_param) {
                $that.addGovCadrePersonInfo.govPersonId = _param.govPersonId;
                $that.addGovCadrePersonInfo.personName = _param.personName;
            });
        },
        methods: {
            addGovCadrePersonValidate() {
                return vc.validate.validate({
                    addGovCadrePersonInfo: vc.component.addGovCadrePersonInfo
                }, {
                    'addGovCadrePersonInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人员名称不能为空"
                        }
                    ]
                });
            },
            saveaGovPoorPersonInfo: function () {
                if (!vc.component.addGovCadrePersonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.addGovCadrePersonInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCadrePersonInfo);
                    $('#addGovCadrePersonModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govPersonLabelRel/savePersonLabelRel',
                    JSON.stringify(vc.component.addGovCadrePersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCadrePersonModel').modal('hide');
                            vc.component.clearaddGovCadrePersonInfo();
                            vc.emit('govCadrePersonManage', 'listGovPerson', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;

                vc.emit('chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeaddGovCadrePerson: function () {
                $that.clearaddGovCadrePersonInfo();
                vc.emit('govCadrePersonManage', 'listGovPerson', {});
            },
            clearaddGovCadrePersonInfo: function () {
                vc.component.addGovCadrePersonInfo = {
                    govPersonId: '',
                    lableCd: '6010',
                    lableName: '扶贫干部',
                    lableType: 'P',
                    caId: '',
                    personName: ''

                };
            }
        }
    });

})(window.vc);
