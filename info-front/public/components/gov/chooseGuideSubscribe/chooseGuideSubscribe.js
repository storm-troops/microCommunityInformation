(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGuideSubscribe:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGuideSubscribeInfo:{
                guideSubscribes:[],
                _currentGuideSubscribeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGuideSubscribe','openChooseGuideSubscribeModel',function(_param){
                $('#chooseGuideSubscribeModel').modal('show');
                vc.component._refreshChooseGuideSubscribeInfo();
                vc.component._loadAllGuideSubscribeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGuideSubscribeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('guideSubscribe.listGuideSubscribes',
                             param,
                             function(json){
                                var _guideSubscribeInfo = JSON.parse(json);
                                vc.component.chooseGuideSubscribeInfo.guideSubscribes = _guideSubscribeInfo.guideSubscribes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGuideSubscribe:function(_guideSubscribe){
                if(_guideSubscribe.hasOwnProperty('name')){
                     _guideSubscribe.guideSubscribeName = _guideSubscribe.name;
                }
                vc.emit($props.emitChooseGuideSubscribe,'chooseGuideSubscribe',_guideSubscribe);
                vc.emit($props.emitLoadData,'listGuideSubscribeData',{
                    guideSubscribeId:_guideSubscribe.guideSubscribeId
                });
                $('#chooseGuideSubscribeModel').modal('hide');
            },
            queryGuideSubscribes:function(){
                vc.component._loadAllGuideSubscribeInfo(1,10,vc.component.chooseGuideSubscribeInfo._currentGuideSubscribeName);
            },
            _refreshChooseGuideSubscribeInfo:function(){
                vc.component.chooseGuideSubscribeInfo._currentGuideSubscribeName = "";
            }
        }

    });
})(window.vc);
