(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovLabel:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovLabelInfo:{
                govLabels:[],
                govLabelRels:[],
                labelRel: [],
                govPersonId:'',
                labelCd:'',
                caId:'',
                govLabelRelString:'',
                _currentGovLabelName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovLabel','openChooseGovLabelModel',function(_param){
                vc.component.chooseGovLabelInfo.govPersonId = _param.govPersonId;
                vc.component.chooseGovLabelInfo.caId = _param.caId;
                $('#chooseGovLabelModel').modal('show');
                vc.component._refreshChooseGovLabelInfo();
                vc.component._loadAllGovLabelInfo(1,20,'');
                vc.component._loadGovLabelRelInfo(1,20,_param.govPersonId);
            });
            vc.on('chooseGovLabelPlus','paginationPlus', 'page_event', function (_currentPage) {
                vc.component._loadAllGovLabelInfo(_currentPage, 20,'');
            });
        },
        methods:{
            _loadAllGovLabelInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        labelType:"C",
                        caId:vc.getCurrentCommunity().caId,
                    }
                };
                //发送get请求
               vc.http.apiGet('/govLabel/queryGovLabel',
                             param,
                             function(json){
                                var _govLabelInfo = JSON.parse(json);
                                vc.component.chooseGovLabelInfo.govLabels = _govLabelInfo.data;
                                vc.emit('chooseGovLabelPlus','paginationPlus', 'init', {
                                    total: _govLabelInfo.records,
                                    currentPage: _page
                                });
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            _loadGovLabelRelInfo:function(_page,_row,govPersonId){
                $that.chooseGovLabelInfo.labelRel = [];
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        govPersonId:govPersonId
                    }
                };
                //发送get请求
               vc.http.apiGet('/govPersonLabelRel/queryGovPersonLabelRel',
                             param,
                             function(json){
                                var _govLabelInfo = JSON.parse(json);
                                let govLabelRels = _govLabelInfo.data;
                                vc.component.chooseGovLabelInfo.govLabelRels = _govLabelInfo.data;
                                let _labelRel = "";
                                govLabelRels.forEach(item => {
                                    if((_labelRel+",").indexOf(item.labelCd+",") ==-1){
                                        $that.chooseGovLabelInfo.labelRel.push(item.labelCd);
                                        _labelRel = vc.component.chooseGovLabelInfo.labelRel.toString();
                                    }
                                  });
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            selectPersonLabel: function(){
                let _labelRel = vc.component.chooseGovLabelInfo.labelRel.toString();
                vc.component.chooseGovLabelInfo.govLabelRelString = _labelRel;
                if(_labelRel){
                    $that.saveGovPersonLabels();
                }
            },
            saveGovPersonLabels: function () {
                vc.http.apiPost(
                    '/govPersonLabelRel/saveGovPersonLabelRel',
                    JSON.stringify(vc.component.chooseGovLabelInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            chooseGovLabel:function(_govLabel){
                if(_govLabel.hasOwnProperty('name')){
                     _govLabel.govLabelName = _govLabel.name;
                }
                vc.emit($props.emitChooseGovLabel,'chooseGovLabel',_govLabel);
                vc.emit($props.emitLoadData,'listGovLabelData',{
                    govLabelId:_govLabel.govLabelId
                });
                $('#chooseGovLabelModel').modal('hide');
            },
            queryGovLabels:function(){
                vc.component._loadAllGovLabelInfo(1,10,vc.component.chooseGovLabelInfo._currentGovLabelName);
            },
            _refreshChooseGovLabelInfo:function(){
                vc.component.chooseGovLabelInfo._currentGovLabelName = "";
            }
        }

    });
})(window.vc);
