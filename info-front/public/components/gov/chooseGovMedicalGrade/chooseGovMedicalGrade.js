(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMedicalGrade:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMedicalGradeInfo:{
                govMedicalGrades:[],
                _currentGovMedicalGradeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMedicalGrade','openChooseGovMedicalGradeModel',function(_param){
                $('#chooseGovMedicalGradeModel').modal('show');
                vc.component._refreshChooseGovMedicalGradeInfo();
                vc.component._loadAllGovMedicalGradeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMedicalGradeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMedicalGrade/queryGovMedicalGrade',
                             param,
                             function(json){
                                var _govMedicalGradeInfo = JSON.parse(json);
                                vc.component.chooseGovMedicalGradeInfo.govMedicalGrades = _govMedicalGradeInfo.govMedicalGrades;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMedicalGrade:function(_govMedicalGrade){
                if(_govMedicalGrade.hasOwnProperty('name')){
                     _govMedicalGrade.govMedicalGradeName = _govMedicalGrade.name;
                }
                vc.emit($props.emitChooseGovMedicalGrade,'chooseGovMedicalGrade',_govMedicalGrade);
                vc.emit($props.emitLoadData,'listGovMedicalGradeData',{
                    govMedicalGradeId:_govMedicalGrade.govMedicalGradeId
                });
                $('#chooseGovMedicalGradeModel').modal('hide');
            },
            queryGovMedicalGrades:function(){
                vc.component._loadAllGovMedicalGradeInfo(1,10,vc.component.chooseGovMedicalGradeInfo._currentGovMedicalGradeName);
            },
            _refreshChooseGovMedicalGradeInfo:function(){
                vc.component.chooseGovMedicalGradeInfo._currentGovMedicalGradeName = "";
            }
        }

    });
})(window.vc);
