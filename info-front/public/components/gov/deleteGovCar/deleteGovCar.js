(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCarInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCar', 'openDeleteGovCarModal', function (_params) {

                vc.component.deleteGovCarInfo = _params;
                $('#deleteGovCarModel').modal('show');

            });
        },
        methods: {
            deleteGovCar: function () {
                vc.component.deleteGovCarInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/car/deleteCar',
                    JSON.stringify(vc.component.deleteGovCarInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCarModel').modal('hide');
                            vc.emit('govCarManage', 'listGovCar', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovCarModel: function () {
                $('#deleteGovCarModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
