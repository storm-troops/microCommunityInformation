(function (vc, vm) {

    vc.extends({
        data: {
            editGovOwnerInfo: {
                govOwnerId: '',
                caId: '',
                roomId: '',
                roomNum: '',
                ownerNum: '',
                ownerName: '',
                ownerType: '',
                idCard: '',
                ownerTel: '',
                ownerAddress: '',
                ramark: '',
                govCommunityAreas: [],
                govRooms:[]

            }
        },
        _initMethod: function () {
            $that._listEditGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('editGovOwner', 'openEditGovOwnerModal', function (_params) {
                vc.component.refreshEditGovOwnerInfo();
                $('#editGovOwnerModel').modal('show');
                vc.copyObject(_params, vc.component.editGovOwnerInfo);
                $that._listEditGovRooms(vc.component.editGovOwnerInfo.caId);
            });
        },
        methods: {
            editGovOwnerValidate: function () {
                return vc.validate.validate({
                    editGovOwnerInfo: vc.component.editGovOwnerInfo
                }, {
                    'editGovOwnerInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovOwnerInfo.roomId': [
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "房屋编号id超长"
                        },
                    ],
                    'editGovOwnerInfo.roomNum': [
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "所属房屋超长"
                        },
                    ],
                    'editGovOwnerInfo.ownerNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "户号超长"
                        },
                    ],
                    'editGovOwnerInfo.ownerName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户主不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "户主超长"
                        },
                    ],
                    'editGovOwnerInfo.ownerType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "6",
                            errInfo: "户别超长"
                        },
                    ],
                    'editGovOwnerInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "身份证格式错误"
                        },
                    ],
                    'editGovOwnerInfo.ownerTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户主电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "户主电话太长"
                        },
                    ],
                    'editGovOwnerInfo.ownerAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户籍地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "房屋产权超长"
                        },
                    ],
                    'editGovOwnerInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovOwnerInfo.govOwnerId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户籍管理ID不能为空"
                        }]

                });
            },
            editGovOwner: function () {
                if (!vc.component.editGovOwnerValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if(!vc.component.isCardNoEdit(vc.component.addGovOwnerInfo.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.http.apiPost(
                    '/govOwner/updateGovOwner',
                    JSON.stringify(vc.component.editGovOwnerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovOwnerModel').modal('hide');
                            vc.emit('govOwnerManage', 'listGovOwner', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovOwnerInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovOwnerInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovOwnerInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            isCardNoEdit: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            _listEditGovRooms: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govRoom/queryGovRoom',
                    param,
                    function (json, res) {
                        var _govRoomManageInfo = JSON.parse(json);
                        vc.component.editGovOwnerInfo.total = _govRoomManageInfo.total;
                        vc.component.editGovOwnerInfo.records = _govRoomManageInfo.records;
                        vc.component.editGovOwnerInfo.govRooms = _govRoomManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _setEditRoomNums:function(_roomId){
                $that.editGovOwnerInfo.govRooms.forEach(item => {
                    if (item.govRoomId == _roomId) {
                        $that.editGovOwnerInfo.roomNum = item.roomNum;
                    }
                });
            },
            refreshEditGovOwnerInfo: function () {
                let _govCommunityAreas=vc.component.editGovOwnerInfo.govCommunityAreas;
                let _govRooms=vc.component.editGovOwnerInfo.govRooms;
                vc.component.editGovOwnerInfo = {
                    govOwnerId: '',
                    caId: '',
                    roomId: '',
                    roomNum: '',
                    ownerNum: '',
                    ownerName: '',
                    ownerType: '',
                    idCard: '',
                    ownerTel: '',
                    ownerAddress: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas,
                    govRooms: _govRooms
                }
            }
        }
    });

})(window.vc, window.vc.component);
