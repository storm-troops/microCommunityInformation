(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovSymptomTypeInfo: {
                symptomId: '',
                caId: '',
                hospitalId: '',
                name: '',
                seq: '',
                remark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovSymptomType', 'openAddGovSymptomTypeModal', function () {
                $('#addGovSymptomTypeModel').modal('show');
            });
        },
        methods: {
            addGovSymptomTypeValidate() {
                return vc.validate.validate({
                    addGovSymptomTypeInfo: vc.component.addGovSymptomTypeInfo
                }, {
                    'addGovSymptomTypeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovSymptomTypeInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "症状名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "症状名称不能超过64"
                        },
                    ],
                    'addGovSymptomTypeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ]
                });
            },
            saveGovSymptomTypeInfo: function () {
                vc.component.addGovSymptomTypeInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovSymptomTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovSymptomTypeInfo);
                    $('#addGovSymptomTypeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govSymptomType/saveGovSymptomType',
                    JSON.stringify(vc.component.addGovSymptomTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovSymptomTypeModel').modal('hide');
                            vc.component.clearAddGovSymptomTypeInfo();
                            vc.emit('govSymptomTypeManage', 'listGovSymptomType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovSymptomTypeInfo: function () {
                vc.component.addGovSymptomTypeInfo = {
                    symptomId: '',
                    caId: '',
                    hospitalId: '',
                    name: '',
                    seq: '',
                    remark: '',

                };
            }
        }
    });

})(window.vc);
