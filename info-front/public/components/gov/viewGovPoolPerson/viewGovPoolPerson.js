/**
    人口管理 组件
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            viewGovPoolPersonInfo: {
                govPersons: [],
                politicalOutlooks: [],
                govOwners: [],
                govRooms: [],
                govHelpPolicyLists: []
            }
        },
        _initMethod: function () {
            let _govPersonId = vc.getParam("govPersonId");
            let _govOwnerId = vc.getParam("govOwnerId");
            let _govHelpListId = vc.getParam("govHelpListId");
            //根据请求参数查询 查询人口
            vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS, _govPersonId);
            //根据请求参数查询 户籍
            vc.component._listGovOwners(DEFAULT_PAGE, DEFAULT_ROWS, _govOwnerId);
            //根据请求参数查询 房屋信息
            vc.component._listGovRooms(DEFAULT_PAGE, DEFAULT_ROWS, _govOwnerId);
            //根据请求参数查询 帮扶记录
            vc.component._listGovHelpPolicyLists(DEFAULT_PAGE, DEFAULT_ROWS, _govHelpListId);

            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.viewGovPoolPersonInfo.politicalOutlooks = _data;
            });
        },
        _initEvent: function () {
            vc.on('viewGovPoolPersonInfo', 'chooseGovPerson', function (_app) {
                //vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovPoolPersonInfo);
            });

            vc.on('viewGovPoolPersonInfo', 'onIndex', function (_index) {
                vc.component.viewGovPoolPersonInfo.index = _index;
            });

        },
        methods: {
            _listGovPersons: function (_page, _rows, _govPersonId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        govPersonId: _govPersonId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPerson/queryGovPerson',
                    param,
                    function (json, res) {
                        var _govPersonManageInfo = JSON.parse(json);
                        vc.component.viewGovPoolPersonInfo.govPersons = _govPersonManageInfo.data;
                        //vc.copyObject(_govPersonManageInfo.data[0], vc.component.viewGovPoolPersonInfo);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovOwners: function (_page, _rows,_govOwnerId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        govOwnerId: _govOwnerId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govOwner/queryGovOwner',
                    param,
                    function (json, res) {
                        var _govOwnerManageInfo = JSON.parse(json);
                        vc.component.viewGovPoolPersonInfo.govOwners = _govOwnerManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovRooms: function (_page, _rows,_govOwnerId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        ownerId: _govOwnerId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govRoom/queryGovRoom',
                    param,
                    function (json, res) {
                        var _govRoomManageInfo = JSON.parse(json);
                        vc.component.viewGovPoolPersonInfo.govRooms = _govRoomManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovHelpPolicyLists: function (_page, _rows,_govHelpListId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        govHelpListId: _govHelpListId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHelpPolicyList/queryGovHelpPolicyList',
                    param,
                    function (json, res) {
                        var _govHelpPolicyListManageInfo = JSON.parse(json);
                        vc.component.viewGovPoolPersonInfo.govHelpPolicyLists = _govHelpPolicyListManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSpecName: function (_politicalOutlook) {
                let _retutest = '';
                $that.viewGovPoolPersonInfo.politicalOutlooks.forEach(_item => {
                    if (_item.statusCd == _politicalOutlook) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _goBack: function () {
                vc.goBack();
            },
        }
    });

})(window.vc);
