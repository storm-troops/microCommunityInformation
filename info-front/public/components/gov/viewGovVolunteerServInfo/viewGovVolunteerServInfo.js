/**
    服务领域 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovVolunteerServInfo:{
                index:0,
                flowComponent:'viewGovVolunteerServInfo',
                servId:'',
volunteerId:'',
caId:'',
name:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovVolunteerServInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovVolunteerServInfo','chooseGovVolunteerServ',function(_app){
                vc.copyObject(_app, vc.component.viewGovVolunteerServInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovVolunteerServInfo);
            });

            vc.on('viewGovVolunteerServInfo', 'onIndex', function(_index){
                vc.component.viewGovVolunteerServInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovVolunteerServInfoModel(){
                vc.emit('chooseGovVolunteerServ','openChooseGovVolunteerServModel',{});
            },
            _openAddGovVolunteerServInfoModel(){
                vc.emit('addGovVolunteerServ','openAddGovVolunteerServModal',{});
            },
            _loadGovVolunteerServInfoData:function(){

            }
        }
    });

})(window.vc);
