(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovWorkTypeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovWorkType', 'openDeleteGovWorkTypeModal', function (_params) {

                vc.component.deleteGovWorkTypeInfo = _params;
                $('#deleteGovWorkTypeModel').modal('show');

            });
        },
        methods: {
            deleteGovWorkType: function () {
                vc.http.apiPost(
                    '/govWorkType/deleteGovWorkType',
                    JSON.stringify(vc.component.deleteGovWorkTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovWorkTypeModel').modal('hide');
                            vc.emit('govWorkTypeManage', 'listGovWorkType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovWorkTypeModel: function () {
                $('#deleteGovWorkTypeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
