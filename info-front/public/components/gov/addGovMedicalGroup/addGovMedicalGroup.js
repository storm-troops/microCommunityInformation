(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMedicalGroupInfo: {
                groupId: '',
                caId: '',
                hospitalId: '',
                hospitalName: '',
                name: '',
                seq: '',
                ramark: '',
                

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovMedicalGroup', 'openAddGovMedicalGroupModal', function () {
                $('#addGovMedicalGroupModel').modal('show');
            });
            vc.on('viewGovCompanyInfo', 'chooseGovCompany', function (_param) {
                $that.addGovMedicalGroupInfo.hospitalId = _param.govCompanyId;
                $that.addGovMedicalGroupInfo.hospitalName = _param.companyName;
            });
        },
        methods: {
            addGovMedicalGroupValidate() {
                return vc.validate.validate({
                    addGovMedicalGroupInfo: vc.component.addGovMedicalGroupInfo
                }, {
                    'addGovMedicalGroupInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovMedicalGroupInfo.hospitalId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "医院ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "医院ID不能超过30"
                        },
                    ],
                    'addGovMedicalGroupInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "团队名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "团队名称不能超过64"
                        },
                    ],
                    'addGovMedicalGroupInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ],
                    'addGovMedicalGroupInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],

                });
            },
            saveGovMedicalGroupInfo: function () {
                vc.component.addGovMedicalGroupInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovMedicalGroupValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMedicalGroupInfo);
                    $('#addGovMedicalGroupModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMedicalGroup/saveGovMedicalGroup',
                    JSON.stringify(vc.component.addGovMedicalGroupInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMedicalGroupModel').modal('hide');
                            vc.component.clearAddGovMedicalGroupInfo();
                            vc.emit('govMedicalGroupManage', 'listGovMedicalGroup', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChooseGovCompanyModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovCompany', 'openChooseGovCompanyModel', _caId);
            },
            _closeAddGovMedicalGroup: function () {
                $that.clearAddGovMedicalGroupInfo();
                vc.emit('govMedicalGroupManage', 'listGovMedicalGroup', {});
            },
            clearAddGovMedicalGroupInfo: function () {
                vc.component.addGovMedicalGroupInfo = {
                    caId: '',
                    hospitalId: '',
                    hospitalName: '',
                    name: '',
                    seq: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
