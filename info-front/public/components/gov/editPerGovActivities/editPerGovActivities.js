(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            editPerGovActivitiesInfo: {
                perActivitiesId: '',
                title: '',
                typeCd: '',
                headerImg: '',
                caId: '',
                context: '',
                userId: '',
                userName: '',
                startTime: '',
                endTime: '',
                activityTime: '',
                state: '',
                oldPersons:[],
                statusCd: '',
            }
        },
        _initMethod: function () {
            $that._initSummernoteInfo();
        },
        _initEvent: function () {
            vc.on('editPerGovActivities', 'openeditPerGovActivitiesModal', function (_params) {
                $('#editPerGovActivitiesModel').modal('show');
                vc.copyObject(_params, vc.component.editPerGovActivitiesInfo);
                console.log(_params);
            });
            
            vc.on('viewGovOldBirthPerson', 'page_event', function (_param) {
                $that.editPerGovActivitiesInfo.oldPersons = _param;
            });
        },
        methods: {
            editPerGovActivitiesValidate() {
                return vc.validate.validate({
                    editPerGovActivitiesInfo: vc.component.editPerGovActivitiesInfo
                }, {
                    'editPerGovActivitiesInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "记录标题不能超过200"
                        },
                    ],
                    'editPerGovActivitiesInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "记录内容不能超过长文本"
                        },
                    ],

                    'editPerGovActivitiesInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'editPerGovActivitiesInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结束时间不能超过时间类型"
                        },
                    ],
                    'editPerGovActivitiesInfo.activityTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "活动时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "活动时间不能超过时间类型"
                        },
                    ]
                });
            },
            editPerGovActivities: function () {
                if (!vc.component.editPerGovActivitiesValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.editPerGovActivitiesInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.editPerGovActivitiesInfo);
                    $('#editPerGovActivitiesModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/perGovActivities/savePerGovActivities',
                    JSON.stringify(vc.component.editPerGovActivitiesInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editPerGovActivitiesModel').modal('hide');
                            vc.component.cleareditPerGovActivitiesInfo();
                            vc.emit('perGovActivitiesManage', 'listPerGovActivities', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(errInfo);
                    });
            },
            _initSummernoteInfo: function () {
                vc.initDateTime('editStartTime', function (_value) {
                    $that.editPerGovActivitiesInfo.startTime = _value;
                });
                vc.initDateTime('editEndTime', function (_value) {
                    $that.editPerGovActivitiesInfo.endTime = _value;
                });
                vc.initDateTime('editActivityTime', function (_value) {
                    $that.editPerGovActivitiesInfo.activityTime = _value;
                });

                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入公告内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendeditFile($summernote,files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.editPerGovActivitiesInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendeditFile: function ($summernote,files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _cleareditPerGovActivitiesInfo: function () {
                $that.cleareditPerGovActivitiesInfo();
                vc.emit('perGovActivitiesManage', 'listPerGovActivities', {});
            },
            cleareditPerGovActivitiesInfo: function () {
                vc.component.editPerGovActivitiesInfo = {
                    perActivitiesId: '',
                    title: '',
                    typeCd: '',
                    headerImg: '',
                    caId: '',
                    context: '',
                    userId: '',
                    userName: '',
                    startTime: '',
                    endTime: '',
                    activityTime: '',
                    state: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
