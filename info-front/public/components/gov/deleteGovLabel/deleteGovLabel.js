(function(vc,vm){

    vc.extends({
        data:{
            deleteGovLabelInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovLabel','openDeleteGovLabelModal',function(_params){

                vc.component.deleteGovLabelInfo = _params;
                $('#deleteGovLabelModel').modal('show');

            });
        },
        methods:{
            deleteGovLabel:function(){
                vc.component.deleteGovLabelInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govLabel/deleteGovLabel',
                    JSON.stringify(vc.component.deleteGovLabelInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovLabelModel').modal('hide');
                            vc.emit('govLabelManage','listGovLabel',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovLabelModel:function(){
                $('#deleteGovLabelModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
