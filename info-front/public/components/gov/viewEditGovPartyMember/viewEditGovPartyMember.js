(function (vc) {
    vc.extends({
        data: {
            viewEditGovPartyMemberInfo: {
                govMembers: [],
                orgId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('viewEditGovPartyMember', 'openviewEditGovPartyMemberModel', function (_param) {
                vc.component._refreshviewEditGovPartyMemberInfo();
                console.log(_param);
                $that.viewEditGovPartyMemberInfo.govMembers = _param;
                //vc.component._loadAllGovRoomInfo(1, 10, '');
            });
            vc.on('viewEditGovPartyMember', 'selectPartyOrgs', function (_param) {
                console.log(_param);
                $that.viewEditGovPartyMemberInfo.orgId = _param;
            });
            vc.on('meetingListChooseGovPartyMemberOrg', 'chooseGovPartyMemberOrg', function (_param) {
                $that.viewEditGovPartyMemberInfo.govMembers.push(_param);
                vc.emit('viewEditGovPartyMember', 'page_event', $that.viewEditGovPartyMemberInfo.govMembers);
            });
            
        },
        methods: {
            _loadAllPartyMemberInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingMemberRel/queryGovMeetingMemberRel',
                    param,
                    function (json) {
                        var _govRoomInfo = JSON.parse(json);
                        vc.component.viewEditGovPartyMemberInfo.govRooms = _govRoomInfo.govRooms;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChooseGovPartyMemberOrg: function () {
                let _caId = vc.getCurrentCommunity().caId;
                //let _orgId = $that.viewEditGovPartyMemberInfo.orgId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                
                var param ={
                    caId: _caId
                }
                vc.emit('chooseGovPartyMemberOrg', 'openchooseGovPartyMemberOrgModel', param);
            },
            queryvPartyMember: function () {
                vc.component._loadAllPartyMemberInfo(1, 10, vc.component.viewEditGovPartyMemberInfo._currentGovRoomName);
            },
            _openDeleteEditGovPartyMemberOrg: function (_govMember) {

                let _tmpGovMembers = [];
                $that.viewEditGovPartyMemberInfo.govMembers.forEach(item => {
                    if (item.govMemberId != _govMember.govMemberId) {
                        _tmpGovMembers.push(item);
                    }
                });
                $that.viewEditGovPartyMemberInfo.govMembers = _tmpGovMembers;
                vc.emit('viewEditGovPartyMember', 'page_event', $that.viewEditGovPartyMemberInfo.govMembers);
            },
            _refreshviewEditGovPartyMemberInfo: function () {
                vc.component.viewEditGovPartyMemberInfo._currentGovRoomName = "";
                $that.viewEditGovPartyMemberInfo.govMembers = [];
            }
        }

    });
})(window.vc);
