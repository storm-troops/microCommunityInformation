/**
    症状类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovSymptomTypeInfo:{
                index:0,
                flowComponent:'viewGovSymptomTypeInfo',
                symptomId:'',
caId:'',
hospitalId:'',
name:'',
seq:'',
remark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovSymptomTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovSymptomTypeInfo','chooseGovSymptomType',function(_app){
                vc.copyObject(_app, vc.component.viewGovSymptomTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovSymptomTypeInfo);
            });

            vc.on('viewGovSymptomTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovSymptomTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovSymptomTypeInfoModel(){
                vc.emit('chooseGovSymptomType','openChooseGovSymptomTypeModel',{});
            },
            _openAddGovSymptomTypeInfoModel(){
                vc.emit('addGovSymptomType','openAddGovSymptomTypeModal',{});
            },
            _loadGovSymptomTypeInfoData:function(){

            }
        }
    });

})(window.vc);
