(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovHealthTerm:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovHealthTermInfo:{
                govHealthTerms:[],
                _currentGovHealthTermName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovHealthTerm','openChooseGovHealthTermModel',function(_param){
                $('#chooseGovHealthTermModel').modal('show');
                vc.component._refreshChooseGovHealthTermInfo();
                vc.component._loadAllGovHealthTermInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovHealthTermInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govHealthTerm/queryGovHealthTerm',
                             param,
                             function(json){
                                var _govHealthTermInfo = JSON.parse(json);
                                vc.component.chooseGovHealthTermInfo.govHealthTerms = _govHealthTermInfo.govHealthTerms;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovHealthTerm:function(_govHealthTerm){
                if(_govHealthTerm.hasOwnProperty('name')){
                     _govHealthTerm.govHealthTermName = _govHealthTerm.name;
                }
                vc.emit($props.emitChooseGovHealthTerm,'chooseGovHealthTerm',_govHealthTerm);
                vc.emit($props.emitLoadData,'listGovHealthTermData',{
                    govHealthTermId:_govHealthTerm.govHealthTermId
                });
                $('#chooseGovHealthTermModel').modal('hide');
            },
            queryGovHealthTerms:function(){
                vc.component._loadAllGovHealthTermInfo(1,10,vc.component.chooseGovHealthTermInfo._currentGovHealthTermName);
            },
            _refreshChooseGovHealthTermInfo:function(){
                vc.component.chooseGovHealthTermInfo._currentGovHealthTermName = "";
            }
        }

    });
})(window.vc);
