(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovCase: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovCaseInfo: {
                govCases: [],
                _currentGovCaseName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovCase', 'openChooseGovCaseModel', function (_param) {
                $('#chooseGovCaseModel').modal('show');
                vc.component._refreshChooseGovCaseInfo();
                vc.component._loadAllGovCaseInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovCaseInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCase/queryGovCase',
                    param,
                    function (json) {
                        var _govCaseInfo = JSON.parse(json);
                        vc.component.chooseGovCaseInfo.govCases = _govCaseInfo.govCases;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovCase: function (_govCase) {
                if (_govCase.hasOwnProperty('name')) {
                    _govCase.govCaseName = _govCase.name;
                }
                vc.emit($props.emitChooseGovCase, 'chooseGovCase', _govCase);
                vc.emit($props.emitLoadData, 'listGovCaseData', {
                    govCaseId: _govCase.govCaseId
                });
                $('#chooseGovCaseModel').modal('hide');
            },
            queryGovCases: function () {
                vc.component._loadAllGovCaseInfo(1, 10, vc.component.chooseGovCaseInfo._currentGovCaseName);
            },
            _refreshChooseGovCaseInfo: function () {
                vc.component.chooseGovCaseInfo._currentGovCaseName = "";
            }
        }

    });
})(window.vc);
