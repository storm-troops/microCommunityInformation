(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovOldPersonInfo: {
                oldId: '',
                caId: '',
                govCommunityId: '',
                personName: '',
                personTel: '',
                personAge: '',
                contactPerson: '',
                contactTel: '',
                servPerson: '',
                servTel: '',
                ramark: '',
                idCard: '',
                birthday: '',
                personSex: '',
                nation: '',
                maritalStatus: 'Y',
                timeAmount: '0',
                typeId: '',
                govCommunitys: [],
                govOldPersonTypes: [],
                nationData : [
                    {id:1 ,name:'汉族'},
                    {id:2 ,name:'蒙古族'},
                    {id:3 ,name:'回族'},
                    {id:4 ,name:'藏族'},
                    {id:5 ,name:'维吾尔族'},
                    {id:6 ,name:'苗族'},
                    {id:7 ,name:'彝族'},
                    {id:8 ,name:'壮族'},
                    {id:9 ,name:'布依族'},
                    {id:10,name:'朝鲜族'},
                    {id:11,name:'满族'},
                    {id:12,name:'侗族'},
                    {id:13,name:'瑶族'},
                    {id:14,name:'白族'},
                    {id:15,name:'土家族'},
                    {id:16,name:'哈尼族'},
                    {id:17,name:'哈萨克族'},
                    {id:18,name:'傣族'},
                    {id:19,name:'黎族'},
                    {id:20,name:'傈僳族'},
                    {id:21,name:'佤族'},
                    {id:22,name:'畲族'},
                    {id:23,name:'高山族'},
                    {id:24,name:'拉祜族'},
                    {id:25,name:'水族'},
                    {id:26,name:'东乡族'},
                    {id:27,name:'纳西族'},
                    {id:28,name:'景颇族'},
                    {id:29,name:'柯尔克孜族'},
                    {id:30,name:'土族'},
                    {id:31,name:'达翰尔族'},
                    {id:32,name:'么佬族'},
                    {id:33,name:'羌族'},
                    {id:34,name:'布朗族'},
                    {id:35,name:'撒拉族'},
                    {id:36,name:'毛南族'},
                    {id:37,name:'仡佬族'},
                    {id:38,name:'锡伯族'},
                    {id:39,name:'阿昌族'},
                    {id:40,name:'普米族'},
                    {id:41,name:'塔吉克族'},
                    {id:42,name:'怒族'},
                    {id:43,name:'乌孜别克族'},
                    {id:44,name:'俄罗斯族'},
                    {id:45,name:'鄂温克族'},
                    {id:46,name:'德昂族'},
                    {id:47,name:'保安族'},
                    {id:48,name:'裕固族'},
                    {id:49,name:'京族'},
                    {id:50,name:'塔塔尔族'},
                    {id:51,name:'独龙族'},
                    {id:52,name:'鄂伦春族'},
                    {id:53,name:'赫哲族'},
                    {id:54,name:'门巴族'},
                    {id:55,name:'珞巴族'},
                    {id:56,name:'基诺族'},
            
            ]

            }
        },
        _initMethod: function () {
         
        },
        _initEvent: function () {
            vc.on('addGovOldPerson', 'openAddGovOldPersonModal', function () {
                vc.initDate('addBirthday', function (_value) {
                    $that.addGovOldPersonInfo.birthday = _value;
                });
                $that._addGovCommunitys(vc.getCurrentCommunity().caId);
                $that._addGovOldPersonTypes();
                vc.component.addGovOldPersonInfo.caId = vc.getCurrentCommunity().caId;
                $('#addGovOldPersonModel').modal('show');
            });
        },
        methods: {
            addGovOldPersonValidate() {
                return vc.validate.validate({
                    addGovOldPersonInfo: vc.component.addGovOldPersonInfo
                }, {
                    'addGovOldPersonInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区不能超过30"
                        },
                    ],
                    'addGovOldPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'addGovOldPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话不能超过11"
                        },
                    ],
                    'addGovOldPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证号码不能为空"
                        }
                    ],
                    'addGovOldPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'addGovOldPersonInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        },
                        {
                            limit: "date",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'addGovOldPersonInfo.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'addGovOldPersonInfo.personAge': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "年龄不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "年龄不能超过11"
                        },
                    ],
                    'addGovOldPersonInfo.contactPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "家人联系人不能超过64"
                        },
                    ],
                    'addGovOldPersonInfo.contactTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "家人联系电话不能超过11"
                        },
                    ],
                    'addGovOldPersonInfo.servPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "服务人不能超过64"
                        },
                    ],
                    'addGovOldPersonInfo.servTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "服务人电话不能超过11"
                        },
                    ]
                    ,
                    'addGovOldPersonInfo.timeAmount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "初始时间不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "初始时间格式错误"
                        },
                    ]



                });
            },
            saveGovOldPersonInfo: function () {
                if (!vc.component.addGovOldPersonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                if(!vc.component.isCardNoAdd(vc.component.addGovOldPersonInfo.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovOldPersonInfo);
                    $('#addGovOldPersonModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govOldPerson/saveGovOldPerson',
                    JSON.stringify(vc.component.addGovOldPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovOldPersonModel').modal('hide');
                            vc.component.clearAddGovOldPersonInfo();
                            vc.emit('govOldPersonManage', 'listGovOldPerson', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _addGovOldPersonTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                    param,
                    function (json, res) {
                        var _govOldPersonTypeManageInfo = JSON.parse(json);
                        vc.component.addGovOldPersonInfo.total = _govOldPersonTypeManageInfo.total;
                        vc.component.addGovOldPersonInfo.records = _govOldPersonTypeManageInfo.records;
                        vc.component.addGovOldPersonInfo.govOldPersonTypes = _govOldPersonTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            isCardNoAdd: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            _addGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovOldPersonInfo.total = _govCommunityManageInfo.total;
                        vc.component.addGovOldPersonInfo.records = _govCommunityManageInfo.records;
                        vc.component.addGovOldPersonInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovOldPersonInfo: function () {
                vc.component.addGovOldPersonInfo = {
                    oldId: '',
                    caId: '',
                    govCommunityId: '',
                    personName: '',
                    personTel: '',
                    personAge: '',
                    birthday: '',
                    contactPerson: '',
                    contactTel: '',
                    servPerson: '',
                    servTel: '',
                    ramark: '',
                    idCard: '',
                    personSex: '',
                    nation: '',
                    maritalStatus: 'Y',
                    timeAmount: '0',
                    typeId: '',
                    govCommunitys: [],
                    govOldPersonTypes: [],
                    nationData : [
                        {id:1 ,name:'汉族'},
                        {id:2 ,name:'蒙古族'},
                        {id:3 ,name:'回族'},
                        {id:4 ,name:'藏族'},
                        {id:5 ,name:'维吾尔族'},
                        {id:6 ,name:'苗族'},
                        {id:7 ,name:'彝族'},
                        {id:8 ,name:'壮族'},
                        {id:9 ,name:'布依族'},
                        {id:10,name:'朝鲜族'},
                        {id:11,name:'满族'},
                        {id:12,name:'侗族'},
                        {id:13,name:'瑶族'},
                        {id:14,name:'白族'},
                        {id:15,name:'土家族'},
                        {id:16,name:'哈尼族'},
                        {id:17,name:'哈萨克族'},
                        {id:18,name:'傣族'},
                        {id:19,name:'黎族'},
                        {id:20,name:'傈僳族'},
                        {id:21,name:'佤族'},
                        {id:22,name:'畲族'},
                        {id:23,name:'高山族'},
                        {id:24,name:'拉祜族'},
                        {id:25,name:'水族'},
                        {id:26,name:'东乡族'},
                        {id:27,name:'纳西族'},
                        {id:28,name:'景颇族'},
                        {id:29,name:'柯尔克孜族'},
                        {id:30,name:'土族'},
                        {id:31,name:'达翰尔族'},
                        {id:32,name:'么佬族'},
                        {id:33,name:'羌族'},
                        {id:34,name:'布朗族'},
                        {id:35,name:'撒拉族'},
                        {id:36,name:'毛南族'},
                        {id:37,name:'仡佬族'},
                        {id:38,name:'锡伯族'},
                        {id:39,name:'阿昌族'},
                        {id:40,name:'普米族'},
                        {id:41,name:'塔吉克族'},
                        {id:42,name:'怒族'},
                        {id:43,name:'乌孜别克族'},
                        {id:44,name:'俄罗斯族'},
                        {id:45,name:'鄂温克族'},
                        {id:46,name:'德昂族'},
                        {id:47,name:'保安族'},
                        {id:48,name:'裕固族'},
                        {id:49,name:'京族'},
                        {id:50,name:'塔塔尔族'},
                        {id:51,name:'独龙族'},
                        {id:52,name:'鄂伦春族'},
                        {id:53,name:'赫哲族'},
                        {id:54,name:'门巴族'},
                        {id:55,name:'珞巴族'},
                        {id:56,name:'基诺族'},
                
                ]

                };
            }
        }
    });

})(window.vc);
