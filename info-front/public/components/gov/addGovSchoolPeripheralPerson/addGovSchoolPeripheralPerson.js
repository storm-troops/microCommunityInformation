(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovSchoolPeripheralPersonInfo: {
                schPersonId: '',
                schoolId: '',
                govPersonId: '',
                personName: '',
                schoolName: '',
                caId: '',
                remark: '',
                extentInjury: '',
                isAttention: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovSchoolPeripheralPerson', 'openAddGovSchoolPeripheralPersonModal', function () {
                $('#addGovSchoolPeripheralPersonModel').modal('show');
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                console.log(_param);
                $that.addGovSchoolPeripheralPersonInfo.govPersonId = _param.govPersonId;
                $that.addGovSchoolPeripheralPersonInfo.personName = _param.personName;
            });
        },
        methods: {
            addGovSchoolPeripheralPersonValidate() {
                return vc.validate.validate({
                    addGovSchoolPeripheralPersonInfo: vc.component.addGovSchoolPeripheralPersonInfo
                }, {
                    'addGovSchoolPeripheralPersonInfo.schoolId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "学校ID不能超过30"
                        },
                    ],
                    'addGovSchoolPeripheralPersonInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口编号不能超过30"
                        },
                    ],
                    'addGovSchoolPeripheralPersonInfo.schoolName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "60",
                            errInfo: "学校名称不能超过60"
                        },
                    ],
                    'addGovSchoolPeripheralPersonInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovSchoolPeripheralPersonInfo.extentInjury': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "危害程度不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "危害程度不能超过3"
                        },
                    ],
                    'addGovSchoolPeripheralPersonInfo.isAttention': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否关注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "是否关注不能超过3"
                        },
                    ]
                });
            },
            saveGovSchoolPeripheralPersonInfo: function () {
                vc.component.addGovSchoolPeripheralPersonInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovSchoolPeripheralPersonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovSchoolPeripheralPersonInfo);
                    $('#addGovSchoolPeripheralPersonModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govSchoolPeripheralPerson/saveGovSchoolPeripheralPerson',
                    JSON.stringify(vc.component.addGovSchoolPeripheralPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovSchoolPeripheralPersonModel').modal('hide');
                            vc.component.clearAddGovSchoolPeripheralPersonInfo();
                            vc.emit('govSchoolPeripheralPersonManage', 'listGovSchoolPeripheralPerson', {});
                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(errInfo);
                    });
            },
            _setAddSchool : function(_schoolId){
                $that.govSchoolPeripheralPersonManageInfo.govSchools.forEach(item => {
                    if (item.schoolId == _schoolId) {
                        $that.addGovSchoolPeripheralPersonInfo.schoolName = item.schoolName;
                    }
                });
            },
            _openAddChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeAddGovSchoolPeripheralPerson: function () {
                $that.clearAddGovSchoolPeripheralPersonInfo();
                vc.emit('govSchoolPeripheralPersonManage', 'listGovSchoolPeripheralPerson', {});
            },
            clearAddGovSchoolPeripheralPersonInfo: function () {
                vc.component.addGovSchoolPeripheralPersonInfo = {
                    schPersonId: '',
                    schoolId: '',
                    govPersonId: '',
                    schoolName: '',
                    personName: '',
                    caId: '',
                    remark: '',
                    extentInjury: '',
                    isAttention: '',

                };
            }
        }
    });

})(window.vc);
