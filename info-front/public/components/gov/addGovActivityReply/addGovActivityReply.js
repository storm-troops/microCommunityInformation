(function(vc){

    vc.extends({
        propTypes: {
               callBackListener:vc.propTypes.string, //父组件名称
               callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            addGovActivityReplyInfo:{
                replyId:'',
actId:'',
parentReplyId:'',
context:'',
userId:'',
userName:'',
preUserName:'',
sessionId:'',
preAuthorId:'',
statusCd:'',

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
            vc.on('addGovActivityReply','openAddGovActivityReplyModal',function(){
                $('#addGovActivityReplyModel').modal('show');
            });
        },
        methods:{
            addGovActivityReplyValidate(){
                return vc.validate.validate({
                    addGovActivityReplyInfo:vc.component.addGovActivityReplyInfo
                },{
                    'addGovActivityReplyInfo.replyId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"回复ID不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"回复ID不能超过30"
                        },
                    ],
'addGovActivityReplyInfo.actId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动ID不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"活动ID不能超过30"
                        },
                    ],
'addGovActivityReplyInfo.parentReplyId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"上级回复ID不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"上级回复ID不能超过30"
                        },
                    ],
'addGovActivityReplyInfo.context':[
{
                            limit:"required",
                            param:"",
                            errInfo:"回复内容不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"长文本",
                            errInfo:"回复内容不能超过长文本"
                        },
                    ],
'addGovActivityReplyInfo.userId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"作者不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"作者不能超过30"
                        },
                    ],
'addGovActivityReplyInfo.userName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"作者不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"作者不能超过64"
                        },
                    ],
'addGovActivityReplyInfo.preUserName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"上一级作者不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"上一级作者不能超过64"
                        },
                    ],
'addGovActivityReplyInfo.sessionId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"一次会话ID不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"一次会话ID不能超过30"
                        },
                    ],
'addGovActivityReplyInfo.preAuthorId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"回复作者ID不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"回复作者ID不能超过30"
                        },
                    ],
'addGovActivityReplyInfo.statusCd':[
{
                            limit:"required",
                            param:"",
                            errInfo:"数据状态不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"2",
                            errInfo:"数据状态不能超过2"
                        },
                    ],




                });
            },
            saveGovActivityReplyInfo:function(){
                if(!vc.component.addGovActivityReplyValidate()){
                    vc.toast(vc.validate.errInfo);

                    return ;
                }

                vc.component.addGovActivityReplyInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if(vc.notNull($props.callBackListener)){
                    vc.emit($props.callBackListener,$props.callBackFunction,vc.component.addGovActivityReplyInfo);
                    $('#addGovActivityReplyModel').modal('hide');
                    return ;
                }
                vc.http.apiPost(
                    '/govActivityReply/saveGovActivityReply',
                    JSON.stringify(vc.component.addGovActivityReplyInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovActivityReplyModel').modal('hide');
                            vc.component.clearAddGovActivityReplyInfo();
                            vc.emit('govActivityReplyManage','listGovActivityReply',{});

                            return ;
                        }
                        vc.message(_json.msg);

                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);

                     });
            },
            clearAddGovActivityReplyInfo:function(){
                vc.component.addGovActivityReplyInfo = {
                                            replyId:'',
actId:'',
parentReplyId:'',
context:'',
userId:'',
userName:'',
preUserName:'',
sessionId:'',
preAuthorId:'',
statusCd:'',

                                        };
            }
        }
    });

})(window.vc);
