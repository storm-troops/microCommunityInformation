(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCaseInfo: {
                caseId: '',
                govVictimId: '',
                victimName: '',
                caseName: '',
                caId: '',
                caseNum: '',
                happenTime: '',
                govSuspectId: '',
                suspectName: '',
                settleTime: '',
                briefCondition: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovCase', 'openAddGovCaseModal', function () {
                $('#addGovCaseModel').modal('show');
            });
        },
        methods: {
            addGovCaseValidate() {
                return vc.validate.validate({
                    addGovCaseInfo: vc.component.addGovCaseInfo
                }, {
                    'addGovCaseInfo.caseId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "案件ID不能超过30"
                        },
                    ],
                    'addGovCaseInfo.govVictimId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "受害人ID不能超过30"
                        },
                    ],
                    'addGovCaseInfo.victimName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "受害人名称不能超过64"
                        },
                    ],
                    'addGovCaseInfo.caseName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "案件名称不能超过64"
                        },
                    ],
                    'addGovCaseInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovCaseInfo.caseNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "32",
                            errInfo: "案件编号不能超过32"
                        },
                    ],
                    'addGovCaseInfo.happenTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生日期不能超过时间类型"
                        },
                    ],
                    'addGovCaseInfo.govSuspectId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "嫌疑人ID不能超过30"
                        },
                    ],
                    'addGovCaseInfo.suspectName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "嫌疑人名称不能超过64"
                        },
                    ],
                    'addGovCaseInfo.settleTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结案日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结案日期不能超过时间类型"
                        },
                    ],
                    'addGovCaseInfo.briefCondition': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "简要情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "简要情况不能超过1024"
                        },
                    ],
                    'addGovCaseInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],
                    'addGovCaseInfo.statusCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "数据状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "数据状态不能超过2"
                        },
                    ],




                });
            },
            saveGovCaseInfo: function () {
                if (!vc.component.addGovCaseValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovCaseInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCaseInfo);
                    $('#addGovCaseModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govCase/saveGovCase',
                    JSON.stringify(vc.component.addGovCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCaseModel').modal('hide');
                            vc.component.clearAddGovCaseInfo();
                            vc.emit('govCaseManage', 'listGovCase', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovCaseInfo: function () {
                vc.component.addGovCaseInfo = {
                    caseId: '',
                    govVictimId: '',
                    victimName: '',
                    caseName: '',
                    caId: '',
                    caseNum: '',
                    happenTime: '',
                    govSuspectId: '',
                    suspectName: '',
                    settleTime: '',
                    briefCondition: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
