(function (vc) {
    vc.extends({
        data: {
            viewPartyInfo: {
                orgId: '',
                personId: '',
                govPartyMembers: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('partyInfo', 'switch', function (_param) {
                vc.component._refreshviewGovPartyInfo();
                console.log(_param);
                vc.component._loadPartyInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadPartyInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        personId: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govPartyMember/queryGovPartyMember',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewPartyInfo.govPartyMembers = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewGovPartyInfo: function () {
                $that.viewPartyInfo = {
                    orgId: '',
                    personId: '',
                    govPartyMembers: []
                }
            }
        }

    });
})(window.vc);
