(function(vc,vm){

    vc.extends({
        data:{
            deleteGovProtectionCaseInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovProtectionCase','openDeleteGovProtectionCaseModal',function(_params){

                vc.component.deleteGovProtectionCaseInfo = _params;
                $('#deleteGovProtectionCaseModel').modal('show');

            });
        },
        methods:{
            deleteGovProtectionCase:function(){
                vc.component.deleteGovProtectionCaseInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govProtectionCase/deleteGovProtectionCase',
                    JSON.stringify(vc.component.deleteGovProtectionCaseInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovProtectionCaseModel').modal('hide');
                            vc.emit('govProtectionCaseManage','listGovProtectionCase',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovProtectionCaseModel:function(){
                $('#deleteGovProtectionCaseModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
