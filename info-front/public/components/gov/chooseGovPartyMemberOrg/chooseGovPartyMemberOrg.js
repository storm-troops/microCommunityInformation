(function (vc) {
    vc.extends({
        propTypes: {
            emitchooseGovPartyMemberOrg: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovPartyMemberOrgInfo: {
                govPartyMemberOrgs: []            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovPartyMemberOrg', 'openchooseGovPartyMemberOrgModel', function (_param) {
                $('#chooseGovPartyMemberOrgModel').modal('show');
                vc.component._refreshchooseGovPartyMemberOrgInfo();
                console.log(_param);
                vc.component._loadAllGovPartyMemberOrgInfo(1, 10, _param);
            });
        },
        methods: {
            _loadAllGovPartyMemberOrgInfo: function (_page, _row, _param) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: _param.caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyMember/queryGovPartyMember',
                    param,
                    function (json) {
                        var _govPartyMemberInfo = JSON.parse(json);
                        vc.component.chooseGovPartyMemberOrgInfo.govPartyMemberOrgs = _govPartyMemberInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovPartyMemberOrg: function (_govPartyMember) {

                vc.emit($props.emitchooseGovPartyMemberOrg, 'chooseGovPartyMemberOrg', _govPartyMember);
                $('#chooseGovPartyMemberOrgModel').modal('hide');
            },
            queryGovPartyMembers: function () {
                vc.component._loadAllGovPartyMemberOrgInfo(1, 10, vc.component.chooseGovPartyMemberOrgInfo._currentGovPartyMemberName);
            },
            _refreshchooseGovPartyMemberOrgInfo: function () {
              
            }
        }

    });
})(window.vc);
