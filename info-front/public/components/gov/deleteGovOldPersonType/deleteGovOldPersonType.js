(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovOldPersonTypeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovOldPersonType', 'openDeleteGovOldPersonTypeModal', function (_params) {

                vc.component.deleteGovOldPersonTypeInfo = _params;
                $('#deleteGovOldPersonTypeModel').modal('show');

            });
        },
        methods: {
            deleteGovOldPersonType: function () {
                vc.component.deleteGovOldPersonTypeInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govOldPersonType/deleteGovOldPersonType',
                    JSON.stringify(vc.component.deleteGovOldPersonTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovOldPersonTypeModel').modal('hide');
                            vc.emit('govOldPersonTypeManage', 'listGovOldPersonType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovOldPersonTypeModel: function () {
                $('#deleteGovOldPersonTypeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
