(function(vc,vm){

    vc.extends({
        data:{
            editGovActivityPersonInfo:{
                actPerId:'',
                caId:'',
                actId:'',
                personName:'',
                personLink:'',
                personAge:'',
                personAddress:'',
                remark:'',

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('editGovActivityPerson','openEditGovActivityPersonModal',function(_params){
                vc.component.refreshEditGovActivityPersonInfo();
                $('#editGovActivityPersonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovActivityPersonInfo );
                vc.component.editGovActivityPersonInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods:{
            editGovActivityPersonValidate:function(){
                        return vc.validate.validate({
                            editGovActivityPersonInfo:vc.component.editGovActivityPersonInfo
                        },{
                            'editGovActivityPersonInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'editGovActivityPersonInfo.actId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"活动名称超长"
                        },
                    ],
'editGovActivityPersonInfo.personName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人员名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"人员名称超长"
                        },
                    ],
'editGovActivityPersonInfo.personLink':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人员电话不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"请填写人员电话"
                        },
                    ],
'editGovActivityPersonInfo.personAge':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人员年龄不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"请填写人员年龄"
                        },
                    ],
'editGovActivityPersonInfo.personAddress':[
{
                            limit:"required",
                            param:"",
                            errInfo:"住址不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"512",
                            errInfo:"住址超长"
                        },
                    ],
'editGovActivityPersonInfo.remark':[
{
                            limit:"required",
                            param:"",
                            errInfo:"备注不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"1024",
                            errInfo:"备注太长"
                        },
                    ],
'editGovActivityPersonInfo.actPerId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"报名管理ID不能为空"
                        }]

                        });
             },
            editGovActivityPerson:function(){
                if(!vc.component.editGovActivityPersonValidate()){
                    vc.toast(vc.validate.errInfo);
                    return ;
                }

                vc.http.apiPost(
                    '/govActivityPerson/updateGovActivityPerson',
                    JSON.stringify(vc.component.editGovActivityPersonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovActivityPersonModel').modal('hide');
                             vc.emit('govActivityPersonManage','listGovActivityPerson',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);
                     });
            },
            refreshEditGovActivityPersonInfo:function(){
                vc.component.editGovActivityPersonInfo= {
                  actPerId:'',
caId:'',
actId:'',
personName:'',
personLink:'',
personAge:'',
personAddress:'',
remark:'',

                }
            }
        }
    });

})(window.vc,window.vc.component);
