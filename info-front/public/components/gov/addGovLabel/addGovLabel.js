(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovLabelInfo: {
                govLabelId: '',
                caId: '',
                labelName: '',
                labelCd: '',
                labelType: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovLabel', 'openAddGovLabelModal', function () {
                $('#addGovLabelModel').modal('show');
            });
        },
        methods: {
            addGovLabelValidate() {
                return vc.validate.validate({
                    addGovLabelInfo: vc.component.addGovLabelInfo
                }, {
                    'addGovLabelInfo.labelName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "标签名称不能超过30"
                        },
                    ],
                    'addGovLabelInfo.labelCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "标签不能超过30"
                        },
                    ],
                    'addGovLabelInfo.labelType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "标签类型不能超过12"
                        },
                    ],
                });
            },
            saveGovLabelInfo: function () {
                if (!vc.component.addGovLabelValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.addGovLabelInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovLabelInfo);
                    $('#addGovLabelModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govLabel/saveGovLabel',
                    JSON.stringify(vc.component.addGovLabelInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovLabelModel').modal('hide');
                            vc.component.clearAddGovLabelInfo();
                            vc.emit('govLabelManage', 'listGovLabel', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovLabelInfo: function () {
                vc.component.addGovLabelInfo = {
                    govLabelId: '',
                    caId: '',
                    labelName: '',
                    labelCd: '',
                    labelType: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
