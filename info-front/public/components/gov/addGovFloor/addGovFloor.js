(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovFloorInfo: {
                govFloorId: '',
                caId: '',
                govCommunityId: '',
                floorNum: '',
                floorName: '',
                floorType: '',
                floorArea: '',
                layerCount: '',
                unitCount: '',
                floorUse: '',
                personName: '',
                personLink: '',
                floorIcon: '',
                ramark: '',
                govCommunityAreas: [],
                govCommunitys: [] ,
                govBuildingTypes: []

            }
        },
        _initMethod: function () {
         
        },
        _initEvent: function () {
            vc.on('addGovFloor', 'openAddGovFloorModal', function () {
                vc.component.addGovFloorInfo.caId = vc.getCurrentCommunity().caId;
                $that._listAddFloorGovCommunitys(vc.component.addGovFloorInfo.caId);
                $that._listAddFloorGovBuildingTypes();
                $that._listAddFloorGovCommunityAreas();
                $('#addGovFloorModel').modal('show');
            });
            vc.on("addGovFloor", "notifyUploadCoverImage", function (_param) {
                if (_param.length > 0) {
                    console.log(_param);
                    vc.component.addGovFloorInfo.floorIcon = _param[0];
                } else {
                    vc.component.addGovFloorInfo.floorIcon = '';
                }
            });
        },
        methods: {
            addGovFloorValidate() {
                return vc.validate.validate({
                    addGovFloorInfo: vc.component.addGovFloorInfo
                }, {
                    'addGovFloorInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "建筑分类名称超长"
                        },
                    ],
                    'addGovFloorInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区名称超长"
                        },
                    ],
                    'addGovFloorInfo.floorNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "建筑物编号超长"
                        },
                    ],
                    'addGovFloorInfo.floorName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "建筑物名称超长"
                        },
                    ],
                    'addGovFloorInfo.floorType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "建筑物类型超长"
                        },
                    ],
                    'addGovFloorInfo.floorArea': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "楼栋面积不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "楼栋面积格式错误"
                        },
                    ],
                    'addGovFloorInfo.layerCount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "楼层数不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "楼层数格式错误"
                        },
                    ],
                    'addGovFloorInfo.unitCount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "单元数不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "单元数格式错误"
                        },
                    ],
                    'addGovFloorInfo.floorUse': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物用途不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "建筑物用途超长"
                        },
                    ],
                    'addGovFloorInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任人超长"
                        },
                    ],
                    'addGovFloorInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "联系电话格式错误"
                        },
                    ],
                    'addGovFloorInfo.floorIcon': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑图片不能为空"
                        }
                    ],
                    'addGovFloorInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],




                });
            },
            saveGovFloorInfo: function () {
                if (!vc.component.addGovFloorValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //vc.component.addGovFloorInfo.communityId = vc.getCurrentCommunity().communityId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovFloorInfo);
                    $('#addGovFloorModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govFloor/saveGovFloor',
                    JSON.stringify(vc.component.addGovFloorInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovFloorModel').modal('hide');
                            vc.component.clearAddGovFloorInfo();
                            vc.emit('addGovFloorCover','uploadImage', 'clearImage', {});
                            vc.emit('govFloorManage', 'listGovFloor', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddFloorGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovFloorInfo.total = _govCommunityManageInfo.total;
                        vc.component.addGovFloorInfo.records = _govCommunityManageInfo.records;
                        vc.component.addGovFloorInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddFloorGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovFloorInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovFloorInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovFloorInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddFloorGovBuildingTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govBuildingType/queryGovBuildingType',
                    param,
                    function (json, res) {
                        var _govBuildingTypeManageInfo = JSON.parse(json);
                        vc.component.addGovFloorInfo.total = _govBuildingTypeManageInfo.total;
                        vc.component.addGovFloorInfo.records = _govBuildingTypeManageInfo.records;
                        vc.component.addGovFloorInfo.govBuildingTypes = _govBuildingTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovFloorInfo: function () {
                vc.component.addGovFloorInfo = {
                    caId: '',
                    govCommunityId: '',
                    floorNum: '',
                    floorName: '',
                    floorType: '',
                    floorArea: '',
                    layerCount: '',
                    unitCount: '',
                    floorUse: '',
                    personName: '',
                    personLink: '',
                    floorIcon: '',
                    ramark: '',
                    govCommunityAreas: [],
                    govCommunitys: [] ,
                    govBuildingTypes: []

                };
            }
        }
    });

})(window.vc);
