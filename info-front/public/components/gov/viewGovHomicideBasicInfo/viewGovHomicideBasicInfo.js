/**
    命案信息 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovHomicideBasicInfo:{
                index:0,
                flowComponent:'viewGovHomicideBasicInfo',
                govHomicideId:'',
caId:'',
homicideNum:'',
homicideName:'',
happenTime:'',
victimId:'',
victimName:'',
suspicionId:'',
suspicionName:'',
endTime:'',
leadRamark:'',
statusCd:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovHomicideBasicInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovHomicideBasicInfo','chooseGovHomicideBasic',function(_app){
                vc.copyObject(_app, vc.component.viewGovHomicideBasicInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovHomicideBasicInfo);
            });

            vc.on('viewGovHomicideBasicInfo', 'onIndex', function(_index){
                vc.component.viewGovHomicideBasicInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovHomicideBasicInfoModel(){
                vc.emit('chooseGovHomicideBasic','openChooseGovHomicideBasicModel',{});
            },
            _openAddGovHomicideBasicInfoModel(){
                vc.emit('addGovHomicideBasic','openAddGovHomicideBasicModal',{});
            },
            _loadGovHomicideBasicInfoData:function(){

            }
        }
    });

})(window.vc);
