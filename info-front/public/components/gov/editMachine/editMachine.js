(function (vc, vm) {

    vc.extends({
        data: {
            editMachineInfo: {
                machineId: '',
                caId: '',
                machineCode: '',
                machineVersion: '',
                machineTypeCd: '',
                govCommunityId: '',
                machineName: '',
                authCode: '',
                machineIp: '',
                machineMac: '',
                locationTypeCd: '',
                locationObjId: '',
                state: '',
                typeId:'',
                isShow:'',
                heartbeatTime: '',
                machineTypes: [],
                govCommunitys:[]
            }
        },
        _initMethod: function () {
            $that._listEditMachineTypes();
            $that._listEditGovCommunitys();
        },
        _initEvent: function () {
            vc.on('editMachine', 'openEditMachineModal', function (_params) {
                vc.component.refreshEditMachineInfo();
                $('#editMachineModel').modal('show');
                vc.copyObject(_params, vc.component.editMachineInfo);
                vc.component.editMachineInfo.typeId = _params.typeId;
                $that.setEditMachineTypeCd(vc.component.editMachineInfo.typeId);
                vc.component.editMachineInfo.caId = vc.getCurrentCommunity().caId;
                console.log(_params);
                console.log($that.editMachineInfo);
            });
        },
        methods: {
            editMachineValidate: function () {
                return vc.validate.validate({
                    editMachineInfo: vc.component.editMachineInfo
                }, {
                    'editMachineInfo.machineId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备ID不能超过30"
                        },
                    ],
                    'editMachineInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editMachineInfo.machineCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备编码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备编码不能超过30"
                        },
                    ],
                    'editMachineInfo.machineVersion': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备版本不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备版本不能超过30"
                        },
                    ],
                    'editMachineInfo.machineTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备类型不能超过30"
                        },
                    ],
                    'editMachineInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区ID不能超过30"
                        },
                    ],
                    'editMachineInfo.machineName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "设备名称不能超过200"
                        },
                    ],
                    'editMachineInfo.authCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "授权码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "授权码不能超过64"
                        },
                    ],
                    'editMachineInfo.machineIp': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备IP不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "设备IP不能超过64"
                        },
                    ],
                    'editMachineInfo.machineMac': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备mac不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "设备mac不能超过64"
                        },
                    ],
                    'editMachineInfo.locationTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "位置ID,请查看不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "位置ID,请查看不能超过30"
                        },
                    ],
                    'editMachineInfo.locationObjId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "对象ID不能超过30"
                        },
                    ],
                    'editMachineInfo.state': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "设备状态不能超过12"
                        },
                    ],
                    'editMachineInfo.heartbeatTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备心跳时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "设备心跳时间不能超过时间类型"
                        },
                    ],
                    'editMachineInfo.isShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否显示不能为空"
                        }
                    ]

                });
            },
            editMachine: function () {
                if (!vc.component.editMachineValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/machine/updateMachine',
                    JSON.stringify(vc.component.editMachineInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editMachineModel').modal('hide');
                            vc.emit('machineManage', 'listMachine', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditMachineTypes: function () {

                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/machineType/queryMachineType',
                    param,
                    function (json, res) {
                        var _machineTypeManageInfo = JSON.parse(json);
                        vc.component.editMachineInfo.machineTypes = _machineTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            
            _listEditGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editMachineInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            setEditMachineTypeCd: function (_typeId) {
                vc.component.editMachineInfo.machineTypes.forEach(item => {
                    if (item.typeId == _typeId) {
                        vc.component.editMachineInfo.machineTypeCd = item.machineTypeCd;
                        if(item.machineTypeCd == '9998' || item.machineTypeCd == '9994'){
                            $that.editMachineInfo.direction= '3306';
                        }else{
                            $that.editMachineInfo.direction= '';
                        }
                    }
                    
                });
            },
            refreshEditMachineInfo: function () {
                let _machineTypes = vc.component.editMachineInfo.machineTypes;
                let _govCommunitys = vc.component.editMachineInfo.govCommunitys;
                vc.component.editMachineInfo = {
                    machineId: '',
                    caId: '',
                    machineCode: '',
                    machineVersion: '',
                    machineTypeCd: '',
                    govCommunityId: '',
                    machineName: '',
                    authCode: '',
                    machineIp: '',
                    machineMac: '',
                    locationTypeCd: '',
                    locationObjId: '',
                    state: '',
                    isShow: '',
                    statusCd: '',
                    heartbeatTime: '',
                    machineTypes: _machineTypes,
                    govCommunitys:_govCommunitys
                }
            }
        }
    });

})(window.vc, window.vc.component);
