/**
    信访管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPetitionLetterInfo:{
                index:0,
                flowComponent:'viewGovPetitionLetterInfo',
                petitionLetterId:'',
caId:'',
govCommunityId:'',
letterCode:'',
queryCode:'',
petitionTime:'',
petitionPersonName:'',
petitionPersonId:'',
personLink:'',
idCard:'',
petitionType:'',
petitionPurpose:'',
context:'',
state:'',
imgUrl:'',
remark:'',
dealTime:'',
dealDepartmentId:'',
dealDepartmentName:'',
dealUserName:'',
dealUserId:'',
dealOpinion:'',
receiveUserId:'',
receiveUserName:'',
receiveDepartmentId:'',
receiveDepartmentName:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPetitionLetterInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPetitionLetterInfo','chooseGovPetitionLetter',function(_app){
                vc.copyObject(_app, vc.component.viewGovPetitionLetterInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPetitionLetterInfo);
            });

            vc.on('viewGovPetitionLetterInfo', 'onIndex', function(_index){
                vc.component.viewGovPetitionLetterInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPetitionLetterInfoModel(){
                vc.emit('chooseGovPetitionLetter','openChooseGovPetitionLetterModel',{});
            },
            _openAddGovPetitionLetterInfoModel(){
                vc.emit('addGovPetitionLetter','openAddGovPetitionLetterModal',{});
            },
            _loadGovPetitionLetterInfoData:function(){

            }
        }
    });

})(window.vc);
