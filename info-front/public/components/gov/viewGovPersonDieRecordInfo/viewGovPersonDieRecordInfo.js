/**
    临终送别 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPersonDieRecordInfo:{
                index:0,
                flowComponent:'viewGovPersonDieRecordInfo',
                dieRecordId:'',
dieId:'',
govPersonId:'',
title:'',
typeCd:'',
context:'',
activityTime:'',
caId:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPersonDieRecordInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPersonDieRecordInfo','chooseGovPersonDieRecord',function(_app){
                vc.copyObject(_app, vc.component.viewGovPersonDieRecordInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPersonDieRecordInfo);
            });

            vc.on('viewGovPersonDieRecordInfo', 'onIndex', function(_index){
                vc.component.viewGovPersonDieRecordInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPersonDieRecordInfoModel(){
                vc.emit('chooseGovPersonDieRecord','openChooseGovPersonDieRecordModel',{});
            },
            _openAddGovPersonDieRecordInfoModel(){
                vc.emit('addGovPersonDieRecord','openAddGovPersonDieRecordModal',{});
            },
            _loadGovPersonDieRecordInfoData:function(){

            }
        }
    });

})(window.vc);
