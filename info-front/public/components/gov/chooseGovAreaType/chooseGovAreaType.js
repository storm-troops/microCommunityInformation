(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovAreaType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovAreaTypeInfo:{
                govAreaTypes:[],
                _currentGovAreaTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovAreaType','openChooseGovAreaTypeModel',function(_param){
                $('#chooseGovAreaTypeModel').modal('show');
                vc.component._refreshChooseGovAreaTypeInfo();
                vc.component._loadAllGovAreaTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovAreaTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govAreaType/queryGovAreaType',
                             param,
                             function(json){
                                var _govAreaTypeInfo = JSON.parse(json);
                                vc.component.chooseGovAreaTypeInfo.govAreaTypes = _govAreaTypeInfo.govAreaTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovAreaType:function(_govAreaType){
                if(_govAreaType.hasOwnProperty('name')){
                     _govAreaType.govAreaTypeName = _govAreaType.name;
                }
                vc.emit($props.emitChooseGovAreaType,'chooseGovAreaType',_govAreaType);
                vc.emit($props.emitLoadData,'listGovAreaTypeData',{
                    govAreaTypeId:_govAreaType.govAreaTypeId
                });
                $('#chooseGovAreaTypeModel').modal('hide');
            },
            queryGovAreaTypes:function(){
                vc.component._loadAllGovAreaTypeInfo(1,10,vc.component.chooseGovAreaTypeInfo._currentGovAreaTypeName);
            },
            _refreshChooseGovAreaTypeInfo:function(){
                vc.component.chooseGovAreaTypeInfo._currentGovAreaTypeName = "";
            }
        }

    });
})(window.vc);
