(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addMachineInfo: {
                machineId: '',
                caId: '',
                machineCode: '',
                machineVersion: '',
                machineTypeCd: '',
                govCommunityId: '',
                machineName: '',
                authCode: '',
                machineIp: '',
                machineMac: '',
                isShow: '',
                locationTypeCd: '-1',
                locationObjId: '-1',
                state: '运行',
                heartbeatTime: '10',
                machineTypes: [],
                govCommunitys: []
            }
        },
        _initMethod: function () {
         
        },
        _initEvent: function () {
            vc.on('addMachine', 'openAddMachineModal', function () {
                $that._listAddMachineTypes();
                $that._listAddGovCommunitys();
                $('#addMachineModel').modal('show');
            });
        },
        methods: {
            addMachineValidate() {
                return vc.validate.validate({
                    addMachineInfo: vc.component.addMachineInfo
                }, {

                    'addMachineInfo.machineCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备编码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备编码不能超过30"
                        },
                    ],
                    'addMachineInfo.machineVersion': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备版本不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备版本不能超过30"
                        },
                    ],
                    'addMachineInfo.machineTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "设备类型不能超过30"
                        },
                    ],
                    'addMachineInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区ID不能超过30"
                        },
                    ],
                    'addMachineInfo.machineName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "设备名称不能超过200"
                        },
                    ],
                    'addMachineInfo.authCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "授权码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "授权码不能超过64"
                        },
                    ],
                    'addMachineInfo.machineIp': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备IP不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "设备IP不能超过64"
                        },
                    ],
                    'addMachineInfo.machineMac': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设备mac不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "设备mac不能超过64"
                        },
                    ],
                    'addMachineInfo.isShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否显示不能为空"
                        }
                    ]

                });
            },
            saveMachineInfo: function () {
                if (!vc.component.addMachineValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addMachineInfo.caId = vc.getCurrentCommunity().caId;

                vc.http.apiPost(
                    '/machine/saveMachine',
                    JSON.stringify(vc.component.addMachineInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addMachineModel').modal('hide');
                            vc.component.clearAddMachineInfo();
                            vc.emit('machineManage', 'listMachine', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddMachineTypes: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/machineType/queryMachineType',
                    param,
                    function (json, res) {
                        var _machineTypeManageInfo = JSON.parse(json);
                        console.log(_machineTypeManageInfo.data);
                        $that.addMachineInfo.machineTypes = _machineTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        console.log(_govCommunityManageInfo.data);
                        $that.addMachineInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            setAddMachineTypeCd: function (_typeId) {
                vc.component.addMachineInfo.machineTypes.forEach(item => {
                    if (item.typeId == _typeId) {
                        vc.component.addMachineInfo.machineTypeCd = item.machineTypeCd;
                        if (item.machineTypeCd == '9998' || item.machineTypeCd == '9994') {
                            $that.addMachineInfo.direction = '3306';
                            $that.addMachineInfo.isShow = 'false';
                        } else {
                            $that.addMachineInfo.direction = '';
                            $that.addMachineInfo.isShow = 'true';
                        }
                    }

                });
            },
            clearAddMachineInfo: function () {
                vc.component.addMachineInfo = {
                    machineId: '',
                    caId: '',
                    machineCode: '',
                    machineVersion: '',
                    machineTypeCd: '',
                    govCommunityId: '',
                    machineName: '',
                    authCode: '',
                    machineIp: '',
                    machineMac: '',
                    isShow:'',
                    locationTypeCd: '-1',
                    locationObjId: '-1',
                    state: '运行',
                    heartbeatTime: '10',
                    machineTypes: [],
                    govCommunitys: []
                };
            }
        }
    });

})(window.vc);
