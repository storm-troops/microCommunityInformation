(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPetitionLetterInfo: {
                petitionLetterId: '',
                caId: '',
                govCommunityId: '',
                letterCode: '',
                queryCode: '',
                petitionTime: '',
                petitionPersonName: '',
                petitionPersonId: '',
                personLink: '',
                idCard: '',
                petitionType: '',
                petitionForm: '',
                petitionPurpose: '',
                context: '',
                state: '1001',
                imgUrl: '',
                remark: '',
                dealDepartmentId: '',
                dealDepartmentName: '',
                dealUserName: '',
                dealUserId: '',
                dealOpinion: '',
                receiveUserId: '',
                receiveUserName: '',
                receiveDepartmentId: '',
                receiveDepartmentName: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addPetitionTime', function (_value) {
                $that.addGovPetitionLetterInfo.petitionTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovPetitionLetter', 'openAddGovPetitionLetterModal', function () {
                $('#addGovPetitionLetterModel').modal('show');
            });
            vc.on("addImgUrl", "petitionUploadImage", function (_param) {
                if (!vc.isEmpty(_param) && _param.length > 0) {
                    vc.component.addGovPetitionLetterInfo.imgUrl = _param[0];
                } else {
                    vc.component.addGovPetitionLetterInfo.imgUrl = '';
                }
            });
            vc.on('openChooseGovPetitionPerson', 'chooseGovPetitionPerson', function (_param) {
                vc.component.addGovPetitionLetterInfo.petitionPersonName = _param.personName;
                vc.component.addGovPetitionLetterInfo.petitionPersonId = _param.petitionPersonId;
                vc.component.addGovPetitionLetterInfo.personLink = _param.personTel;
                vc.component.addGovPetitionLetterInfo.idCard = _param.personName;
            });
        },
        methods: {
            addGovPetitionLetterValidate() {
                return vc.validate.validate({
                    addGovPetitionLetterInfo: vc.component.addGovPetitionLetterInfo
                }, {
                  
                    'addGovPetitionLetterInfo.letterCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访件号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访件号不能超过30"
                        },
                    ],
                    'addGovPetitionLetterInfo.queryCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "查询码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "查询码不能超过30"
                        },
                    ],
                    'addGovPetitionLetterInfo.petitionTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "信访日期不能超过时间类型"
                        },
                    ],
                    'addGovPetitionLetterInfo.petitionPersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访人名称不能超过30"
                        },
                    ],
                    'addGovPetitionLetterInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "信访人电话不能超过11"
                        },
                    ],
                    'addGovPetitionLetterInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访人身份证不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "20",
                            errInfo: "信访人身份证不能超过20"
                        },
                    ],
                    'addGovPetitionLetterInfo.petitionType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访类型不能超过30"
                        },
                    ],
                    'addGovPetitionLetterInfo.petitionForm': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访形式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访形式不能超过30"
                        },
                    ],
                    'addGovPetitionLetterInfo.petitionPurpose': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访目的不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访目的不能超过30"
                        },
                    ],
                    'addGovPetitionLetterInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "信访内容不能超过长文本"
                        },
                    ],
              
                    'addGovPetitionLetterInfo.imgUrl': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "照片地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "照片地址不能超过512"
                        },
                    ]
                });
            },
            saveGovPetitionLetterInfo: function () {
                if (!vc.component.addGovPetitionLetterValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovPetitionLetterInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPetitionLetterInfo);
                    $('#addGovPetitionLetterModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govPetitionLetter/saveGovPetitionLetter',
                    JSON.stringify(vc.component.addGovPetitionLetterInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPetitionLetterModel').modal('hide');
                            vc.component.clearAddGovPetitionLetterInfo();
                            vc.emit('govPetitionLetterManage', 'listGovPetitionLetter', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChoosePetitionPersonModel: function () {
                vc.emit( 'chooseGovPetitionPerson', 'openChooseGovPetitionPersonModel', {});
            },
            _closeAddGovPetitionLetter: function () {
                $that.clearAddGovPetitionLetterInfo();
                vc.emit('govPetitionLetterManage', 'listGovPetitionLetter', {});
            },
            clearAddGovPetitionLetterInfo: function () {
                vc.component.addGovPetitionLetterInfo = {
                    petitionLetterId: '',
                    caId: '',
                    govCommunityId: '',
                    letterCode: '',
                    queryCode: '',
                    petitionTime: '',
                    petitionPersonName: '',
                    petitionPersonId: '',
                    personLink: '',
                    idCard: '',
                    petitionType: '',
                    petitionForm: '',
                    petitionPurpose: '',
                    context: '',
                    state: '',
                    imgUrl: '',
                    remark: '',
                    dealDepartmentId: '',
                    dealDepartmentName: '',
                    dealUserName: '',
                    dealUserId: '',
                    dealOpinion: '',
                    receiveUserId: '',
                    receiveUserName: '',
                    receiveDepartmentId: '',
                    receiveDepartmentName: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
