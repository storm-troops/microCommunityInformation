(function(vc,vm){

    vc.extends({
        data:{
            deleteGovGuideTitleInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovGuideTitle','openDeleteGovGuideTitleModal',function(_params){

                vc.component.deleteGovGuideTitleInfo = _params;
                $('#deleteGovGuideTitleModel').modal('show');

            });
        },
        methods:{
            deleteGovGuideTitle:function(){
                vc.component.deleteGovGuideTitleInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govHealthTitle/deleteGovHealthTitle',
                    JSON.stringify(vc.component.deleteGovGuideTitleInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovGuideTitleModel').modal('hide');
                            vc.emit('govGuideTitleManage','listGovGuideTitle',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovGuideTitleModel:function(){
                $('#deleteGovGuideTitleModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
