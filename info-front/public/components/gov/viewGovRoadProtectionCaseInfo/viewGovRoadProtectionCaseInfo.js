/**
    路案事件 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovRoadProtectionCaseInfo:{
                index:0,
                flowComponent:'viewGovRoadProtectionCaseInfo',
                roadCaseId:'',
roadProtectionId:'',
caId:'',
govCommunityId:'',
caseName:'',
caseCode:'',
happenedTime:'',
happenedPlace:'',
idType:'',
idCard:'',
personName:'',
isSolved:'',
personNum:'',
fleeingNum:'',
arrestsNum:'',
detectionContent:'',
caseContent:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovRoadProtectionCaseInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovRoadProtectionCaseInfo','chooseGovRoadProtectionCase',function(_app){
                vc.copyObject(_app, vc.component.viewGovRoadProtectionCaseInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovRoadProtectionCaseInfo);
            });

            vc.on('viewGovRoadProtectionCaseInfo', 'onIndex', function(_index){
                vc.component.viewGovRoadProtectionCaseInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovRoadProtectionCaseInfoModel(){
                vc.emit('chooseGovRoadProtectionCase','openChooseGovRoadProtectionCaseModel',{});
            },
            _openAddGovRoadProtectionCaseInfoModel(){
                vc.emit('addGovRoadProtectionCase','openAddGovRoadProtectionCaseModal',{});
            },
            _loadGovRoadProtectionCaseInfoData:function(){

            }
        }
    });

})(window.vc);
