(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovSpecialFollow:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovSpecialFollowInfo:{
                govSpecialFollows:[],
                _currentGovSpecialFollowName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovSpecialFollow','openChooseGovSpecialFollowModel',function(_param){
                $('#chooseGovSpecialFollowModel').modal('show');
                vc.component._refreshChooseGovSpecialFollowInfo();
                vc.component._loadAllGovSpecialFollowInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovSpecialFollowInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govSpecialFollow/queryGovSpecialFollow',
                             param,
                             function(json){
                                var _govSpecialFollowInfo = JSON.parse(json);
                                vc.component.chooseGovSpecialFollowInfo.govSpecialFollows = _govSpecialFollowInfo.govSpecialFollows;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovSpecialFollow:function(_govSpecialFollow){
                if(_govSpecialFollow.hasOwnProperty('name')){
                     _govSpecialFollow.govSpecialFollowName = _govSpecialFollow.name;
                }
                vc.emit($props.emitChooseGovSpecialFollow,'chooseGovSpecialFollow',_govSpecialFollow);
                vc.emit($props.emitLoadData,'listGovSpecialFollowData',{
                    govSpecialFollowId:_govSpecialFollow.govSpecialFollowId
                });
                $('#chooseGovSpecialFollowModel').modal('hide');
            },
            queryGovSpecialFollows:function(){
                vc.component._loadAllGovSpecialFollowInfo(1,10,vc.component.chooseGovSpecialFollowInfo._currentGovSpecialFollowName);
            },
            _refreshChooseGovSpecialFollowInfo:function(){
                vc.component.chooseGovSpecialFollowInfo._currentGovSpecialFollowName = "";
            }
        }

    });
})(window.vc);
