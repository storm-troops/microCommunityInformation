(function(vc,vm){

    vc.extends({
        data:{
            deleteGovHealthTermInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovHealthTerm','openDeleteGovHealthTermModal',function(_params){

                vc.component.deleteGovHealthTermInfo = _params;
                $('#deleteGovHealthTermModel').modal('show');

            });
        },
        methods:{
            deleteGovHealthTerm:function(){
                vc.component.deleteGovHealthTermInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govHealthTerm/deleteGovHealthTerm',
                    JSON.stringify(vc.component.deleteGovHealthTermInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovHealthTermModel').modal('hide');
                            vc.emit('govHealthTermManage','listGovHealthTerm',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovHealthTermModel:function(){
                $('#deleteGovHealthTermModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
