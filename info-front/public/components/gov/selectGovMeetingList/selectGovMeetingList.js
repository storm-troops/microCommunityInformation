(function (vc, vm) {

    vc.extends({
        data: {
            selectGovMeetingListInfo: {
                govMeetingId: '',
                caId: '',
                typeId: '',
                meetingTypeName: '',
                orgName: '',
                orgId: '',
                govMemberId: '',
                memberName: '',
                meetingName: '',
                meetingTime: '',
                meetingAddress: '',
                meetingBrief: '',
                meetingPoints: '',
                meetingContent: '',
                meetingImgs: [],
                govMembers: [],
                govMeetingTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
           
        },
        _initEvent: function () {
            vc.on('selectGovMeetingList', 'openselectGovMeetingListModal', function (_params) {
                vc.component.refreshselectGovMeetingListInfo();
                $('#selectGovMeetingListModel').modal('show');
                console.log(_params);
                vc.copyObject(_params, vc.component.selectGovMeetingListInfo);
                vc.component.selectGovMeetingListInfo.govMembers = _params.govMeetingMemberRelDtos;
               
                vc.component.selectGovMeetingListInfo.meetingImgs = _params.meetingImgs;
                vc.component.selectGovMeetingListInfo.caId = vc.getCurrentCommunity().caId;

                vc.emit('viewEditGovPartyMemberRecord', 'openviewEditGovPartyMemberRecordModel', $that.selectGovMeetingListInfo.govMembers);
      
                vc.emit('selectGovMeetingListCover','uploadImage', 'notifyPhotos',vc.component.selectGovMeetingListInfo.meetingImgs);
            });
            vc.on("selectGovMeetingList", "notifyUploadCoverImage", function (_param) {
                if(_param.length > 0){
                    vc.component.selectGovMeetingListInfo.meetingImgs=_param;
                }else{
                    vc.component.selectGovMeetingListInfo.meetingImgs = [];
                }
                
            });
        },
        methods: {
            _clearselectGovMeetingListModel: function () {
                $that.refreshselectGovMeetingListInfo();
                vc.emit('govMeetingListManage', 'listGovMeetingList', {});
            },
            refreshselectGovMeetingListInfo: function () {
                let _govMembers = vc.component.selectGovMeetingListInfo.govMembers;
                let _govMeetingTypes = vc.component.selectGovMeetingListInfo.govMeetingTypes;
                let _govPartyOrgs = vc.component.selectGovMeetingListInfo.govPartyOrgs;
                vc.component.selectGovMeetingListInfo = {
                    govMeetingId: '',
                    caId: '',
                    typeId: '',
                    meetingTypeName: '',
                    orgName: '',
                    orgId: '',
                    govMemberId: '',
                    memberName: '',
                    meetingName: '',
                    meetingTime: '',
                    meetingAddress: '',
                    meetingBrief: '',
                    meetingPoints: '',
                    meetingContent: '',
                    meetingImgs: [],
                    govMembers: _govMembers,
                    govMeetingTypes: _govMeetingTypes,
                    govPartyOrgs: _govPartyOrgs
                }
            }
        }
    });

})(window.vc, window.vc.component);
