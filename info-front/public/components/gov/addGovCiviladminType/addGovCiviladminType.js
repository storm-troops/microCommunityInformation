(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovActivitiesTypeInfo: {
                typeName: '',
                typeDesc: '',
                seq: '',
                defalutShow: '',
                caId:'',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovActivitiesType', 'openAddGovActivitiesTypeModal', function () {
                $('#addGovActivitiesTypeModel').modal('show');
            });
        },
        methods: {
            addGovActivitiesTypeValidate() {
                return vc.validate.validate({
                    addGovActivitiesTypeInfo: vc.component.addGovActivitiesTypeInfo
                }, {
                    'addGovActivitiesTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "类型名称超长"
                        },
                    ],
                    'addGovActivitiesTypeInfo.typeDesc': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "类型描述超长"
                        },
                    ],
                    'addGovActivitiesTypeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "顺序超长"
                        },
                    ],
                    'addGovActivitiesTypeInfo.defalutShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小程序是否显示不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "小程序是否显示太长"
                        },
                    ],




                });
            },
            saveGovActivitiesTypeInfo: function () {
                if (!vc.component.addGovActivitiesTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovActivitiesTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovActivitiesTypeInfo);
                    $('#addGovActivitiesTypeModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govCiviladminType/saveGovCiviladminType',
                    JSON.stringify(vc.component.addGovActivitiesTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovActivitiesTypeModel').modal('hide');
                            vc.component.clearAddGovActivitiesTypeInfo();
                            vc.emit('govActivitiesTypeManage', 'listGovActivitiesType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovActivitiesTypeInfo: function () {
                vc.component.addGovActivitiesTypeInfo = {
                    typeCd: '',
                    typeName: '',
                    typeDesc: '',
                    seq: '',
                    defalutShow: '',
                    caId:''

                };
            }
        }
    });

})(window.vc);
