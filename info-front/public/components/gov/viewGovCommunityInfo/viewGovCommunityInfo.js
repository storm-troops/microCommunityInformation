/**
    小区信息 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovCommunityInfo: {
                index: 0,
                flowComponent: 'viewGovCommunityInfo',
                caId: '',
                communityName: '',
                propertyType: '',
                managerName: '',
                personName: '',
                personLink: '',
                communityIcon: '',
                communitySecure: '',
                remark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCommunityInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovCommunityInfo', 'chooseGovCommunity', function (_app) {
                vc.copyObject(_app, vc.component.viewGovCommunityInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovCommunityInfo);
            });

            vc.on('viewGovCommunityInfo', 'onIndex', function (_index) {
                vc.component.viewGovCommunityInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovCommunityInfoModel() {
                vc.emit('chooseGovCommunity', 'openChooseGovCommunityModel', {});
            },
            _openAddGovCommunityInfoModel() {
                vc.emit('addGovCommunity', 'openAddGovCommunityModal', {});
            },
            _loadGovCommunityInfoData: function () {

            }
        }
    });

})(window.vc);
