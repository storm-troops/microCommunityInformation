/**
    标签管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovLabelInfo:{
                index:0,
                flowComponent:'viewGovLabelInfo',
                govLabelId:'',
caId:'',
labelName:'',
labelCd:'',
labelType:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovLabelInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovLabelInfo','chooseGovLabel',function(_app){
                vc.copyObject(_app, vc.component.viewGovLabelInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovLabelInfo);
            });

            vc.on('viewGovLabelInfo', 'onIndex', function(_index){
                vc.component.viewGovLabelInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovLabelInfoModel(){
                vc.emit('chooseGovLabel','openChooseGovLabelModel',{});
            },
            _openAddGovLabelInfoModel(){
                vc.emit('addGovLabel','openAddGovLabelModal',{});
            },
            _loadGovLabelInfoData:function(){

            }
        }
    });

})(window.vc);
