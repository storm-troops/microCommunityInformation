/**
    党组织 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPartyOrgInfo:{
                index:0,
                flowComponent:'viewGovPartyOrgInfo',
                orgName:'',
preOrgId:'',
preOrgName:'',
orgCode:'',
orgSimpleName:'',
personName:'',
personTel:'',
address:'',
orgLevel:'',
orgFlag:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPartyOrgInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPartyOrgInfo','chooseGovPartyOrg',function(_app){
                vc.copyObject(_app, vc.component.viewGovPartyOrgInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPartyOrgInfo);
            });

            vc.on('viewGovPartyOrgInfo', 'onIndex', function(_index){
                vc.component.viewGovPartyOrgInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPartyOrgInfoModel(){
                vc.emit('chooseGovPartyOrg','openChooseGovPartyOrgModel',{});
            },
            _openAddGovPartyOrgInfoModel(){
                vc.emit('addGovPartyOrg','openAddGovPartyOrgModal',{});
            },
            _loadGovPartyOrgInfoData:function(){

            }
        }
    });

})(window.vc);
