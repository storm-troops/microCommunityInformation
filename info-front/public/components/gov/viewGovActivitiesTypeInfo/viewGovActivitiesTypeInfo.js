/**
    公告类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovActivitiesTypeInfo:{
                index:0,
                flowComponent:'viewGovActivitiesTypeInfo',
                typeCd:'',
typeName:'',
typeDesc:'',
seq:'',
defalutShow:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovActivitiesTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovActivitiesTypeInfo','chooseGovActivitiesType',function(_app){
                vc.copyObject(_app, vc.component.viewGovActivitiesTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovActivitiesTypeInfo);
            });

            vc.on('viewGovActivitiesTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovActivitiesTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovActivitiesTypeInfoModel(){
                vc.emit('chooseGovActivitiesType','openChooseGovActivitiesTypeModel',{});
            },
            _openAddGovActivitiesTypeInfoModel(){
                vc.emit('addGovActivitiesType','openAddGovActivitiesTypeModal',{});
            },
            _loadGovActivitiesTypeInfoData:function(){

            }
        }
    });

})(window.vc);
