(function(vc,vm){

    vc.extends({
        data:{
            deleteGuideSubscribeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGuideSubscribe','openDeleteGuideSubscribeModal',function(_params){

                vc.component.deleteGuideSubscribeInfo = _params;
                $('#deleteGuideSubscribeModel').modal('show');

            });
        },
        methods:{
            deleteGuideSubscribe:function(){
                vc.component.deleteGuideSubscribeInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govGuideSubscribe/deleteGovGuideSubscribe',
                    JSON.stringify(vc.component.deleteGuideSubscribeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGuideSubscribeModel').modal('hide');
                            vc.emit('guideSubscribeManage','listGuideSubscribe',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGuideSubscribeModel:function(){
                $('#deleteGuideSubscribeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
