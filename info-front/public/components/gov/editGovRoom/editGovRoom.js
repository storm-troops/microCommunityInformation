(function (vc, vm) {

    vc.extends({
        data: {
            editGovRoomInfo: {
                govRoomId: '',
                caId: '',
                govCommunityId: '',
                govFloorId: '',
                roomNum: '',
                roomType: '',
                roomArea: '',
                layer: '',
                roomAddress: '',
                roomRight: '',
                ownerId: '9999',
                ownerName: '',
                ownerTel: '',
                isConservati: '',
                isSettle: '',
                ramark: '',
                govCommunityAreas: [],
                govCommunitys: [],
                govFloors: [],
                roomTypeTmp:''

            }
        },
        _initMethod: function () {
            $that._listEditGovCommunityAreas();
        
        },
        _initEvent: function () {
            vc.on('editGovRoom', 'openEditGovRoomModal', function (_params) {
                vc.component.refreshEditGovRoomInfo();
                $('#editGovRoomModel').modal('show');
                vc.copyObject(_params, vc.component.editGovRoomInfo);
                $that._listEditGovCommunitys(vc.component.editGovRoomInfo.caId);
                $that._listEditGovFloors(vc.component.editGovRoomInfo.caId,vc.component.editGovRoomInfo.govCommunityId);
                $that.editGovRoomInfo.roomTypeTmp = $that.editGovRoomInfo.roomType;
            });
            vc.on('openChooseGovOwner', 'chooseGovOwner', function (_param) {
                console.log(_param);
                $that.editGovRoomInfo.ownerId = _param.govOwnerId;
                $that.editGovRoomInfo.ownerName = _param.ownerName;
                $that.editGovRoomInfo.ownerTel = _param.ownerTel;
            });
        },
        methods: {
            editGovRoomValidate: function () {
                return vc.validate.validate({
                    editGovRoomInfo: vc.component.editGovRoomInfo
                }, {
                    'editGovRoomInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovRoomInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属小区超长"
                        },
                    ],
                    'editGovRoomInfo.govFloorId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属建筑物不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属建筑物超长"
                        },
                    ],
                    'editGovRoomInfo.roomNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "房屋编号超长"
                        },
                    ],
                    'editGovRoomInfo.roomType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "6",
                            errInfo: "区域地址超长"
                        },
                    ],
                    'editGovRoomInfo.roomArea': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋面积不能为空"
                        }
                    ],
                    'editGovRoomInfo.layer': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "楼层不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "楼层格式错误"
                        },
                    ],
                    'editGovRoomInfo.roomAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "房屋地址太长"
                        },
                    ],
                    'editGovRoomInfo.roomRight': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋产权不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "房屋产权超长"
                        },
                    ],
                    'editGovRoomInfo.ownerId': [
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "房主ID超长"
                        },
                    ],
                    'editGovRoomInfo.ownerName': [
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "房主超长"
                        },
                    ],
                    'editGovRoomInfo.isConservati': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否常住不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "是否常住超长了"
                        },
                    ],
                    'editGovRoomInfo.isSettle': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否落户不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "是否落户超长了"
                        },
                    ],
                    'editGovRoomInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovRoomInfo.govRoomId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋管理ID不能为空"
                        }]

                });
            },
            editGovRoom: function () {
                if (!vc.component.editGovRoomValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govRoom/updateGovRoom',
                    JSON.stringify(vc.component.editGovRoomInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovRoomModel').modal('hide');
                            vc.emit('govRoomManage', 'listGovRoom', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovRoomInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovRoomInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovRoomInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovRoomInfo.total = _govCommunityManageInfo.total;
                        vc.component.editGovRoomInfo.records = _govCommunityManageInfo.records;
                        vc.component.editGovRoomInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovFloors: function (_caId,_GovCommunityId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId,
                        govCommunityId: _GovCommunityId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        vc.component.editGovRoomInfo.total = _govFloorManageInfo.total;
                        vc.component.editGovRoomInfo.records = _govFloorManageInfo.records;
                        vc.component.editGovRoomInfo.govFloors = _govFloorManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _setOwner:function(_ownerId){
                vc.component.editGovRoomInfo.ownerId=_ownerId;
            },
            _openEditChooseGovOwnerModel:function(){
                vc.emit('chooseGovOwner', 'openChooseGovOwnerModel', {});
            },
            _closeEditGovOwnerModel: function () {
                $that.refreshEditGovRoomInfo();
                vc.emit('govRoomManage', 'listGovRoom', {});
            },
            refreshEditGovRoomInfo: function () {
                let _govCommunityAreas = vc.component.editGovRoomInfo.govCommunityAreas;
                let _govCommunitys = vc.component.editGovRoomInfo.govCommunitys;
                let _govFloors = vc.component.editGovRoomInfo.govFloors;
                vc.component.editGovRoomInfo = {
                    govRoomId: '',
                    caId: '',
                    govCommunityId: '',
                    govFloorId: '',
                    roomNum: '',
                    roomType: '',
                    roomArea: '',
                    layer: '',
                    roomAddress: '',
                    roomRight: '',
                    ownerId: '',
                    ownerName: '',
                    ownerTel: '',
                    isConservati: '',
                    isSettle: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas,
                    govCommunitys: _govCommunitys,
                    govFloors: _govFloors
                }
            }
        }
    });

})(window.vc, window.vc.component);
