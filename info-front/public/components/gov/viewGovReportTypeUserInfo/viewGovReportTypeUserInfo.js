/**
    报事类型人员 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovReportTypeUserInfo:{
                index:0,
                flowComponent:'viewGovReportTypeUserInfo',
                reportType:'',
staffId:'',
staffName:'',
caId:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovReportTypeUserInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovReportTypeUserInfo','chooseGovReportTypeUser',function(_app){
                vc.copyObject(_app, vc.component.viewGovReportTypeUserInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovReportTypeUserInfo);
            });

            vc.on('viewGovReportTypeUserInfo', 'onIndex', function(_index){
                vc.component.viewGovReportTypeUserInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovReportTypeUserInfoModel(){
                vc.emit('chooseGovReportTypeUser','openChooseGovReportTypeUserModel',{});
            },
            _openAddGovReportTypeUserInfoModel(){
                vc.emit('addGovReportTypeUser','openAddGovReportTypeUserModal',{});
            },
            _loadGovReportTypeUserInfoData:function(){

            }
        }
    });

})(window.vc);
