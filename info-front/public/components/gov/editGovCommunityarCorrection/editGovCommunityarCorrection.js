(function (vc, vm) {

    vc.extends({
        data: {
            editGovCommunityarCorrectionInfo: {
                correctionId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                correctionStartTime: '',
                correctionEndTime: '',
                correctionReason: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGovCommunityarCorrectionInfo.correctionStartTime = value;
                });
            $('.editEndTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true
            });
            $('.editEndTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editEndTime").val();
                    vc.component.editGovCommunityarCorrectionInfo.correctionEndTime = value;
                });
        },
        _initEvent: function () {
            vc.on('editGovCommunityarCorrection', 'openEditGovCommunityarCorrectionModal', function (_params) {
                vc.component.refreshEditGovCommunityarCorrectionInfo();
                $('#editGovCommunityarCorrectionModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCommunityarCorrectionInfo);
                vc.component.editGovCommunityarCorrectionInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovCommunityarCorrectionValidate: function () {
                return vc.validate.validate({
                    editGovCommunityarCorrectionInfo: vc.component.editGovCommunityarCorrectionInfo
                }, {
                    'editGovCommunityarCorrectionInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "矫正者名称不能超过64"
                        },
                    ],
                    'editGovCommunityarCorrectionInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'editGovCommunityarCorrectionInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'editGovCommunityarCorrectionInfo.correctionStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "矫正开始时间不能超过时间类型"
                        },
                    ],
                    'editGovCommunityarCorrectionInfo.correctionEndTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "矫正结束时间不能超过时间类型"
                        },
                    ],
                    'editGovCommunityarCorrectionInfo.correctionReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "矫正原因不能超过128"
                        },
                    ],
                    'editGovCommunityarCorrectionInfo.correctionId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正者ID不能为空"
                        }]

                });
            },
            editGovCommunityarCorrection: function () {
                if (!vc.component.editGovCommunityarCorrectionValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCommunityarCorrection/updateGovCommunityarCorrection',
                    JSON.stringify(vc.component.editGovCommunityarCorrectionInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCommunityarCorrectionModel').modal('hide');
                            vc.emit('govCommunityarCorrectionManage', 'listGovCommunityarCorrection', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovCommunityarCorrectionInfo: function () {
                vc.component.editGovCommunityarCorrectionInfo = {
                    correctionId: '',
                    correctionId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    correctionStartTime: '',
                    correctionEndTime: '',
                    correctionReason: '',
                    ramark: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
