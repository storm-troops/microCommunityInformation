(function (vc, vm) {

    vc.extends({
        data: {
            viewGovPetitionLetterInfo: {
                petitionLetterId: '',
                    petitionLetterId: '',
                    caId: '',
                    govCommunityId: '',
                    letterCode: '',
                    queryCode: '',
                    petitionTime: '',
                    petitionPersonName: '',
                    petitionPersonId: '',
                    personLink: '',
                    idCard: '',
                    petitionType: '',
                    petitionForm: '',
                    petitionPurpose: '',
                    context: '',
                    state: '',
                    imgUrl: '',
                    remark: '',
                    dealDepartmentId: '',
                    dealDepartmentName: '',
                    dealUserName: '',
                    dealUserId: '',
                    dealOpinion: '',
                    receiveUserId: '',
                    receiveUserName: '',
                    receiveDepartmentId: '',
                    receiveDepartmentName: '',
                    statusCd: '',
            },
            auditGovPetitionLetterInfo: {
                petitionLetterId: '',
                state: '',
                dealDepartmentName: '',
                dealUserName: '',
                dealOpinion: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('auditGovPetitionLetter', 'openAuditGovPetitionLetterModal', function (_params) {
                vc.component.refreshEditGovPetitionLetterInfo();
                $('#editGovPetitionLetterModel').modal('show');
                vc.copyObject(_params, vc.component.auditGovPetitionLetterInfo);
                vc.copyObject(_params, vc.component.viewGovPetitionLetterInfo);
                vc.component.auditGovPetitionLetterInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovPetitionLetterValidate: function () {
                return vc.validate.validate({
                    auditGovPetitionLetterInfo: vc.component.auditGovPetitionLetterInfo
                }, {
                    'auditGovPetitionLetterInfo.dealDepartmentName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "处理部门不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "处理部门不能超过30"
                        },
                    ],
                    'auditGovPetitionLetterInfo.dealUserName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "处理人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "处理人不能超过30"
                        },
                    ],
                    'auditGovPetitionLetterInfo.dealOpinion': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "处理意见不能为空"
                        }
                    ]
                });
            },
            closeEditActivitiesInfo: function () {
                vc.emit('govPetitionLetterManage', 'listGovPetitionLetter', {});
            },
            auditGovPetitionLetter: function () {
                if (!vc.component.editGovPetitionLetterValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.http.apiPost(
                    '/govPetitionLetter/updateGovPetitionLetter',
                    JSON.stringify(vc.component.auditGovPetitionLetterInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPetitionLetterModel').modal('hide');
                            vc.emit('govPetitionLetterManage', 'listGovPetitionLetter', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _openEditChoosePetitionPersonModel: function () {
                vc.emit( 'chooseGovPetitionPerson', 'openChooseGovPetitionPersonModel', {});
            },
            refreshEditGovPetitionLetterInfo: function () {
                vc.component.auditGovPetitionLetterInfo = {
                    petitionLetterId: '',
                    state: '',
                    dealDepartmentName: '',
                    dealUserName: '',
                    dealOpinion: '',

                },
                vc.component.viewGovPetitionLetterInfo = {
                    petitionLetterId: '',
                    petitionLetterId: '',
                    caId: '',
                    govCommunityId: '',
                    letterCode: '',
                    queryCode: '',
                    petitionTime: '',
                    petitionPersonName: '',
                    petitionPersonId: '',
                    personLink: '',
                    idCard: '',
                    petitionType: '',
                    petitionForm: '',
                    petitionPurpose: '',
                    context: '',
                    state: '',
                    imgUrl: '',
                    remark: '',
                    dealDepartmentId: '',
                    dealDepartmentName: '',
                    dealUserName: '',
                    dealUserId: '',
                    dealOpinion: '',
                    receiveUserId: '',
                    receiveUserName: '',
                    receiveDepartmentId: '',
                    receiveDepartmentName: '',
                    statusCd: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
