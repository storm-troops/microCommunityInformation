(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovRoadProtectionInfo: {
                roadProtectionId: '',
                caId: '',
                govCommunityId: '',
                roadType: '',
                roadName: '',
                belongToDepartment: '',
                departmentTel: '',
                departmentAddress: '',
                leadingCadreName: '',
                leaderLink: '',
                manageDepartment: '',
                manageDepTel: '',
                manageDepAddress: '',
                branchLeaders: '',
                branchLeadersTel: '',
                safeHiddenGrade: '',
                safeTrouble: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovRoadProtection', 'openAddGovRoadProtectionModal', function () {
                $('#addGovRoadProtectionModel').modal('show');
            });
        },
        methods: {
            addGovRoadProtectionValidate() {
                return vc.validate.validate({
                    addGovRoadProtectionInfo: vc.component.addGovRoadProtectionInfo
                }, {
                    'addGovRoadProtectionInfo.roadType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "路线类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "路线类型不能超过30"
                        },
                    ],
                    'addGovRoadProtectionInfo.roadName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "名称不能超过100"
                        },
                    ],
                    'addGovRoadProtectionInfo.belongToDepartment': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "隶属单位名称不能超过100"
                        },
                    ],
                    'addGovRoadProtectionInfo.departmentTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "隶属单位电话不能超过30"
                        },
                    ],
                    'addGovRoadProtectionInfo.departmentAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "隶属单位地址不能超过30"
                        },
                    ],
                    'addGovRoadProtectionInfo.leadingCadreName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位负责人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "隶属单位负责人姓名不能超过30"
                        },
                    ],
                    'addGovRoadProtectionInfo.leaderLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位负责人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "隶属单位负责人电话不能超过11"
                        },
                    ],
                    'addGovRoadProtectionInfo.manageDepartment': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管辖单位名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "管辖单位名称不能超过100"
                        },
                    ],
                    'addGovRoadProtectionInfo.manageDepTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管辖单位联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "管辖单位联系方式不能超过30"
                        },
                    ],
                    'addGovRoadProtectionInfo.manageDepAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管辖单位地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "管辖单位地址不能超过100"
                        },
                    ],
                    'addGovRoadProtectionInfo.branchLeaders': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管治保负责人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分管治保负责人姓名不能超过30"
                        },
                    ],
                    'addGovRoadProtectionInfo.branchLeadersTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管治保负责人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "分管治保负责人电话不能超过11"
                        },
                    ],
                    'addGovRoadProtectionInfo.safeHiddenGrade': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安隐患等级不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "5",
                            errInfo: "治安隐患等级不能超过5"
                        },
                    ],
                    'addGovRoadProtectionInfo.safeTrouble': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安隐患问题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "治安隐患问题不能超过长文本"
                        },
                    ],
                });
            },
            saveGovRoadProtectionInfo: function () {
                vc.component.addGovRoadProtectionInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovRoadProtectionValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovRoadProtectionInfo);
                    $('#addGovRoadProtectionModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govRoadProtection/saveGovRoadProtection',
                    JSON.stringify(vc.component.addGovRoadProtectionInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovRoadProtectionModel').modal('hide');
                            vc.component.clearAddGovRoadProtectionInfo();
                            vc.emit('govRoadProtectionManage', 'listGovRoadProtection', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovRoadProtectionInfo: function () {
                vc.component.addGovRoadProtectionInfo = {
                    roadProtectionId: '',
                    caId: '',
                    govCommunityId: '',
                    roadType: '',
                    roadName: '',
                    belongToDepartment: '',
                    departmentTel: '',
                    departmentAddress: '',
                    leadingCadreName: '',
                    leaderLink: '',
                    manageDepartment: '',
                    manageDepTel: '',
                    manageDepAddress: '',
                    branchLeaders: '',
                    branchLeadersTel: '',
                    safeHiddenGrade: '',
                    safeTrouble: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
