(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPersonDieInfo: {
                dieId: '',
                govPersonId: '',
                govPersonName: '',
                caId: '',
                govCommunityId: '',
                personType: '',
                dieType: '',
                diePlace: '',
                imgProve: '',
                dieTime: '',
                startTime: '',
                endTime: '',
                funeralPlace: '',
                contactPerson: '',
                contactTel: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addDieTime', function (_value) {
                $that.addGovPersonDieInfo.dieTime = _value;
            });
            vc.initDateTime('dieStartTime', function (_value) {
                $that.addGovPersonDieInfo.startTime = _value;
            });
            vc.initDateTime('dieEndTime', function (_value) {
                $that.addGovPersonDieInfo.endTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovPersonDie', 'openAddGovPersonDieModal', function () {
                $('#addGovPersonDieModel').modal('show');
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                $that.addGovPersonDieInfo.govPersonId = _param.govPersonId;
                $that.addGovPersonDieInfo.govPersonName = _param.personName;
            });
            vc.on("addImgProve", "notifyUploadImage", function (_param) {
                if (!vc.isEmpty(_param) && _param.length > 0) {
                    vc.component.addGovPersonDieInfo.imgProve = _param[0];
                } else {
                    vc.component.addGovPersonDieInfo.imgProve = '';
                }
            });
        },
        methods: {
            addGovPersonDieValidate() {
                return vc.validate.validate({
                    addGovPersonDieInfo: vc.component.addGovPersonDieInfo
                }, {
                    'addGovPersonDieInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口编号不能超过30"
                        },
                    ],
                    'addGovPersonDieInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovPersonDieInfo.diePlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "去世地点/名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "去世地点/名称不能超过200"
                        },
                    ],
                    'addGovPersonDieInfo.imgProve': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "去世证明图片不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "去世证明图片不能超过200"
                        },
                    ],
                    'addGovPersonDieInfo.dieTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "去世时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "去世时间不能超过时间类型"
                        },
                    ],
                    'addGovPersonDieInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "预计开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "预计开始时间不能超过时间类型"
                        },
                    ],
                    'addGovPersonDieInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "预计结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "预计结束时间不能超过时间类型"
                        },
                    ],
                    'addGovPersonDieInfo.funeralPlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "葬礼地点/名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "葬礼地点/名称不能超过200"
                        },
                    ],
                    'addGovPersonDieInfo.contactPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "联系人不能超过64"
                        },
                    ],
                    'addGovPersonDieInfo.contactTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "联系电话不能超过11"
                        },
                    ],
                });
            },
            saveGovPersonDieInfo: function () {
                vc.component.addGovPersonDieInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovPersonDieValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPersonDieInfo);
                    $('#addGovPersonDieModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govPersonDie/saveGovPersonDie',
                    JSON.stringify(vc.component.addGovPersonDieInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPersonDieModel').modal('hide');
                            vc.component.clearAddGovPersonDieInfo();
                            vc.emit('govPersonDieManage', 'listGovPersonDie', {});
                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
               // vc.emit('chooseGovOldPerson', 'openChooseGovOldPersonModel', param);
                vc.emit( 'chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeAddPersonDie: function () {
                $that.clearAddGovPersonDieInfo();
                vc.emit('govPersonDieManage', 'listGovPersonDie', {});
            },

            clearAddGovPersonDieInfo: function () {
                vc.component.addGovPersonDieInfo = {
                    dieId: '',
                    govPersonId: '',
                    caId: '',
                    govCommunityId: '',
                    personType: '',
                    dieType: '',
                    diePlace: '',
                    imgProve: '',
                    dieTime: '',
                    startTime: '',
                    endTime: '',
                    funeralPlace: '',
                    contactPerson: '',
                    contactTel: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
