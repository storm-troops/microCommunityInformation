(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovCommunityLocation:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovCommunityLocationInfo:{
                govCommunityLocations:[],
                _currentGovCommunityLocationName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovCommunityLocation','openChooseGovCommunityLocationModel',function(_param){
                $('#chooseGovCommunityLocationModel').modal('show');
                vc.component._refreshChooseGovCommunityLocationInfo();
                vc.component._loadAllGovCommunityLocationInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovCommunityLocationInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govCommunityLocation.listGovCommunityLocations',
                             param,
                             function(json){
                                var _govCommunityLocationInfo = JSON.parse(json);
                                vc.component.chooseGovCommunityLocationInfo.govCommunityLocations = _govCommunityLocationInfo.govCommunityLocations;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovCommunityLocation:function(_govCommunityLocation){
                if(_govCommunityLocation.hasOwnProperty('name')){
                     _govCommunityLocation.govCommunityLocationName = _govCommunityLocation.name;
                }
                vc.emit($props.emitChooseGovCommunityLocation,'chooseGovCommunityLocation',_govCommunityLocation);
                vc.emit($props.emitLoadData,'listGovCommunityLocationData',{
                    govCommunityLocationId:_govCommunityLocation.govCommunityLocationId
                });
                $('#chooseGovCommunityLocationModel').modal('hide');
            },
            queryGovCommunityLocations:function(){
                vc.component._loadAllGovCommunityLocationInfo(1,10,vc.component.chooseGovCommunityLocationInfo._currentGovCommunityLocationName);
            },
            _refreshChooseGovCommunityLocationInfo:function(){
                vc.component.chooseGovCommunityLocationInfo._currentGovCommunityLocationName = "";
            }
        }

    });
})(window.vc);
