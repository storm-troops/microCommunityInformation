(function(vc,vm){

    vc.extends({
        data:{
            deleteGovActivityTypeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovActivityType','openDeleteGovActivityTypeModal',function(_params){

                vc.component.deleteGovActivityTypeInfo = _params;
                $('#deleteGovActivityTypeModel').modal('show');

            });
        },
        methods:{
            deleteGovActivityType:function(){
                //vc.component.deleteGovActivityTypeInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govActivityType/deleteGovActivityType',
                    JSON.stringify(vc.component.deleteGovActivityTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovActivityTypeModel').modal('hide');
                            vc.emit('govActivityTypeManage','listGovActivityType',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovActivityTypeModel:function(){
                $('#deleteGovActivityTypeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
