/**
    小区位置 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovCommunityLocationInfo:{
                index:0,
                flowComponent:'viewGovCommunityLocationInfo',
                govCommunityId:'',
name:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCommunityLocationInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovCommunityLocationInfo','chooseGovCommunityLocation',function(_app){
                vc.copyObject(_app, vc.component.viewGovCommunityLocationInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovCommunityLocationInfo);
            });

            vc.on('viewGovCommunityLocationInfo', 'onIndex', function(_index){
                vc.component.viewGovCommunityLocationInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovCommunityLocationInfoModel(){
                vc.emit('chooseGovCommunityLocation','openChooseGovCommunityLocationModel',{});
            },
            _openAddGovCommunityLocationInfoModel(){
                vc.emit('addGovCommunityLocation','openAddGovCommunityLocationModal',{});
            },
            _loadGovCommunityLocationInfoData:function(){

            }
        }
    });

})(window.vc);
