(function(vc,vm){

    vc.extends({
        data:{
            deleteGovFollowupSurveyInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovFollowupSurvey','openDeleteGovFollowupSurveyModal',function(_params){

                vc.component.deleteGovFollowupSurveyInfo = _params;
                $('#deleteGovFollowupSurveyModel').modal('show');

            });
        },
        methods:{
            deleteGovFollowupSurvey:function(){
                vc.component.deleteGovFollowupSurveyInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govFollowupSurvey/deleteGovFollowupSurvey',
                    JSON.stringify(vc.component.deleteGovFollowupSurveyInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovFollowupSurveyModel').modal('hide');
                            vc.emit('govFollowupSurveyManage','listGovFollowupSurvey',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovFollowupSurveyModel:function(){
                $('#deleteGovFollowupSurveyModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
