/**
    障碍者 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovMentalDisordersInfo:{
                index:0,
                flowComponent:'viewGovMentalDisordersInfo',
                disordersId:'',
name:'',
caId:'',
address:'',
tel:'',
disordersStartTime:'',
disordersReason:'',
emergencyPerson:'',
emergencyTel:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMentalDisordersInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovMentalDisordersInfo','chooseGovMentalDisorders',function(_app){
                vc.copyObject(_app, vc.component.viewGovMentalDisordersInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovMentalDisordersInfo);
            });

            vc.on('viewGovMentalDisordersInfo', 'onIndex', function(_index){
                vc.component.viewGovMentalDisordersInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovMentalDisordersInfoModel(){
                vc.emit('chooseGovMentalDisorders','openChooseGovMentalDisordersModel',{});
            },
            _openAddGovMentalDisordersInfoModel(){
                vc.emit('addGovMentalDisorders','openAddGovMentalDisordersModal',{});
            },
            _loadGovMentalDisordersInfoData:function(){

            }
        }
    });

})(window.vc);
