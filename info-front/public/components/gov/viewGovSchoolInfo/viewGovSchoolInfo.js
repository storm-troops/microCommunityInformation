/**
    学校 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovSchoolInfo:{
                index:0,
                flowComponent:'viewGovSchoolInfo',
                schoolId:'',
caId:'',
schoolName:'',
schoolAddress:'',
schoolType:'',
areaCode:'',
studentNum:'',
schoolmaster:'',
masterTel:'',
partSafetyLeader:'',
partLeaderTel:'',
safetyLeader:'',
leaderTel:'',
securityLeader:'',
securityLeaderTel:'',
securityPersonnelNum:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovSchoolInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovSchoolInfo','chooseGovSchool',function(_app){
                vc.copyObject(_app, vc.component.viewGovSchoolInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovSchoolInfo);
            });

            vc.on('viewGovSchoolInfo', 'onIndex', function(_index){
                vc.component.viewGovSchoolInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovSchoolInfoModel(){
                vc.emit('chooseGovSchool','openChooseGovSchoolModel',{});
            },
            _openAddGovSchoolInfoModel(){
                vc.emit('addGovSchool','openAddGovSchoolModal',{});
            },
            _loadGovSchoolInfoData:function(){

            }
        }
    });

})(window.vc);
