/**
    建筑分类 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovBuildingTypeInfo: {
                index: 0,
                flowComponent: 'viewGovBuildingTypeInfo',
                typeName: '',
                caName: '',
                remark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovBuildingTypeInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovBuildingTypeInfo', 'chooseGovBuildingType', function (_app) {
                vc.copyObject(_app, vc.component.viewGovBuildingTypeInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovBuildingTypeInfo);
            });

            vc.on('viewGovBuildingTypeInfo', 'onIndex', function (_index) {
                vc.component.viewGovBuildingTypeInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovBuildingTypeInfoModel() {
                vc.emit('chooseGovBuildingType', 'openChooseGovBuildingTypeModel', {});
            },
            _openAddGovBuildingTypeInfoModel() {
                vc.emit('addGovBuildingType', 'openAddGovBuildingTypeModal', {});
            },
            _loadGovBuildingTypeInfoData: function () {

            }
        }
    });

})(window.vc);
