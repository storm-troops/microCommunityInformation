(function (vc, vm) {

    vc.extends({
        data: {
            editGovGridObjRelInfo: {
                govGridObjId: '',
                govGridId: '',
                objTypeCd: '',
                objTypeCds: [],
                caId: '',
                objId: '',
                objName: '',
                startTime: '',
                endTime: '',
                workStatus: '',
                ramark: '',
                govCommunitys: [],
                govFloors: [],
                communityId: '',
                floorId: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovGridObjRel', 'openEditGovGridObjRelModal', function (_params) {
                vc.component.refreshEditGovGridObjRelInfo();
                $('#editGovGridObjRelModel').modal('show');
                vc.copyObject(_params, vc.component.editGovGridObjRelInfo);
                vc.component.editGovGridObjRelInfo.caId = vc.getCurrentCommunity().caId;
                if ($that.editGovGridObjRelInfo.objTypeCd == '9002') {
                    $that.editGovGridObjRelInfo.communityId = $that.editGovGridObjRelInfo.objId;
                }
                if ($that.editGovGridObjRelInfo.objTypeCd == '9003') {
                    $that.editGovGridObjRelInfo.floorId = $that.editGovGridObjRelInfo.objId;
                    $that._listEditGovFloorLoad($that.editGovGridObjRelInfo.floorId);
                }
                vc.getDict('gov_grid_obj_rel', "obj_type_cd", function (_data) {
                    vc.component.editGovGridObjRelInfo.objTypeCds = _data;
                });
                $that._listEditGovCommunitys();
                vc.initDate('startTimes', function (_value) {
                    $that.editGovGridObjRelInfo.startTime = _value;
                });
                vc.initDate('endTimes', function (_value) {
                    $that.editGovGridObjRelInfo.endTime = _value;
                });
            });
        },
        methods: {
            editGovGridObjRelValidate: function () {
                return vc.validate.validate({
                    editGovGridObjRelInfo: vc.component.editGovGridObjRelInfo
                }, {
                    'editGovGridObjRelInfo.govGridObjId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "网格对象编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "网格对象编号不能超过30"
                        },
                    ],
                    'editGovGridObjRelInfo.govGridId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "网格员编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "网格员编号不能超过30"
                        },
                    ],
                    'editGovGridObjRelInfo.objTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象类型编码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "对象类型编码不能超过11"
                        },
                    ],
                    'editGovGridObjRelInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovGridObjRelInfo.objId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "对象ID不能超过30"
                        },
                    ],
                    'editGovGridObjRelInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'editGovGridObjRelInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结束时间不能超过时间类型"
                        },
                    ],
                    'editGovGridObjRelInfo.workStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "工作状态不能超过11"
                        },
                    ]
                });
            },
            editGovGridObjRel: function () {
                if (!vc.component.editGovGridObjRelValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govGridObjRel/updateGovGridObjRel',
                    JSON.stringify(vc.component.editGovGridObjRelInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovGridObjRelModel').modal('hide');
                            vc.emit('govGridObjRelManage', 'listGovGridObjRel', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovGridObjRelInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovFloors: function (_GovCommunityId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        govCommunityId: _GovCommunityId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        vc.component.editGovGridObjRelInfo.govFloors = _govFloorManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovFloorLoad: function (_govFloorId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        govFloorId: _govFloorId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        $that.editGovGridObjRelInfo.communityId = _govFloorManageInfo.data[0].govCommunityId;
                        $that._listEditGovFloors($that.editGovGridObjRelInfo.communityId);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            selectEditObj: function (_objTypeCd) {
                if (!_objTypeCd) {
                    return;
                }
                $that.editGovGridObjRelInfo.objId = '';
                if (_objTypeCd == '9001') {
                    $that.editGovGridObjRelInfo.objId = vc.getCurrentCommunity().caId;
                    $that.editGovGridObjRelInfo.objName = vc.getCurrentCommunity().caName;
                }
            },
            selecEditFloor: function (_communityId) {
                if (!_communityId) {
                    return;
                }
                $that.editGovGridObjRelInfo.objId = '';
                $that.editGovGridObjRelInfo.objName = '';
                if ($that.editGovGridObjRelInfo.objTypeCd == '9002') {

                    $that.editGovGridObjRelInfo.objId = _communityId;
                    $that.editGovGridObjRelInfo.govCommunitys.forEach(_item => {
                        if (_item.govCommunityId == _communityId) {
                            $that.editGovGridObjRelInfo.objName = _item.communityName;
                        }
                    });


                } else if ($that.editGovGridObjRelInfo.objTypeCd == '9003') {
                    $that._listEditGovFloors(_communityId);
                }
            },
            setEditObj: function (_floorId) {
                if (!_floorId) {
                    return;
                }
                $that.editGovGridObjRelInfo.objId = '';
                $that.editGovGridObjRelInfo.objId = _floorId;
                $that.editGovGridObjRelInfo.govFloors.forEach(_item => {
                    if (_item.govFloorId == _floorId) {
                        $that.editGovGridObjRelInfo.objName = _item.floorName;
                    }
                });
            },
            refreshEditGovGridObjRelInfo: function () {
                let _govCommunitys = vc.component.editGovGridObjRelInfo.govCommunitys;
                let _objTypeCds = vc.component.editGovGridObjRelInfo.objTypeCds;
                let _govFloors = vc.component.editGovGridObjRelInfo.govFloors;

                vc.component.editGovGridObjRelInfo = {
                    govGridObjId: '',
                    govGridId: '',
                    objTypeCd: '',
                    objTypeCds: _objTypeCds,
                    caId: '',
                    objId: '',
                    objName: '',
                    startTime: '',
                    endTime: '',
                    workStatus: '',
                    ramark: '',
                    govCommunitys: _govCommunitys,
                    govFloors: _govFloors,
                    communityId: '',
                    floorId: ''
                }
            }
        }
    });

})(window.vc, window.vc.component);
