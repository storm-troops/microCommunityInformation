(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovSchoolPeripheralPerson:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovSchoolPeripheralPersonInfo:{
                govSchoolPeripheralPersons:[],
                _currentGovSchoolPeripheralPersonName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovSchoolPeripheralPerson','openChooseGovSchoolPeripheralPersonModel',function(_param){
                $('#chooseGovSchoolPeripheralPersonModel').modal('show');
                vc.component._refreshChooseGovSchoolPeripheralPersonInfo();
                vc.component._loadAllGovSchoolPeripheralPersonInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovSchoolPeripheralPersonInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govSchoolPeripheralPerson/queryGovSchoolPeripheralPerson',
                             param,
                             function(json){
                                var _govSchoolPeripheralPersonInfo = JSON.parse(json);
                                vc.component.chooseGovSchoolPeripheralPersonInfo.govSchoolPeripheralPersons = _govSchoolPeripheralPersonInfo.govSchoolPeripheralPersons;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovSchoolPeripheralPerson:function(_govSchoolPeripheralPerson){
                if(_govSchoolPeripheralPerson.hasOwnProperty('name')){
                     _govSchoolPeripheralPerson.govSchoolPeripheralPersonName = _govSchoolPeripheralPerson.name;
                }
                vc.emit($props.emitChooseGovSchoolPeripheralPerson,'chooseGovSchoolPeripheralPerson',_govSchoolPeripheralPerson);
                vc.emit($props.emitLoadData,'listGovSchoolPeripheralPersonData',{
                    govSchoolPeripheralPersonId:_govSchoolPeripheralPerson.govSchoolPeripheralPersonId
                });
                $('#chooseGovSchoolPeripheralPersonModel').modal('hide');
            },
            queryGovSchoolPeripheralPersons:function(){
                vc.component._loadAllGovSchoolPeripheralPersonInfo(1,10,vc.component.chooseGovSchoolPeripheralPersonInfo._currentGovSchoolPeripheralPersonName);
            },
            _refreshChooseGovSchoolPeripheralPersonInfo:function(){
                vc.component.chooseGovSchoolPeripheralPersonInfo._currentGovSchoolPeripheralPersonName = "";
            }
        }

    });
})(window.vc);
