(function (vc) {
    vc.extends({
        data: {
            viewPersonLabelInfo: {
                orgId: '',
                govPersonId: '',
                simplifyAcceptanceInfo: {},
                labels: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('viewPersonLabel', 'switch', function (_param) {
                vc.component._refreshviewGovPartyMemberInfo();
                console.log(_param);
                vc.component._loadPersonLabelsInfo(_param.govPersonId);
            });
            vc.on('viewPersonLabelInit', 'switch', function (_param) {
                vc.component._refreshviewGovPartyMemberInfo();
                console.log(_param);
                $that.viewPersonLabelInfo.simplifyAcceptanceInfo = _param.govPersonId;
                vc.component._loadPersonLabelsInfo(_param.govPersonId.govPersonId);
            });
        },
        methods: {
            _loadPersonLabelsInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        govPersonId: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govPersonLabelRel/queryGovPersonLabelRel',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewPersonLabelInfo.labels = _labelInfo.data;
                        _labelInfo.data.forEach(element => {
                            if('6006' == element.labelCd){
                                $that.viewPersonLabelInfo.simplifyAcceptanceInfo.labelCd = element.labelCd;
                            }else if ('6007' == element.labelCd){
                                $that.viewPersonLabelInfo.simplifyAcceptanceInfo.labelCd = element.labelCd;
                            }else if ('6009' == element.labelCd){
                                $that.viewPersonLabelInfo.simplifyAcceptanceInfo.labelCd = element.labelCd;
                            }else if ('6010' == element.labelCd){
                                $that.viewPersonLabelInfo.simplifyAcceptanceInfo.labelCd = element.labelCd;
                            }
                        });
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _openPersonLabelsInfo: function (_govMember) {
                
            },
            _refreshviewGovPartyMemberInfo: function () {
                $that.viewPersonLabelInfo = {
                    orgId: '',
                    govPersonId: '',
                    labels: []
                }
            }
        }

    });
})(window.vc);
