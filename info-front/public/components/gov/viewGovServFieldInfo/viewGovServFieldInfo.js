/**
    服务领域 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovServFieldInfo:{
                index:0,
                flowComponent:'viewGovServFieldInfo',
                servId:'',
caId:'',
name:'',
isShow:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovServFieldInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovServFieldInfo','chooseGovServField',function(_app){
                vc.copyObject(_app, vc.component.viewGovServFieldInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovServFieldInfo);
            });

            vc.on('viewGovServFieldInfo', 'onIndex', function(_index){
                vc.component.viewGovServFieldInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovServFieldInfoModel(){
                vc.emit('chooseGovServField','openChooseGovServFieldModel',{});
            },
            _openAddGovServFieldInfoModel(){
                vc.emit('addGovServField','openAddGovServFieldModal',{});
            },
            _loadGovServFieldInfoData:function(){

            }
        }
    });

})(window.vc);
