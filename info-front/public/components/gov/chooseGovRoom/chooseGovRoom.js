(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovRoom: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovRoomInfo: {
                govRooms: [],
                _currentGovRoomName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovRoom', 'openChooseGovRoomModel', function (_param) {
                $('#chooseGovRoomModel').modal('show');
                vc.component._refreshChooseGovRoomInfo();
                vc.component._loadAllGovRoomInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovRoomInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        communityId: vc.getCurrentCommunity().communityId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('govRoom.listGovRooms',
                    param,
                    function (json) {
                        var _govRoomInfo = JSON.parse(json);
                        vc.component.chooseGovRoomInfo.govRooms = _govRoomInfo.govRooms;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovRoom: function (_govRoom) {
                if (_govRoom.hasOwnProperty('name')) {
                    _govRoom.govRoomName = _govRoom.name;
                }
                vc.emit($props.emitChooseGovRoom, 'chooseGovRoom', _govRoom);
                vc.emit($props.emitLoadData, 'listGovRoomData', {
                    govRoomId: _govRoom.govRoomId
                });
                $('#chooseGovRoomModel').modal('hide');
            },
            queryGovRooms: function () {
                vc.component._loadAllGovRoomInfo(1, 10, vc.component.chooseGovRoomInfo._currentGovRoomName);
            },
            _refreshChooseGovRoomInfo: function () {
                vc.component.chooseGovRoomInfo._currentGovRoomName = "";
            }
        }

    });
})(window.vc);
