(function (vc, vm) {

    vc.extends({
        data: {
            editGovSchoolInfo: {
                schoolId: '',
                caId: '',
                schoolName: '',
                schoolAddress: '',
                schoolType: '',
                areaCode: '',
                areaName: '',
                studentNum: '',
                schoolmaster: '',
                masterTel: '',
                partSafetyLeader: '',
                partLeaderTel: '',
                safetyLeader: '',
                leaderTel: '',
                securityLeader: '',
                securityLeaderTel: '',
                securityPersonnelNum: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovSchool', 'openEditGovSchoolModal', function (_params) {
                vc.component.refreshEditGovSchoolInfo();
                $('#editGovSchoolModel').modal('show');
                vc.copyObject(_params, vc.component.editGovSchoolInfo);
                vc.component.editGovSchoolInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovSchoolValidate: function () {
                return vc.validate.validate({
                    editGovSchoolInfo: vc.component.editGovSchoolInfo
                }, {
                    'editGovSchoolInfo.schoolName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "60",
                            errInfo: "学校名称不能超过60"
                        },
                    ],
                    'editGovSchoolInfo.schoolAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "学校地址不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.schoolType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校办学类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "学校办学类型不能超过3"
                        },
                    ],
                    'editGovSchoolInfo.areaCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属行政区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "所属行政区不能超过3"
                        },
                    ],
                    'editGovSchoolInfo.studentNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "在校学生数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "6",
                            errInfo: "在校学生数不能超过6"
                        },
                    ],
                    'editGovSchoolInfo.schoolmaster': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "校长姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "校长姓名不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.masterTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "校长联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "校长联系方式不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.partSafetyLeader': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管安保负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分管安保负责人不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.partLeaderTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管安保负责人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分管安保负责人联系电话不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.safetyLeader': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "安保负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "安保负责人不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.leaderTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "安保负责人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "安保负责人联系电话不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.securityLeader': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "治安负责人不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.securityLeaderTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安负责人联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "治安负责人联系方式不能超过30"
                        },
                    ],
                    'editGovSchoolInfo.securityPersonnelNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "安全保卫人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "安全保卫人数不能超过30"
                        },
                    ]
                });
            },
            editGovSchool: function () {
                if (!vc.component.editGovSchoolValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govSchool/updateGovSchool',
                    JSON.stringify(vc.component.editGovSchoolInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovSchoolModel').modal('hide');
                            vc.emit('govSchoolManage', 'listGovSchool', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovSchoolInfo: function () {
                vc.component.editGovSchoolInfo = {
                    schoolId: '',
                    caId: '',
                    schoolName: '',
                    schoolAddress: '',
                    schoolType: '',
                    areaCode: '',
                    studentNum: '',
                    schoolmaster: '',
                    masterTel: '',
                    partSafetyLeader: '',
                    partLeaderTel: '',
                    safetyLeader: '',
                    leaderTel: '',
                    securityLeader: '',
                    securityLeaderTel: '',
                    securityPersonnelNum: '',
                    statusCd: '',
                    areaName: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
