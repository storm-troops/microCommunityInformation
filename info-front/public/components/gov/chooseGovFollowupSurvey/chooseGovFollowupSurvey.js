(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovFollowupSurvey:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovFollowupSurveyInfo:{
                govFollowupSurveys:[],
                _currentGovFollowupSurveyName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovFollowupSurvey','openChooseGovFollowupSurveyModel',function(_param){
                $('#chooseGovFollowupSurveyModel').modal('show');
                vc.component._refreshChooseGovFollowupSurveyInfo();
                vc.component._loadAllGovFollowupSurveyInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovFollowupSurveyInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govFollowupSurvey/queryGovFollowupSurvey',
                             param,
                             function(json){
                                var _govFollowupSurveyInfo = JSON.parse(json);
                                vc.component.chooseGovFollowupSurveyInfo.govFollowupSurveys = _govFollowupSurveyInfo.govFollowupSurveys;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovFollowupSurvey:function(_govFollowupSurvey){
                if(_govFollowupSurvey.hasOwnProperty('name')){
                     _govFollowupSurvey.govFollowupSurveyName = _govFollowupSurvey.name;
                }
                vc.emit($props.emitChooseGovFollowupSurvey,'chooseGovFollowupSurvey',_govFollowupSurvey);
                vc.emit($props.emitLoadData,'listGovFollowupSurveyData',{
                    govFollowupSurveyId:_govFollowupSurvey.govFollowupSurveyId
                });
                $('#chooseGovFollowupSurveyModel').modal('hide');
            },
            queryGovFollowupSurveys:function(){
                vc.component._loadAllGovFollowupSurveyInfo(1,10,vc.component.chooseGovFollowupSurveyInfo._currentGovFollowupSurveyName);
            },
            _refreshChooseGovFollowupSurveyInfo:function(){
                vc.component.chooseGovFollowupSurveyInfo._currentGovFollowupSurveyName = "";
            }
        }

    });
})(window.vc);
