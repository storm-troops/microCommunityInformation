(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovEventsType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovEventsTypeInfo:{
                govEventsTypes:[],
                _currentGovEventsTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovEventsType','openChooseGovEventsTypeModel',function(_param){
                $('#chooseGovEventsTypeModel').modal('show');
                vc.component._refreshChooseGovEventsTypeInfo();
                vc.component._loadAllGovEventsTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovEventsTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govEventsType/queryGovEventsType',
                             param,
                             function(json){
                                var _govEventsTypeInfo = JSON.parse(json);
                                vc.component.chooseGovEventsTypeInfo.govEventsTypes = _govEventsTypeInfo.govEventsTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovEventsType:function(_govEventsType){
                if(_govEventsType.hasOwnProperty('name')){
                     _govEventsType.govEventsTypeName = _govEventsType.name;
                }
                vc.emit($props.emitChooseGovEventsType,'chooseGovEventsType',_govEventsType);
                vc.emit($props.emitLoadData,'listGovEventsTypeData',{
                    govEventsTypeId:_govEventsType.govEventsTypeId
                });
                $('#chooseGovEventsTypeModel').modal('hide');
            },
            queryGovEventsTypes:function(){
                vc.component._loadAllGovEventsTypeInfo(1,10,vc.component.chooseGovEventsTypeInfo._currentGovEventsTypeName);
            },
            _refreshChooseGovEventsTypeInfo:function(){
                vc.component.chooseGovEventsTypeInfo._currentGovEventsTypeName = "";
            }
        }

    });
})(window.vc);
