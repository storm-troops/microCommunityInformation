/**
    党内职务 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovWorkTypeInfo:{
                index:0,
                flowComponent:'viewGovWorkTypeInfo',
                workTypeName:'',
state:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovWorkTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovWorkTypeInfo','chooseGovWorkType',function(_app){
                vc.copyObject(_app, vc.component.viewGovWorkTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovWorkTypeInfo);
            });

            vc.on('viewGovWorkTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovWorkTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovWorkTypeInfoModel(){
                vc.emit('chooseGovWorkType','openChooseGovWorkTypeModel',{});
            },
            _openAddGovWorkTypeInfoModel(){
                vc.emit('addGovWorkType','openAddGovWorkTypeModal',{});
            },
            _loadGovWorkTypeInfoData:function(){

            }
        }
    });

})(window.vc);
