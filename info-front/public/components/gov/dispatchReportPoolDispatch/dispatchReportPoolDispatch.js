(function (vc) {
    vc.extends({
        data: {
            dispatchReportPoolDispatchInfo: {
                reportId: '',
                ruId: '',
                staffId: '',
                staffName: '',
                context: '',
                action: '',
                govReportUsers: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('dispatchReportPoolDispatch', 'opendispatchReportPoolDispatchModal', function (_pool) {
                $that.dispatchReportPoolDispatchInfo.ruId = _pool.ruId;
                $that.dispatchReportPoolDispatchInfo.reportId = _pool.reportId;
                $that.dispatchReportPoolDispatchInfo.action = _pool.action;
                $that._listgovReportUsers();
                if (_pool.hasOwnProperty('action') && _pool.action == 'BACK') {
                    $that.dispatchReportPoolDispatchInfo.staffId = _pool.preStaffId;
                    $that.dispatchReportPoolDispatchInfo.staffName = _pool.preStaffName;
                }
                $('#dispatchReportPoolDispatchModel').modal('show');
            });
        },
        methods: {
            dispatchReportPoolDispatchValidate() {
                return vc.validate.validate({
                    dispatchReportPoolDispatchInfo: vc.component.dispatchReportPoolDispatchInfo
                }, {
                    'dispatchReportPoolDispatchInfo.reportId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报修单不能为空"
                        }
                    ],
                    'dispatchReportPoolDispatchInfo.staffId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报修师傅不能为空"
                        }
                    ],
                    'dispatchReportPoolDispatchInfo.staffName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报修师傅不能为空"
                        }
                    ],
                    'dispatchReportPoolDispatchInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "处理意见不能为空"
                        }
                    ]
                });
            },
            _dispatchReportPoolDispatchInfo: function () {
                $that.dispatchReportPoolDispatchInfo.govReportUsers.forEach(item => {
                    if (item.staffId == $that.dispatchReportPoolDispatchInfo.staffId) {
                        $that.dispatchReportPoolDispatchInfo.staffName = item.staffName;
                    }
                });
                if (!vc.component.dispatchReportPoolDispatchValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.component.dispatchReportPoolDispatchInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govReportUser/poolDispatch',
                    JSON.stringify(vc.component.dispatchReportPoolDispatchInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#dispatchReportPoolDispatchModel').modal('hide');
                            vc.component.cleardispatchReportPoolDispatchInfo();
                            vc.emit('reportPoolManage', 'listReportPool', {});
                            vc.emit('reportPoolDispatchManage', 'listHousekeepingPool', {});
                            vc.toast("操作成功");
                        } else if (_json.code == 404) {
                            vc.toast(_json.msg);
                        }
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            cleardispatchReportPoolDispatchInfo: function () {
                vc.component.dispatchReportPoolDispatchInfo = {
                    reportId: '',
                    ruId: '',
                    staffId: '',
                    staffName: '',
                    context: '',
                    action: '',
                    govReportUsers: []
                };
            },
            _listgovReportUsers: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        ruId: $that.dispatchReportPoolDispatchInfo.reportType,
                        state: ''
                    }
                };
                //发送get请求
                vc.http.apiGet('/govReportTypeUser/queryGovReportTypeUser',
                    param,
                    function (json, res) {
                        var _poolTypeUserManageInfo = JSON.parse(json);
                        vc.component.dispatchReportPoolDispatchInfo.govReportUsers = _poolTypeUserManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
        }
    });
})(window.vc);
