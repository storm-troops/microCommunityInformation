(function (vc, vm) {

    vc.extends({
        data: {
            editGovHelpPolicyInfo: {
                govHelpId: '',
                caId: '',
                helpName: '',
                helpTime: '',
                helpBrief: ''
            }
        },
        _initMethod: function () {
            $that._initEditNoticeInfo();
        },
        _initEvent: function () {
            vc.on('editGovHelpPolicy', 'openEditGovHelpPolicyModal', function (_params) {
                vc.component.refreshEditGovHelpPolicyInfo();
                $('#editGovHelpPolicyModel').modal('show');
                vc.copyObject(_params, vc.component.editGovHelpPolicyInfo);
                vc.component.editGovHelpPolicyInfo.caId = vc.getCurrentCommunity().caId;
                $(".eidtSummernote").summernote('code', vc.component.editGovHelpPolicyInfo.helpBrief);
            });
        },
        methods: {
            editGovHelpPolicyValidate: function () {
                return vc.validate.validate({
                    editGovHelpPolicyInfo: vc.component.editGovHelpPolicyInfo
                }, {
                    'editGovHelpPolicyInfo.govHelpId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶政策ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "帮扶政策ID不能超过30"
                        },
                    ],
                    'editGovHelpPolicyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovHelpPolicyInfo.helpName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "政策名称不能超过64"
                        },
                    ],
                    'editGovHelpPolicyInfo.helpTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'editGovHelpPolicyInfo.helpBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "政策内容不能超过1024"
                        },
                    ]

                });
            },
            editGovHelpPolicy: function () {
                if (!vc.component.editGovHelpPolicyValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govHelpPolicy/updateGovHelpPolicy',
                    JSON.stringify(vc.component.editGovHelpPolicyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovHelpPolicyModel').modal('hide');
                            vc.emit('govHelpPolicyManage', 'listGovHelpPolicy', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _initEditNoticeInfo: function () {

                var $summernote = $('.eidtSummernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入政策内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendEditFile($summernote, files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.editGovHelpPolicyInfo.helpBrief = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendEditFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            closeEdiGovHelpPolicy: function () {
                $that.refreshEditGovHelpPolicyInfo();
                vc.emit('govHelpPolicyManage', 'listGovHelpPolicy', {});

            },
            refreshEditGovHelpPolicyInfo: function () {
                vc.component.editGovHelpPolicyInfo = {
                    govHelpId: '',
                    caId: '',
                    helpName: '',
                    helpTime: '',
                    helpBrief: ''
                }
            }
        }
    });

})(window.vc, window.vc.component);
