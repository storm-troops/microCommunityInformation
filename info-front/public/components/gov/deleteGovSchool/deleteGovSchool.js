(function(vc,vm){

    vc.extends({
        data:{
            deleteGovSchoolInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovSchool','openDeleteGovSchoolModal',function(_params){

                vc.component.deleteGovSchoolInfo = _params;
                $('#deleteGovSchoolModel').modal('show');

            });
        },
        methods:{
            deleteGovSchool:function(){
                vc.component.deleteGovSchoolInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govSchool/deleteGovSchool',
                    JSON.stringify(vc.component.deleteGovSchoolInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovSchoolModel').modal('hide');
                            vc.emit('govSchoolManage','listGovSchool',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovSchoolModel:function(){
                $('#deleteGovSchoolModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
