(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovHealth: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovHealthInfo: {
                govHealths: [],
                _currentGovHealthName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovHealth', 'openChooseGovHealthModel', function (_param) {
                $('#chooseGovHealthModel').modal('show');
                vc.component._refreshChooseGovHealthInfo();
                console.log(_param);
                $that.chooseGovHealthInfo.govPersonId=_param._questionAnswer.govPersonId;
                vc.component._loadAllGovHealthInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovHealthInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHealth/queryGovHealth',
                    param,
                    function (json) {
                        var _govHealthInfo = JSON.parse(json);
                        vc.component.chooseGovHealthInfo.total = _govHealthInfo.total;
                        vc.component.chooseGovHealthInfo.records = _govHealthInfo.records;
                        vc.component.chooseGovHealthInfo.govHealths = _govHealthInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.chooseGovHealthInfo.records,
                            currentPage: _page
                        });
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovHealth: function (_govHealth) {
                if (_govHealth.hasOwnProperty('name')) {
                    _govHealth.govHealthName = _govHealth.name;
                }
                _govHealth.govPersonId = $that.chooseGovHealthInfo.govPersonId;
                vc.emit($props.emitChooseGovHealth, 'chooseGovHealth', _govHealth);
                vc.emit($props.emitLoadData, 'listGovHealthData', {
                    govHealthId: _govHealth.govHealthId
                });
                $('#chooseGovHealthModel').modal('hide');
            },
            queryGovHealths: function () {
                vc.component._loadAllGovHealthInfo(1, 10, vc.component.chooseGovHealthInfo._currentGovHealthName);
            },
            _refreshChooseGovHealthInfo: function () {
                vc.component.chooseGovHealthInfo._currentGovHealthName = "";
            }
        }

    });
})(window.vc);
