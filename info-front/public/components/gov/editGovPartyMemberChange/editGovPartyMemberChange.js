(function (vc, vm) {

    vc.extends({
        data: {
            editGovPartyMemberChangeInfo: {
                govChangeId: '',
                caId: '',
                govMemberId: '',
                personName: '',
                srcOrgId: '',
                srcOrgName: '',
                targetOrgId: '',
                targetOrgName: '',
                chagneType: '',
                ramark: '',
                state: '1102',
                govPartyOrgs: [],
                govCommunityAreas: []

            }
        },
        _initMethod: function () {
            $that._listEditGovPartyOrgs();
            $that._listEditGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('editGovPartyMemberChange', 'openEditGovPartyMemberChangeModal', function (_params) {
                vc.component.refreshEditGovPartyMemberChangeInfo();
                $('#editGovPartyMemberChangeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPartyMemberChangeInfo);
            });
            vc.on('openChooseGovPartyMember', 'chooseGovPartyMember', function (_param) {
                $that.editGovPartyMemberChangeInfo.govMemberId = _param.govMemberId;
                $that.editGovPartyMemberChangeInfo.personName = _param.personName;
            });
        },
        methods: {
            editGovPartyMemberChangeValidate: function () {
                return vc.validate.validate({
                    editGovPartyMemberChangeInfo: vc.component.editGovPartyMemberChangeInfo
                }, {
                    'editGovPartyMemberChangeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.govMemberId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "党员名称超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "党员名称超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.srcOrgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "源组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "源组织名称超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.srcOrgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "源组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "源组织名称超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.targetOrgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "目标组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "目标组织名称超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.targetOrgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "目标组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "目标组织名称超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.chagneType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党关系类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "党关系类型超长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovPartyMemberChangeInfo.govChangeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党关系管理ID不能为空"
                        }]

                });
            },
            editGovPartyMemberChange: function () {
                if (!vc.component.editGovPartyMemberChangeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govPartyMemberChange/updateGovPartyMemberChange',
                    JSON.stringify(vc.component.editGovPartyMemberChangeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPartyMemberChangeModel').modal('hide');
                            vc.emit('govPartyMemberChangeManage', 'listGovPartyMemberChange', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _closeEditGovPartyMemberChange: function () {
                $that.refreshEditGovPartyMemberChangeInfo();
                vc.emit('govPartyMemberChangeManage', 'listGovPartyMemberChange', {});
            },
            _listEditGovPartyOrgs: function (_page, _rows) {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovPartyMemberChangeInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.editGovPartyMemberChangeInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.editGovPartyMemberChangeInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovPartyMemberChangeInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovPartyMemberChangeInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovPartyMemberChangeInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openEditChooseGovPartyMember: function (_caId) {
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('chooseGovPartyMember', 'openChooseGovPartyMemberModel', $that.editGovPartyMemberChangeInfo);
            },
            _setEditPartySrcOrgName : function(_srcOrgId){
                $that.editGovPartyMemberChangeInfo.govPartyOrgs.forEach(item => {
                    if (item.orgId == _srcOrgId) {
                        $that.editGovPartyMemberChangeInfo.srcOrgName = item.orgName;
                    }
                });
            },
            _setEditPartyTargetOrgName : function(_targetOrgId){
                $that.editGovPartyMemberChangeInfo.govPartyOrgs.forEach(item => {
                    if (item.orgId == _targetOrgId) {
                        $that.editGovPartyMemberChangeInfo.targetOrgName = item.orgName;
                    }
                });
            },
            refreshEditGovPartyMemberChangeInfo: function () {
                let _govPartyOrgs = vc.component.editGovPartyMemberChangeInfo.govPartyOrgs;
                let _govCommunityAreas = vc.component.editGovPartyMemberChangeInfo.govCommunityAreas;
                vc.component.editGovPartyMemberChangeInfo = {
                    govChangeId: '',
                    caId: '',
                    govMemberId: '',
                    personName: '',
                    srcOrgId: '',
                    srcOrgName: '',
                    targetOrgId: '',
                    targetOrgName: '',
                    chagneType: '',
                    ramark: '',
                    state: '1102',
                    govPartyOrgs: _govPartyOrgs,
                    govCommunityAreas: _govCommunityAreas
                }
            }
        }
    });

})(window.vc, window.vc.component);
