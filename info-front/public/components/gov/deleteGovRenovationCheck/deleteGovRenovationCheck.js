(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovRenovationCheckInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovRenovationCheck', 'openDeleteGovRenovationCheckModal', function (_params) {

                vc.component.deleteGovRenovationCheckInfo = _params;
                $('#deleteGovRenovationCheckModel').modal('show');

            });
        },
        methods: {
            deleteGovRenovationCheck: function () {
                vc.component.deleteGovRenovationCheckInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govRenovationCheck/deleteGovRenovationCheck',
                    JSON.stringify(vc.component.deleteGovRenovationCheckInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovRenovationCheckModel').modal('hide');
                            vc.emit('govRenovationCheckManage', 'listGovRenovationCheck', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovRenovationCheckModel: function () {
                $('#deleteGovRenovationCheckModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
