(function (vc, vm) {

    vc.extends({
        data: {
            editGovSchoolPeripheralPersonInfo: {
                schPersonId: '',
                schoolId: '',
                govPersonId: '',
                personName: '',
                schoolName: '',
                caId: '',
                remark: '',
                extentInjury: '',
                isAttention: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovSchoolPeripheralPerson', 'openEditGovSchoolPeripheralPersonModal', function (_params) {
                vc.component.refreshEditGovSchoolPeripheralPersonInfo();
                $('#editGovSchoolPeripheralPersonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovSchoolPeripheralPersonInfo);
                vc.component.editGovSchoolPeripheralPersonInfo.caId = vc.getCurrentCommunity().caId;
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                $that.editGovSchoolPeripheralPersonInfo.govPersonId = _param.govPersonId;
                $that.editGovSchoolPeripheralPersonInfo.personName = _param.personName;
            });
        },
        methods: {
            editGovSchoolPeripheralPersonValidate: function () {
                return vc.validate.validate({
                    editGovSchoolPeripheralPersonInfo: vc.component.editGovSchoolPeripheralPersonInfo
                }, {
                    'editGovSchoolPeripheralPersonInfo.schPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "校园周边重点人员id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "校园周边重点人员id不能超过30"
                        },
                    ],
                    'editGovSchoolPeripheralPersonInfo.schoolId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "学校ID不能超过30"
                        },
                    ],
                    'editGovSchoolPeripheralPersonInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口编号不能超过30"
                        },
                    ],
                    'editGovSchoolPeripheralPersonInfo.schoolName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "60",
                            errInfo: "学校名称不能超过60"
                        },
                    ],
                    'editGovSchoolPeripheralPersonInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovSchoolPeripheralPersonInfo.extentInjury': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "危害程度不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "危害程度不能超过3"
                        },
                    ],
                    'editGovSchoolPeripheralPersonInfo.isAttention': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否关注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "是否关注不能超过3"
                        },
                    ]
                });
            },
            editGovSchoolPeripheralPerson: function () {
                if (!vc.component.editGovSchoolPeripheralPersonValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govSchoolPeripheralPerson/updateGovSchoolPeripheralPerson',
                    JSON.stringify(vc.component.editGovSchoolPeripheralPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovSchoolPeripheralPersonModel').modal('hide');
                            vc.emit('govSchoolPeripheralPersonManage', 'listGovSchoolPeripheralPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _setEditSchool : function(_schoolId){
                $that.govSchoolPeripheralPersonManageInfo.govSchools.forEach(item => {
                    if (item.schoolId == _schoolId) {
                        $that.editGovSchoolPeripheralPersonInfo.schoolName = item.schoolName;
                    }
                });
            },
            _openEditChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeEditGovSchoolPeripheral: function () {
                $that.refreshEditGovSchoolPeripheralPersonInfo();
                vc.emit('govSchoolPeripheralPersonManage', 'listGovSchoolPeripheralPerson', {});
            },
            refreshEditGovSchoolPeripheralPersonInfo: function () {
                vc.component.editGovSchoolPeripheralPersonInfo = {
                    schPersonId: '',
                    schoolId: '',
                    govPersonId: '',
                    schoolName: '',
                    personName: '',
                    caId: '',
                    remark: '',
                    extentInjury: '',
                    isAttention: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
