/**
    活动管理 组件
**/
(function (vc) {

    vc.extends({
       
        data: {
            viewGovActivityInfo: {
                index: 0,
                flowComponent: 'viewGovActivityInfo',
                caId: '',
                actName: '',
                actTime: '',
                actAddress: '',
                personCount: '',
                context: '',
                contactName: '',
                contactLink: '',

            }
        },
        _initMethod: function () {
            let _actId = vc.getParam("actId");
            console.log("_actId",_actId);
            vc.component._loadGovActivityInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovActivityInfo', 'chooseGovActivity', function (_app) {
                vc.copyObject(_app, vc.component.viewGovActivityInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovActivityInfo);
            });

            vc.on('viewGovActivityInfo', 'onIndex', function (_index) {
                vc.component.viewGovActivityInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovActivityInfoModel() {
                vc.emit('chooseGovActivity', 'openChooseGovActivityModel', {});
            },
            _openAddGovActivityInfoModel() {
                vc.emit('addGovActivity', 'openAddGovActivityModal', {});
            },
            _loadGovActivityInfoData: function () {

            }
        }
    });

})(window.vc);
