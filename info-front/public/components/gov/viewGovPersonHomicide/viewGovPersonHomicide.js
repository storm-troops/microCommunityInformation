/**
    人口管理 组件
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            viewGovPersonHomicideInfo: {
                govPersons: [],
                politicalOutlooks: [],
                govOwners: [],
                govRooms: [],
                govHomicideBasics: []
            }
        },
        _initMethod: function () {
            let _govPersonId = vc.getParam("govPersonId");
            let _govOwnerId = vc.getParam("govOwnerId");
            let _govHomicideId = vc.getParam("govHomicideId");
            //根据请求参数查询 查询人口
            vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS, _govPersonId);
            //根据请求参数查询 户籍
            vc.component._listGovOwners(DEFAULT_PAGE, DEFAULT_ROWS, _govOwnerId);
            //根据请求参数查询 房屋信息
            vc.component._listGovRooms(DEFAULT_PAGE, DEFAULT_ROWS, _govOwnerId);
            //根据请求参数查询 犯罪信息
            vc.component._listGovHomicideBasics(DEFAULT_PAGE, DEFAULT_ROWS, _govHomicideId);

            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.viewGovPersonHomicideInfo.politicalOutlooks = _data;
            });
        },
        _initEvent: function () {
            vc.on('viewGovPersonHomicideInfo', 'chooseGovPerson', function (_app) {
                //vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovPersonHomicideInfo);
            });

            vc.on('viewGovPersonHomicideInfo', 'onIndex', function (_index) {
                vc.component.viewGovPersonHomicideInfo.index = _index;
            });

        },
        methods: {
            _listGovPersons: function (_page, _rows, _govPersonId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        govPersonId: _govPersonId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPerson/queryGovPerson',
                    param,
                    function (json, res) {
                        var _govPersonManageInfo = JSON.parse(json);
                        vc.component.viewGovPersonHomicideInfo.govPersons = _govPersonManageInfo.data;
                        //vc.copyObject(_govPersonManageInfo.data[0], vc.component.viewGovPersonHomicideInfo);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovOwners: function (_page, _rows,_govOwnerId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        govOwnerId: _govOwnerId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govOwner/queryGovOwner',
                    param,
                    function (json, res) {
                        var _govOwnerManageInfo = JSON.parse(json);
                        vc.component.viewGovPersonHomicideInfo.govOwners = _govOwnerManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovRooms: function (_page, _rows,_govOwnerId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        ownerId: _govOwnerId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govRoom/queryGovRoom',
                    param,
                    function (json, res) {
                        var _govRoomManageInfo = JSON.parse(json);
                        vc.component.viewGovPersonHomicideInfo.govRooms = _govRoomManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovHomicideBasics: function (_page, _rows,_govHomicideId) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        govHomicideId: _govHomicideId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHomicideBasic/queryGovHomicideBasic',
                    param,
                    function (json, res) {
                        var _govHomicideBasicManageInfo = JSON.parse(json);
                        vc.component.viewGovPersonHomicideInfo.govHomicideBasics = _govHomicideBasicManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSpecName: function (_politicalOutlook) {
                let _retutest = '';
                $that.viewGovPersonHomicideInfo.politicalOutlooks.forEach(_item => {
                    if (_item.statusCd == _politicalOutlook) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            }
        }
    });

})(window.vc);
