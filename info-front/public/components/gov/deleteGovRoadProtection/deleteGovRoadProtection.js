(function(vc,vm){

    vc.extends({
        data:{
            deleteGovRoadProtectionInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovRoadProtection','openDeleteGovRoadProtectionModal',function(_params){

                vc.component.deleteGovRoadProtectionInfo = _params;
                $('#deleteGovRoadProtectionModel').modal('show');

            });
        },
        methods:{
            deleteGovRoadProtection:function(){
                vc.component.deleteGovRoadProtectionInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govRoadProtection/deleteGovRoadProtection',
                    JSON.stringify(vc.component.deleteGovRoadProtectionInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovRoadProtectionModel').modal('hide');
                            vc.emit('govRoadProtectionManage','listGovRoadProtection',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovRoadProtectionModel:function(){
                $('#deleteGovRoadProtectionModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
