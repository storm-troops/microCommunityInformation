(function (vc, vm) {

    vc.extends({
        data: {
            editGovPersonInfo: {
                govPersonId: '',
                caId: '',
                personType: '',
                idType: '',
                idCard: '',
                personName: '',
                personTel: '',
                personSex: '',
                prePersonName: '',
                birthday: '',
                nation: '',
                nativePlace: '',
                politicalOutlook: '',
                politicalOutlooks: [],
                maritalStatus: '',
                religiousBelief: '',
                ramark: '',
                govCommunityAreas: [],
                nationData : [
                    {id:1 ,name:'汉族'},
                    {id:2 ,name:'蒙古族'},
                    {id:3 ,name:'回族'},
                    {id:4 ,name:'藏族'},
                    {id:5 ,name:'维吾尔族'},
                    {id:6 ,name:'苗族'},
                    {id:7 ,name:'彝族'},
                    {id:8 ,name:'壮族'},
                    {id:9 ,name:'布依族'},
                    {id:10,name:'朝鲜族'},
                    {id:11,name:'满族'},
                    {id:12,name:'侗族'},
                    {id:13,name:'瑶族'},
                    {id:14,name:'白族'},
                    {id:15,name:'土家族'},
                    {id:16,name:'哈尼族'},
                    {id:17,name:'哈萨克族'},
                    {id:18,name:'傣族'},
                    {id:19,name:'黎族'},
                    {id:20,name:'傈僳族'},
                    {id:21,name:'佤族'},
                    {id:22,name:'畲族'},
                    {id:23,name:'高山族'},
                    {id:24,name:'拉祜族'},
                    {id:25,name:'水族'},
                    {id:26,name:'东乡族'},
                    {id:27,name:'纳西族'},
                    {id:28,name:'景颇族'},
                    {id:29,name:'柯尔克孜族'},
                    {id:30,name:'土族'},
                    {id:31,name:'达翰尔族'},
                    {id:32,name:'么佬族'},
                    {id:33,name:'羌族'},
                    {id:34,name:'布朗族'},
                    {id:35,name:'撒拉族'},
                    {id:36,name:'毛南族'},
                    {id:37,name:'仡佬族'},
                    {id:38,name:'锡伯族'},
                    {id:39,name:'阿昌族'},
                    {id:40,name:'普米族'},
                    {id:41,name:'塔吉克族'},
                    {id:42,name:'怒族'},
                    {id:43,name:'乌孜别克族'},
                    {id:44,name:'俄罗斯族'},
                    {id:45,name:'鄂温克族'},
                    {id:46,name:'德昂族'},
                    {id:47,name:'保安族'},
                    {id:48,name:'裕固族'},
                    {id:49,name:'京族'},
                    {id:50,name:'塔塔尔族'},
                    {id:51,name:'独龙族'},
                    {id:52,name:'鄂伦春族'},
                    {id:53,name:'赫哲族'},
                    {id:54,name:'门巴族'},
                    {id:55,name:'珞巴族'},
                    {id:56,name:'基诺族'},
            
            ]
            }
        },
        _initMethod: function () {
            vc.initDate('editBirthday', function (_value) {
                $that.editGovPersonInfo.birthday = _value;
            });
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.editGovPersonInfo.politicalOutlooks = _data;
            });
            $that._listEditGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('editGovPerson', 'openEditGovPersonModal', function (_params) {
                vc.component.refreshEditGovPersonInfo();
                $('#editGovPersonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPersonInfo);
            });
        },
        methods: {
            editGovPersonValidate: function () {
                return vc.validate.validate({
                    editGovPersonInfo: vc.component.editGovPersonInfo
                }, {
                    'editGovPersonInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovPersonInfo.personType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "人口类型超长"
                        },
                    ],
                    'editGovPersonInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "证件类型物超长"
                        },
                    ],
                    'editGovPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码超长"
                        },
                    ],
                    'editGovPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名超长"
                        },
                    ],
                    'editGovPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'editGovPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "性别超长"
                        },
                    ],
                    'editGovPersonInfo.prePersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "曾用名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "曾用名太长"
                        },
                    ],
                    'editGovPersonInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        }
                    ],
                    'editGovPersonInfo.nation': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "民族超长"
                        },
                    ],
                    'editGovPersonInfo.nativePlace': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "籍贯超长"
                        },
                    ],
                    'editGovPersonInfo.politicalOutlook': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "政治面貌超长"
                        },
                    ],
                    'editGovPersonInfo.maritalStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "婚姻状况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "婚姻状况超长了"
                        },
                    ],
                    'editGovPersonInfo.religiousBelief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "宗教信仰不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "宗教信仰超长了"
                        },
                    ]

                });
            },
            editGovPerson: function () {
                if (!vc.component.editGovPersonValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if(!vc.component.isCardNoEdit(vc.component.editGovPersonInfo.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.http.apiPost(
                    '/govPerson/updateGovPerson',
                    JSON.stringify(vc.component.editGovPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPersonModel').modal('hide');
                            vc.emit('govPersonManage', 'listGovPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovPersonInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovPersonInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovPersonInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            isCardNoEdit: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            refreshEditGovPersonInfo: function () {
                let _govCommunityAreas =  vc.component.editGovPersonInfo.govCommunityAreas;
                let _politicalOutlooks =  vc.component.editGovPersonInfo.politicalOutlooks;
                vc.component.editGovPersonInfo = {
                    govPersonId: '',
                    caId: '',
                    personType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    personTel: '',
                    personSex: '',
                    prePersonName: '',
                    birthday: '',
                    nation: '',
                    nativePlace: '',
                    politicalOutlook: '',
                    politicalOutlooks: _politicalOutlooks,
                    maritalStatus: '',
                    religiousBelief: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas,
                    nationData : [
                        {id:1 ,name:'汉族'},
                        {id:2 ,name:'蒙古族'},
                        {id:3 ,name:'回族'},
                        {id:4 ,name:'藏族'},
                        {id:5 ,name:'维吾尔族'},
                        {id:6 ,name:'苗族'},
                        {id:7 ,name:'彝族'},
                        {id:8 ,name:'壮族'},
                        {id:9 ,name:'布依族'},
                        {id:10,name:'朝鲜族'},
                        {id:11,name:'满族'},
                        {id:12,name:'侗族'},
                        {id:13,name:'瑶族'},
                        {id:14,name:'白族'},
                        {id:15,name:'土家族'},
                        {id:16,name:'哈尼族'},
                        {id:17,name:'哈萨克族'},
                        {id:18,name:'傣族'},
                        {id:19,name:'黎族'},
                        {id:20,name:'傈僳族'},
                        {id:21,name:'佤族'},
                        {id:22,name:'畲族'},
                        {id:23,name:'高山族'},
                        {id:24,name:'拉祜族'},
                        {id:25,name:'水族'},
                        {id:26,name:'东乡族'},
                        {id:27,name:'纳西族'},
                        {id:28,name:'景颇族'},
                        {id:29,name:'柯尔克孜族'},
                        {id:30,name:'土族'},
                        {id:31,name:'达翰尔族'},
                        {id:32,name:'么佬族'},
                        {id:33,name:'羌族'},
                        {id:34,name:'布朗族'},
                        {id:35,name:'撒拉族'},
                        {id:36,name:'毛南族'},
                        {id:37,name:'仡佬族'},
                        {id:38,name:'锡伯族'},
                        {id:39,name:'阿昌族'},
                        {id:40,name:'普米族'},
                        {id:41,name:'塔吉克族'},
                        {id:42,name:'怒族'},
                        {id:43,name:'乌孜别克族'},
                        {id:44,name:'俄罗斯族'},
                        {id:45,name:'鄂温克族'},
                        {id:46,name:'德昂族'},
                        {id:47,name:'保安族'},
                        {id:48,name:'裕固族'},
                        {id:49,name:'京族'},
                        {id:50,name:'塔塔尔族'},
                        {id:51,name:'独龙族'},
                        {id:52,name:'鄂伦春族'},
                        {id:53,name:'赫哲族'},
                        {id:54,name:'门巴族'},
                        {id:55,name:'珞巴族'},
                        {id:56,name:'基诺族'},
                
                ]
                }
            }
        }
    });

})(window.vc, window.vc.component);
