(function (vc, vm) {

    vc.extends({
        data: {
            editGovVolunteerInfo: {
                volunteerId: '',
                govPersonId:'',
                name: '',
                caId: '',
                address: '',
                tel: '',
                student: '',
                company: '',
                volWork: '',
                edu: '',
                goodAtSkills: [],
                freeTimes: [],
                freeTime: [],
                ramark: '',
                politicalOutlooks: [],
                edus: [],
                goodAtSkillss: [],
                volunteerServ: [],
                volunteerServs: [],
                persons: {
                    idType: '1',
                    idCard: '',
                    politicalOutlook: '',
                    nativePlace: '',
                    nation: '',
                    personSex: '',

                },
                nationData : [
                    {id:1 ,name:'汉族'},
                    {id:2 ,name:'蒙古族'},
                    {id:3 ,name:'回族'},
                    {id:4 ,name:'藏族'},
                    {id:5 ,name:'维吾尔族'},
                    {id:6 ,name:'苗族'},
                    {id:7 ,name:'彝族'},
                    {id:8 ,name:'壮族'},
                    {id:9 ,name:'布依族'},
                    {id:10,name:'朝鲜族'},
                    {id:11,name:'满族'},
                    {id:12,name:'侗族'},
                    {id:13,name:'瑶族'},
                    {id:14,name:'白族'},
                    {id:15,name:'土家族'},
                    {id:16,name:'哈尼族'},
                    {id:17,name:'哈萨克族'},
                    {id:18,name:'傣族'},
                    {id:19,name:'黎族'},
                    {id:20,name:'傈僳族'},
                    {id:21,name:'佤族'},
                    {id:22,name:'畲族'},
                    {id:23,name:'高山族'},
                    {id:24,name:'拉祜族'},
                    {id:25,name:'水族'},
                    {id:26,name:'东乡族'},
                    {id:27,name:'纳西族'},
                    {id:28,name:'景颇族'},
                    {id:29,name:'柯尔克孜族'},
                    {id:30,name:'土族'},
                    {id:31,name:'达翰尔族'},
                    {id:32,name:'么佬族'},
                    {id:33,name:'羌族'},
                    {id:34,name:'布朗族'},
                    {id:35,name:'撒拉族'},
                    {id:36,name:'毛南族'},
                    {id:37,name:'仡佬族'},
                    {id:38,name:'锡伯族'},
                    {id:39,name:'阿昌族'},
                    {id:40,name:'普米族'},
                    {id:41,name:'塔吉克族'},
                    {id:42,name:'怒族'},
                    {id:43,name:'乌孜别克族'},
                    {id:44,name:'俄罗斯族'},
                    {id:45,name:'鄂温克族'},
                    {id:46,name:'德昂族'},
                    {id:47,name:'保安族'},
                    {id:48,name:'裕固族'},
                    {id:49,name:'京族'},
                    {id:50,name:'塔塔尔族'},
                    {id:51,name:'独龙族'},
                    {id:52,name:'鄂伦春族'},
                    {id:53,name:'赫哲族'},
                    {id:54,name:'门巴族'},
                    {id:55,name:'珞巴族'},
                    {id:56,name:'基诺族'},
            
            ]
            }
        },
        _initMethod: function () {
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.editGovVolunteerInfo.politicalOutlooks = _data;
            });
            vc.getDict('gov_volunteer', "edu", function (_data) {
                vc.component.editGovVolunteerInfo.edus = _data;
            });
            vc.getDict('gov_volunteer', "good_at_skills", function (_data) {
                vc.component.editGovVolunteerInfo.goodAtSkillss = _data;
            });
            vc.getDict('gov_volunteer', "free_time", function (_data) {
                vc.component.editGovVolunteerInfo.freeTimes = _data;
            });
            $that._listEditGovServFields();
        },
        _initEvent: function () {
            vc.on('editGovVolunteer', 'openEditGovVolunteerModal', function (_params) {
                vc.component.refreshEditGovVolunteerInfo();
                $('#editGovVolunteerModel').modal('show');
                console.log(_params);
                vc.copyObject(_params, vc.component.editGovVolunteerInfo);
                vc.copyObject(_params, vc.component.editGovVolunteerInfo.persons);
                vc.component.editGovVolunteerInfo.goodAtSkills = JSON.parse(_params.goodAtSkills);
                vc.component.editGovVolunteerInfo.freeTime = JSON.parse(_params.freeTime);
                vc.component.editGovVolunteerInfo.volunteerServ = JSON.parse(_params.volunteerServ);

                vc.component.editGovVolunteerInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovVolunteerValidate: function () {
                return vc.validate.validate({
                    editGovVolunteerInfo: vc.component.editGovVolunteerInfo
                }, {
                    'editGovVolunteerInfo.volunteerId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "志愿者ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "志愿者ID不能超过30"
                        },
                    ],
                    'editGovVolunteerInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口ID不能为空"
                        }
                    ],
                    'editGovVolunteerInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "志愿者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "志愿者名称不能超过64"
                        },
                    ],
                    'editGovVolunteerInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovVolunteerInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'editGovVolunteerInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'editGovVolunteerInfo.student': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否学生不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否学生不能超过12"
                        },
                    ],
                    'editGovVolunteerInfo.company': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校或工作单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "学校或工作单位不能超过128"
                        },
                    ],
                    'editGovVolunteerInfo.volWork': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "从业情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "从业情况不能超过128"
                        },
                    ],
                    'editGovVolunteerInfo.edu': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学历不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "学历不能超过12"
                        },
                    ],
                    'editGovVolunteerInfo.goodAtSkills': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "擅长技能不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "擅长技能不能超过512"
                        },
                    ],
                    'editGovVolunteerInfo.freeTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "空闲时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "空闲时间不能超过64"
                        },
                    ]

                });
            },
            editGovVolunteer: function () {
                if (!vc.component.editGovVolunteerValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                console.log(vc.component.editGovVolunteerInfo.volunteerServ);

                if (vc.component.editGovVolunteerInfo.goodAtSkills.length < 1) {
                    vc.toast("擅长技能请选择1-3种");
                    return;
                }
                if (vc.component.editGovVolunteerInfo.goodAtSkills.length > 3) {
                    vc.toast("擅长技能请选择1-3种");
                    return;
                }
                if (vc.component.editGovVolunteerInfo.volunteerServ.length > 3) {
                    vc.toast("服务领域请选择1-3种");

                    return;
                }
                if (vc.component.editGovVolunteerInfo.volunteerServ.length < 1) {
                    vc.toast("服务领域请选择1-3种");

                    return;
                }
                if(!vc.component.isCardNoEdit(vc.component.editGovVolunteerInfo.persons.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.http.apiPost(
                    '/govVolunteer/updateGovVolunteer',
                    JSON.stringify(vc.component.editGovVolunteerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovVolunteerModel').modal('hide');
                            vc.emit('govVolunteerManage', 'listGovVolunteer', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovServFields: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        isShow: 'Y'
                    }
                };

                //发送get请求
                vc.http.apiGet('/govServField/queryGovServField',
                    param,
                    function (json, res) {
                        var _govServFieldManageInfo = JSON.parse(json);
                        vc.component.editGovVolunteerInfo.volunteerServs = _govServFieldManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            isCardNoEdit: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            refreshEditGovVolunteerInfo: function () {
                let _goodAtSkillss = $that.editGovVolunteerInfo.goodAtSkillss;
                let _freeTimes = $that.editGovVolunteerInfo.freeTimes;
                let _politicalOutlooks = $that.editGovVolunteerInfo.politicalOutlooks;
                let _edus = $that.editGovVolunteerInfo.edus;
                let _volunteerServs = $that.editGovVolunteerInfo.volunteerServs;
                vc.component.editGovVolunteerInfo = {
                    volunteerId: '',
                    govPersonId:'',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    student: '',
                    company: '',
                    volWork: '',
                    edu: '',
                    goodAtSkills: [],
                    freeTimes: _freeTimes,
                    freeTime: [],
                    ramark: '',
                    politicalOutlooks: _politicalOutlooks,
                    edus: _edus,
                    goodAtSkillss: _goodAtSkillss,
                    volunteerServ: [],
                    volunteerServs: _volunteerServs,
                    persons: {
                        idType: '1',
                        idCard: '',
                        politicalOutlook: '',
                        nativePlace: '',
                        nation: '',
                        personSex: '',
    
                    },
                    nationData : [
                        {id:1 ,name:'汉族'},
                        {id:2 ,name:'蒙古族'},
                        {id:3 ,name:'回族'},
                        {id:4 ,name:'藏族'},
                        {id:5 ,name:'维吾尔族'},
                        {id:6 ,name:'苗族'},
                        {id:7 ,name:'彝族'},
                        {id:8 ,name:'壮族'},
                        {id:9 ,name:'布依族'},
                        {id:10,name:'朝鲜族'},
                        {id:11,name:'满族'},
                        {id:12,name:'侗族'},
                        {id:13,name:'瑶族'},
                        {id:14,name:'白族'},
                        {id:15,name:'土家族'},
                        {id:16,name:'哈尼族'},
                        {id:17,name:'哈萨克族'},
                        {id:18,name:'傣族'},
                        {id:19,name:'黎族'},
                        {id:20,name:'傈僳族'},
                        {id:21,name:'佤族'},
                        {id:22,name:'畲族'},
                        {id:23,name:'高山族'},
                        {id:24,name:'拉祜族'},
                        {id:25,name:'水族'},
                        {id:26,name:'东乡族'},
                        {id:27,name:'纳西族'},
                        {id:28,name:'景颇族'},
                        {id:29,name:'柯尔克孜族'},
                        {id:30,name:'土族'},
                        {id:31,name:'达翰尔族'},
                        {id:32,name:'么佬族'},
                        {id:33,name:'羌族'},
                        {id:34,name:'布朗族'},
                        {id:35,name:'撒拉族'},
                        {id:36,name:'毛南族'},
                        {id:37,name:'仡佬族'},
                        {id:38,name:'锡伯族'},
                        {id:39,name:'阿昌族'},
                        {id:40,name:'普米族'},
                        {id:41,name:'塔吉克族'},
                        {id:42,name:'怒族'},
                        {id:43,name:'乌孜别克族'},
                        {id:44,name:'俄罗斯族'},
                        {id:45,name:'鄂温克族'},
                        {id:46,name:'德昂族'},
                        {id:47,name:'保安族'},
                        {id:48,name:'裕固族'},
                        {id:49,name:'京族'},
                        {id:50,name:'塔塔尔族'},
                        {id:51,name:'独龙族'},
                        {id:52,name:'鄂伦春族'},
                        {id:53,name:'赫哲族'},
                        {id:54,name:'门巴族'},
                        {id:55,name:'珞巴族'},
                        {id:56,name:'基诺族'},
                
                ]
                }
            }
        }
    });

})(window.vc, window.vc.component);
