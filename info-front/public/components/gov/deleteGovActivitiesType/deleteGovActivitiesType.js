(function(vc,vm){

    vc.extends({
        data:{
            deleteGovActivitiesTypeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovActivitiesType','openDeleteGovActivitiesTypeModal',function(_params){

                vc.component.deleteGovActivitiesTypeInfo = _params;
                $('#deleteGovActivitiesTypeModel').modal('show');

            });
        },
        methods:{
            deleteGovActivitiesType:function(){
                vc.component.deleteGovActivitiesTypeInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govActivitiesType/deleteGovActivitiesType',
                    JSON.stringify(vc.component.deleteGovActivitiesTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovActivitiesTypeModel').modal('hide');
                            vc.emit('govActivitiesTypeManage','listGovActivitiesType',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovActivitiesTypeModel:function(){
                $('#deleteGovActivitiesTypeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
