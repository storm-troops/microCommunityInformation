/**
    涉及区域类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovAreaTypeInfo:{
                index:0,
                flowComponent:'viewGovAreaTypeInfo',
                typeId:'',
caId:'',
name:'',
isShow:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovAreaTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovAreaTypeInfo','chooseGovAreaType',function(_app){
                vc.copyObject(_app, vc.component.viewGovAreaTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovAreaTypeInfo);
            });

            vc.on('viewGovAreaTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovAreaTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovAreaTypeInfoModel(){
                vc.emit('chooseGovAreaType','openChooseGovAreaTypeModel',{});
            },
            _openAddGovAreaTypeInfoModel(){
                vc.emit('addGovAreaType','openAddGovAreaTypeModal',{});
            },
            _loadGovAreaTypeInfoData:function(){

            }
        }
    });

})(window.vc);
