(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovActivities:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovActivitiesInfo:{
                govActivitiess:[],
                _currentGovActivitiesName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovActivities','openChooseGovActivitiesModel',function(_param){
                $('#chooseGovActivitiesModel').modal('show');
                vc.component._refreshChooseGovActivitiesInfo();
                vc.component._loadAllGovActivitiesInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovActivitiesInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govActivities.listGovActivitiess',
                             param,
                             function(json){
                                var _govActivitiesInfo = JSON.parse(json);
                                vc.component.chooseGovActivitiesInfo.govActivitiess = _govActivitiesInfo.govActivitiess;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovActivities:function(_govActivities){
                if(_govActivities.hasOwnProperty('name')){
                     _govActivities.govActivitiesName = _govActivities.name;
                }
                vc.emit($props.emitChooseGovActivities,'chooseGovActivities',_govActivities);
                vc.emit($props.emitLoadData,'listGovActivitiesData',{
                    govActivitiesId:_govActivities.govActivitiesId
                });
                $('#chooseGovActivitiesModel').modal('hide');
            },
            queryGovActivitiess:function(){
                vc.component._loadAllGovActivitiesInfo(1,10,vc.component.chooseGovActivitiesInfo._currentGovActivitiesName);
            },
            _refreshChooseGovActivitiesInfo:function(){
                vc.component.chooseGovActivitiesInfo._currentGovActivitiesName = "";
            }
        }

    });
})(window.vc);
