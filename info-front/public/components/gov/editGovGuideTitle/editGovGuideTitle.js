(function (vc, vm) {

    vc.extends({
        data: {
            editGovGuideTitleInfo: {
                titleId: '',
                titleType: '',
                title: '',
                seq: '',
                termId: '',
                govHealthTerms: [],
                healthId: '',
                titleValues: []
                
            }
        },
        _initMethod: function () {
            $that._listEditGovHealthTerms();
        },
        _initEvent: function () {
            vc.on('editGovGuideTitle', 'openEditGovGuideTitleModal', function (_params) {
                vc.component.refreshEditGovGuideTitleInfo();
                $('#editGovGuideTitleModel').modal('show');
                vc.copyObject(_params, vc.component.editGovGuideTitleInfo);
                $that.editGovGuideTitleInfo.titleValues = _params.govHealthTitleValueDtos;
                console.log($that.editGovGuideTitleInfo.titleValues );
                vc.component.editGovGuideTitleInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovGuideTitleValidate: function () {
                return vc.validate.validate({
                    editGovGuideTitleInfo: vc.component.editGovGuideTitleInfo
                }, {
                    'editGovGuideTitleInfo.titleType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "题目类型不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "题目类型格式错误"
                        },
                    ],
                    'editGovGuideTitleInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "问卷题目不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "256",
                            errInfo: "问卷题目太长"
                        },
                    ],
                    'editGovGuideTitleInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "顺序必须是数字"
                        },
                    ],
                    'editGovGuideTitleInfo.termId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "体检项不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "体检项格式错误"
                        },
                    ],
                    'editGovGuideTitleInfo.titleId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "题目ID不能为空"
                        }]

                });
            },
            editGovGuideTitle: function () {
                if (!vc.component.editGovGuideTitleValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govHealthTitle/updateGovHealthTitle',
                    JSON.stringify(vc.component.editGovGuideTitleInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovGuideTitleModel').modal('hide');
                            vc.emit('govGuideTitleManage', 'listGovGuideTitle', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovHealthTerms: function () {
                var param = {
                    params: {
                        page: 1 ,
                        row: 50 ,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHealthTerm/queryGovHealthTerm',
                    param,
                    function (json, res) {
                        var _govHealthTermManageInfo = JSON.parse(json);
                        vc.component.editGovGuideTitleInfo.govHealthTerms = _govHealthTermManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovGuideTitleInfo: function () {
                let _govHealthTerms = vc.component.editGovGuideTitleInfo.govHealthTerms;
                vc.component.editGovGuideTitleInfo = {
                    titleId: '',
                    titleType: '',
                    title: '',
                    seq: '',
                    healthId: '',
                    termId: '',
                    govHealthTerms: _govHealthTerms,
                    titleValues: []
                }
            },
            _addEditTitleValue: function () {
                $that.editGovGuideTitleInfo.titleValues.push(
                    {
                        value: '',
                        seq: $that.editGovGuideTitleInfo.titleValues.length + 1
                    }
                );
            },
            _deleteEditTitleValue: function (_seq) {
                console.log(_seq);

                let _newTitleValues = [];
                let _tmpTitleValues = $that.editGovGuideTitleInfo.titleValues;
                _tmpTitleValues.forEach(item => {
                    if (item.seq != _seq) {
                        _newTitleValues.push({
                            value: item.value,
                            seq: _newTitleValues.length + 1
                        })
                    }
                });

                $that.editGovGuideTitleInfo.titleValues = _newTitleValues;
            }
        }
    });

})(window.vc, window.vc.component);
