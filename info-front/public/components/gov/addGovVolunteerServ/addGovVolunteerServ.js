(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovVolunteerServInfo: {
                servId: '',
                volunteerId: '',
                caId: '',
                name: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovVolunteerServ', 'openAddGovVolunteerServModal', function () {
                $('#addGovVolunteerServModel').modal('show');
            });
        },
        methods: {
            addGovVolunteerServValidate() {
                return vc.validate.validate({
                    addGovVolunteerServInfo: vc.component.addGovVolunteerServInfo
                }, {
                    'addGovVolunteerServInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "领域名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "领域名称不能超过64"
                        },
                    ],
                    'addGovVolunteerServInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],




                });
            },
            saveGovVolunteerServInfo: function () {
                if (!vc.component.addGovVolunteerServValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovVolunteerServInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovVolunteerServInfo);
                    $('#addGovVolunteerServModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govVolunteerServ/saveGovVolunteerServ',
                    JSON.stringify(vc.component.addGovVolunteerServInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovVolunteerServModel').modal('hide');
                            vc.component.clearAddGovVolunteerServInfo();
                            vc.emit('govVolunteerServManage', 'listGovVolunteerServ', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovVolunteerServInfo: function () {
                vc.component.addGovVolunteerServInfo = {
                    servId: '',
                    volunteerId: '',
                    caId: '',
                    name: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
