/**
    矫正者 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovCommunityarCorrectionInfo:{
                index:0,
                flowComponent:'viewGovCommunityarCorrectionInfo',
                correctionId:'',
name:'',
caId:'',
address:'',
tel:'',
correctionStartTime:'',
correctionEndTime:'',
correctionReason:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCommunityarCorrectionInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovCommunityarCorrectionInfo','chooseGovCommunityarCorrection',function(_app){
                vc.copyObject(_app, vc.component.viewGovCommunityarCorrectionInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovCommunityarCorrectionInfo);
            });

            vc.on('viewGovCommunityarCorrectionInfo', 'onIndex', function(_index){
                vc.component.viewGovCommunityarCorrectionInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovCommunityarCorrectionInfoModel(){
                vc.emit('chooseGovCommunityarCorrection','openChooseGovCommunityarCorrectionModel',{});
            },
            _openAddGovCommunityarCorrectionInfoModel(){
                vc.emit('addGovCommunityarCorrection','openAddGovCommunityarCorrectionModal',{});
            },
            _loadGovCommunityarCorrectionInfoData:function(){

            }
        }
    });

})(window.vc);
