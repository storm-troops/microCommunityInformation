(function (vc) {
    vc.extends({
        data: {
            viewOldBirthInfo: {
                oldPersons: [],
                orgId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('viewGovOldBirthPerson', 'openviewGovPartyMemberModel', function (_param) {
                vc.component._refreshviewGovPartyMemberInfo();
                //vc.component._loadAllGovRoomInfo(1, 10, '');
            });
            vc.on('viewGovOldBirthPerson', 'selectPartyOrgs', function (_param) {
                console.log(_param);
                $that.viewOldBirthInfo.orgId = _param;
            });
            vc.on('oldBirthChooseGovOldPerson', 'chooseGovOldPerson', function (_param) {
                $that.viewOldBirthInfo.oldPersons.push(_param);
                vc.emit('viewGovOldBirthPerson', 'page_event', $that.viewOldBirthInfo.oldPersons);
            });
        },
        methods: {
            _loadAllPartyMemberInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingMemberRel/queryGovMeetingMemberRel',
                    param,
                    function (json) {
                        var _govRoomInfo = JSON.parse(json);
                        vc.component.viewOldBirthInfo.govRooms = _govRoomInfo.govRooms;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChooseOldBirth: function () {
                let _caId = vc.getCurrentCommunity().caId;
                //let _orgId = $that.viewOldBirthInfo.orgId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                
                var param ={
                    caId: _caId
                }
                vc.emit('chooseGovOldPerson', 'openChooseGovOldPersonModel', param);
            },
            queryvPartyMember: function () {
                vc.component._loadAllPartyMemberInfo(1, 10, vc.component.viewOldBirthInfo._currentGovRoomName);
            },
            _openDeleteOldBirthInfo: function (_govMember) {
                let _tmpGovMembers = [];
                $that.viewOldBirthInfo.oldPersons.forEach(item => {
                    if (item.oldId != _govMember.oldId) {
                        _tmpGovMembers.push(item);
                    }
                });
                $that.viewOldBirthInfo.oldPersons = _tmpGovMembers;
                vc.emit('viewGovOldBirthPerson', 'page_event', $that.viewOldBirthInfo.oldPersons);
            },
            _refreshviewGovPartyMemberInfo: function () {
                vc.component.viewOldBirthInfo._currentGovRoomName = "";
                $that.viewOldBirthInfo.oldPersons = [];
            }
        }

    });
})(window.vc);
