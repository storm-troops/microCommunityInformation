(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMeetingType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMeetingTypeInfo:{
                govMeetingTypes:[],
                _currentGovMeetingTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMeetingType','openChooseGovMeetingTypeModel',function(_param){
                $('#chooseGovMeetingTypeModel').modal('show');
                vc.component._refreshChooseGovMeetingTypeInfo();
                vc.component._loadAllGovMeetingTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMeetingTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMeetingType/queryGovMeetingType',
                             param,
                             function(json){
                                var _govMeetingTypeInfo = JSON.parse(json);
                                vc.component.chooseGovMeetingTypeInfo.govMeetingTypes = _govMeetingTypeInfo.govMeetingTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMeetingType:function(_govMeetingType){
                if(_govMeetingType.hasOwnProperty('name')){
                     _govMeetingType.govMeetingTypeName = _govMeetingType.name;
                }
                vc.emit($props.emitChooseGovMeetingType,'chooseGovMeetingType',_govMeetingType);
                vc.emit($props.emitLoadData,'listGovMeetingTypeData',{
                    govMeetingTypeId:_govMeetingType.govMeetingTypeId
                });
                $('#chooseGovMeetingTypeModel').modal('hide');
            },
            queryGovMeetingTypes:function(){
                vc.component._loadAllGovMeetingTypeInfo(1,10,vc.component.chooseGovMeetingTypeInfo._currentGovMeetingTypeName);
            },
            _refreshChooseGovMeetingTypeInfo:function(){
                vc.component.chooseGovMeetingTypeInfo._currentGovMeetingTypeName = "";
            }
        }

    });
})(window.vc);
