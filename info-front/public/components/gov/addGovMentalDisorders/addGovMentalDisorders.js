(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMentalDisordersInfo: {
                disordersId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                disordersStartTime: '',
                disordersReason: '',
                emergencyPerson: '',
                emergencyTel: '',
                ramark: ''

            }
        },
        _initMethod: function () {
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGovMentalDisordersInfo.disordersStartTime = _value;
            });

        },
        _initEvent: function () {
            vc.on('addGovMentalDisorders', 'openAddGovMentalDisordersModal', function () {
                $('#addGovMentalDisordersModel').modal('show');
            });
        },
        methods: {
            addGovMentalDisordersValidate() {
                return vc.validate.validate({
                    addGovMentalDisordersInfo: vc.component.addGovMentalDisordersInfo
                }, {
                    'addGovMentalDisordersInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "障碍者名称不能超过64"
                        },
                    ],
                
                    'addGovMentalDisordersInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'addGovMentalDisordersInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'addGovMentalDisordersInfo.disordersStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "障碍开始时间不能超过时间类型"
                        },
                    ],
                    'addGovMentalDisordersInfo.disordersReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "障碍原因不能超过128"
                        },
                    ],
                    'addGovMentalDisordersInfo.emergencyPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "紧急联系人不能超过64"
                        },
                    ],
                    'addGovMentalDisordersInfo.emergencyTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "紧急联系电话不能超过11"
                        },
                    ]

                });
            },
            saveGovMentalDisordersInfo: function () {
                if (!vc.component.addGovMentalDisordersValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovMentalDisordersInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMentalDisordersInfo);
                    $('#addGovMentalDisordersModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMentalDisorders/saveGovMentalDisorders',
                    JSON.stringify(vc.component.addGovMentalDisordersInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMentalDisordersModel').modal('hide');
                            vc.component.clearAddGovMentalDisordersInfo();
                            vc.emit('govMentalDisordersManage', 'listGovMentalDisorders', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovMentalDisordersInfo: function () {
                vc.component.addGovMentalDisordersInfo = {
                    disordersId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    disordersStartTime: '',
                    disordersReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
