(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPersonDieRecord:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPersonDieRecordInfo:{
                govPersonDieRecords:[],
                _currentGovPersonDieRecordName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPersonDieRecord','openChooseGovPersonDieRecordModel',function(_param){
                $('#chooseGovPersonDieRecordModel').modal('show');
                vc.component._refreshChooseGovPersonDieRecordInfo();
                vc.component._loadAllGovPersonDieRecordInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPersonDieRecordInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govPersonDieRecord/queryGovPersonDieRecord',
                             param,
                             function(json){
                                var _govPersonDieRecordInfo = JSON.parse(json);
                                vc.component.chooseGovPersonDieRecordInfo.govPersonDieRecords = _govPersonDieRecordInfo.govPersonDieRecords;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPersonDieRecord:function(_govPersonDieRecord){
                if(_govPersonDieRecord.hasOwnProperty('name')){
                     _govPersonDieRecord.govPersonDieRecordName = _govPersonDieRecord.name;
                }
                vc.emit($props.emitChooseGovPersonDieRecord,'chooseGovPersonDieRecord',_govPersonDieRecord);
                vc.emit($props.emitLoadData,'listGovPersonDieRecordData',{
                    govPersonDieRecordId:_govPersonDieRecord.govPersonDieRecordId
                });
                $('#chooseGovPersonDieRecordModel').modal('hide');
            },
            queryGovPersonDieRecords:function(){
                vc.component._loadAllGovPersonDieRecordInfo(1,10,vc.component.chooseGovPersonDieRecordInfo._currentGovPersonDieRecordName);
            },
            _refreshChooseGovPersonDieRecordInfo:function(){
                vc.component.chooseGovPersonDieRecordInfo._currentGovPersonDieRecordName = "";
            }
        }

    });
})(window.vc);
