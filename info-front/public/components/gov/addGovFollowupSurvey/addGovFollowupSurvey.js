(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovFollowupSurveyInfo: {
                surveyId: '',
                surveyTime: '',
                govPersonId: '',
                govPersonName: '',
                surveyWay: '',
                symptoms: '',
                symptomsSel: [],
                caId: '',
                lifeStyleGuide: '',
                drugCompliance: '',
                adrs: '',
                surveyType: '',
                medication: '',
                referralReason: '',
                referralDepartment: '',
                surveyAdvice: '',
                surveyConclusion: '',
                nextSurveyTime: '',
                surveyDoctor: '',
                remark: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addSurveyTime', function (_value) {
                $that.addGovFollowupSurveyInfo.surveyTime = _value;
            });
            vc.initDateTime('addNextSurveyTime', function (_value) {
                $that.addGovFollowupSurveyInfo.nextSurveyTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovFollowupSurvey', 'openAddGovFollowupSurveyModal', function () {
                $('#addGovFollowupSurveyModel').modal('show');
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                $that.addGovFollowupSurveyInfo.govPersonId = _param.govPersonId;
                $that.addGovFollowupSurveyInfo.govPersonName = _param.personName;
            });
        },
        methods: {
            addGovFollowupSurveyValidate() {
                return vc.validate.validate({
                    addGovFollowupSurveyInfo: vc.component.addGovFollowupSurveyInfo
                }, {
                    'addGovFollowupSurveyInfo.surveyTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "随访时间不能超过时间类型"
                        },
                    ],
                    'addGovFollowupSurveyInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口ID不能超过30"
                        },
                    ],
                    'addGovFollowupSurveyInfo.surveyWay': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访方式：1001门诊不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "4",
                            errInfo: "随访方式：1001门诊不能超过4"
                        },
                    ],
                    'addGovFollowupSurveyInfo.symptoms': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "症状不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "症状不能超过500"
                        },
                    ],
                    'addGovFollowupSurveyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovFollowupSurveyInfo.lifeStyleGuide': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生活方式指导不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "生活方式指导不能超过2000"
                        },
                    ],
                    'addGovFollowupSurveyInfo.drugCompliance': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服药依从性1001规律不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "服药依从性1001规律不能超过12"
                        },
                    ],
                    'addGovFollowupSurveyInfo.adrs': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "药物不良反应不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "药物不良反应不能超过2"
                        },
                    ],
                    'addGovFollowupSurveyInfo.surveyType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访分类：1001控制满意不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "4",
                            errInfo: "随访分类：1001控制满意不能超过4"
                        },
                    ],
                    'addGovFollowupSurveyInfo.medication': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "用药情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "用药情况不能超过2000"
                        },
                    ],
                    'addGovFollowupSurveyInfo.referralReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "转诊原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "转诊原因不能超过200"
                        },
                    ],
                    'addGovFollowupSurveyInfo.referralDepartment': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "机构及科别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "机构及科别不能超过200"
                        },
                    ],
                    'addGovFollowupSurveyInfo.surveyAdvice': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访建议不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "随访建议不能超过2000"
                        },
                    ],
                    'addGovFollowupSurveyInfo.surveyConclusion': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访结论不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "随访结论不能超过2000"
                        },
                    ],
                    'addGovFollowupSurveyInfo.nextSurveyTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "下次随访时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "下次随访时间不能超过时间类型"
                        },
                    ],
                    'addGovFollowupSurveyInfo.surveyDoctor': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访医生不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "随访医生不能超过100"
                        },
                    ],
                    'addGovFollowupSurveyInfo.remark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]
                });
            },
            saveGovFollowupSurveyInfo: function () {
                $that.addGovFollowupSurveyInfo.symptoms = vc.component.addGovFollowupSurveyInfo.symptomsSel.toString();
                vc.component.addGovFollowupSurveyInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovFollowupSurveyValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovFollowupSurveyInfo);
                    $('#addGovFollowupSurveyModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govFollowupSurvey/saveGovFollowupSurvey',
                    JSON.stringify(vc.component.addGovFollowupSurveyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovFollowupSurveyModel').modal('hide');
                            vc.component.clearAddGovFollowupSurveyInfo();
                            vc.emit('govFollowupSurveyManage', 'listGovFollowupSurvey', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeAddGovFollowupSurvey: function () {
                $that.clearAddGovFollowupSurveyInfo();
                vc.emit('govFollowupSurveyManage', 'listGovFollowupSurvey', {});
            },
            clearAddGovFollowupSurveyInfo: function () {
                vc.component.addGovFollowupSurveyInfo = {
                    surveyId: '',
                    surveyTime: '',
                    govPersonId: '',
                    surveyWay: '',
                    govPersonName: '',
                    symptomsSel: [],
                    symptoms: '',
                    caId: '',
                    lifeStyleGuide: '',
                    drugCompliance: '',
                    adrs: '',
                    surveyType: '',
                    medication: '',
                    referralReason: '',
                    referralDepartment: '',
                    surveyAdvice: '',
                    surveyConclusion: '',
                    nextSurveyTime: '',
                    surveyDoctor: '',
                    remark: '',

                };
            }
        }
    });

})(window.vc);
