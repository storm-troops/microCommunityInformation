(function (vc, vm) {

    vc.extends({
        data: {
            editGovEventsTypeInfo: {
                typeId: '',
                caId: '',
                typeName: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovEventsType', 'openEditGovEventsTypeModal', function (_params) {
                vc.component.refreshEditGovEventsTypeInfo();
                $('#editGovEventsTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovEventsTypeInfo);
                vc.component.editGovEventsTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovEventsTypeValidate: function () {
                return vc.validate.validate({
                    editGovEventsTypeInfo: vc.component.editGovEventsTypeInfo
                }, {
                    'editGovEventsTypeInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "类型ID不能超过30"
                        },
                    ],
                    'editGovEventsTypeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovEventsTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "类型名称不能超过64"
                        },
                    ],
                    'editGovEventsTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovEventsType: function () {
                if (!vc.component.editGovEventsTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govEventsType/updateGovEventsType',
                    JSON.stringify(vc.component.editGovEventsTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovEventsTypeModel').modal('hide');
                            vc.emit('govEventsTypeManage', 'listGovEventsType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovEventsTypeInfo: function () {
                vc.component.editGovEventsTypeInfo = {
                    typeId: '',
                    caId: '',
                    typeName: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
