(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovRoadProtectionCaseInfo: {
                roadCaseId: '',
                roadProtectionId: '',
                caId: '',
                caseName: '',
                caseType: '',
                caseCode: '',
                happenedTime: '',
                happenedPlace: '',
                idType: '',
                idCard: '',
                personName: '',
                isSolved: '',
                personNum: '',
                fleeingNum: '',
                arrestsNum: '',
                detectionContent: '',
                caseContent: '',
                statusCd: '',
            }
        },
        _initMethod: function () {
            vc.initDateTime('addHappenedTime', function (_value) {
                $that.addGovRoadProtectionCaseInfo.happenedTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovRoadProtectionCase', 'openAddGovRoadProtectionCaseModal', function () {
                $('#addGovRoadProtectionCaseModel').modal('show');
            });
        },
        methods: {
            addGovRoadProtectionCaseValidate() {
                return vc.validate.validate({
                    addGovRoadProtectionCaseInfo: vc.component.addGovRoadProtectionCaseInfo
                }, {
                    'addGovRoadProtectionCaseInfo.roadProtectionId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "护路护线ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "护路护线ID不能超过30"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.caseName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件编号不能超过100"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.caseCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件名称不能超过100"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.happenedTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生日期不能超过时间类型"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.happenedPlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "发生地点不能超过200"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "主犯（嫌疑人）证件类型不能超过12"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）证件号码不能超过64"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）姓名不能超过64"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.isSolved': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否破案不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "是否破案不能超过2"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.personNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "作案人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "作案人数不能超过3"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.fleeingNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "在逃人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "在逃人数不能超过3"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.arrestsNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "抓捕人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "抓捕人数不能超过3"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.detectionContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件侦破情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案件侦破情况不能超过长文本"
                        },
                    ],
                    'addGovRoadProtectionCaseInfo.caseContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案（事）件情况不能超过长文本"
                        },
                    ],

                });
            },
            saveGovRoadProtectionCaseInfo: function () {
                vc.component.addGovRoadProtectionCaseInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovRoadProtectionCaseValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovRoadProtectionCaseInfo);
                    $('#addGovRoadProtectionCaseModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govRoadProtectionCase/saveGovRoadProtectionCase',
                    JSON.stringify(vc.component.addGovRoadProtectionCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovRoadProtectionCaseModel').modal('hide');
                            vc.component.clearAddGovRoadProtectionCaseInfo();
                            vc.emit('govRoadProtectionCaseManage', 'listGovRoadProtectionCase', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovRoadProtectionCaseInfo: function () {
                vc.component.addGovRoadProtectionCaseInfo = {
                    roadCaseId: '',
                    roadProtectionId: '',
                    caId: '',
                    caseName: '',
                    caseCode: '',
                    happenedTime: '',
                    happenedPlace: '',
                    caseType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    isSolved: '',
                    personNum: '',
                    fleeingNum: '',
                    arrestsNum: '',
                    detectionContent: '',
                    caseContent: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
