(function (vc, vm) {

    vc.extends({
        data: {
            editGovBuildingTypeInfo: {
                typeId: '',
                typeName: '',
                caName: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovBuildingType', 'openEditGovBuildingTypeModal', function (_params) {
                vc.component.refreshEditGovBuildingTypeInfo();
                $('#editGovBuildingTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovBuildingTypeInfo);
                //vc.component.editGovBuildingTypeInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovBuildingTypeValidate: function () {
                return vc.validate.validate({
                    editGovBuildingTypeInfo: vc.component.editGovBuildingTypeInfo
                }, {
                    'editGovBuildingTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑分类名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "建筑分类名称超长"
                        },
                    ],
                    'editGovBuildingTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],
                    'editGovBuildingTypeInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑分类ID不能为空"
                        }]

                });
            },
            editGovBuildingType: function () {
                if (!vc.component.editGovBuildingTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govBuildingType/updateGovBuildingType',
                    JSON.stringify(vc.component.editGovBuildingTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovBuildingTypeModel').modal('hide');
                            vc.emit('govBuildingTypeManage', 'listGovBuildingType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovBuildingTypeInfo: function () {
                vc.component.editGovBuildingTypeInfo = {
                    typeId: '',
                    typeName: '',
                    caName: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
