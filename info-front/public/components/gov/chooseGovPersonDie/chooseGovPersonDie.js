(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPersonDie:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPersonDieInfo:{
                govPersonDies:[],
                _currentGovPersonDieName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPersonDie','openChooseGovPersonDieModel',function(_param){
                $('#chooseGovPersonDieModel').modal('show');
                vc.component._refreshChooseGovPersonDieInfo();
                vc.component._loadAllGovPersonDieInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPersonDieInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govPersonDie/queryGovPersonDie',
                             param,
                             function(json){
                                var _govPersonDieInfo = JSON.parse(json);
                                vc.component.chooseGovPersonDieInfo.govPersonDies = _govPersonDieInfo.govPersonDies;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPersonDie:function(_govPersonDie){
                if(_govPersonDie.hasOwnProperty('name')){
                     _govPersonDie.govPersonDieName = _govPersonDie.name;
                }
                vc.emit($props.emitChooseGovPersonDie,'chooseGovPersonDie',_govPersonDie);
                vc.emit($props.emitLoadData,'listGovPersonDieData',{
                    govPersonDieId:_govPersonDie.govPersonDieId
                });
                $('#chooseGovPersonDieModel').modal('hide');
            },
            queryGovPersonDies:function(){
                vc.component._loadAllGovPersonDieInfo(1,10,vc.component.chooseGovPersonDieInfo._currentGovPersonDieName);
            },
            _refreshChooseGovPersonDieInfo:function(){
                vc.component.chooseGovPersonDieInfo._currentGovPersonDieName = "";
            }
        }

    });
})(window.vc);
