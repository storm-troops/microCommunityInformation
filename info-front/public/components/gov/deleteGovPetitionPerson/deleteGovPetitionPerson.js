(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovPersonInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovPetitionPerson', 'openDeleteGovPetitionPersonModel', function (_params) {
                vc.component.deleteGovPersonInfo = _params;
                $('#deleteGovPetitionPersonModel').modal('show');

            });
        },
        methods: {
            deleteGovPerson: function () {
                vc.http.apiPost(
                    '/govPersonLabelRel/deleteGovPersonLabelRel',
                    JSON.stringify(vc.component.deleteGovPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPetitionPersonModel').modal('hide');
                            vc.emit('govPersonManage', 'listGovPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovPersonModel: function () {
                $('#deleteGovPetitionPersonModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
