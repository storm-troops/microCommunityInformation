/**
    公司组织 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovCompanyInfo: {
                index: 0,
                flowComponent: 'viewGovCompanyInfo',
                caId: '',
                companyName: '',
                companyType: '',
                idCard: '',
                artificialPerson: '',
                companyAddress: '',
                registerTime: '',
                personName: '',
                personIdCard: '',
                personTel: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCompanyInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovCompanyInfo', 'chooseGovCompany', function (_app) {
                vc.copyObject(_app, vc.component.viewGovCompanyInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovCompanyInfo);
            });

            vc.on('viewGovCompanyInfo', 'onIndex', function (_index) {
                vc.component.viewGovCompanyInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovCompanyInfoModel() {
                vc.emit('chooseGovCompany', 'openChooseGovCompanyModel', {});
            },
            _openAddGovCompanyInfoModel() {
                vc.emit('addGovCompany', 'openAddGovCompanyModal', {});
            },
            _loadGovCompanyInfoData: function () {

            }
        }
    });

})(window.vc);
