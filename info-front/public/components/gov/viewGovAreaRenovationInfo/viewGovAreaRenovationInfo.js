/**
    重点地区排查整治 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovAreaRenovationInfo:{
                index:0,
                flowComponent:'viewGovAreaRenovationInfo',
                govRenovationId:'',
caId:'',
typeId:'',
securityKey:'',
securityProblem:'',
securityRange:'',
leadCompany:'',
leadParticipate:'',
name:'',
tel:'',
startTime:'',
endTime:'',
crackCriminal:'',
crackSecurity:'',
leadRamark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovAreaRenovationInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovAreaRenovationInfo','chooseGovAreaRenovation',function(_app){
                vc.copyObject(_app, vc.component.viewGovAreaRenovationInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovAreaRenovationInfo);
            });

            vc.on('viewGovAreaRenovationInfo', 'onIndex', function(_index){
                vc.component.viewGovAreaRenovationInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovAreaRenovationInfoModel(){
                vc.emit('chooseGovAreaRenovation','openChooseGovAreaRenovationModel',{});
            },
            _openAddGovAreaRenovationInfoModel(){
                vc.emit('addGovAreaRenovation','openAddGovAreaRenovationModal',{});
            },
            _loadGovAreaRenovationInfoData:function(){

            }
        }
    });

})(window.vc);
