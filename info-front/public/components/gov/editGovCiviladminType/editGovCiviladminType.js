(function (vc, vm) {

    vc.extends({
        data: {
            editGovActivitiesTypeInfo: {
                typeCd: '',
                typeName: '',
                typeDesc: '',
                seq: '',
                defalutShow: '',
                caId: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovActivitiesType', 'openEditGovActivitiesTypeModal', function (_params) {
                vc.component.refreshEditGovActivitiesTypeInfo();
                $('#editGovActivitiesTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovActivitiesTypeInfo);
                vc.component.editGovActivitiesTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovActivitiesTypeValidate: function () {
                return vc.validate.validate({
                    editGovActivitiesTypeInfo: vc.component.editGovActivitiesTypeInfo
                }, {
                    'editGovActivitiesTypeInfo.typeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公告类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "报事类型名称超长"
                        },
                    ],
                    'editGovActivitiesTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "类型名称超长"
                        },
                    ],
                    'editGovActivitiesTypeInfo.typeDesc': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "类型描述超长"
                        },
                    ],
                    'editGovActivitiesTypeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "顺序超长"
                        },
                    ],
                    'editGovActivitiesTypeInfo.defalutShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小程序是否显示不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "小程序是否显示太长"
                        },
                    ],
                    'editGovActivitiesTypeInfo.typeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公告类型ID不能为空"
                        }]

                });
            },
            editGovActivitiesType: function () {
                if (!vc.component.editGovActivitiesTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCiviladminType/updateGovCiviladminType',
                    JSON.stringify(vc.component.editGovActivitiesTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovActivitiesTypeModel').modal('hide');
                            vc.emit('govActivitiesTypeManage', 'listGovActivitiesType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovActivitiesTypeInfo: function () {
                vc.component.editGovActivitiesTypeInfo = {
                    typeCd: '',
                    typeCd: '',
                    typeName: '',
                    typeDesc: '',
                    seq: '',
                    defalutShow: '',
                    caId: ''
                }
            }
        }
    });

})(window.vc, window.vc.component);
