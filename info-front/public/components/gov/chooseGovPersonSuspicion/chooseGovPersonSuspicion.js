(function (vc) {
    vc.extends({
        propTypes: {
            emitchooseGovPersonSuspicion: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovPersonSuspicionInfo: {
                govPersons: [],
                _personName: '',
                _caId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovPersonSuspicionName', 'openchooseGovPersonSuspicionModel', function (_param) {
                $that.chooseGovPersonSuspicionInfo._caId = _param;
                $('#chooseGovPersonSuspicionModel').modal('show');
                vc.component._refreshchooseGovPersonSuspicionInfo();
                vc.component._loadAllGovPersonSuspicionInfo(1, 10, '',$that.chooseGovPersonSuspicionInfo._caId);
            });
        },
        methods: {
            _loadAllGovPersonSuspicionInfo: function (_page, _row, _name,_caId) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        personName: _name,
                        caId:_caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPerson/queryGovPerson',
                    param,
                    function (json) {
                        var _govPersonInfo = JSON.parse(json);
                        vc.component.chooseGovPersonSuspicionInfo.total = _govPersonInfo.total;
                        vc.component.chooseGovPersonSuspicionInfo.records = _govPersonInfo.records;
                        vc.component.chooseGovPersonSuspicionInfo.govPersons = _govPersonInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.chooseGovPersonSuspicionInfo.records,
                            currentPage: _page
                        });
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovPersonSuspicion: function (_govPerson) {
                vc.emit($props.emitchooseGovPersonSuspicion, 'chooseGovPersonSuspicion', _govPerson);
                $('#chooseGovPersonSuspicionModel').modal('hide');
            },
            queryGovPersons: function () {
                vc.component._loadAllGovPersonSuspicionInfo(1, 10, vc.component.chooseGovPersonSuspicionInfo._personName,$that.chooseGovPersonSuspicionInfo._caId);
            },
            _refreshchooseGovPersonSuspicionInfo: function () {
                vc.component.chooseGovPersonSuspicionInfo._personName = "";
            }
        }

    });
})(window.vc);
