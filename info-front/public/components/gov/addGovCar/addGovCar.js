(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCarInfo: {
                carId: '',
                govCommunityId: '',
                paId: '',
                carNum: '',
                carBrand: '',
                carColor: '',
                carType: '',
                startTime: '',
                endTime: '',
                remark: '',
                govCommunitys: [],
                govParkingAreas: []
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovCar', 'openAddGovCarModal', function () {
                $that._listAddGovCommunitys();
                $that._listAddGovParkingAreas();
                $('#addGovCarModel').modal('show');
            });
        },
        methods: {
            addGovCarValidate() {
                return vc.validate.validate({
                    addGovCarInfo: vc.component.addGovCarInfo
                }, {
                    'addGovCarInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "小区超长"
                        },
                    ],
                    'addGovCarInfo.paId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "停车场不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "停车场超长"
                        },
                    ],
                    'addGovCarInfo.carNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车牌号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "车牌号超过12位"
                        },
                    ],
                    'addGovCarInfo.carBrand': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆品牌不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "50",
                            errInfo: "车辆品牌超过50位"
                        },
                    ],
                    'addGovCarInfo.carColor': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆颜色不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "车辆颜色超过12位"
                        },
                    ],
                    'addGovCarInfo.carType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "车辆类型超长"
                        },
                    ],
                    'addGovCarInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "起租时间不能为空"
                        },
                        {
                            limit: "datetime",
                            param: "",
                            errInfo: "起租时间格式错误"
                        },
                    ],
                    'addGovCarInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "截租时间不能为空"
                        },
                        {
                            limit: "datetime",
                            param: "",
                            errInfo: "截租时间格式错误"
                        },
                    ],
                    'addGovCarInfo.remark': [
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "备注内容不能超过200"
                        },
                    ],
                });
            },
            _listAddGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovCarInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },

            _listAddGovParkingAreas: function (_page, _rows) {

                vc.component.govParkingAreaManageInfo.conditions.page = _page;
                vc.component.govParkingAreaManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govParkingAreaManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/parkingArea/queryParkingArea',
                    param,
                    function (json, res) {
                        var _govParkingAreaManageInfo = JSON.parse(json);
                        let govParkingAreas = _govParkingAreaManageInfo.data;
                        $that.addGovCarInfo.govParkingAreas = govParkingAreas;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            saveGovCarInfo: function () {
                if (!vc.component.addGovCarValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovCarInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCarInfo);
                    $('#addGovCarModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/car/saveCar',
                    JSON.stringify(vc.component.addGovCarInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCarModel').modal('hide');
                            vc.component.clearAddGovCarInfo();
                            vc.emit('govCarManage', 'listGovCar', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovCarInfo: function () {
                vc.component.addGovCarInfo = {
                    govCommunityId: '',
                    paId: '',
                    carNum: '',
                    carBrand: '',
                    carColor: '',
                    carType: '',
                    startTime: '',
                    endTime: '',
                    remark: '',
                    govCommunitys: [],
                    govParkingAreas: []
                };
            }
        }
    });

})(window.vc);
