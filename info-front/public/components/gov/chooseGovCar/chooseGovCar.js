(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovCar:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovCarInfo:{
                govCars:[],
                _currentGovCarName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovCar','openChooseGovCarModel',function(_param){
                $('#chooseGovCarModel').modal('show');
                vc.component._refreshChooseGovCarInfo();
                vc.component._loadAllGovCarInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovCarInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govCar.listGovCars',
                             param,
                             function(json){
                                var _govCarInfo = JSON.parse(json);
                                vc.component.chooseGovCarInfo.govCars = _govCarInfo.govCars;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovCar:function(_govCar){
                if(_govCar.hasOwnProperty('name')){
                     _govCar.govCarName = _govCar.name;
                }
                vc.emit($props.emitChooseGovCar,'chooseGovCar',_govCar);
                vc.emit($props.emitLoadData,'listGovCarData',{
                    govCarId:_govCar.govCarId
                });
                $('#chooseGovCarModel').modal('hide');
            },
            queryGovCars:function(){
                vc.component._loadAllGovCarInfo(1,10,vc.component.chooseGovCarInfo._currentGovCarName);
            },
            _refreshChooseGovCarInfo:function(){
                vc.component.chooseGovCarInfo._currentGovCarName = "";
            }
        }

    });
})(window.vc);
