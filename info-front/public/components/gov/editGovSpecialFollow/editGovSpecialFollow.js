(function (vc, vm) {

    vc.extends({
        data: {
            editGovSpecialFollowInfo: {
                followId: '',
                specialPersonId: '',
                name: '',
                caId: '',
                specialStartTime: '',
                specialReason: '',
                isWork: '',
                workPerson: '',
                workPersonTel: '',
                workAddress: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGovSpecialFollowInfo.specialStartTime = value;
                });
        },
        _initEvent: function () {
            vc.on('editGovSpecialFollow', 'openEditGovSpecialFollowModal', function (_params) {
                vc.component.refreshEditGovSpecialFollowInfo();
                $('#editGovSpecialFollowModel').modal('show');
                vc.copyObject(_params, vc.component.editGovSpecialFollowInfo);
                vc.component.editGovSpecialFollowInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovSpecialFollowValidate: function () {
                return vc.validate.validate({
                    editGovSpecialFollowInfo: vc.component.editGovSpecialFollowInfo
                }, {
                    'editGovSpecialFollowInfo.specialPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "特殊人员ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "特殊人员ID不能超过30"
                        },
                    ],
                    'editGovSpecialFollowInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "特殊人员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "特殊人员名称不能超过64"
                        },
                    ],
                    'editGovSpecialFollowInfo.specialStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "登记时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "登记时间不能超过时间类型"
                        },
                    ],
                    'editGovSpecialFollowInfo.specialReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "近况说明不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "近况说明不能超过128"
                        },
                    ],
                    'editGovSpecialFollowInfo.isWork': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否有工作不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "是否有工作不能超过64"
                        },
                    ],
                    'editGovSpecialFollowInfo.workPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "负责人不能超过128"
                        },
                    ],
                    'editGovSpecialFollowInfo.workPersonTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "负责人联系电话不能超过11"
                        },
                    ],
                    'editGovSpecialFollowInfo.workAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "工作地址不能超过128"
                        },
                    ]
                });
            },
            editGovSpecialFollow: function () {
                if (!vc.component.editGovSpecialFollowValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govSpecialFollow/updateGovSpecialFollow',
                    JSON.stringify(vc.component.editGovSpecialFollowInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovSpecialFollowModel').modal('hide');
                            vc.emit('govSpecialFollowManage', 'listGovSpecialFollow', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovSpecialFollowInfo: function () {
                vc.component.editGovSpecialFollowInfo = {
                    followId: '',
                    followId: '',
                    specialPersonId: '',
                    name: '',
                    caId: '',
                    specialStartTime: '',
                    specialReason: '',
                    isWork: '',
                    workPerson: '',
                    workPersonTel: '',
                    workAddress: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
