(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPartyMemberChange:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPartyMemberChangeInfo:{
                govPartyMemberChanges:[],
                _currentGovPartyMemberChangeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPartyMemberChange','openChooseGovPartyMemberChangeModel',function(_param){
                $('#chooseGovPartyMemberChangeModel').modal('show');
                vc.component._refreshChooseGovPartyMemberChangeInfo();
                vc.component._loadAllGovPartyMemberChangeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPartyMemberChangeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govPartyMemberChange.listGovPartyMemberChanges',
                             param,
                             function(json){
                                var _govPartyMemberChangeInfo = JSON.parse(json);
                                vc.component.chooseGovPartyMemberChangeInfo.govPartyMemberChanges = _govPartyMemberChangeInfo.govPartyMemberChanges;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPartyMemberChange:function(_govPartyMemberChange){
                if(_govPartyMemberChange.hasOwnProperty('name')){
                     _govPartyMemberChange.govPartyMemberChangeName = _govPartyMemberChange.name;
                }
                vc.emit($props.emitChooseGovPartyMemberChange,'chooseGovPartyMemberChange',_govPartyMemberChange);
                vc.emit($props.emitLoadData,'listGovPartyMemberChangeData',{
                    govPartyMemberChangeId:_govPartyMemberChange.govPartyMemberChangeId
                });
                $('#chooseGovPartyMemberChangeModel').modal('hide');
            },
            queryGovPartyMemberChanges:function(){
                vc.component._loadAllGovPartyMemberChangeInfo(1,10,vc.component.chooseGovPartyMemberChangeInfo._currentGovPartyMemberChangeName);
            },
            _refreshChooseGovPartyMemberChangeInfo:function(){
                vc.component.chooseGovPartyMemberChangeInfo._currentGovPartyMemberChangeName = "";
            }
        }

    });
})(window.vc);
