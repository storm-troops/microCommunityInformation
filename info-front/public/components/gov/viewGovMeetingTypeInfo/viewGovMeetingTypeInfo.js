/**
    会议类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovMeetingTypeInfo:{
                index:0,
                flowComponent:'viewGovMeetingTypeInfo',
                typeId:'',
caId:'',
meetingTypeName:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMeetingTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovMeetingTypeInfo','chooseGovMeetingType',function(_app){
                vc.copyObject(_app, vc.component.viewGovMeetingTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovMeetingTypeInfo);
            });

            vc.on('viewGovMeetingTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovMeetingTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovMeetingTypeInfoModel(){
                vc.emit('chooseGovMeetingType','openChooseGovMeetingTypeModel',{});
            },
            _openAddGovMeetingTypeInfoModel(){
                vc.emit('addGovMeetingType','openAddGovMeetingTypeModal',{});
            },
            _loadGovMeetingTypeInfoData:function(){

            }
        }
    });

})(window.vc);
