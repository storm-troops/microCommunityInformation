(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovHomicideBasic:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovHomicideBasicInfo:{
                govHomicideBasics:[],
                _currentGovHomicideBasicName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovHomicideBasic','openChooseGovHomicideBasicModel',function(_param){
                $('#chooseGovHomicideBasicModel').modal('show');
                vc.component._refreshChooseGovHomicideBasicInfo();
                vc.component._loadAllGovHomicideBasicInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovHomicideBasicInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govHomicideBasic/queryGovHomicideBasic',
                             param,
                             function(json){
                                var _govHomicideBasicInfo = JSON.parse(json);
                                vc.component.chooseGovHomicideBasicInfo.govHomicideBasics = _govHomicideBasicInfo.govHomicideBasics;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovHomicideBasic:function(_govHomicideBasic){
                if(_govHomicideBasic.hasOwnProperty('name')){
                     _govHomicideBasic.govHomicideBasicName = _govHomicideBasic.name;
                }
                vc.emit($props.emitChooseGovHomicideBasic,'chooseGovHomicideBasic',_govHomicideBasic);
                vc.emit($props.emitLoadData,'listGovHomicideBasicData',{
                    govHomicideBasicId:_govHomicideBasic.govHomicideBasicId
                });
                $('#chooseGovHomicideBasicModel').modal('hide');
            },
            queryGovHomicideBasics:function(){
                vc.component._loadAllGovHomicideBasicInfo(1,10,vc.component.chooseGovHomicideBasicInfo._currentGovHomicideBasicName);
            },
            _refreshChooseGovHomicideBasicInfo:function(){
                vc.component.chooseGovHomicideBasicInfo._currentGovHomicideBasicName = "";
            }
        }

    });
})(window.vc);
