(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMeetingTypeInfo: {
                typeId: '',
                caId: '',
                meetingTypeName: '',
                ramark: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovMeetingType', 'openAddGovMeetingTypeModal', function () {
                $('#addGovMeetingTypeModel').modal('show');
            });
        },
        methods: {
            addGovMeetingTypeValidate() {
                return vc.validate.validate({
                    addGovMeetingTypeInfo: vc.component.addGovMeetingTypeInfo
                }, {
                    
                    'addGovMeetingTypeInfo.meetingTypeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "类型名称不能超过64"
                        },
                    ]

                });
            },
            saveGovMeetingTypeInfo: function () {
                if (!vc.component.addGovMeetingTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovMeetingTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMeetingTypeInfo);
                    $('#addGovMeetingTypeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMeetingType/saveGovMeetingType',
                    JSON.stringify(vc.component.addGovMeetingTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMeetingTypeModel').modal('hide');
                            vc.component.clearAddGovMeetingTypeInfo();
                            vc.emit('govMeetingTypeManage', 'listGovMeetingType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovMeetingTypeInfo: function () {
                vc.component.addGovMeetingTypeInfo = {
                    typeId: '',
                    caId: '',
                    meetingTypeName: ''
                };
            }
        }
    });

})(window.vc);
