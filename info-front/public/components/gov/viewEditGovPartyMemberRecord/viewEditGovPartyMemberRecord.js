(function (vc) {
    vc.extends({
        data: {
            viewEditGovPartyMemberRecordInfo: {
                govMembers: [],
                orgId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('viewEditGovPartyMemberRecord', 'openviewEditGovPartyMemberRecordModel', function (_param) {
                vc.component._refreshviewEditGovPartyMemberRecordInfo();
                console.log(_param);
                $that.viewEditGovPartyMemberRecordInfo.govMembers = _param;
                //vc.component._loadAllGovRoomInfo(1, 10, '');
            });
            vc.on('viewEditGovPartyMemberRecord', 'selectPartyOrgs', function (_param) {
                console.log(_param);
                $that.viewEditGovPartyMemberRecordInfo.orgId = _param;
            });
            vc.on('meetingListChooseGovPartyMemberOrg', 'chooseGovPartyMemberOrg', function (_param) {
                $that.viewEditGovPartyMemberRecordInfo.govMembers.push(_param);
                vc.emit('viewEditGovPartyMemberRecord', 'page_event', $that.viewEditGovPartyMemberRecordInfo.govMembers);
            });
            
        },
        methods: {
            _loadAllPartyMemberInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingMemberRel/queryGovMeetingMemberRel',
                    param,
                    function (json) {
                        var _govRoomInfo = JSON.parse(json);
                        vc.component.viewEditGovPartyMemberRecordInfo.govRooms = _govRoomInfo.govRooms;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChooseGovPartyMemberOrg: function () {
                let _caId = vc.getCurrentCommunity().caId;
                //let _orgId = $that.viewEditGovPartyMemberRecordInfo.orgId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                
                var param ={
                    caId: _caId
                }
                vc.emit('chooseGovPartyMemberOrg', 'openchooseGovPartyMemberOrgModel', param);
            },
            queryvPartyMember: function () {
                vc.component._loadAllPartyMemberInfo(1, 10, vc.component.viewEditGovPartyMemberRecordInfo._currentGovRoomName);
            },
            _openDeleteEditGovPartyMemberOrg: function (_govMember) {

                let _tmpGovMembers = [];
                $that.viewEditGovPartyMemberRecordInfo.govMembers.forEach(item => {
                    if (item.govMemberId != _govMember.govMemberId) {
                        _tmpGovMembers.push(item);
                    }
                });
                $that.viewEditGovPartyMemberRecordInfo.govMembers = _tmpGovMembers;
                vc.emit('viewEditGovPartyMemberRecord', 'page_event', $that.viewEditGovPartyMemberRecordInfo.govMembers);
            },
            _refreshviewEditGovPartyMemberRecordInfo: function () {
                vc.component.viewEditGovPartyMemberRecordInfo._currentGovRoomName = "";
                $that.viewEditGovPartyMemberRecordInfo.govMembers = [];
            }
        }

    });
})(window.vc);
