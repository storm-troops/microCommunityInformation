(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovHelpPolicyInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovHelpPolicy', 'openDeleteGovHelpPolicyModal', function (_params) {

                vc.component.deleteGovHelpPolicyInfo = _params;
                $('#deleteGovHelpPolicyModel').modal('show');

            });
        },
        methods: {
            deleteGovHelpPolicy: function () {
                vc.component.deleteGovHelpPolicyInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govHelpPolicy/deleteGovHelpPolicy',
                    JSON.stringify(vc.component.deleteGovHelpPolicyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovHelpPolicyModel').modal('hide');
                            vc.emit('govHelpPolicyManage', 'listGovHelpPolicy', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovHelpPolicyModel: function () {
                $('#deleteGovHelpPolicyModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
