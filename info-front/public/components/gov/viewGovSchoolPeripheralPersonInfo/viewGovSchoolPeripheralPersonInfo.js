/**
    校园周边重点人员 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovSchoolPeripheralPersonInfo:{
                index:0,
                flowComponent:'viewGovSchoolPeripheralPersonInfo',
                schPersonId:'',
schoolId:'',
govPersonId:'',
schoolName:'',
caId:'',
extentInjury:'',
isAttention:'',
:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovSchoolPeripheralPersonInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovSchoolPeripheralPersonInfo','chooseGovSchoolPeripheralPerson',function(_app){
                vc.copyObject(_app, vc.component.viewGovSchoolPeripheralPersonInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovSchoolPeripheralPersonInfo);
            });

            vc.on('viewGovSchoolPeripheralPersonInfo', 'onIndex', function(_index){
                vc.component.viewGovSchoolPeripheralPersonInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovSchoolPeripheralPersonInfoModel(){
                vc.emit('chooseGovSchoolPeripheralPerson','openChooseGovSchoolPeripheralPersonModel',{});
            },
            _openAddGovSchoolPeripheralPersonInfoModel(){
                vc.emit('addGovSchoolPeripheralPerson','openAddGovSchoolPeripheralPersonModal',{});
            },
            _loadGovSchoolPeripheralPersonInfoData:function(){

            }
        }
    });

})(window.vc);
