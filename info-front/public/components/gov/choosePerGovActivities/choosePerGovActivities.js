(function(vc){
    vc.extends({
        propTypes: {
           emitChoosePerGovActivities:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            choosePerGovActivitiesInfo:{
                perGovActivitiess:[],
                _currentPerGovActivitiesName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('choosePerGovActivities','openChoosePerGovActivitiesModel',function(_param){
                $('#choosePerGovActivitiesModel').modal('show');
                vc.component._refreshChoosePerGovActivitiesInfo();
                vc.component._loadAllPerGovActivitiesInfo(1,10,'');
            });
        },
        methods:{
            _loadAllPerGovActivitiesInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/perGovActivities/queryPerGovActivities',
                             param,
                             function(json){
                                var _perGovActivitiesInfo = JSON.parse(json);
                                vc.component.choosePerGovActivitiesInfo.perGovActivitiess = _perGovActivitiesInfo.perGovActivitiess;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            choosePerGovActivities:function(_perGovActivities){
                if(_perGovActivities.hasOwnProperty('name')){
                     _perGovActivities.perGovActivitiesName = _perGovActivities.name;
                }
                vc.emit($props.emitChoosePerGovActivities,'choosePerGovActivities',_perGovActivities);
                vc.emit($props.emitLoadData,'listPerGovActivitiesData',{
                    perGovActivitiesId:_perGovActivities.perGovActivitiesId
                });
                $('#choosePerGovActivitiesModel').modal('hide');
            },
            queryPerGovActivitiess:function(){
                vc.component._loadAllPerGovActivitiesInfo(1,10,vc.component.choosePerGovActivitiesInfo._currentPerGovActivitiesName);
            },
            _refreshChoosePerGovActivitiesInfo:function(){
                vc.component.choosePerGovActivitiesInfo._currentPerGovActivitiesName = "";
            }
        }

    });
})(window.vc);
