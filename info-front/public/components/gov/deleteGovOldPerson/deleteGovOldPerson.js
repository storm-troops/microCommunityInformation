(function(vc,vm){

    vc.extends({
        data:{
            deleteGovOldPersonInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovOldPerson','openDeleteGovOldPersonModal',function(_params){

                vc.component.deleteGovOldPersonInfo = _params;
                $('#deleteGovOldPersonModel').modal('show');

            });
        },
        methods:{
            deleteGovOldPerson:function(){
                vc.component.deleteGovOldPersonInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govOldPerson/deleteGovOldPerson',
                    JSON.stringify(vc.component.deleteGovOldPersonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovOldPersonModel').modal('hide');
                            vc.emit('govOldPersonManage','listGovOldPerson',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovOldPersonModel:function(){
                $('#deleteGovOldPersonModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
