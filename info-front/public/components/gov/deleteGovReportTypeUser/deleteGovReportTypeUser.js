(function(vc,vm){

    vc.extends({
        data:{
            deleteGovReportTypeUserInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovReportTypeUser','openDeleteGovReportTypeUserModal',function(_params){

                vc.component.deleteGovReportTypeUserInfo = _params;
                $('#deleteGovReportTypeUserModel').modal('show');

            });
        },
        methods:{
            deleteGovReportTypeUser:function(){
                vc.component.deleteGovReportTypeUserInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govReportTypeUser/deleteGovReportTypeUser',
                    JSON.stringify(vc.component.deleteGovReportTypeUserInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovReportTypeUserModel').modal('hide');
                            vc.emit('govReportTypeUserManage','listGovReportTypeUser',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovReportTypeUserModel:function(){
                $('#deleteGovReportTypeUserModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
