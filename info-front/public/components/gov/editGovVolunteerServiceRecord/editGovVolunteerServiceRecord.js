(function (vc, vm) {

    vc.extends({
        data: {
            editGovVolunteerServiceRecordInfo: {
                serviceRecordId: '',
                serviceRecordId: '',
                title: '',
                typeCd: '',
                img: '',
                context: '',
                caId: '',
                startTime: '',
                endTime: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovVolunteerServiceRecord', 'openEditGovVolunteerServiceRecordModal', function (_params) {
                vc.component.refreshEditGovVolunteerServiceRecordInfo();
                $('#editGovVolunteerServiceRecordModel').modal('show');
                vc.copyObject(_params, vc.component.editGovVolunteerServiceRecordInfo);
                vc.component.editGovVolunteerServiceRecordInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovVolunteerServiceRecordValidate: function () {
                return vc.validate.validate({
                    editGovVolunteerServiceRecordInfo: vc.component.editGovVolunteerServiceRecordInfo
                }, {
                    'editGovVolunteerServiceRecordInfo.serviceRecordId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务记录ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "服务记录ID不能超过30"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "记录标题不能超过200"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.typeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "文章类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "文章类型不能超过30"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.img': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "照片名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "照片名称不能超过200"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "活动内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "活动内容不能超过长文本"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结束时间不能超过时间类型"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.statusCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "数据状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "数据状态不能超过2"
                        },
                    ],
                    'editGovVolunteerServiceRecordInfo.serviceRecordId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务记录ID不能为空"
                        }]

                });
            },
            editGovVolunteerServiceRecord: function () {
                if (!vc.component.editGovVolunteerServiceRecordValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govVolunteerServiceRecord/updateGovVolunteerServiceRecord',
                    JSON.stringify(vc.component.editGovVolunteerServiceRecordInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovVolunteerServiceRecordModel').modal('hide');
                            vc.emit('govVolunteerServiceRecordManage', 'listGovVolunteerServiceRecord', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovVolunteerServiceRecordInfo: function () {
                vc.component.editGovVolunteerServiceRecordInfo = {
                    serviceRecordId: '',
                    serviceRecordId: '',
                    title: '',
                    typeCd: '',
                    img: '',
                    context: '',
                    caId: '',
                    startTime: '',
                    endTime: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
