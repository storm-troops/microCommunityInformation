/**
    党组织简介 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPartyOrgContextInfo:{
                index:0,
                flowComponent:'viewGovPartyOrgContextInfo',
                orgId:'',
context:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPartyOrgContextInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPartyOrgContextInfo','chooseGovPartyOrgContext',function(_app){
                vc.copyObject(_app, vc.component.viewGovPartyOrgContextInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPartyOrgContextInfo);
            });

            vc.on('viewGovPartyOrgContextInfo', 'onIndex', function(_index){
                vc.component.viewGovPartyOrgContextInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPartyOrgContextInfoModel(){
                vc.emit('chooseGovPartyOrgContext','openChooseGovPartyOrgContextModel',{});
            },
            _openAddGovPartyOrgContextInfoModel(){
                vc.emit('addGovPartyOrgContext','openAddGovPartyOrgContextModal',{});
            },
            _loadGovPartyOrgContextInfoData:function(){

            }
        }
    });

})(window.vc);
