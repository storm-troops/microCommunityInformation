(function(vc,vm){

    vc.extends({
        data:{
            deleteGovGridTypeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovGridType','openDeleteGovGridTypeModal',function(_params){

                vc.component.deleteGovGridTypeInfo = _params;
                $('#deleteGovGridTypeModel').modal('show');

            });
        },
        methods:{
            deleteGovGridType:function(){
                vc.component.deleteGovGridTypeInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govGridType/deleteGovGridType',
                    JSON.stringify(vc.component.deleteGovGridTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovGridTypeModel').modal('hide');
                            vc.emit('govGridTypeManage','listGovGridType',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovGridTypeModel:function(){
                $('#deleteGovGridTypeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
